
Int=zeros(length(dataInt),1);
for i=1:length(dataInt)
    Int.mean(i)=mean(dataInt{i});
    Int.std(i)=std(dataInt{i});
    Int.max(i)=max(smooth(dataInt{i},10));
    Int.min(i)=min(smooth(dataInt{i},10));
end
