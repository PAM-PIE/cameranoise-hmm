function [Bobs]=ExpandB(B,obsvect);
% Erweitert B um die Hidden States

[Q T] = size(B);
Q = length(obsvect);
Bobs = zeros(Q,T);
for q=1:Q
    Bobs(q,:) = B(obsvect(q),:); 
end;
