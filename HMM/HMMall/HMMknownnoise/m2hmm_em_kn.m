function [LL, prior, transmat, mu, Sigma, mixmat, Memo] = ...
  m2hmm_em(TimeBin, data, dataS, prior, transmat, obsvect, mu, Sigma, mixmat, varargin);
% LEARN_MHMM Compute the ML parameters of an HMM with (mixtures of) Gaussians output using EM.
% [ll_trace, prior, transmat, mu, sigma, mixmat] = learn_mhmm(data, ...
%   prior0, transmat0, obsvect, mu0, sigma0, mixmat0, ...)
% Additional obsvect, NZ 19.06.2007
% adj_prior = 0 (init) NZ 25.08.2008
%
% Notation: Q(t) = hidden state, Y(t) = observation, M(t) = mixture variable
%
% INPUTS:
% data{ex}(:,t) or data(:,t,ex) if all sequences have the same length
% prior(i) = Pr(Q(1) = i),
% transmat(i,j) = Pr(Q(t+1)=j | Q(t)=i)
% mu(:,j,k) = E[Y(t) | Q(t)=j, M(t)=k ]
% Sigma(:,:,j,k) = Cov[Y(t) | Q(t)=j, M(t)=k]
% mixmat(j,k) = Pr(M(t)=k | Q(t)=j) : set to [] or ones(Q,1) if only one mixture component
%
% Optional parameters may be passed as 'param_name', param_value pairs.
% Parameter names are shown below; default values in [] - if none, argument is mandatory.
%
% 'max_iter' - max number of EM iterations [10]
% 'thresh' - convergence threshold [1e-4]
% 'verbose' - if 1, print out loglik at every iteration [1]
% 'cov_type' - 'full', 'diag' or 'spherical' ['full']
%
% To clamp some of the parameters, so learning does not change them:
% 'adj_prior' - if 0, do not change prior [1]
% 'adj_trans' - if 0, do not change transmat [1]
% 'adj_mix' - if 0, do not change mixmat [1]
% 'adj_mu' - if 0, do not change mu [1]
% 'adj_Sigma' - if 0, do not change Sigma [1]
%
% If the number of mixture components differs depending on Q, just set  the trailing
% entries of mixmat to 0, e.g., 2 components if Q=1, 3 components if Q=2,
% then set mixmat(1,3)=0. In this case, B2(1,3,:)=1.0.

if ~isstr(varargin{1}) % catch old syntax
  error('optional arguments should be passed as string/value pairs')
end

[max_iter, thresh, verbose, cov_type,  adj_prior, adj_trans, adj_mix, adj_mu, adj_Sigma] = ...
  process_options(varargin, 'max_iter', 10, 'thresh', 1e-4, 'verbose', 1, ...
  'cov_type', 'full', 'adj_prior', 0, 'adj_trans', 1, 'adj_mix', 1, ...
  'adj_mu', 1, 'adj_Sigma', 1);

previous_loglik = -inf;
loglik = 0;
converged = 0;
num_iter = 1;
LL = [];

if ~iscell(data)
  data = num2cell(data, [1 2]); % each elt of the 3rd dim gets its own cell
end
numex = length(data);


O = size(data{1},1);
Q = length(prior);
Qo = length(mu);

if isempty(mixmat)
  mixmat = ones(Qo,1);
end
M = size(mixmat,2);
if M == 1
  adj_mix = 0;
end

while (num_iter <= max_iter) & ~converged
  % E step
  [lltrace, exp_num_trans, exp_num_visits1, mu1, Sigma1] = ...
    ess_mhmm(prior, transmat, obsvect, mixmat, mu, Sigma, data, dataS);

  loglik=sum(lltrace);

  % M step
  if adj_prior
    prior = normalise(exp_num_visits1);
  end
  if adj_trans
    transmat = mk_stochastic(exp_num_trans);
  end
%   if adj_mix
%     mixmat = mk_stochastic(postmix);
%   end
%   if adj_mu | adj_Sigma
%     postmix2 = zeros(Qo,M);
%     m2 = zeros(O,Qo,M);
%     op2 = zeros(O,O,Qo,M);
%     ip2 = zeros(Qo,M);
%     mS2 = zeros(O,Qo,M);
%     for q=1:Q
%       postmix2(obsvect(q)) = postmix2(obsvect(q)) + postmix(q);
%       m2(:,obsvect(q)) = m2(:,obsvect(q)) + m(:,q);
%       op2(:,:,obsvect(q)) = op2(:,:,obsvect(q)) + op(:,:,q);
%       ip2(obsvect(q)) = ip2(obsvect(q)) + ip(q);
%       mS2(:,obsvect(q)) = mS2(:,obsvect(q)) + mS(:,q);
%     end
%     [mu2, Sigma2] = mixgauss_Mstep_kn(postmix2, m2, op2, ip2, mS2, 'cov_type', cov_type);
%     if adj_mu
%       mu = reshape(mu2, [O Qo M]);
%     end
%     if adj_Sigma
%       Sigma = reshape(Sigma2, [O O Qo M]);
%     end
%   end
  %[lltest0] = m2hmm_logprob_kn(data, dataS, prior, transmat, obsvect, mu, Sigma, mixmat);
    if adj_mu
      mu = reshape(mu1, [O Qo M]);
    end
    if adj_Sigma
      Sigma = reshape(Sigma1, [O O Qo M]);
    end

  %if verbose, fprintf(1, 'iteration %d, loglik = %f\n', num_iter, loglik); end
  LL = [LL loglik];
  %   if length(LL)>=20
  %     loglik_diff = diff(smooth(LL,20));
  %     converged = (loglik_diff(end) < thresh);
  %   else
  %     converged = false;
  %   end
  converged = em_converged(loglik, previous_loglik, thresh);
  if verbose,
    fprintf(1, '#%03d   ll=%+.6E   ', num_iter, loglik);
    fprintf(1, '%.2f  ', mu); fprintf(' mu');
    %if ~isfinite(previous_loglik), previous_loglik=loglik; end;
    fprintf(1, '\n       dl=%+.6E   ', loglik-previous_loglik);
    %fprintf(1, '\n                           ');
    fprintf(1, '%.3f ',squeeze(Sigma)); fprintf(' mu_var');
    fprintf(1, '\n                           ');
    fprintf(1, '%4.0f  ', -TimeBin./log(diag(transmat(1:Qo,1:Qo)))+(Q/Qo-1)*TimeBin); fprintf(' tau');
    if 0
      %fprintf(1, '\n       dyn=%3.2f%%        ', sum(BurstDyn.state)/numex*100);
      fprintf(1, '\n                          ');
      fprintf(1, '%4.0f%% ', 100*accumarray(obsvect',m)'./sum(m));  fprintf(' occurrence');
    end
  end

  Memo(num_iter).mu1=mu;
  Memo(num_iter).Sigma1=Sigma;
  Memo(num_iter).transmat1=transmat;
  Memo(num_iter).lltrace=lltrace;

  num_iter =  num_iter + 1;
  if verbose, fprintf(1, '\n', num_iter, loglik); end
  previous_loglik = loglik;
  if verbose
    plotLL(LL);
    drawnow;
  end
end


%%%%%%%%%

function [loglik, exp_num_trans, exp_num_visits1, mu1, Sigma1] = ...
  ess_mhmm(prior, transmat, obsvect, mixmat, mu, Sigma, data, dataS)
% ESS_MHMM Compute the Expected Sufficient Statistics for a MOG Hidden Markov Model.
%
% Outputs:
% exp_num_trans(i,j)   = sum_l sum_{t=2}^T Pr(Q(t-1) = i, Q(t) = j| Obs(l))
% exp_num_visits1(i)   = sum_l Pr(Q(1)=i | Obs(l))
%
% Let w(i,k,t,l) = P(Q(t)=i, M(t)=k | Obs(l))
% where Obs(l) = Obs(:,:,l) = O_1 .. O_T for sequence l
% Then
% postmix(i,k) = sum_l sum_t w(i,k,t,l) (posterior mixing weights/ responsibilities)
% m(:,i,k)   = sum_l sum_t w(i,k,t,l) * Obs(:,t,l)
% ip(i,k) = sum_l sum_t w(i,k,t,l) * Obs(:,t,l)' * Obs(:,t,l)
% op(:,:,i,k) = sum_l sum_t w(i,k,t,l) * Obs(:,t,l) * Obs(:,t,l)'

bLogBurstwise=false;

verbose = 0;

%[O T numex] = size(data);
numex = length(data);
O = size(data{1},1);
Q = length(prior);
M = size(mixmat,2);
exp_num_trans = zeros(Q,Q);
exp_num_visits1 = zeros(Q,1);
Sgamma = data;
Sw = zeros(Q,M);       %Sw=postmix
SY = zeros(O,Q,M);     %SY=m
SYY = zeros(O,O,Q,M);  %SYY=op
SYTY = zeros(Q,M);     %SYTY=ip
mS = zeros(O,Q,M);    %05.11.2008 NZ3
mix = (M>1);

loglik = 0;
if verbose, fprintf(1, 'forwards-backwards example # '); end
for ex=1:numex
  if verbose, fprintf(1, '%d ', ex); end
  %obs = data(:,:,ex);
  obs = data{ex};
  obsS = dataS{ex};
  T = size(obs,2);
  if bLogBurstwise, cT=1/T; else cT=1; end
  if mix
    error('[m2hmm_em_kn]: DIESER FALL IST NICHT VORGESEHEN!');
    [B, B2] = mixgauss_prob(obs, mu, Sigma, mixmat);
    [alpha, beta, gamma,  current_loglik, xi, gamma2] = ...
      fwdback(prior, transmat, B, 'obslik2', B2, 'mixmat', mixmat);
  else
    B = mixgauss_prob_kn(obs, obsS, mu, Sigma);
    B = max(eps,B);
    Bobs = ExpandB(B,obsvect);
    [alpha, beta, gamma,  current_loglik, xi] = ...
      fwdback(prior, transmat, Bobs);
  end
  %loglik = loglik +  current_loglik;
  loglik(ex) = cT*current_loglik;
  %if verbose, fprintf(1, 'll at ex %d = %f\n', ex, loglik); end

  exp_num_trans = exp_num_trans + cT*sum(xi,3);
  exp_num_visits1 = exp_num_visits1 + cT*gamma(:,1);

  if mix
    error('No MOGs!!!');
    %postmix = postmix + cT*sum(gamma2,3);
  else
    %postmix = postmix + cT*sum(gamma,2);
    %gamma2 = reshape(gamma, [Q 1 T]); % gamma2(i,m,t) = gamma(i,t)
    Sgamma{ex}=gamma;
  end
  %   for i=1:Q
  %     for k=1:M
  %       w = reshape(gamma2(i,k,:), [1 T]); % w(t) = w(i,k,t,l)
  %       wobs = obs .* repmat(w, [O 1]); % wobs(:,t) = w(t) * obs(:,t)
  %       m(:,i,k) = m(:,i,k) + cT*sum(wobs, 2); % m(:) = sum_t w(t) obs(:,t)
  %       op(:,:,i,k) = op(:,:,i,k) + cT*wobs * obs'; % op(:,:) = sum_t w(t) * obs(:,t) * obs(:,t)'
  %       ip(i,k) = ip(i,k) + cT*sum(sum(wobs .* obs, 2)); % ip = sum_t w(t) * obs(:,t)' * obs(:,t)
  %       % add params for timedependent variances (05.11.2008 NZ3)
  %       wobsS = obsS .* repmat(w, [O 1]); % wobsS(:,t) = w(t) * obsS(:,t)
  %       mS(:,i,k) = mS(:,i,k) + cT*sum(wobsS, 2); % m(:) = sum_t w(t) obsS(:,t)
  %     end
  %   end
end

Sgamma=[Sgamma{:}];

vart = @(v0,i) (1./( (mu(i).*(1-mu(i))./[dataS{:}]) + v0));
wvart = @(v0,i) Sgamma(i,:) .* vart(v0,i); %wvart(:,t) = w(t) * vart(:,t)^(-1)
wvartvart = @(v0,i) wvart(v0,i) .* vart(v0,i); %wvartvart(:,t) = w(t) * vart(:,t)^(-2)
Sw = @(v0,i) sum(wvart(v0,i),2); % Sw(:,t) = sum_t w(t) vart(:,t)^(-1)
SY = @(v0,i) sum(wvart(v0,i).*[data{:}],2); % SY(:) = sum_t w(t) vart(:,t)^(-1) obs(:,t)
S2w = @(v0,i) sum(wvartvart(v0,i),2); % S2w = sum_t w(t) vart(:,t)^(-2)
S2Y = @(v0,i) sum(wvartvart(v0,i).*[data{:}],2); % S2Y = sum_t w(t) vart(:,t)^(-2) obs(:,t)
S2YY = @(v0,i) wvartvart(v0,i).*[data{:}]*[data{:}]'; % S2Y = sum_t w(t) vart(:,t)^(-2) obs(:,t) obs(:,t)'
% for i=1:Q
%   Sw(i) = sum(wvart(Sigma(:,:,i),i),2); % Sw(:,t) = sum_t w(t) vart(:,t)^(-1)
%   SY(i) = sum(wvart(Sigma(:,:,i),i).*[data{:}],2); % SY(:) = sum_t w(t) vart(:,t)^(-1) obs(:,t)
% end

mu1 = mu;
Sigma1 = Sigma;
for i=1:Q
  mu1(i) = SY(Sigma(:,:,i),i) / Sw(Sigma(:,:,i),i);
  Sv1=@(v0)(S2YY(v0,i)-mu1(i)*S2Y(v0,i)-S2Y(v0,i)*mu1(i)'+S2w(v0,i)*mu1(i)*mu1(i)'-Sw(v0,i));
  if Sigma1(:,:,i)>2*eps
    Sigma1(:,:,i)=max(eps,fzero(Sv1,Sigma(:,:,i)));
  end
  mu1(i)=max(eps,min(1-eps,mu1(i)));
end

if verbose, fprintf(1, '\n'); end
