function m = sqdist_kn(p, q, Sdata, Sigma)
% SQDIST      Squared Euclidean or Mahalanobis distance.
% SQDIST(p,q)   returns m(i,j) = (p(:,i) - q(:,j))'*(p(:,i) - q(:,j)).
% SQDIST(p,q,A) returns m(i,j) = (p(:,i) - q(:,j))'*A(i)*(p(:,i) - q(:,j)).
% NZ 09.Sep.2008, 05.Nov.2008

%  From Tom Minka's lightspeed toolbox

[d, pn] = size(p);
[d, qn] = size(q);

if nargin == 2
  
  for i=1:qn
    m(i,:) = p.*p + q(i).*q(i) - 2.*p.*q(i);
  end
%   pmag = sum(p .* p, 1);
%   qmag = sum(q .* q, 1);
%   m = repmat(qmag, pn, 1) + repmat(pmag', 1, qn) - 2*p'*q;
  %m = ones(pn,1)*qmag + pmag'*ones(1,qn) - 2*p'*q;
  
else

  if isempty(Sdata) | isempty(Sigma) | isempty(p)
    error('sqdist: empty matrices');
  end
  
  for j=1:qn
    A=1./(q(j)*(1-q(j))./Sdata+squeeze(Sigma(:,:,j)));
    m(:,j) = A.*p.*p + A.*q(j).*q(j) - 2.*A.*p.*q(j);
  end
%   Ap = A*p;
%   Aq = A*q;
%   pmag = sum(p .* Ap, 1);
%   qmag = sum(q .* Aq, 1);
%   m = repmat(qmag, pn, 1) + repmat(pmag', 1, qn) - 2*p'*Aq;
  
end
