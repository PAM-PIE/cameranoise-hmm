function B = multinomial_probNoise(datach, obsmat, datadt, I1, I2, TimeRes)
% EVAL_PDF_COND_MULTINOMIAL Evaluate pdf of conditional multinomial 
% function B = eval_pdf_cond_multinomial(data, obsmat)
%
% Notation: Y = observation (O values), Q = conditioning variable (K values)
%
% Inputs:
% data(t) = t'th observation - must be an integer in {1,2,...,K}: cannot be 0!
% obsmat(i,o) = Pr(Y(t)=o | Q(t)=i)
%
% Output:
% B(i,t) = Pr(y(t) | Q(t)=i)

mu = obsmat(:,2);
[Q O] = size(obsmat);
T = prod(size(datach)); % length(data);
B = zeros(Q,T);

pN1=1-exp(-I1*datadt*TimeRes);
pN2=1-exp(-I2*datadt*TimeRes);

mu_t=zeros(2,Q,T);
for q=1:Q
  mu_t(2,q,:)=(1-(pN1+pN2))*mu(q)+pN2;
  mu_t(1,q,:)=1-mu_t(2,q,:);
end

for t=1:T
  %B(:,t) = obsmat(:, datach(t));
  B(:,t) = mu_t(datach(t),:,t);
end
