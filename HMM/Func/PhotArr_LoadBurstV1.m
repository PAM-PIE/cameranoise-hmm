function [trajFRETdt, trajFRETch, trajALEXdt, IDFile, IDBurst, InfoStartMS] = PhotArr_LoadBurst(ProjektName,MaxAnzBursts);

sFileFRETdt = [ProjektName,'.$BurstPhotFRET'];
sFileFRETch = [ProjektName,'.$BurstPhotCh'];
sFileALEXdt = [ProjektName,'.$BurstPhotALEX'];
sFileHeader = [ProjektName,'.$BurstPhotHeader'];
cVers = 1;

if ~exist(sFileFRETdt,'file')
    error(['File ',sFileFRETdt,' not found!']);
end
if ~exist(sFileFRETch,'file')
    error(['File ',sFileFRETch,' not found!']);
end
if ~exist(sFileHeader,'file')
    error(['File ',sFileHeader,' not found!']);
end
bAlex=exist(sFileALEXdt,'file');

[InfoIndex InfoFilePosFRET InfoLengthFRET InfoFilePosALEX InfoLengthALEX InfoStartMS] = textread(sFileHeader, '%s %d %d %d %d %f', 'delimiter', ' ', 'headerlines', 1);

[sIDFile,sIDBurst] = strtok(InfoIndex,'-');
sIDBurst = strtok(sIDBurst,'-');
IDFile = str2num(strvcat(sIDFile{:}));
IDBurst = str2num(strvcat(sIDBurst{:}));

hFileFRETdt = fopen(sFileFRETdt, 'r');
hFileFRETch = fopen(sFileFRETch, 'r');
if bAlex, hFileALEXdt = fopen(sFileALEXdt, 'r'); end
iBurst = 0;
AnzBurst = length(InfoIndex);
if exist('MaxAnzBursts'), AnzBurst=min(AnzBurst,MaxAnzBursts); end
while iBurst < AnzBurst
    iBurst = iBurst + 1;
    dummy = fread(hFileFRETdt, 1, 'int32')';
    dummy = fread(hFileFRETch, 1, 'int8')';
    trajFRETdt{iBurst} = fread(hFileFRETdt, InfoLengthFRET(iBurst)-1, 'int32')';
    trajFRETch{iBurst} = fread(hFileFRETch, InfoLengthFRET(iBurst)-1, 'int8')';
    if bAlex
        dummy = fread(hFileALEXdt, 1, 'int32')'; 
        trajALEXdt{iBurst} = fread(hFileALEXdt, InfoLengthALEX(iBurst)-1, 'int32')';         
    else trajALEXdt{iBurst}=[]; end
end
fclose(hFileFRETdt);
fclose(hFileFRETch);
if bAlex, fclose(hFileALEXdt); end
