function [StufenArr] = PhotPath2Stufen(TimeBin,TimeRes,photpath,datadt,datach,Q,MinLen,BurstLL,minLL,minInt1,minInt2);
%function [StufenArr] = PhotPath2Stufen(TimeBin,TimeRes,photpath,datadt,datach,Q,MinLen,Inoise1,Inoise2,BurstLL,minLL,minInt1,minInt2);

StufenArr = [];
%BurstInt=BurstData(2,:);
%BurstPF=BurstData(1,:);
datat=cumsum(datadt);

if ~exist('minLL'), minLL=0; end
LL=minLL; 
dwell=datadt(1)*TimeRes/TimeBin;
istart=1;
for i=2:length(photpath)
    if photpath(i) == photpath(i-1)
        dwell=dwell+datadt(i)*TimeRes/TimeBin; %umrechnen in TimeBins
    else
        if sum(photpath(i-1)==Q)&(dwell>=MinLen)
          % unten muss das gleiche stehen wie hier
            tempLL = BurstLL(istart:i-1);
            tempLL = tempLL(find(tempLL~=0));
            tempInt1 = sum(datach(istart:i-1)==1)/dwell;
            tempInt2 = sum(datach(istart:i-1)==2)/dwell;
            if tempLL>0, LL=exp(mean(log(tempLL))); else LL=0; end;
            if (LL>=minLL)&&(tempInt1>=minInt1)&&(tempInt2>=minInt2)
                StufenArr(end+1).PFclass = photpath(i-1);
                StufenArr(end).Start = datat(istart)*TimeRes/TimeBin;
                StufenArr(end).End = datat(i-1)*TimeRes/TimeBin;
                StufenArr(end).MW = tempInt2./max(1,tempInt1+tempInt2);
                StufenArr(end).Iavg1 = tempInt1;
                StufenArr(end).Iavg2 = tempInt2;
                if exist('BurstLL')
                    StufenArr(end).LL = LL; 
                end
            end
          % unten muss das gleiche stehen wie hier
        end
        dwell=datadt(i)*TimeRes/TimeBin;
        istart=i;
    end
end
%letzte Stufe:
if isempty(i), return; end
i=i+1;
if sum(photpath(i-1)==Q)&(dwell-1>=MinLen)
    % wie oben
            tempLL = BurstLL(istart:i-1);
            tempLL = tempLL(find(tempLL~=0));
            tempInt1 = sum(datach(istart:i-1)==1)/dwell;
            tempInt2 = sum(datach(istart:i-1)==2)/dwell;
            if tempLL>0, LL=exp(mean(log(tempLL))); else LL=0; end;
            if (LL>=minLL)&&(tempInt1>=minInt1)&&(tempInt2>=minInt2)
                StufenArr(end+1).PFclass = photpath(i-1);
                StufenArr(end).Start = datat(istart)*TimeRes/TimeBin;
                StufenArr(end).End = datat(i-1)*TimeRes/TimeBin;
                StufenArr(end).MW = tempInt2./max(1,tempInt1+tempInt2);
                StufenArr(end).Iavg1 = tempInt1;
                StufenArr(end).Iavg2 = tempInt2;
                if exist('BurstLL')
                    StufenArr(end).LL = LL; 
                end
            end
    % wie oben
end
