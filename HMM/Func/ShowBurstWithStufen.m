function [datatxt]=ShowBurstWithStufen(dataPF, dataInt, mu1, Sigma1, mixmat1, transmat1, prior1, IDBurst, dataStartMS, TimeBin, cMinLL, Qremain, SimStufen, SimParam);

dataDonor = abs(dataInt).*(1-dataPF);
dataAkzeptor = abs(dataInt).*dataPF;
%data=dataPF;
%dataw=dataInt; %normpdf(dataInt,50,20);
%dataw=dataw./sum(dataw)*length(dataw); 
Q=length(mu1);
if 1
    B = beta_prob(dataPF, dataInt, mu1, Sigma1, mixmat1);
else
    B = mixgauss_probw(dataPF, dataInt, mu1, Sigma1, mixmat1);
end
path = viterbi_path(prior1, transmat1, B);
llpath = prob_path(prior1, transmat1, B, path);

if nargout==0 
    Stufen = Path2Stufen(path, 1:Q, 1, dataPF, dataInt, llpath, 0);  %Nur Stufen 1..Q-1 auswählen, min. Stufenlänge
    hold off; clf;
    sLineStyle={}; 
    for i=1:length(Stufen), 
        if (Stufen(i).LL>=cMinLL)&(sum(Stufen(i).PFclass==Qremain)),
            sLineStyle{i}='-'; 
        else 
            sLineStyle{i}=':'; 
        end; 
    end

    ax(1)=subplot(3,1,1);
    %plot(dataPF,'b');
    %plotwithcol([1:length(dataPF)]',dataPF',dataInt',0.7,[0 0 1]);
    set(gca,'LineStyleOrder',{'-'});
    plot(dataPF);
    hold on;
    if numel(Stufen)>0
        set(gca,'LineStyleOrder',sLineStyle);
        plot([[Stufen(:).Start];[Stufen(:).End]+1],...
            [mu1([Stufen(:).PFclass]);mu1([Stufen(:).PFclass])],...
            'color',[1 0.5 0],'LineWidth',2);
    end
    title([sprintf('Burst %d',[IDBurst]),': ',sprintf('%g ms (%d * [%g ms])',[dataStartMS,length(dataInt),TimeBin])]);
    xlim([1 length(dataInt)]);
    ylim([0 1]);
    ylabel('P');
    for i=1:length(Stufen)
        text((Stufen(i).Start+Stufen(i).End)/2, 0.9, int2str(Stufen(i).PFclass),...
            'HorizontalAlignment','center','Color',[1 0.5 0]);
    end
    % SimStufen
    if exist('SimStufen')
        for i=1:length(SimStufen.PFclass)
            text((SimStufen.Start(i)+SimStufen.End(i))/2, 0.1, int2str(SimStufen.PFclass(i)),...
                'HorizontalAlignment','center','Color',[0 0 0]);
        end
        stairs([SimStufen.Start;SimStufen.End(end)],...
            [SimParam.mu1(SimStufen.PFclass),SimParam.mu1(SimStufen.PFclass(end))],...
            'color',[0 0 0],'LineStyle',':');
        %plot([SimStufen.Start';SimStufen.End'],...
        %    [SimParam.mu1(SimStufen.PFclass);SimParam.mu1(SimStufen.PFclass)],...
        %    'color',[1 0 0]);
    end
    
    ax(3)=subplot(3,1,3);
    set(gca,'LineStyleOrder',{'-'});
    plot(llpath);
    hold on;
    if numel(Stufen)>0
        set(gca,'LineStyleOrder',sLineStyle,'ColorOrder',[1 0.5 0]);
        plot([[Stufen(:).Start];[Stufen(:).End]+1],...
            [Stufen(:).LL;Stufen(:).LL],...
            'LineWidth',2);
    end
    xlim([1 length(dataInt)]);
    ylim([1e-4 1e2]);
    set(gca,'YScale','log');
    ylabel('LL');

    Stufen = Path2Stufen(path, Qremain, 1, dataPF, dataInt, llpath, cMinLL);  %Nur Stufen 1..Q-1 auswählen, min. Stufenlänge
    ax(2)=subplot(3,1,2);
    plot(dataDonor,'color',[0 0.5 0]);
    hold on;
    title(sprintf('%d FRET level',[length(Stufen)]));
    plot(dataAkzeptor,'color',[1 0 0]);
    if numel(Stufen)>0
        %dataStufen = max(dataInt) * 0.95 * Stufen2Traj(Stufen);
        %plot([1:length(dataStufen)]+1,dataStufen,'color',[0.5 0.5 0]);
        dataStufenx=reshape([[Stufen.Start]',[Stufen.End]'+1]',length(Stufen)*2,1);
        dataStufeny=repmat([max(dataInt)*0.95;0],length(Stufen),1);
        stairs(dataStufenx,dataStufeny,'color',[0.5 0.5 0]);
    end
    xlim([1 length(dataInt)]);
    ylim([0 max(dataInt)]);
    ylabel('counts / ms');
    %Achsen verlinken
    linkaxes(ax,'x');
    %disp(sprintf('Burst %d von %d',[iBurst,AnzBursts]));
else
    Stufen = Path2Stufen(path, Qremain, 1, dataPF, dataInt, llpath, cMinLL);  %Nur Stufen 1..Q-1 auswählen, min. Stufenlänge
    dataStufen = zeros(1,length(dataDonor));
    for i=1:length(Stufen), dataStufen(Stufen(i).Start:Stufen(i).End)=mu1(Stufen(i).PFclass); end;
    datatxt=[[1:length(dataDonor)]'-1,dataDonor(:),dataAkzeptor(:),dataPF(:),dataStufen(:)];
end
