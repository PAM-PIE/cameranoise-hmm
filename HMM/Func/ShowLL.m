function [] = ShowLL(LL, Qn, DataLength);

    plot(Qn,LL,'linewidth', 3, 'color', [0 0.5 0],...
        'Marker','o', 'MarkerFaceColor',[1 1 1]);
    grid on;
    xlabel('number of states','FontSize',20);
    ylabel('log Likelihood','FontSize',20);
    set(gca,'XTick',[Qn]);
    
    %NoParams=(1:length(LL))'.*[(1:length(LL))-1]'+2;  ???
    %Unabh. Param der Transition-Matrix: Q*(Q-1)
    %Anzahl der Zust�nde mit 1 Observablen: Q
    NoParams=(Qn.*Qn+Qn*1);
    BIC=2*LL'-NoParams'.*log(DataLength');

    figure;
    plot(Qn,BIC,'linewidth', 3, 'color', [0 0.5 0],...
        'Marker','o', 'MarkerFaceColor',[1 1 1]);
    grid on;
    xlabel('number of states','FontSize',20);
    ylabel('BIC','FontSize',20);
    set(gca,'XTick',[Qn]);
    set(gca,'FontSize',20);
end
