function [StufenArr] = Path2Stufen(path,Q,MinLen,BurstInt1,BurstInt2,BurstInt3,...
  Inoise1,Inoise2,Inoise3,BurstLL,minLL,minInt1,minInt2,minInt3);

cMinRelevant=0.5;
cMaxPFstd=Inf; %0.15;
bALEX=~isempty(BurstInt3);

if ~exist('minLL'), minLL=0; end
LL=minLL;
path=[path,NaN];
dwell=1;
iStart=[];
iEnd=[];
idx=0;
for i=2:length(path)
  if path(i) == path(i-1)
    dwell=dwell+1;
  else
    if sum(path(i-1)==Q)&(dwell>=MinLen)
      idx=idx+1;
      iStart(idx) = i-dwell;
      iEnd(idx) = i-1;
    end
    dwell = 1;    
  end
end

StufenArr=[];
idx=0;
for i=1:length(iStart)
  i1 = iStart(i);
  i2 = iEnd(i);
  tempInt1 = BurstInt1(i1:i2);
  tempInt2 = BurstInt2(i1:i2);
  if bALEX, tempInt3 = BurstInt3(i1:i2); end
  tempInoise1 = Inoise1(i1:i2);
  tempInoise2 = Inoise2(i1:i2);
  tempInt1korr = max(0,abs(tempInt1)-tempInoise1);
  tempInt2korr = max(0,abs(tempInt2)-tempInoise2);
  tempPF = tempInt2korr./(tempInt1korr+tempInt2korr+(tempInt1korr+tempInt2korr==0));
  tempInt1 = tempInt1(find(tempInt1>=0));
  tempInt2 = tempInt2(find(tempInt2>=0));
  if bALEX, tempInt3 = tempInt3(find(tempInt3>=0)); end
  meanInt1 = sum(tempInt1)/(length(tempInt1)+(length(tempInt1)==0));
  meanInt2 = sum(tempInt2)/(length(tempInt2)+(length(tempInt2)==0));
  meanInt3 = 0;
  if bALEX, meanInt3 = sum(tempInt3)/(length(tempInt3)+(length(tempInt3)==0)); end
  len=min([length(tempInt1) length(tempInt2)]);  
  if bALEX, len=min(len,length(tempInt3)); end
  dwell=i2-i1+1;
  
  tempLL = BurstLL(i1:i2);
  tempLL = tempLL(find(tempLL~=0));
  if tempLL>0, LL=exp(mean(log(tempLL))); else LL=0; end;
  
  if (LL>=minLL)&&(meanInt1>=minInt1)&&(meanInt2>=minInt2)&&(meanInt3>=minInt3)&&...
      (len>MinLen)&&(len/dwell>cMinRelevant)&&(std(tempPF)<=cMaxPFstd)
    idx=idx+1;
    StufenArr(idx).PFclass = path(i1);
    StufenArr(idx).Start = i1;
    StufenArr(idx).End = i2;
    StufenArr(idx).MW = mean(tempPF);
    StufenArr(idx).MWstd = std(tempPF);
    StufenArr(idx).Iavg1 = meanInt1;
    StufenArr(idx).Iavg2 = meanInt2;
    if exist('BurstLL')
      StufenArr(idx).LL = LL;
    end
  end
  
end
