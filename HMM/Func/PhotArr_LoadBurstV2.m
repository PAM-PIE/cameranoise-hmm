function [trajDonor, trajAcc, trajALEX, IDFile, IDBurst, InfoStartMS] = PhotArr_LoadBurst(ProjektName,MaxAnzBursts);

sFileDonor = [ProjektName,'.$BurstPhotDonor'];
sFileAcc = [ProjektName,'.$BurstPhotAcc'];
sFileALEX = [ProjektName,'.$BurstPhotALEX'];
sFileHeader = [ProjektName,'.$BurstPhotHeader'];
cVers = 1;

if ~exist(sFileDonor,'file')
  error(['File ',sFileDonor,' not found!']);
end
if ~exist(sFileAcc,'file')
  error(['File ',sFileAcc,' not found!']);
end
bAlex=exist(sFileALEX,'file');
if ~exist(sFileHeader,'file')
  error(['File ',sFileHeader,' not found!']);
end

[InfoIndex InfoFilePosDonor InfoLengthDonor InfoFilePosAcc InfoLengthAcc InfoFilePosALEX InfoLengthALEX InfoStartMS] = ...
  textread(sFileHeader, '%s %d %d %d %d %d %d %f', 'delimiter', ' ', 'headerlines', 1);

[sIDFile,sIDBurst] = strtok(InfoIndex,'-');
sIDBurst = strtok(sIDBurst,'-');
IDFile = str2num(strvcat(sIDFile{:}));
IDBurst = str2num(strvcat(sIDBurst{:}));

hFileDonor = fopen(sFileDonor, 'r');
hFileAcc = fopen(sFileAcc, 'r');
if bAlex, hFileALEX = fopen(sFileALEX, 'r'); end
iBurst = 0;
AnzBurst = length(InfoIndex);
if exist('MaxAnzBursts'), AnzBurst=min(AnzBurst,MaxAnzBursts); end
while iBurst < AnzBurst
  iBurst = iBurst + 1;
  trajDonor{iBurst} = fread(hFileDonor, InfoLengthDonor(iBurst), 'int32')';
  trajAcc{iBurst} = fread(hFileAcc, InfoLengthAcc(iBurst), 'int32')';
  if bAlex
    trajALEX{iBurst} = fread(hFileALEX, InfoLengthALEX(iBurst), 'int32')';
  else trajALEX{iBurst}=[]; end
end
fclose(hFileDonor);
fclose(hFileAcc);
if bAlex, fclose(hFileALEX); end
