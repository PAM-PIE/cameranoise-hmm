function [dataSteps] = StatPath(Q,path);

for i=1:Q
    dataSteps{i} = [];
end

dwell=1;
for i=2:length(path)
    if path(i) == path(i-1)
        dwell=dwell+1;
    else
        dataSteps{path(i-1)}(end+1) = dwell;
        dwell = 1;
    end
end

if 1
    for i=1:Q
        subplot(2,2,i);
        hist(dataSteps{i},1:10);
    end
end