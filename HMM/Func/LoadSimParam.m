function [SimParam]=LoadSimParam(filenamestamm);
    %Datei laden
    fname=[filenamestamm,'.param']; 
    [paramfile]=textread(fname,'%s\r\n','headerlines',1,'delimiter','');
    %Initialisierung der Hilfsvariablen, dann Parameter auslesen
    cvalue=25:25-1+7;
    %Anzahl Files
    index=strmatch('Sim. Anzahl Files:',paramfile);
    SimParam.AnzFiles=str2num(paramfile{index}(cvalue));
    %Soll-Stufen
    iFRET=1;
    while 1
        index=strmatch(['FRET ProxFact ',int2str(iFRET),':'],paramfile);
        if isempty(index), break; end
        SimParam.mu1(iFRET)=str2num(paramfile{index}(cvalue));
        iFRET=iFRET+1;
    end
    %Zeitauflösung der Simulation
    index=strmatch('Sim. Zeitauflösung:',paramfile);
    SimParam.timeressim_ms=str2num(paramfile{index}(cvalue))/1000;
    %Zeitauflösung der Binning-Zeit (Diese könnte eigentlich vom 
    %Burst Analyzer weiter reduziert werden!!!)
    index=strmatch('Sim. Timetrace Bin:',paramfile);
    SimParam.timeresbin_ms=str2num(paramfile{index}(cvalue));
    %Anzahl Moleküle
    index=strmatch('Molek.schnell Anzahl:',paramfile);
    SimParam.AnzFree=str2num(paramfile{index}(cvalue));
    index=strmatch('Molek.langsam Anzahl:',paramfile);
    SimParam.AnzBound=str2num(paramfile{index}(cvalue));
end
