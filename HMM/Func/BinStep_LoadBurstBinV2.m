function [trajIntA, trajIntB, IDFile, IDBurst, InfoStartMS] = BinStep_LoadBurstBinV2(ProjektName,MaxAnzBursts);

FileBinIntA = [ProjektName,'.$BurstBinIntA'];
FileBinIntB = [ProjektName,'.$BurstBinIntB'];
FileHeader = [ProjektName,'.$BurstHeader'];
cVers = 2;

if ~exist(FileBinIntA,'file')
    error(['File ',FileBinIntA,' not found!']);
end
if ~exist(FileBinIntB,'file')
    error(['File ',FileBinIntB,' not found!']);
end
if ~exist(FileHeader,'file')
    error(['File ',FileHeader,' not found!']);
end

[InfoIndex InfoFilePos InfoLength InfoStartMS] = textread(FileHeader, '%s %d %d %f', 'delimiter', ' ', 'headerlines', 1);

trajIntA = {};
trajIntB = {};

[sIDFile,sIDBurst] = strtok(InfoIndex,'-');
sIDBurst = strtok(sIDBurst,'-');
IDFile = str2num(strvcat(sIDFile{:}));
IDBurst = str2num(strvcat(sIDBurst{:}));

fBinIntA = fopen(FileBinIntA, 'r');
fBinIntB = fopen(FileBinIntB, 'r');
iBurst = 0;
AnzBurst = length(InfoIndex);
if exist('MaxAnzBursts'), AnzBurst=min(AnzBurst,MaxAnzBursts); end
while iBurst < AnzBurst
    iBurst = iBurst + 1;
    trajIntA{iBurst} = fread(fBinIntA, InfoLength(iBurst), 'int32')';
    trajIntB{iBurst} = fread(fBinIntB, InfoLength(iBurst), 'int32')';
    %subplot(2,1,1); plot(trajPF{iBurst},'b'); ylim([0 1]);
    %subplot(2,1,2); plot(trajInt{iBurst},'k'); 
end
fclose(fBinIntA);
fclose(fBinIntB);
