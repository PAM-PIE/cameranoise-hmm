function [TimeBin, StufenKlassen, KlassenWerte, KlassenStdAbw, Dwelltime, ...
    LearnKlassenWerte, LearnKlassenStdAbw, LearnDwelltime] = LoadHMMParams(filename);

    fid = fopen(filename, 'rt');
    if fid < 0
        disp(['File "',filename,'" not found!'])
        return
    end

    sdata = textscan(fid,'%s %s','delimiter','\t');
    fclose(fid);

    irow = strmatch('TimeBinningMS',sdata{1});
    TimeBin = str2num(strvcat(sdata{2}(irow)))';
    
    irow = strmatch('StufenKlassen',sdata{1});
    StufenKlassen = str2num(strvcat(sdata{2}(irow)))';

    irow = strmatch('Klasse',sdata{1});
    KlassenWerte = str2num(strvcat(sdata{2}(irow)))';
    
    irow = strmatch('KlStdabw',sdata{1});
    KlassenStdAbw = str2num(strvcat(sdata{2}(irow)))';
    if length(KlassenStdAbw)==1, KlassenStdAbw=repmat(KlassenStdAbw,1,StufenKlassen); end
    
    irow = strmatch('Dwelltime',sdata{1});
    Dwelltime = str2num(strvcat(sdata{2}(irow)))';
    if length(Dwelltime)==1, Dwelltime=repmat(Dwelltime,1,StufenKlassen); end
    
    irow = strmatch('LearnKlasse',sdata{1});
    LearnKlassenWerte = str2num(strvcat(sdata{2}(irow)))';
    if length(LearnKlassenWerte)==1, LearnKlassenWerte=repmat(LearnKlassenWerte,1,StufenKlassen); end
    
    irow = strmatch('LearnKlStdabw',sdata{1});
    LearnKlassenStdAbw = str2num(strvcat(sdata{2}(irow)))';
    if length(LearnKlassenStdAbw)==1, LearnKlassenStdAbw=repmat(LearnKlassenStdAbw,1,StufenKlassen); end
    
    irow = strmatch('LearnDwelltime',sdata{1});
    LearnDwelltime = str2num(strvcat(sdata{2}(irow)))';
    if length(LearnDwelltime)==1, LearnDwelltime=repmat(LearnDwelltime,1,StufenKlassen); end
    
end
