function [trajStufen] = Stufen2Traj(Stufen);

trajStufen = zeros(Stufen(end).End,1);
for i=1:length(Stufen)
    trajStufen(Stufen(i).Start:Stufen(i).End-1) = 1;
end
