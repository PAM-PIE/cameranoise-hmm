function [trajInt, IDFile, IDBurst, InfoStartMS] = BinStep_LoadBurstBinV2b(ProjektName,MaxAnzBursts);

FileBinIntA = [ProjektName,'.$BurstBinIntA'];
FileBinIntB = [ProjektName,'.$BurstBinIntB'];
FileBinIntC = [ProjektName,'.$BurstBinIntC'];
FileHeader = [ProjektName,'.$BurstHeader'];
cVers = 2;

if ~exist(FileBinIntA,'file')
    error(['File ',FileBinIntA,' not found!']);
end
if ~exist(FileBinIntB,'file')
    error(['File ',FileBinIntB,' not found!']);
end
if ~exist(FileHeader,'file')
    error(['File ',FileHeader,' not found!']);
end
bAlex=exist(FileBinIntC,'file');

[InfoIndex InfoFilePos InfoLength InfoStartMS] = textread(FileHeader, '%s %d %d %f', 'delimiter', ' ', 'headerlines', 1);

trajInt = {};

[sIDFile,sIDBurst] = strtok(InfoIndex,'-');
sIDBurst = strtok(sIDBurst,'-');
IDFile = str2num(strvcat(sIDFile{:}));
IDBurst = str2num(strvcat(sIDBurst{:}));

fBinIntA = fopen(FileBinIntA, 'r');
fBinIntB = fopen(FileBinIntB, 'r');
if bAlex, fBinIntC = fopen(FileBinIntC, 'r'); end
iBurst = 0;
AnzBurst = length(InfoIndex);
if exist('MaxAnzBursts'), AnzBurst=min(AnzBurst,MaxAnzBursts); end
while iBurst < AnzBurst
    iBurst = iBurst + 1;
    trajInt{1,iBurst} = fread(fBinIntA, InfoLength(iBurst), 'int32')';
    trajInt{2,iBurst} = fread(fBinIntB, InfoLength(iBurst), 'int32')';
    if bAlex, trajInt{3,iBurst} = fread(fBinIntC, InfoLength(iBurst), 'int32')'; end
    %subplot(2,1,1); plot(trajPF{iBurst},'b'); ylim([0 1]);
    %subplot(2,1,2); plot(trajInt{iBurst},'k'); 
end
fclose(fBinIntA);
fclose(fBinIntB);
if bAlex, fclose(fBinIntC); end
