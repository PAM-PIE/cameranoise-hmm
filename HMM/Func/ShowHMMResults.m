function ShowHMMResults(AllStufen,BurstInt,Qanz);
%Zeigt eine einfache statistische Auswertung der Daten an

if ~exist('TripleArr')
    [StufenArr] = StufenHMM2StufenArr(AllStufen,BurstInt,5,1);
    DoubleArr=StufenFilter(StufenArr,'2',true);
    TripleArr=StufenFilter(StufenArr,'3+',true);
end

close all;

if 1
    figure;
    for i=1:Qanz
        subplot(Qanz+1,1,i); 
        idwell=find([TripleArr(:,2).PFclass]==i); 
        dwell=[TripleArr(idwell,2).Length];
        Fit_HistExp1(dwell,1:2:100,1,false);
        title(['Zustand',int2str(i), ' #', int2str(length(dwell))]);
    end
    subplot(Qanz+1,1,Qanz+1); 
    dwell=[TripleArr(:,2).Length];
    Fit_HistExp1(dwell,1:2:120,3,false);
    title(['Gesamt #', int2str(length(dwell))]);
end

if 1
    figure;
    hold off;
    x=0:0.02:1;
    Stufen=[AllStufen{:}];
    idata=find(([Stufen(:).MW]>0)&([Stufen(:).MW]<1)); 
    Stufen=Stufen(idata);
    data=hist([Stufen(:).MW],x);
    bar(x,data,'BarWidth',1,'FaceColor',0.9*[1 1 1]);
    title(['PF #', int2str(length(Stufen))]);
    hold on
    for i=1:Qanz
        idata=find([Stufen(:).PFclass]==i);
        data=[Stufen(idata).MW];
        bfit=betafit(data);
        plot(x,(length(data)/(length(x)-1))*betapdf(x,bfit(1),bfit(2)),'r','LineWidth',2);
    end
    xlim([0 1]);
end

