function [D]=propdens(p,d)

for i=1:length(d)
    D(i) = (1-p)*p^(d(i)-1);
end

