function [trajPF, trajInt, IDFile, IDBurst, InfoStartMS] = BinStep_LoadBurstBinV1(ProjektName,MaxAnzBursts);

FileBinPF = [ProjektName,'.$BurstBinPF'];
FileBinInt = [ProjektName,'.$BurstBinInt'];
FileHeader = [ProjektName,'.$BurstHeader'];

if ~exist(FileBinPF,'file')
    error(['File ',FileBinPF,' not found!']);
end
if ~exist(FileBinInt,'file')
    error(['File ',FileBinInt,' not found!']);
end
if ~exist(FileHeader,'file')
    error(['File ',FileHeader,' not found!']);
end

[InfoIndex InfoFilePos InfoLength InfoStartMS] = textread(FileHeader, '%s %d %d %f', 'delimiter', ' ', 'headerlines', 1);

trajPF = {};
trajInt = {};

[sIDFile,sIDBurst] = strtok(InfoIndex,'-');
sIDBurst = strtok(sIDBurst,'-');
IDFile = str2num(strvcat(sIDFile{:}));
IDBurst = str2num(strvcat(sIDBurst{:}));

fBinPF = fopen(FileBinPF, 'r');
fBinInt = fopen(FileBinInt, 'r');
iBurst = 0;
AnzBurst = length(InfoIndex);
if exist('MaxAnzBursts'), AnzBurst=min(AnzBurst,MaxAnzBursts); end
while iBurst < AnzBurst
    iBurst = iBurst + 1;
    trajPF{iBurst} = fread(fBinPF, InfoLength(iBurst), 'float32')';
    trajInt{iBurst} = fread(fBinInt, InfoLength(iBurst), 'int32')';
    %subplot(2,1,1); plot(trajPF{iBurst},'b'); ylim([0 1]);
    %subplot(2,1,2); plot(trajInt{iBurst},'k'); 
end
fclose(fBinPF);
fclose(fBinInt);
