function [StufenArr] = Path2Stufen(path,Q,MinLen,BurstInt1,...
  Inoise1,BurstLL,minLL,minInt1);

cMinRelevant=0;
cMaxPFstd=Inf; %0.15;

if ~exist('minLL'), minLL=0; end
LL=minLL;
path=[path,NaN];
dwell=1;
iStart=[];
iEnd=[];
idx=0;
for i=2:length(path)
  if path(i) == path(i-1)
    dwell=dwell+1;
  else
    if sum(path(i-1)==Q)&(dwell>=MinLen)
      idx=idx+1;
      iStart(idx) = i-dwell;
      iEnd(idx) = i-1;
    end
    dwell = 1;    
  end
end

StufenArr=[];
idx=0;
for i=1:length(iStart)
  i1 = iStart(i);
  i2 = iEnd(i);
  tempInt1 = BurstInt1(i1:i2);
  tempInt1korr = max(0,abs(tempInt1-Inoise1));
  tempInt1 = tempInt1(find(tempInt1>=0));
  meanInt1 = sum(tempInt1)/(length(tempInt1)+(length(tempInt1)==0));
  len=length(tempInt1);
  dwell=i2-i1+1;
  
  tempLL = BurstLL(i1:i2);
  tempLL = tempLL(find(tempLL~=0));
  if tempLL>0, LL=exp(mean(log(tempLL))); else LL=0; end;
  
  if (LL>=minLL)&&(meanInt1>=minInt1)&&...
      (len>MinLen)&&(len/dwell>cMinRelevant)
    idx=idx+1;
    StufenArr(idx).PFclass = path(i1);
    StufenArr(idx).Start = i1;
    StufenArr(idx).End = i2;
    StufenArr(idx).Iavg1 = meanInt1;
    if exist('BurstLL')
      StufenArr(idx).LL = LL;
    end
  end
  
end
