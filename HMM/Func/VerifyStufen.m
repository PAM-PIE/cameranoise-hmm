function [q]=VerifyStufen(HMMStufen, HMMLength, SollStufen);
%function [q]=VerifyStufen(HMMStufen, HMMLength, SollStufen);
%Diese Funktion vergleicht f�r jeden Zeitschritt die mit dem HMM gefundenen 
%Stufen mit der echten Stufenklasse.
%Das Ergebnis sind die Anzahl ms von richtig erkannten Zust�nden zu der
%Gesamtdauer der analysierten Bursts

gleich=0;
gesamt=0;
for iBurst=1:length(HMMStufen)
    PFclassHMM=zeros(HMMLength(iBurst),1);
    for iStufe=1:length(HMMStufen{iBurst})
        PFclassHMM(ceil(HMMStufen{iBurst}(iStufe).Start):ceil(HMMStufen{iBurst}(iStufe).End)-1) = ...
            HMMStufen{iBurst}(iStufe).PFclass;
    end
    PFclassSoll=zeros(HMMLength(iBurst),1);
    for iStufe=1:length(SollStufen{iBurst}.PFclass)
        PFclassSoll(ceil(SollStufen{iBurst}.Start(iStufe)):ceil(SollStufen{iBurst}.End(iStufe)-1)) = ...
            SollStufen{iBurst}.PFclass(iStufe);
    end
    gleich=gleich+sum((PFclassHMM==PFclassSoll));
    gesamt=gesamt+sum(PFclassHMM>0);
end

q=gleich/gesamt;
end
