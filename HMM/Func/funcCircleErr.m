function Err = funcCircleErr(p, data);

% p(1) = x0
% p(2) = y0
% p(3) = r0
% data(:,1) = datax
% data(:,2) = datay

Err = (data(:,1)-p(1)).*(data(:,1)-p(1))+(data(:,2)-p(2)).*(data(:,2)-p(2))-p(3)*p(3);
%{ 
extrem slow!
for i=1:size(data,1)
    Err(i) = (data(i,1)-p(1))^2+(data(i,2)-p(2))^2-p(3)*p(3);
end
Err = Err';
%}
end
