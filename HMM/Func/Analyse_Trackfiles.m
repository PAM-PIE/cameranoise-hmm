%function Analyse_Trackfiles(data, traj, phi);

if exist('traj')
    bSollpos = ~isempty(traj);
else
    bSollpos = false;
end
disp('start...');
bild = 0;

%data_ref = [data(:,2) data(:,3)];
%data_spotorig1 = [data(:,5), data(:,6)];
%data_spotorig2 = [data(:,8), data(:,9)];

if 1
    %data_spot = [data_orig1(:,1) - data_ref(:,1), data_orig1(:,2) - data_ref(:,2)];
    %datax = data_spot(:,1);
    %datay = data_spot(:,2);
    %datax = data(:,3);
    %datay = data(:,4);
else
    if 1
        datax = testdatax';
        datay = testdatay';
    else
        trajx = r0*cos(phi(traj)/180*pi);
        trajy = r0*sin(phi(traj)/180*pi);
        pathx = r0*cos(phi(path)/180*pi);
        pathy = r0*sin(phi(path)/180*pi);
    end
end

datax=datax(:);
datay=datay(:);
clear data_spot;

if 1
    p = FitCircle(datax, datay);
else
    p = [0 1E-6 0];
end

set(gcf,'Units','pixels');
set(gcf,'Position',[10 500 700 250]);
set(gcf,'PaperPositionMode','auto');    % <-- Ohne diese Anweisung druckt der SaveAs-Befehl nur Mist!

if bild
    subplot(1,2,1);
    hold off;
    plot(datax-p(1), datay-p(2), '.');
    hold on;
    plot(0,0, 'r.');
    t=0:2*pi/120:2*pi;
    plot(p(3)*cos(t), p(3)*sin(t), 'r');
    %rose(data,120);
    clear t;
    hold off;
    subplot(1,2,2);
end
    
dataw = atan2(datay'-p(2),datax'-p(1));%+pi*(1-sign(testdatay'-p(2))); 
plot(AddWinkel(dataw),'b');
hold on;
if bSollpos
    dataw=phi(traj)/180*pi;
    plot(AddWinkel(dataw),'color',[0 0.5 0]);
end
if exist('path')
    dataw=hmm_phi(path)/180*pi;
    plot(AddWinkel(dataw),'r');
end
xlabel('frames');
ylabel('revolutions');
hold off;
    

clear bild;
%clear datax;
%clear datay;
