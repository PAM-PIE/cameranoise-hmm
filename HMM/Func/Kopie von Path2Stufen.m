function [StufenArr] = Path2Stufen(path,Q,MinLen,BurstData,BurstLL,minLL);

StufenArr = [];
BurstInt=BurstData(2,:);
BurstPF=BurstData(1,:);

if ~exist('minLL'), minLL=0; end
LL=minLL; 
dwell=1;
for i=2:length(path)
    if path(i) == path(i-1)
        dwell=dwell+1;
    else
        if sum(path(i-1)==Q)&(dwell>=MinLen)
            if exist('BurstLL'), LL=exp(mean(log(BurstLL(i-dwell:i-1)))); end
            if LL>=minLL
                StufenArr(end+1).PFclass = path(i-1);
                StufenArr(end).Start = i-dwell;
                StufenArr(end).End = i-1;
                StufenArr(end).MW = sum(BurstPF(i-dwell:i-1).*BurstInt(i-dwell:i-1))/max(1,sum(BurstInt(i-dwell:i-1)));
                if exist('BurstLL')
                    StufenArr(end).LL = LL; 
                end
            end
        end
        dwell = 1;
    end
end
%letzte Stufe:
if isempty(i), return; end
i=i+1;
if sum(path(i-1)==Q)&(dwell-1>=MinLen)
    if exist('BurstLL'), LL=exp(mean(log(BurstLL(i-dwell:i-1)))); end
    if LL>=minLL
        StufenArr(end+1).PFclass = path(i-1);
        StufenArr(end).Start = i-dwell;
        StufenArr(end).End = i-1;
        StufenArr(end).MW = sum(BurstPF(i-dwell:i-1).*BurstInt(i-dwell:i-1))/sum(BurstInt(i-dwell:i-1));
        if exist('BurstLL')
            StufenArr(end).LL = LL;
        end
    end
end
