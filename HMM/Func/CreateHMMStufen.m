function [Stufen,Stufen_]=CreateHMMStufen(dataPF,dataInt,mu1,Sigma1,mixmat1,obsvect,prior1,transmat1,cMinLL);
%function[Stufen]=CreateHMMStufen(...
%   dataPF,dataInt,mu1,Sigma1,mixmat1,obsvect,prior1,transmat1,cMinLL);

bUseBetaFunc=true;
Q=length(obsvect);

if ~iscell(dataPF)
        if bUseBetaFunc
            B = beta_prob(dataPF, dataInt, mu1, Sigma1, mixmat1);
        else
            B = mixgauss_prob(dataPF, mu1, Sigma1);
        end
        Bobs = ExpandB(B,obsvect);
        path = viterbi_path(prior1, transmat1, Bobs);
        llpath = prob_path(prior1, transmat1, Bobs, path);
        LL = sum(llpath)/length(llpath);
        Stufen = Path2Stufen(path, 1:Q, 1, dataPF, dataInt, llpath, cMinLL);  %Nur Stufen 1..Q-1 auswählen, min. Stufenlänge
        if 1
            path_ = obsvect(path);
            llpath_ = prob_path(prior1, transmat1, Bobs, path_);
            llpath_(find(llpath_==0)) = NaN;
            Stufen_ = Path2Stufen(path_, 1:Q, 1, dataPF, dataInt, llpath_, cMinLL);  %Nur Stufen 1..Q-1 auswählen, min. Stufenlänge
        end
else    
    for iBurst=1:numel(dataPF)
        if bUseBetaFunc
            B = beta_prob(dataPF{iBurst}, dataInt{iBurst}, mu1, Sigma1, mixmat1);
        else
            B = mixgauss_prob(dataPF{iBurst}, mu1, Sigma1);
        end
        Bobs = ExpandB(B,obsvect);
        path = viterbi_path(prior1, transmat1, Bobs);
        llpath = prob_path(prior1, transmat1, Bobs, path);
        LL = sum(llpath)/length(llpath);
        Stufen{iBurst} = Path2Stufen(path, 1:Q, 1, dataPF{iBurst}, dataInt{iBurst}, llpath, cMinLL);  %Nur Stufen 1..Q-1 auswählen, min. Stufenlänge
        if 1
            path_ = obsvect(path);
            llpath_ = prob_path(prior1, transmat1, Bobs, path_);
            Stufen_{iBurst} = Path2Stufen(path_, 1:Q, 1, dataPF{iBurst}, dataInt{iBurst}, llpath_, cMinLL);  %Nur Stufen 1..Q-1 auswählen, min. Stufenlänge
        end
    end
end

end