function [datarot] = AddWinkel(dataw)

datarot(1) = 0;
for i=2:length(dataw)
    diff = dataw(i-1) - dataw(i);   %positiver Wert = Uhrzeigersinn = mathematisch neg!
    datarot(i) = datarot(i-1) + diff/2/pi;
    if abs(diff) > pi
        datarot(i) = datarot(i) - 1*sign(diff);
    end
end

end

