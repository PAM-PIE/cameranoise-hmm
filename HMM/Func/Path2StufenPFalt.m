function [StufenArr] = Path2Stufen(path,Q,MinLen,BurstPF,BurstInt,BurstLL,minLL);

StufenArr = [];
%BurstInt=BurstData(2,:);
%BurstPF=BurstData(1,:);

if ~exist('minLL'), minLL=0; end
LL=minLL; 
dwell=1;
for i=2:length(path)
    if path(i) == path(i-1)
        dwell=dwell+1;
    else
        if sum(path(i-1)==Q)&(dwell>=MinLen)
            %BurstLL=max(1E-15,BurstLL);
            tempLL = BurstLL(i-dwell:i-1);
            tempLL = tempLL(find(tempLL~=0));
            if tempLL>0, LL=exp(mean(log(tempLL))); else LL=0; end;
            if LL>=minLL
                StufenArr(end+1).PFclass = path(i-1);
                StufenArr(end).Start = i-dwell;
                StufenArr(end).End = i-1;
                %StufenArr(end).MW = sum(BurstPF(i-dwell:i-1).*abs(BurstInt(i-dwell:i-1)))/max(1,sum(abs(BurstInt(i-dwell:i-1))));
                StufenArr(end).PFmean = mean(BurstPF(i-dwell:i-1));
                StufenArr(end).PFstd = std(BurstPF(i-dwell:i-1));
                if exist('BurstLL')
                    StufenArr(end).LL = LL; 
                end
            end
        end
        dwell = 1;
    end
end
%letzte Stufe:
if isempty(i), return; end
i=i+1;
if sum(path(i-1)==Q)&(dwell-1>=MinLen)
    tempLL = BurstLL(i-dwell:i-1);
    tempLL = tempLL(find(tempLL~=0));
    if tempLL>0, LL=exp(mean(log(tempLL))); else LL=0; end;
    if LL>=minLL
        StufenArr(end+1).PFclass = path(i-1);
        StufenArr(end).Start = i-dwell;
        StufenArr(end).End = i-1;
        %StufenArr(end).MW = sum(BurstPF(i-dwell:i-1).*abs(BurstInt(i-dwell:i-1)))/sum(abs(BurstInt(i-dwell:i-1)));
        StufenArr(end).PFmean = mean(BurstPF(i-dwell:i-1));
        StufenArr(end).PFstd = std(BurstPF(i-dwell:i-1));
        if exist('BurstLL')
            StufenArr(end).LL = LL;
        end
    end
end
