function [SollStufen]=LoadSimStufen(filenamestamm, HMMStartMS, HMMLength, HMMFileNr, PartNr, SimParam);
%function [SollStufen]=LoadSollStufen(filenamestamm, HMMStartMS, HMMLength, HMMFileNr, PartNr, SimParam);
%Diese Funktion l�dt die SollStufen aus dem entsprchenden File und
%generiert ein SollStufen-Array mit der gleichen Struktur wie das
%HMMStufenArr
cTime=SimParam.timeresbin_ms/SimParam.timeressim_ms;
%State-Dateien einladen
for iFile=1:SimParam.AnzFiles
    fname=[filenamestamm,sprintf('_%03d',[iFile]),'.state',int2str(PartNr)]; 
    if SimParam.AnzFiles==1, fname=[filenamestamm,'.state',int2str(PartNr)]; end
    [molstatex{iFile} molstatey{iFile}]=textread(fname,'%d\t%d\r\n');
end
%Ergebnis-Arr erzeugen
SollStufen={};
for iBurst=1:length(HMMStartMS)
   SollBurst=[];
   iArr1=find(molstatex{HMMFileNr(iBurst)}*SimParam.timeressim_ms>=HMMStartMS(iBurst),1,'first');
   iArr2=find(molstatex{HMMFileNr(iBurst)}*SimParam.timeressim_ms<=(HMMStartMS(iBurst)+HMMLength(iBurst)),1,'last');
   if iArr1>1
       SollBurst.PFclass=molstatey{HMMFileNr(iBurst)}(iArr1-1:iArr2);
       SollBurst.Start=[1;molstatex{HMMFileNr(iBurst)}(iArr1:iArr2)*SimParam.timeressim_ms-HMMStartMS(iBurst)+1];
       SollBurst.End=molstatex{HMMFileNr(iBurst)}(iArr1:iArr2)*SimParam.timeressim_ms-HMMStartMS(iBurst)+1;
   else
       SollBurst.PFclass=molstatey{HMMFileNr(iBurst)}(iArr1:iArr2);
       SollBurst.Start=molstatex{HMMFileNr(iBurst)}(iArr1:iArr2)*SimParam.timeressim_ms-HMMStartMS(iBurst)+1;
       SollBurst.End=molstatex{HMMFileNr(iBurst)}(iArr1+1:iArr2)*SimParam.timeressim_ms-HMMStartMS(iBurst)+1;
   end
   SollBurst.End(end+1)=HMMLength(iBurst)+.5;
   SollStufen{iBurst}=SollBurst;
end

end
