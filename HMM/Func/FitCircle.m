function [p] = FitCircle(xdata, ydata);

x0 = mean(xdata);
y0 = mean(ydata);
r0 = 0.8*(std(xdata)+std(ydata));

options = optimset('Display','off');
[p] = lsqcurvefit(@funcCircleErr,[x0;y0;r0],[xdata(:) ydata(:)],zeros(1*length(xdata),1),[],[],options);

end

