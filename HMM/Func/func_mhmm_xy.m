%function [] = try_mhmm_xy(Q, data, sollpos, traj, phi);

bSave = true;
bSollpos = false;

cSize = 8; %Pixel
cHist = 32; %Counts for histogram

IterUpdate = 100;
IterMax = 0;
Threshold = 1E-6;
%O = 1;         %Number of coefficients in a vector 
%T = 100;        %Number of vectors in a sequence 
%nex = 1;        %Number of sequences 
[O T nex] = size(data);
if O > T
    error('Transform "data"!');
end
    
M = 1;          %Number of mixtures 
%Q = 6;          %Number of states 
cov_type = 'full';

disp('start...');
%data = randn(O,T,nex);
% initial guess of parameters
%prior0 = normalise(rand(Q,1));
%transmat0 = mk_stochastic(rand(Q,Q));
prior0 = 1/Q*ones(Q,1);
transmat0 = 1/Q*ones(Q,Q);

[mu0, Sigma0] = mixgauss_init(Q*M, data, cov_type);
mu0 = reshape(mu0, [O Q M]);
phiinit=[0 30 120 150 240 270];
for t=1:Q
    [mu0(1,t) mu0(2,t)] = pol2cart((t-1)*2*pi/Q +0*(- 2*pi/Q/2 + 1/180*pi), 3.8);
    %[mu0(1,t) mu0(2,t)] = pol2cart(phiinit(t)/180*pi, 0.5);
end

Sigma0 = [];
for t=1:Q, Sigma0 = [Sigma0; eye(2)]; end
%Sigma0 = [1 0 1 0 1 0; 0 1 0 1 0 1];  %kein Auto-Guess, sondern reproduzierbares Lernen
Sigma0 = reshape(Sigma0', [O O Q M]);
mixmat0 = mk_stochastic(rand(Q,M));

mixmat0 = ones(Q,M);

prior1 = prior0;
transmat1 = transmat0;
mu1 = mu0;
Sigma1 = Sigma0;
mixmat1 = mixmat0;
LL=[];
trajLL=[];
iter = 0;
databin = hist3(data', {-cSize/2:cSize/cHist:cSize/2 -cSize/2:cSize/cHist:cSize/2})';

set(gcf,'Units','pixels');
set(gcf,'Position',[10 500 700 250]);
set(gcf,'PaperPositionMode','auto');    % <-- Ohne diese Anweisung druckt der SaveAs-Befehl nur Mist!

%aviobj=avifile('HMMxy','compression','None','fps',2,'quality',75);

while (isempty(LL))|(length(LL)==IterUpdate)

    [LL, prior1, transmat1, mu1, Sigma1, mixmat1] = ...
        mhmm_em(data, prior1, transmat1, mu1, Sigma1, mixmat1, 'max_iter', IterUpdate, 'thresh', Threshold);
    trajLL=[trajLL, LL];
    iter = iter+length(LL);
    disp(['total number of iterations: ',int2str(iter)]);

    subplot(1,2,1);
        hold off;
        %show picture
        contourf(-cSize/2:cSize/cHist:cSize/2, -cSize/2:cSize/cHist:cSize/2, databin, 50, 'LineStyle','none');
        set(gca, 'PlotBoxAspectRatio',[1 1 1]);
        xlabel('size/px');
        ylabel('size/px');
        colorbar;
        hold on;
        
%        plot(mu1(1,:),mu1(2,:),'o',...
%            'MarkerEdgeColor','b',...
%            'MarkerFaceColor','w',...
%            'MarkerSize',12);
        mu1var=[];
        for i=1:O, [mu1var] = [mu1var(:)'; sqrt(squeeze(Sigma1(i,i,:)))'/2]; end 
        %for i=1:O, annotation(gcf,'ellipse',[mu1(1,i) mu1(2,i) mu1var(1,i) mu1var(2,i)]/cSize+0.5); end
            
        plot(mu0(1,:),mu0(2,:),'o',...
            'MarkerEdgeColor','w',...
            'MarkerSize',12);
        phi0=0:pi/16:2*pi;
        for i=1:Q
            patch(mu1(1,i)+mu1var(1,i)*cos(phi0),mu1(2,i)+mu1var(1,i)*sin(phi0),'w','FaceAlpha',0.6);
            text(mu1(1,i),mu1(2,i),int2str(i),'HorizontalAlignment','center','color','b');
            text(mu0(1,i),mu0(2,i),int2str(i),'HorizontalAlignment','center','color','w');
        end
        if bSollpos, plot(sollpos(1,:),sollpos(2,:),'+','color','k'); end;
        
        hold off;
        
    subplot(1,2,2);
        loglik = mhmm_logprob(data, prior1, transmat1, mu1, Sigma1, mixmat1);
        disp(loglik);
        B = mixgauss_prob(data, mu1, Sigma1, mixmat1);
        path = viterbi_path(prior1, transmat1, B);
        hmm_phi=mod((atan(mu1(2,:)./mu1(1,:))/pi*180 + 90*(1-sign(mu1(1,:))) + 180*(1-sign(mu1(2,:)))),360);
        hmm_r=sqrt(mu1(1,:).*mu1(1,:)+mu1(2,:).*mu1(2,:));
        xx=xlim; yy=ylim;

        %Analyse_Trackfiles(data, traj, phi);
        Analyse_Trackfiles;
        set(gca, 'PlotBoxAspectRatio',[1 1 1]);
        
        text(2/15*xx(2),yy(2)-2*yy(2)/20,sprintf('%d iterations',length(trajLL)),'HorizontalAlignment','left','color','k','FontSize',9);
        text(2/15*xx(2),yy(2)-4*yy(2)/20,sprintf('%.4f LL',trajLL(end)),'HorizontalAlignment','left','color','k','FontSize',9);
        if bSollpos
            text(11/15*xx(2),-2*yy(2)/20,num2str(r0,'%.2f'),'HorizontalAlignment','right','color',[0 0.5 0],'FontSize',9);
        end
        text(14/15*xx(2),-2*yy(2)/20,num2str(mean(hmm_r),'%.2f'),'HorizontalAlignment','right','color','r','FontSize',9);
        if bSollpos
            for i=1:length(phi)
                text(11/15*xx(2),(9-1.5*i)*yy(2)/20,[num2str(phi(i),'%.0f'),'�'],'HorizontalAlignment','right','color',[0 0.5 0],'FontSize',9);
            end
        end
        for i=1:Q
            text(14/15*xx(2),(9-1.5*i)*yy(2)/20,[num2str(hmm_phi(i),'%.0f'),'�'],'HorizontalAlignment','right','color','r','FontSize',9);
        end
    
        %title(filename);
        filename=['iteration',sprintf('%03d',length(trajLL))];
        drawnow;
    
    if bSave, saveas(gcf,filename,'png'); end
%{
    subplot(2,3,4:6);
        plot(trajLL,'m','LineWidth',3);
        
        ylabel('log Liklehood');
        xlabel('iterations');
        xlim([1 62]);
        ylim([-5000 -2000]);
%}
    %end
    
    if IterMax>0
        if iter >= IterMax break; end
    end
    
end
    %aviobj=close(aviobj);

    loglik = mhmm_logprob(data, prior1, transmat1, mu1, Sigma1, mixmat1);
    if exist('LLall'), LLall = [LLall; loglik]; end
    disp(loglik);
    B = mixgauss_prob(data, mu1, Sigma1, mixmat1);
    path = viterbi_path(prior1, transmat1, B);

    mu1var=[];
    for i=1:O
        [mu1var] = [mu1var(:)'; squeeze(Sigma1(i,i,:))'];
    end
    
    mu1
    mu1var
    transmat1
    
    hmm_phi
    hmm_r
    
    dlmwrite('results.txt', 'mu1', 'newline', 'pc', '-append');
    dlmwrite('results.txt', mu1, 'newline', 'pc', '-append');
    dlmwrite('results.txt', 'mu1var', 'newline', 'pc', '-append');
    dlmwrite('results.txt', mu1var, 'newline', 'pc', '-append');
    dlmwrite('results.txt', 'transmat1', 'newline', 'pc', '-append');
    dlmwrite('results.txt', transmat1, 'newline', 'pc', '-append');
    dlmwrite('results.txt', 'hmm_phi', 'newline', 'pc', '-append');
    dlmwrite('results.txt', hmm_phi, 'newline', 'pc', '-append');
    dlmwrite('results.txt', 'hmm_r', 'newline', 'pc', '-append');
    dlmwrite('results.txt', hmm_r, 'newline', 'pc', '-append');
    dlmwrite('results.txt', 'LL', 'newline', 'pc', '-append');
    dlmwrite('results.txt', trajLL, 'newline', 'pc', '-append');
    dlmwrite('results.txt', 'last LL', 'newline', 'pc', '-append');
    dlmwrite('results.txt', loglik, 'newline', 'pc', '-append');
