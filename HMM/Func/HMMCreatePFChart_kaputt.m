function HMMCreatePFChart(data, bCalcPF, r0)
%This function creates Charts from a BAP-File
%The Chart shows correlations of Proximity Factors of adjacent Stufen

%
%sSeqFrw = '123';
%sSeqRev = '321';   
%{
sSeqFrwTriples36 = strvcat('112','123','234','345','455','554','543','432','321','211');
sSeqRevTriples36 = strvcat('121','232','343','454','545','434','323','212','111','555');
sSeqFrwTriples72 = strvcat('135','245','354','453','542','531','421','312','213','124');
sSeqRevTriples72 = strvcat('131','242','353','454','545','535','424','313','212','121');
sSeqFrwTriples108 = strvcat('144','135','253','352','441','531','522','413','314','225');
sSeqRevTriples108 = strvcat('141','252','353','444','535','525','414','313','222','131');
sSeqFrwTriples144 = strvcat('152','251','341','432','523','514','415','325','234','143');
sSeqRevTriples144 = strvcat('151','252','343','434','525','515','414','323','232','141');
sSeqFrwTriples3672 = strvcat('112','123','234','345','455','554','543','432','321','211',...
                             '113','134','245','355','454','532','421','311','212','124','235','542');
sSeqRevTriples3672 = strvcat('121','232','343','545','434','323','111','555','132','243',...
                             '132','243','354','534','423','312');
sSeqFrwTriples367272 = strvcat('112','123','234','345','455','554','543','432','321','211',...
                               '113','134','245','355','454','532','421','311','212','124','235','542',...
                               '135','354','453','531','421','312','213','124');
sSeqRevTriples367272 = strvcat('121','232','343','545','434','323','111','555','132','243',...
                               '534','423',...
                               '131','242','353','535','424','313');
sSeqFrwTriples108144 = strvcat('144','135','253','352','441','531','522','413','314','225',...
                               '152','251','341','432','523','514','415','325','234','143');
sSeqRevTriples108144 = strvcat('141','252','353','444','535','525','414','313','222','131',...
                               '151','343','434','525','515','323','232');

sSeqFrwTriplesAll = strvcat('112','123','234','345','455','554','543','432','321','211',...
                            '113','134','245','355','454','532','421','311','212','124','235','542',...
                            '135','354','453','531','421','312','213','124',...
                            '144','135','253','352','441','531','522','413','314','225',...
                            '152','251','341','432','523','514','415','325','234','143');
sSeqRevTriplesAll = strvcat('121','232','343','545','434','323','111','555','132','243',...
                            '534','423',...
                            '131','242','353','535','424','313',...
                            '141','252','353','444','535','525','414','313','222','333',...
                            '151','343','434','525','515','323','232');
%}
% for 3 level analysis: LMH = 234 , HML = 432
sSeqFrwTriplesLMH = strvcat('123','231','312');
sSeqRevTriplesHML = strvcat('321','213','132');
%
bShowGrid = true;

sSeqFrwTriples = sSeqFrwTriplesLMH;
sSeqRevTriples = sSeqRevTriplesHML;

%{
cFRET_minFrw(1) = 0.00;   % 0.10 0.06 
cFRET_maxFrw(1) = 0.05;   % 0.21 0.12
cFRET_minFrw(2) = 0.05;   % 0.21 0.12
cFRET_maxFrw(2) = 0.40;   % 0.33 0.20
cFRET_minFrw(3) = 0.40;   % 0.33 0.20
cFRET_maxFrw(3) = 0.70;   % 0.52 0.33
cFRET_minFrw(4) = 0.70;   % 0.52 0.33
cFRET_maxFrw(4) = 0.99;   % 0.71 0.58
cFRET_minFrw(5) = 0.99;   % 0.71 0.58
cFRET_maxFrw(5) = 1.00;   % 0.99 0.85
%}
%{
sSeqFrw = 'LMH';    %START ALWAYS WITH "L"!
sSeqRev = 'LHM';    %START ALWAYS WITH "L"!
cFRET_minFrw(1) = 0.10;    %Low FRET
cFRET_maxFrw(1) = 0.21;    
cFRET_minFrw(2) = 0.52;    %Medium FRET
cFRET_maxFrw(2) = 0.71;    
cFRET_minFrw(3) = 0.71;    %High FRET
cFRET_maxFrw(3) = 0.99;
%}
cMinIntens = 10;
cMaxIntens = +inf;
cMinPeak = 0;
cMaxPeak = 300;
bShowContour = true;

%color defnition
colTripleFrw = [0 0 1];
colTripleRev = [1 0 0];
colTripleOther = [0 0.5 0];
colDoubleFrw = [0 0 1];
colDoubleRev = [1 0 0];
colDoubleOther = [0 0.5 0];
colTripleErrIntens = colTripleFrw;
colTripleErrProxFact = colTripleFrw;
colTripleErrRepLevel = colTripleFrw;
colTripleErrBackLevel = colTripleFrw;

%for Rev-Triples
cFRET_minRev = cFRET_minFrw;
cFRET_maxRev = cFRET_maxFrw;
for i=1:length(sSeqRev)
    j=strfind(sSeqFrw,sSeqRev(i));
    cFRET_minRev(i) = cFRET_minFrw(j);
    cFRET_maxRev(i) = cFRET_maxFrw(j);
end

DoubleArr = data{1};
TripleArr = data{2};
bFileOk = true;

if bFileOk
    %Triples
    TripleArrFrw = [];
    TripleArrRev = [];
%    TripleArrOther = [];
    TripleArrErrIntens = [];
    TripleArrErrProxFact = [];
    TripleArrErrRepLevel = [];
    TripleArrErrBackLevel = [];
    for iMain = 1:size(TripleArr,1)
        bItemOk = true;

        %if bCalcPF 
            aTripleIntens = [TripleArr(iMain,1).SumIntens; TripleArr(iMain,2).SumIntens; TripleArr(iMain,3).SumIntens];
            aTriplePeak = [TripleArr(iMain,1).SumPeaks; TripleArr(iMain,2).SumPeaks; TripleArr(iMain,3).SumPeaks];
            if bItemOk
                [bItemOk] = CheckRange(aTripleIntens, cMinIntens*ones(3,1), cMaxIntens*ones(3,1));
                if ~bItemOk 
                    TripleArrErrIntens = [TripleArrErrIntens; TripleArr(iMain,:)]; 
                else
                    [bItemOk] = CheckRange(aTriplePeak, cMinPeak*ones(3,1), cMaxPeak*ones(3,1));
                    if ~bItemOk 
                        TripleArrErrIntens = [TripleArrErrIntens; TripleArr(iMain,:)]; 
                    end;            
                end
            end
        %end
        
%{       
        %collect all Triples as Forward and skip all proceeding analaysis
        if (TripleArr(iMain,1).r~=inf)&(TripleArr(iMain,2).r~=inf)&(TripleArr(iMain,3).r~=inf)
            TripleArrFrw = [TripleArrFrw; TripleArr(iMain,:)];
        end
%}
%   
        if bItemOk
            aTriplePF = [TripleArr(iMain,1).ProxFact; TripleArr(iMain,2).ProxFact; TripleArr(iMain,3).ProxFact];
            
            seq = EstimateSequence(aTriplePF, cFRET_minFrw, cFRET_maxFrw, sSeqFrw);
            if strfind(seq,'-')
                bItemOk = false;
                TripleArrErrProxFact = [TripleArrErrProxFact; TripleArr(iMain,:)];
            end
            if bItemOk
                %seqperm = GenStrPerm(seq,sSeqFrw);
                seqperm = seq;
                if ~isempty(strmatch(seqperm, sSeqFrwTriples))
                    TripleArrFrw = [TripleArrFrw; TripleArr(iMain,:)];
                    bItemOk = false;
                end
                if ~isempty(strmatch(seqperm, sSeqRevTriples))
                    TripleArrRev = [TripleArrRev; TripleArr(iMain,:)];
                    bItemOk = false;
                end
            end
            if bItemOk
                if ~isempty(strmatch(seq,strvcat('LL','MM','HH')))
                    TripleArrErrRepLevel = [TripleArrErrRepLevel; TripleArr(iMain,:)];
                else
                    TripleArrErrBackLevel = [TripleArrErrBackLevel; TripleArr(iMain,:)];
                end
            end
        end
    end
%
    fprintf(1,'--------------------------------------------\n',[]);
    fprintf(1,'%5d Triples in BAP-File found.\n',size(TripleArr,1));
    fprintf(1,'%5d %s-Triples found.\n',[size(TripleArrFrw,1),sSeqFrw]);
    fprintf(1,'%5d %s-Triples found.\n',[size(TripleArrRev,1),sSeqRev]);
    fprintf(1,'%5d Triples skipped due to intensity check.\n',[size(TripleArrErrIntens,1)]);
    fprintf(1,'%5d Triples skipped due to prox factor range check.\n',[size(TripleArrErrProxFact,1)]);
    fprintf(1,'%5d Triples skipped due adjacent equal levels occoured.\n',[size(TripleArrErrRepLevel,1)]);
    fprintf(1,'%5d Triples skipped due backward steps occoured.\n',[size(TripleArrErrBackLevel,1)]);
    
    %Doubles
    DoubleArrFrw = [];
    DoubleArrRev = [];
    DoubleArrOther = [];
    for iMain = 1:size(DoubleArr,1)
        bItemOk = true;
        
        if bCalcPF
            aDoubleIntens = [DoubleArr(iMain,1).SumIntens; DoubleArr(iMain,2).SumIntens];
            if bItemOk
                [bItemOk] = CheckRange(aDoubleIntens, cMinIntens*ones(2,1), cMaxIntens*ones(2,1));
                if ~bItemOk 
                    DoubleArrOther = [DoubleArrOther; DoubleArr(iMain,:)]; 
                else
                    [bItemOk] = CheckRange(aDoublePeak, cMinPeak*ones(2,1), cMaxPeak*ones(2,1));
                    if ~bItemOk 
                        DoubleArrOther = [DoubleArrOther; DoubleArr(iMain,:)]; 
                    end
                end;            
            end
        end
        
        if bItemOk
            aDoublePF = [DoubleArr(iMain,1).ProxFact; DoubleArr(iMain,2).ProxFact];
            [DoubleSeq] = GetStufenClass(aDoublePF, cFRET_minFrw, cFRET_maxFrw, sSeqFrw);
            bItemOk = ~isempty( strfind([sSeqFrw, sSeqFrw],DoubleSeq) );
            if bItemOk
                DoubleArrFrw = [DoubleArrFrw; DoubleArr(iMain,:)];
            else
                bItemOk = ~isempty( strfind([sSeqRev, sSeqRev],DoubleSeq) );
                if bItemOk
                    DoubleArrRev = [DoubleArrRev; DoubleArr(iMain,:)];
                else
                    DoubleArrOther = [DoubleArrOther; DoubleArr(iMain,:)];
                end
            end
        end
    end
    fprintf(1,'--------------------------------------------\n',[]);
    fprintf(1,'%5d Doubles in BAP-File found.\n',size(DoubleArr,1));
    fprintf(1,'%5d %s-Doubles found.\n',[size(DoubleArrFrw,1),sSeqFrw]);
    fprintf(1,'%5d %s-Doubles found.\n',[size(DoubleArrRev,1),sSeqRev]);
    fprintf(1,'%5d other Doubles found.\n',size(DoubleArrOther,1));    
    fprintf(1,'--------------------------------------------\n',[]);
    
    %Create figure
    figure1 = figure('PaperPosition',[0.6345 6.345 20.3 15.23],'PaperSize',[20.98 29.68]);

    %% Create axes
    axis(gca,[0 1 0 1]);
    set(gca, 'XTick', 0:0.2:1);
    set(gca, 'YTick', 0:0.2:1);
%    set(gca, 'XTick', 0:0.2:1);
%    set(gca, 'YTick', 0:0.2:1);
    set(gca, 'FontSize', 14);
    xlabel(gca,'E_F_R_E_T level 1');
    ylabel(gca,'E_F_R_E_T level 2');
    grid(gca,'on');
    hold(gca,'all');
    %{
    if ~isempty(TripleArrOther)
        plot([TripleArrOther(:,1).ProxFact]', [TripleArrOther(:,2).ProxFact]', 'o', ...
        'MarkerEdgeColor',colTripleOther,...
        'MarkerFaceColor',colTripleOther,...
        'MarkerSize',4,...
        'DisplayName', 'Triple other');
    end
    %}
    if ~isempty(DoubleArrOther)
        plot([DoubleArrOther(:,1).Efret]', [DoubleArrOther(:,2).Efret]', 's', ...
        'MarkerEdgeColor',colDoubleOther,...
        'MarkerFaceColor',colDoubleOther,...
        'MarkerSize',4,...
        'DisplayName', 'Double other');
    end
    if ~isempty(TripleArrFrw)
        plot([TripleArrFrw(:,1).Efret]', [TripleArrFrw(:,2).Efret]', 'o', ...
        'MarkerEdgeColor',colTripleFrw,...
        'MarkerFaceColor',colTripleFrw,...
        'MarkerSize',8,...
        'DisplayName', ['Triple ',sSeqFrw]);
    end
    if ~isempty(TripleArrRev)
        plot([TripleArrRev(:,1).Efret]', [TripleArrRev(:,2).Efret]', 'o', ...
        'MarkerEdgeColor',colTripleRev,...
        'MarkerSize',8,...
        'DisplayName', ['Triple ',sSeqRev]);
    end
    if ~isempty(DoubleArrFrw)
        plot([DoubleArrFrw(:,1).Efret]', [DoubleArrFrw(:,2).Efret]', 's', ...
        'MarkerEdgeColor',colDoubleFrw,...
        'MarkerFaceColor',colDoubleFrw,...
        'MarkerSize',8,...
        'DisplayName', ['Double ',sSeqFrw]);
    end
    if ~isempty(DoubleArrRev)
        plot([DoubleArrRev(:,1).Efret]', [DoubleArrRev(:,2).Efret]', 's', ...
        'MarkerEdgeColor',colDoubleRev,...
        'MarkerSize',8,...
        'DisplayName', ['Double ',sSeqRev]);
    end
    
    if ~isempty(TripleArrErrIntens)
        plot([TripleArrErrIntens(:,1).Efret]', [TripleArrErrIntens(:,2).Efret]', 'o', ...
        'MarkerEdgeColor',colTripleErrIntens,...
        'MarkerSize',4,...
        'DisplayName', ['Triple Err Intens']);
    end
    if ~isempty(TripleArrErrProxFact)
        plot([TripleArrErrProxFact(:,1).Efret]', [TripleArrErrProxFact(:,2).Efret]', 'o', ...
        'MarkerEdgeColor',colTripleErrProxFact,...
        'MarkerSize',4,...
        'DisplayName', ['Triple Err Efret']);
    end
    if ~isempty(TripleArrErrRepLevel)
        plot([TripleArrErrRepLevel(:,1).Efret]', [TripleArrErrRepLevel(:,2).Efret]', 'o', ...
        'MarkerEdgeColor',colTripleErrRepLevel,...
        'MarkerSize',4,...
        'DisplayName', ['Triple Err RepLevel']);
    end
    if ~isempty(TripleArrErrBackLevel)
        plot([TripleArrErrBackLevel(:,1).Efret]', [TripleArrErrBackLevel(:,2).Efret]', 'o', ...
        'MarkerEdgeColor',colTripleErrBackLevel,...
        'MarkerSize',4,...
        'DisplayName', ['Triple Err BackLevel']);
    end
    set(gca, 'PlotBoxAspectRatio', [1 1 1]);

    [pathstr,name,ext,versn] = fileparts(filenameBAP);
    if ~isempty(pathstr) pathstr=[pathstr,'\']; end
    filenameFIG = [pathstr,name];
    saveas(figure1, filenameFIG, 'fig');
    %saveas(figure1, filenameFIG, 'png');

    if bShowContour
        
        hContour = figure;
        hDwelltime = figure;
        
        %Frw
        fprintf(1,'  preparing contour plot 1/4...');
        ContourArr = [];
        for i=1:size(TripleArrFrw,1)
            if (TripleArrFrw(i,1).r~=inf)&(TripleArrFrw(i,2).r~=inf)
                ContourArr = [ContourArr; TripleArrFrw(i,1:2)];
            end
            if TripleArrFrw(i,3).iPos==TripleArrFrw(i,3).Anzahl
                if (TripleArrFrw(i,2).r~=inf)&(TripleArrFrw(i,3).r~=inf)
                    ContourArr = [ContourArr; TripleArrFrw(i,2:3)];
                end
            end
        end
        if length(ContourArr)>0
            set(0,'CurrentFigure',hContour);
            subplot(2,2,1);
            ShowContourPlot([ContourArr(:,1).r]', [ContourArr(:,2).r]', CalcRFromEfret(cFRET_minFrw, r0), CalcRFromEfret(cFRET_maxFrw, r0), bShowGrid);
            ShowSollPositionen(2.5, 3.2, 4.2, 36, 20, 10);
            %ShowSollPositionen(2.5, 3.2, 4.2, 72, 20, 10);
            %ShowSollPositionen(2.5, 3.2, 4.2, 108, 20, 10);
            %ShowSollPositionen(2.5, 3.2, 4.2, 144, 20, 10);
            title([sSeqFrw, ' #', int2str(size(ContourArr,1))]);
            
            set(0,'CurrentFigure',hDwelltime);
            subplot(2,2,1);
            dwell = [TripleArrFrw(:,3).Start]'-[TripleArrFrw(:,2).Start]';
            Fit_HistExp1(dwell,1:1:100,6,false);
            title([sSeqFrw, ' #', int2str(size(TripleArrFrw,1))]);
        end
        
        fprintf(1,'ok\n');

        %Rev
        fprintf(1,'  preparing contour plot 2/4...');
        ContourArr = [];
        for i=1:size(TripleArrRev,1)
            if (TripleArrRev(i,1).r~=inf)&(TripleArrRev(i,2).r~=inf)
                ContourArr = [ContourArr; TripleArrRev(i,1:2)];
            end
            if TripleArrRev(i,3).iPos==TripleArrRev(i,3).Anzahl
                if (TripleArrRev(i,2).r~=inf)&(TripleArrRev(i,3).r~=inf)
                    ContourArr = [ContourArr; TripleArrRev(i,2:3)];
                end
            end
        end
        if length(ContourArr)>0
            set(0,'CurrentFigure',hContour);
            subplot(2,2,2);
            ShowContourPlot([ContourArr(:,1).r]', [ContourArr(:,2).r]', CalcRFromEfret(cFRET_minFrw, r0), CalcRFromEfret(cFRET_maxFrw, r0), bShowGrid);
            ShowSollPositionen(2.5, 3.2, 4.2, 36, 20, 10);
            %ShowSollPositionen(2.5, 3.2, 4.2, 72, 20, 10);
            %ShowSollPositionen(2.5, 3.2, 4.2, 108, 20, 10);
            %ShowSollPositionen(2.5, 3.2, 4.2, 144, 20, 10);
            title([sSeqRev, ' #', int2str(size(ContourArr,1))]);
            
            set(0,'CurrentFigure',hDwelltime);
            subplot(2,2,2);
            dwell = [TripleArrRev(:,3).Start]'-[TripleArrRev(:,2).Start]';
            Fit_HistExp1(dwell,1:1:100,6,false);
            title([sSeqRev, ' #', int2str(size(TripleArrRev,1))]);
        end
        fprintf(1,'ok\n');

        %Err
        fprintf(1,'  preparing contour plot 3/4...');
        ContourArr = [];
        TripleArrErr = [TripleArrErrIntens; TripleArrErrProxFact; TripleArrErrRepLevel; TripleArrErrBackLevel];
        for i=1:size(TripleArrErr,1)
            if (TripleArrErr(i,1).r~=inf)&(TripleArrErr(i,2).r~=inf)
                ContourArr = [ContourArr; TripleArrErr(i,1:2)];
            end
            if TripleArrErr(i,3).iPos==TripleArrErr(i,3).Anzahl
                if (TripleArrErr(i,2).r~=inf)&(TripleArrErr(i,3).r~=inf)
                    ContourArr = [ContourArr; TripleArrErr(i,2:3)];
                end
            end
        end
        if length(ContourArr)>0
            set(0,'CurrentFigure',hContour);
            subplot(2,2,3);
            ShowContourPlot([ContourArr(:,1).r]', [ContourArr(:,2).r]', CalcRFromEfret(cFRET_minFrw, r0), CalcRFromEfret(cFRET_maxFrw, r0), bShowGrid);
            ShowSollPositionen(2.5, 3.2, 4.2, 36, 20, 10);
            %ShowSollPositionen(2.5, 3.2, 4.2, 72, 20, 10);
            %ShowSollPositionen(2.5, 3.2, 4.2, 108, 20, 10);
            %ShowSollPositionen(2.5, 3.2, 4.2, 144, 20, 10);
            title(['all Errors', ' #', int2str(size(ContourArr,1))]);
            
            set(0,'CurrentFigure',hDwelltime);
            subplot(2,2,3);
            dwell = [TripleArrErr(:,3).Start]'-[TripleArrErr(:,2).Start]';
            Fit_HistExp1(dwell,1:1:100,6,false);
            title(['all Errors', ' #', int2str(size(TripleArrErr,1))]);
        end
        fprintf(1,'ok\n');

        %All
        fprintf(1,'  preparing contour plot 4/4...');
        ContourArr = [];
        for i=1:size(TripleArr,1)
            if (TripleArr(i,1).r~=inf)&(TripleArr(i,2).r~=inf)
                ContourArr = [ContourArr; TripleArr(i,1:2)];
            end
            if TripleArr(i,3).iPos==TripleArr(i,3).Anzahl
                if (TripleArr(i,2).r~=inf)&(TripleArr(i,3).r~=inf)
                    ContourArr = [ContourArr; TripleArr(i,2:3)];
                end
            end
        end
        if length(ContourArr)>0
            set(0,'CurrentFigure',hContour);
            subplot(2,2,4);
            ShowContourPlot([ContourArr(:,1).r]', [ContourArr(:,2).r]', CalcRFromEfret(cFRET_minFrw, r0), CalcRFromEfret(cFRET_maxFrw, r0), bShowGrid);
            ShowSollPositionen(2.5, 3.2, 4.2, 36, 20, 10);
            ShowSollPositionen(2.5, 3.2, 4.2, 72, 20, 10);
            ShowSollPositionen(2.5, 3.2, 4.2, 108, 20, 10);
            ShowSollPositionen(2.5, 3.2, 4.2, 144, 20, 10);
            title(['All Triples', ' #', int2str(size(ContourArr,1))]);
            
            set(0,'CurrentFigure',hDwelltime);
            subplot(2,2,4);
            dwell = [TripleArr(:,3).Start]'-[TripleArr(:,2).Start]';
            Fit_HistExp1(dwell,1:1:100,6,false);
            title(['All Triples', ' #', int2str(size(TripleArr,1))]);
        end
        fprintf(1,'ok\n');
%{        
        for i=1:size(DoubleArrFrw,1)
            if (DoubleArrFrw(i,1).r~=inf)&(DoubleArrFrw(i,2).r~=inf)&(DoubleArrFrw(i,1).Anzahl>=3)
                ContourArr = [ContourArr; DoubleArrFrw(i,1:2)];
            end
        end
        for i=1:size(DoubleArrRev,1)
            if (DoubleArrRev(i,1).r~=inf)&(DoubleArrRev(i,2).r~=inf)&(DoubleArrRev(i,1).Anzahl>=3)
                ContourArr = [ContourArr; DoubleArrRev(i,1:2)];
            end
        end
        for i=1:size(DoubleArrOther,1)
            if (DoubleArrOther(i,1).r~=inf)&(DoubleArrOther(i,2).r~=inf)&(DoubleArrOther(i,1).Anzahl>=3)
                ContourArr = [ContourArr; DoubleArrOther(i,1:2)];
            end
        end
        
        ShowContourPlot([ContourArr(:,1).r]', [ContourArr(:,2).r]', CalcRFromEfret(cFRET_minFrw, r0), CalcRFromEfret(cFRET_maxFrw, r0), bShowGrid);
        ShowSollPositionen(2.5, 3, 4, 120, -20, 3);
%}
    end
    
    fprintf(1,'--------------------------------------------\n',[]);
    fprintf(1,'Figure created and saved.\n',[]);
    fprintf(1,'--------------------------------------------\n',[]);

else
    
    disp(['File "', filenameBAP, '" not found. Program terminated.']);

end

end

%--------------------------------------------------------------------------

function ShowContourPlot(x,y, ArrAreasMin, ArrAreasMax, bShowGrid);

binmin = 3;
binwidth = 0.25;
binmax = 9;

xrange = [3 9];
yrange = xrange;

z=[x,y];
h=hist3(z,{binmin:binwidth:binmax binmin:binwidth:binmax})';
contourf(binmin:binwidth:binmax, binmin:binwidth:binmax, h, 100, 'LineStyle','none');
xlim([binmin binmax]); ylim([binmin binmax]);
set(gca, 'PlotBoxAspectRatio', [1 1 1]);
xlabel(gca,'FRET distance 1 / nm');
ylabel(gca,'FRET distance 2 / nm');
colorbar;
xlim(xrange); ylim(yrange);

if bShowGrid
    hold on;
    ShowGrid(xrange, yrange, [ArrAreasMin(:); ArrAreasMax(:)], [ArrAreasMin(:); ArrAreasMax(:)], [1 1 1]);
    hold off;
end

end
