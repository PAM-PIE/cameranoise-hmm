function PlotHMMStufenSeq2D(AllStufen,Q,dataInt)
%Erzeugt einen 2D-Contour-Plot der Stufenabfolgen

[StufenArr] = StufenHMM2StufenArr(AllStufen,dataInt,5,1);
DoubleArr=StufenFilter(StufenArr,'2+',true);

if 0
    data=[[DoubleArr(:,1).PFclass]',[DoubleArr(:,2).PFclass]'];
    hist3(data,{1:Q,1:Q},'FaceAlpha',.65);
    xlabel('Stufe 1'); ylabel('Stufe 2');
    set(gcf,'renderer','opengl');
    view(-20,70);
else
    data=[[DoubleArr(:,1).ProxFact]',[DoubleArr(:,2).ProxFact]'];
    xbin=0:0.02:1;
    datahist=hist3(data,{xbin, xbin})';
    contourf(xbin,xbin,datahist,100,'LineStyle','none');
end

end
