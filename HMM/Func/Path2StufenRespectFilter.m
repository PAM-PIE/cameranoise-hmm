function [StufenArr] = Path2Stufen(path,Q,MinLen,BurstInt1,BurstInt2,BurstInt3,...
  Inoise1,Inoise2,Inoise3,BurstLL,minLL,minInt1,minInt2,minInt3);

StufenArr = [];
%BurstInt=BurstData(2,:);
%BurstPF=BurstData(1,:);
cMinRelevant=0.5;

if ~exist('minLL'), minLL=0; end
LL=minLL;
dwell=1;
for i=2:length(path)
  if path(i) == path(i-1)
    dwell=dwell+1;
  else
    if sum(path(i-1)==Q)&(dwell>=MinLen)
      %BurstLL=max(1E-15,BurstLL);
      tempLL = BurstLL(i-dwell:i-1);
      tempLL = tempLL(find(tempLL~=0));

      tempInt1 = BurstInt1(i-dwell:i-1);
      tempInt1 = tempInt1(find(tempInt1>=0));
      meanInt1 = sum(tempInt1)/(length(tempInt1)+(length(tempInt1)==0));

      tempInt2 = BurstInt2(i-dwell:i-1);
      tempInt2 = tempInt2(find(tempInt2>=0));
      meanInt2 = sum(tempInt2)/(length(tempInt2)+(length(tempInt2)==0));

      tempInt3 = BurstInt3(i-dwell:i-1);
      tempInt3 = tempInt3(find(tempInt3>=0));
      meanInt3 = sum(tempInt3)/(length(tempInt3)+(length(tempInt3)==0));
      
      len=min([length(tempInt1) length(tempInt2) length(tempInt3)]);      

      %ALT:
      %tempInt3 = ...
      %    sum(BurstInt3(i-dwell:i-1))/...
      %    (length(BurstInt3(i-dwell:i-1))+(length(BurstInt3(i-dwell:i-1))==0));
      if tempLL>0, LL=exp(mean(log(tempLL))); else LL=0; end;
      if (LL>=minLL)&&(meanInt1>=minInt1)&&(meanInt2>=minInt2)&&(meanInt3>=minInt3)&&...
        (length(tempInt1)>MinLen)&&(length(tempInt2)>MinLen)&&(length(tempInt3)>MinLen)&&...
        (len/dwell>cMinRelevant)
        StufenArr(end+1).PFclass = path(i-1);
        StufenArr(end).Start = i-dwell;
        StufenArr(end).End = i-1;
        StufenArr(end).MW = CalcPF(tempInt1,tempInt2,Inoise1,Inoise2);
        StufenArr(end).Iavg1 = meanInt1;
        StufenArr(end).Iavg2 = meanInt2;
        %ALT: sum(BurstPF(i-dwell:i-1).*abs(BurstInt(i-dwell:i-1)))/max(1,sum(abs(BurstInt(i-dwell:i-1))));
        if exist('BurstLL')
          StufenArr(end).LL = LL;
        end
      end
    end
    dwell = 1;
  end
end
%letzte Stufe:
if isempty(i), return; end
i=i+1;
if sum(path(i-1)==Q)&(dwell-1>=MinLen)
  tempLL = BurstLL(i-dwell:i-1);
  tempLL = tempLL(find(tempLL~=0));
  tempInt1 = BurstInt1(i-dwell:i-1);
  tempInt1 = tempInt1(find(tempInt1>=0));
  meanInt1 = sum(tempInt1)/(length(tempInt1)+(length(tempInt1)==0));
  tempInt2 = BurstInt2(i-dwell:i-1);
  tempInt2 = tempInt2(find(tempInt2>=0));
  meanInt2 = sum(tempInt2)/(length(tempInt2)+(length(tempInt2)==0));
  tempInt3 = BurstInt3(i-dwell:i-1);
  tempInt3 = tempInt3(find(tempInt3>=0));
  meanInt3 = sum(tempInt3)/(length(tempInt3)+(length(tempInt3)==0));
  len=min([length(tempInt1) length(tempInt2) length(tempInt3)]);      
  %     tempInt1 = ...
  %         sum(BurstInt1(i-dwell:i-1))/...
  %         (length(BurstInt1(i-dwell:i-1))+(length(BurstInt1(i-dwell:i-1))==0));
  %     tempInt2 = ...
  %         sum(BurstInt2(i-dwell:i-1))/...
  %         (length(BurstInt2(i-dwell:i-1))+(length(BurstInt2(i-dwell:i-1))==0));
  %     tempInt3 = ...
  %         sum(BurstInt3(i-dwell:i-1))/...
  %         (length(BurstInt3(i-dwell:i-1))+(length(BurstInt3(i-dwell:i-1))==0));
  if tempLL>0, LL=exp(mean(log(tempLL))); else LL=0; end;
  if (LL>=minLL)&&(meanInt1>=minInt1)&&(meanInt2>=minInt2)&&(meanInt3>=minInt3)&&...
      (length(tempInt1)>MinLen)&&(length(tempInt2)>MinLen)&&(length(tempInt3)>MinLen)&&...
      (len/dwell>cMinRelevant)
    StufenArr(end+1).PFclass = path(i-1);
    StufenArr(end).Start = i-dwell;
    StufenArr(end).End = i-1;
    StufenArr(end).MW = CalcPF(tempInt1,tempInt2,Inoise1,Inoise2);
    StufenArr(end).Iavg1 = meanInt1;
    StufenArr(end).Iavg2 = meanInt2;
    % Alt: sum(BurstPF(i-dwell:i-1).*abs(BurstInt(i-dwell:i-1)))/sum(abs(BurstInt(i-dwell:i-1)));
    if exist('BurstLL')
      StufenArr(end).LL = LL;
    end
  end
end

%---------------------------------
  function [wert]=CalcPF(Int1,Int2,Inoise1,Inoise2);
    if sum(Int1-Inoise1)+sum(Int2-Inoise2)>0
      wert = sum(Int2-Inoise2)/(sum(Int1-Inoise1)+sum(Int2-Inoise2));
      wert = min(1,max(0,wert));
    else
      wert = NaN;
    end
  end

end