function [Result, mu0, NoStufen] = AnalyzeHMMStufen(AllStufen,AnzKlassen);

for iKlasse=1:AnzKlassen
    Result{iKlasse}.Len=[];
    Result{iKlasse}.MW=[];
end

for iBurst=1:length(AllStufen)
    for iStufe=1:length(AllStufen{iBurst})
        iKlasse = AllStufen{iBurst}(iStufe).PFclass;
        if (iStufe>1)&(iStufe<length(AllStufen{iBurst}))
            Result{iKlasse}.Len(end+1)=AllStufen{iBurst}(iStufe).End - AllStufen{iBurst}(iStufe).Start;
        end
        Result{iKlasse}.MW(end+1)=AllStufen{iBurst}(iStufe).MW;
    end
end

if (exist('NoStufen','var'))|(nargout==0)
    NoStufen=[]; %Number of Stufen per Burst
    for iBurst=1:length(AllStufen)
        NoStufen(end+1)=length(AllStufen{iBurst});
    end
end

mu0=[];
for iKlasse=1:AnzKlassen, mu0(iKlasse)=mean(Result{iKlasse}.MW); end

if nargout == 0
    %{
    %PF
    subplot(AnzKlassen,2,1);
    hist([Result{:}.MW(:)],x);
    y=[];
    for iKlasse = 1:AnzKlassen
        % PF
        %hist(Result{iKlasse}.MW(:),0:0.02:1);
        x=0:1/50:1;
        y{iKlasse}=hist(Result{iKlasse}.MW(:),x);
        yges=
    end
    subplot(AnzKlassen,2,3);
    for iKlasse = 1:AnzKlassen
        Fit_HistGauss(x,y{iKlasse});
    end
    xlim([0 1]);
    ylim([0 1.1*max([y{:}])]);
    title(['#',int2str(length(Result{iKlasse}.MW(:)))]);
    y=[];
    %}
    % dwell
    for iKlasse = 1:AnzKlassen
        
        % PF
        subplot(AnzKlassen,2,2*iKlasse-1);
        %hist(Result{iKlasse}.MW(:),0:0.02:1);
        x=0:1/50:1;
        y=hist(Result{iKlasse}.MW(:),x);
        Fit_HistGauss(x,y);
        xlim([0 1]);
        title(['#',int2str(length(Result{iKlasse}.MW(:)))]);
        
        % Dwelltimes
        subplot(AnzKlassen,2,2*iKlasse);
        %hist(Result{iKlasse}.Len(:),0:501);
        Fit_HistExp1(Result{iKlasse}.Len(:),0:4:201,15,true);
        xlim([0 200]);
        xlabel('');
        title(['#',int2str(length(Result{iKlasse}.Len(:)))]);
    end
    figure;
    x=1:max(NoStufen);
    hist(NoStufen,x);
    %xlim([x(1) x(end)]);
end
