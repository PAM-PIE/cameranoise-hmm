function [datatxt]=ShowBurstWithStufen(dataPF, dataInt, mu1, Sigma1, mixmat1, transmat1, prior1, obsvect, iprobfunc, IDBurst, dataStartMS, TimeBin, cMinLL, Qremain, SimStufen, SimParam);

cShowLL = true;

dataDonor = abs(dataInt).*(1-dataPF);
dataAkzeptor = abs(dataInt).*dataPF;
%data=dataPF;
%dataw=dataInt; %normpdf(dataInt,50,20);
%dataw=dataw./sum(dataw)*length(dataw); 
Q=length(mu1);
switch iprobfunc
    case 0
        B_ = mixgauss_prob(dataPF, mu1, Sigma1);
        B = ExpandB(B_,obsvect);
    case 1
        B = mixgauss_probw(dataPF, dataInt, mu1, Sigma1, mixmat1);
    case 2
        B_ = beta_prob(dataPF, dataInt, mu1, Sigma1, mixmat1);
        B = ExpandB(B_,obsvect);
end
path = viterbi_path(prior1, transmat1, B);
llpath = prob_path(prior1, transmat1, B, path);
path_ = obsvect(path);

if nargout==0 
    Stufen = Path2StufenPF(path, Qremain, 1, dataPF, dataInt, llpath, -Inf);  %Nur Stufen 1..Q-1 auswählen, min. Stufenlänge
    Stufen_ = Path2StufenPF(path_, Qremain, 1, dataPF, dataInt, llpath, -Inf);  %Nur Stufen 1..Q-1 auswählen, min. Stufenlänge
    hold off; clf;
    sLineStyle={}; 
    for i=1:length(Stufen_), 
        if (Stufen_(i).LL>=cMinLL)&(sum(Stufen_(i).PFclass==Qremain)),
            sLineStyle{i}='-'; 
        else 
            sLineStyle{i}=':'; 
        end; 
    end

    ax(1)=subplot(3,1,1);
    %plot(dataPF,'b');
    %plotwithcol([1:length(dataPF)]',dataPF',dataInt',0.7,[0 0 1]);
    set(gca,'LineStyleOrder',{'-'});
    plot(dataPF);
    hold on;
    if numel(Stufen_)>0
        set(gca,'LineStyleOrder',sLineStyle);
        set(gca,'ColorOrder',[1 0.5 0]);
        plot([[Stufen_(:).Start];[Stufen_(:).End]+1],...
            [mu1(([Stufen_(:).PFclass]));mu1(([Stufen_(:).PFclass]))],...
            'LineWidth',2);
    end
    title([sprintf('Burst %d',[IDBurst]),': ',sprintf('%g ms (%d * [%g ms])',[dataStartMS,length(dataInt),TimeBin])]);
    xlim([1 length(dataInt)]);
    ylim([0 1]);
    ylabel('P');
    for i=1:length(Stufen_)
        text((Stufen_(i).Start+Stufen_(i).End+1)/2, 0.9, int2str(Stufen_(i).PFclass),...
            'HorizontalAlignment','center','Color',[1 0.5 0]);
    end
    % SimStufen
    if exist('SimStufen')
        for i=1:length(SimStufen.PFclass)
            text((SimStufen.Start(i)+SimStufen.End(i))/2, 0.1, int2str(SimStufen.PFclass(i)),...
                'HorizontalAlignment','center','Color',[0 0 0]);
        end
        stairs([SimStufen.Start;SimStufen.End(end)],...
            [SimParam.mu1(SimStufen.PFclass),SimParam.mu1(SimStufen.PFclass(end))],...
            'color',[0 0 0],'LineStyle',':');
        %plot([SimStufen.Start';SimStufen.End'],...
        %    [SimParam.mu1(SimStufen.PFclass);SimParam.mu1(SimStufen.PFclass)],...
        %    'color',[1 0 0]);
    end
    
    ax(3)=subplot(3,1,3);
    set(gca,'LineStyleOrder',{'-'});
    if cShowLL
        plot(llpath);
        hold on;
        if numel(Stufen_)>0
            set(gca,'LineStyleOrder',sLineStyle);
            set(gca,'ColorOrder',[1 0.5 0]);
            plot([[Stufen_(:).Start];[Stufen_(:).End]+1],...
                [Stufen_(:).LL;Stufen_(:).LL],...
                'LineWidth',2);
        end
    else
        plot(dataInt);
        hold on;
        if numel(Stufen_)>0
            %dataStufen = max(dataInt) * 0.95 * Stufen2Traj(Stufen);
            %plot([1:length(dataStufen)]+1,dataStufen,'color',[0.5 0.5 0]);
            dataStufenx=reshape([[Stufen_.Start]',[Stufen_.End]'+1]',length(Stufen_)*2,1);
            dataStufeny=repmat([max(dataInt)*0.95;0],length(Stufen_),1);
            stairs(dataStufenx,dataStufeny,'color',[0.5 0.5 0]);
        end
    end
    xlim([1 length(dataInt)]);
    if cShowLL
        ylim([1e-4 1e2]);
        set(gca,'YScale','log');
        ylabel('LL');
    else
        ylim([0 max(1,max(dataInt))]);
        set(gca,'YScale','lin');
        ylabel('total intensity');
    end

    ax(2)=subplot(3,1,2);
    plot(dataDonor+dataAkzeptor,'color',[1 1 1]*0.9);
    hold on;
    plot(dataDonor,'color',[0 0.5 0]);
    plot(dataAkzeptor,'color',[1 0 0]);
    title(sprintf('%d FRET level',[length(Stufen_)]));
    if numel(Stufen_)>0
        %dataStufen = max(dataInt) * 0.95 * Stufen2Traj(Stufen);
        %plot([1:length(dataStufen)]+1,dataStufen,'color',[0.5 0.5 0]);
        dataStufenx=reshape([[Stufen_.Start]',[Stufen_.End]'+1]',length(Stufen_)*2,1);
        dataStufeny=repmat([max(dataInt)*0.95;0],length(Stufen_),1);
        stairs(dataStufenx,dataStufeny,'color',[0.5 0.5 0]);
    end
    xlim([1 length(dataInt)]);
    ylim([0 max(1,max(dataInt))]);
    ylabel('counts / ms');
    %Achsen verlinken
    linkaxes(ax,'x');
    %disp(sprintf('Burst %d von %d',[iBurst,AnzBursts]));
else
    Stufen = Path2Stufen(path, Qremain, 1, dataPF, dataInt, llpath, cMinLL);  %Nur Stufen 1..Q-1 auswählen, min. Stufenlänge
    dataStufen = zeros(1,length(dataDonor));
    for i=1:length(Stufen), dataStufen(Stufen(i).Start:Stufen(i).End)=mu1(Stufen(i).PFclass); end;
    datatxt=[[1:length(dataDonor)]'-1,dataDonor(:),dataAkzeptor(:),dataPF(:),dataStufen(:)];
end
