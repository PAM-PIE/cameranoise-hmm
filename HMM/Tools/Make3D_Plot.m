%open masterfile with all files to fit
function [] = Make3D_Plot(masterfile, diagtitle, pngfile)

if length(dir(masterfile))==0
    errordlg(sprintf('Masterfile not found!\n%s\n\nProgram execution terminated.',masterfile), 'Auto Diff-FIT');
    return;
end;
[masterfpath,masterfname,masterfext,masterfversn] = fileparts(masterfile);
disp(['masterfilename: ',masterfname,masterfext]);
disp(['masterfilepath: ',masterfpath]);

if isempty(pngfile)
    pngfile = [masterfpath, '\', masterfname, '_3D.png'];
end;
    
%load all files to fit
ffiles = fopen(masterfile, 'r');
files = textscan(ffiles, '%s',  'delimiter', '\t');  %Dateinamen mit Leerzeichen funktionieren so
files = files{:,1};
fclose(ffiles);
disp(sprintf('\nstart loading %d files...', length(files)));
newplot;

for ifile=1:length(files)
    %declare filenames and pathes
    [picpath,picname,picext,picversn] = fileparts(files{ifile});
    if strcmp(picpath, '')
        picpath = masterfpath;
    end;
    
    %open datafile
    fid = fopen([picpath, '\', files{ifile}], 'r');
    data = textscan(fid, '%f %f %f', 'headerlines', 1);
    waterx = ifile + zeros(length(data{:,1}),1);
    sourcex = data{:,1};
    sourcey = data{:,2};
    fity = data{:,3};
    fclose(fid);
    if length(sourcey) >= 2
        ymin = sourcex(1,1);
        ymax = sourcex(end,1);
    end;
    plot3(waterx, sourcex, sourcey, 'color', [0 0 1]);
    hold on;
    plot3(waterx, sourcex, fity, 'color', 'red');
end;

set(gca, 'XLim', [1 length(files)]);    %1=min, 2=max
set(gca, 'YLim', [ymin ymax]);    %1=min, 2=max
lim = get(gca, 'ZLim');    %1=min, 2=max
set(gca, 'ZLim', [0 lim(2)]);    %1=min, 2=max
set(gca, 'XDir', 'reverse');
xlabel('time * 10 sec');
ylabel('log10(tau/ms)');
zlabel('ACF1');
view([135 25]);
grid on;
title(diagtitle);
hold off;

%save picture
I = getframe(gcf);
imwrite(I.cdata, pngfile);

disp('3D-diagram exported.');
