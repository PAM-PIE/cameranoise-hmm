function [ziplist]=CollectSourceCode(SourceMFile);

IgnorePath=[matlabroot,'\toolbox\'];
[list]=depfun(SourceMFile,'-quiet');
izip=0;
ziplist={};
for idx=1:length(list)
  if exist(list{idx},'file')==2
    if ~strncmpi(list{idx},IgnorePath,length(IgnorePath))
      izip=izip+1;
      ziplist{izip}=list{idx};
    end
  end
end
ziplist=ziplist';
