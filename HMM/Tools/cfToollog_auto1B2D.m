function [cf, ergy, paramval, paramstr] = cfToollog_auto1B2D(sourcex, sourcey, projfile, picfile)
%CFTOOL_AUTO1D    Create plot of datasets and fits
%   CFTOOL_AUTO1D(sourcex,sourcey)
%   Creates a plot, similar to the plot in the main curve fitting
%   window, using the data that you provide as input.  You can
%   apply this function to the same data you used with cftool
%   or with different data.  You may want to edit the function to
%   customize the code and this help message.

    % --- Create fit "fit 1"
    %fo_ = fitoptions('method','NonlinearLeastSquares','Algorithm','Levenberg-Marquardt','DiffMaxChange',0.1,'MaxFunEvals',10000,'MaxIter',1000,'TolFun',1e-008,'TolX',1e-008);
    fo_ = fitoptions('method','NonlinearLeastSquares','Robust','On','Lower',[0 0 0 0 0 0 0 ],'Upper',[100 1 100 1 1000 1000 1000 ],'DiffMaxChange',1,'MaxFunEvals',2000,'MaxIter',1000);
    st_ = [0.1 0.5 1.0 0.1 0.003 0.3 30 ];
    set(fo_,'Startpoint',st_);
    % see also the command "inline"!
    DiffModel = @(x, AmplBunching, AnteilD1, Nf, omegaZ, tauBunching, tauDiff1, tauDiff2) (1/Nf)*(AnteilD1*((1/(1+x/tauDiff1))*(1/(1+omegaZ*omegaZ*x/tauDiff1))^0.5)+(1-AnteilD1)*((1/(1+x/tauDiff2))*(1/(1+omegaZ*omegaZ*x/tauDiff2))^0.5))*(1-(AmplBunching*(1-exp(-x/tauBunching))));
    ft_ = fittype('(1/Nf)*(AnteilD1*((1/(1+x/tauDiff1))*(1/(1+omegaZ*omegaZ*x/tauDiff1))^0.5)+(1-AnteilD1)*((1/(1+x/tauDiff2))*(1/(1+omegaZ*omegaZ*x/tauDiff2))^0.5))*(1-(AmplBunching*(1-exp(-x/tauBunching))))',...
         'dependent',{'y'},'independent',{'x'},...
         'coefficients',{'AmplBunching', 'AnteilD1', 'Nf', 'omegaZ', 'tauBunching', 'tauDiff1', 'tauDiff2'});

    % Fit this model using new data
    [cf_, gof] = fit(sourcex,sourcey,ft_ ,fo_);

    %{
    % Or use coefficients from the original fit:
    if 0
       cv_ = {5.152225622911, -1.769724738159e-005, 0.6237945221059};
       cf_ = cfit(ft_,cv_{:});
    end
    %}

    % Calc Erg-Array
    ergy = sourcey;
    for i=1:length(sourcex)
        ergy(i) = DiffModel(sourcex(i), cf_.AmplBunching, cf_.AnteilD1, cf_.Nf, cf_.omegaZ, cf_.tauBunching, cf_.tauDiff1, cf_.tauDiff2);
    end

    %return data
    cf = cf_;
    paramval(1) = gof.rmse;
    paramval(2) = cf_.Nf;
    paramval(3) = cf_.AnteilD1;
    paramval(4) = cf_.tauDiff1;
    paramval(5) = cf_.tauDiff2;
    paramval(6) = cf_.omegaZ;
    paramval(7) = cf_.AmplBunching;
    paramval(8) = cf_.tauBunching;
    paramstr{1} = 'rmse';
    paramstr{2} = 'Nf';
    paramstr{3} = 'A1';
    paramstr{4} = 'tau1';
    paramstr{5} = 'tau2';
    paramstr{6} = 'oZ';
    paramstr{7} = 'B0';
    paramstr{8} = 'tB';
