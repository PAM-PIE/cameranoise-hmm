function [result]=dec2num(num,sys);
%function [result]=dec2num(num,sys);
%converts a decimal number to the system "num"

imax=round(log(num)/log(sys)+1);
result=0;
for i=imax:-1:0
  num2=div(num,sys^i);
  result=result+num2*10^i;
  num=num-num2*sys^i;
end

  