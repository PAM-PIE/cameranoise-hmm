function [varargout] = mdpdhist(xd, yd, varargin)
% Syntax:
% f = mdpdhist(x,y)
% [xg, yg, f] = mdpdhist(x,y)
% [xg, yg, f] = mdpdhist(x,y, nx, ny)
% mdpdhist(x,y, nx, ny)
% [...] = mdpdhist(..., param1, value1, param2, value2, ...)
% mdpdhist creates a 2-Dimensional Probability density estimate matrix of a dataset with value-pairs by
% creating a grid over the parameter values and looking at the statistical distribution of values
% in this grid. It's to "hist3" as the usual "ksdensity" is to "hist".
% First input argument ist the value in x-direction, second input argument ist the value in y-direction.
% These 2 inputs are required, all others are optional. The 2 vectors should be of the same length.
% Input 3 & 4 are specify the precision of the grid. If they're not given 10 is the default value.
% If x & y are scalar they specify the number of steps in the grid. If
% they're vectors they specify the points at which the data is binned
% directly.
% All other options should be given in the form:
% 'Option', Value
% availabel Options are:
% 'Method':   Method declares the shape of the local area in which data is evaluated.
%             possible options are 'box' and 'circ'. In 'box' a rectangular area is evaluated
%             in 'circ' the area around the gridpoint is circular. Default method is 'box'.
% 'Dim':      size of the 2D-bin. Must always be followed by 2 numerical entries which represent
%             the size of the window in x and y direction
% 'Weight':   how is the Data weighted. Possible values are:
%             'flat' :    no  weightig (default)
%             'lin'  :    Points directly at the bin-center are weighted with 1, Points at the edge of
%                         the bin with 0. Between that the weigth is linear.
%             'dist' :    Points directly at the bin-center are weighted with 1, Points at the edge of
%                         the bin with 0. Between that the weight is scaled by the squared distance to the
%                         center of the bin.
%             'gauss':    Points directly at the bin-center are weighted with 1, Points at the edge of
%                         the bin with 1/e. In case of a 'Method' 'box' Bin this is only true for the
%                         values directly in the middles of the bin edges.
%             Default is 'flat'
% 'Norm':     how is the data normalized. Possible Values:
%             'global':   normalization for entire grid
%             'x'     :   normalization for each line
%             'y'     :   normalization for each row
%             'none'  :   no normalization (default)
% 'Ntype':    Normalisaton Method. Possible Values:
%             'int'   :   normalization to sum(Norm) = 1        
%             'max'   :   normalization to max(Norm) = 1
%             'Ntype' is ignored if 'Norm' is 'none'
% 
% Output Options:
% The script can be used with either 0, 1 or 3 Output Arguments
% if Nout = 0 a Graph of the result will be plotted using 'pcolor'
% if Nout = 1 the derived probability density estimate matrix is returned
% if Nout = 3 the first two output arguments are vectors giving the points in x and y direction at
%             which the probability density was estimated. The third output argument is the
%             probability density estimate matrix           
%
% Example:  [XG, YG, M] = mdpdhist(V1, V2, 15, 15, 'Method', 'circ', 'Dim', 10, 10, 'Weight', 'gauss', 'Norm', 'y', 'Ntype', 'max')
%           will return 15x15 Matrix M where each point M(i,j) is the number of points of N(V1, V2) in a circle around the
%           points XG(i), YG(j) with diameter 10 weighted by a gaussian and sum(M(:,i))=1 for all i.
%           The Output could by plotted with methods like:
%           'pcolor(XG, YG, M), shading interp;'
%           'surf(XG, YG, M), shading interp;'
%           'contour(XG, YG, M)'
% created by Peter Schwaderer 2007/08/08 2fast4@web.de

%% ---- check if at least x & y data is given and correctly sized & correct
%% ---- number of output arguments is given
error(nargchk(2, 100, nargin));
xs = size(xd);
ys = size(yd);
if ~isvector(xd)
    error('First Dataset not a vector');
elseif ~isvector(yd)
    error('First Dataset not a vector');
end
if length(xd) ~= length(yd)
    error('Data vectors not of equal length');
end
if ~any([0 1 3] == nargout)
    error('invalid number of output arguments specified');
end
%% ---- prepare x & y data
if xs(1) ~= 1
    xd = xd';
end
if ys(1) ~= 1
    yd = yd';
end

xex = [min(xd) max(xd)];
yex = [min(yd) max(yd)];

if xex(1) == xex(2) || yex(1) == yex(2)
    error('At least one of inputvectors is uniform. Use ksdensity');
end

%% ---- set Options to default
% create a cell array listing the different options. Set all options to
% default values.

OP = cell(13,1);
OP(1) = {10};
OP(2) = {10};
OP([3, 5, 8, 10 12]) = {'Method' 'Dim' 'Weight' 'Norm' 'Ntype'};
OP([4, 9, 11 13]) = {'box' 'flat' 'none' 'int'};

%% ---- recognize given grid resolution and options
% Look through the input array and search for recognized options to change in
% the Options array 'OP'

if nargin > 3
    if isnumeric(varargin{1})
        OP(1) = varargin(1);
        if nargin > 4 && isnumeric(varargin{2})
            OP(2) = varargin(2);
        end
    end
    for i = 1:nargin-2
        if ischar(varargin{i})
            switch varargin{i}
                case {'Method'}
                    if strmatch(varargin(i+1), {'box', 'circ'})
                        OP(4) = varargin(i+1);
                    else
                        error('Wrong input for Method, must be either box or circ');
                    end
                case {'Dim'}
                    if isvector(varargin(i+1))
                        OP(6) = {varargin(i+1)};
                        if isvector(varargin(i+2))
                            OP(7) = {varargin(i+2)};
                        else
                            OP(7) = {varargin(i+1)};
                        end
                    else
                        error('Wrong input for Dim, must be either 1x1 or 1x2 vector');
                    end
                case {'Weight'}
                    if strmatch(varargin(i+1), {'flat', 'lin', 'dist', 'gauss'})
                        OP(9) = varargin(i+1);
                    else
                        error('Wrong input for Weight, must be either flat, lin, dist or gauss');
                    end
                case {'Norm'}
                    if strmatch(varargin(i+1), {'global', 'x', 'y', 'none'})
                        OP(11) = varargin(i+1);
                    else
                        error('Wrong input for Norm, must be either global, x, y or none');
                    end
                case {'Ntype'}
                    if strmatch(varargin(i+1), {'int', 'max'})
                        OP(13) = varargin(i+1);
                    else
                        error('Wrong input for Norm, must be either global, x, y or none');
                    end
            end
        end
    end
end

% if option 'Dim' is not defined set to default values
if isempty(OP{6}) || isempty(OP{7})
    if isempty(OP{6})
        OP{6} = num2cell((xex(2)-xex(1))/(OP{1}));
    end
    if isempty(OP{7})
        OP{7} = num2cell((yex(2)-yex(1))/(OP{2}));
    end
end
%% ---- setup of x- & y-vector, resultmatrix step- & window-sizes
% find the coordinates which represent the centers of the bins used to
% evaluate the probability density
if isscalar(OP{1})
    xv = incvec(xex, OP{1});                %--- X-Coords of the bincenters
elseif ~isscalar(OP{1}) && isvector(OP{1})
    xv = OP{1};                             %--- X-Coords of the bincenters
else
    error('first precision is neither scalar nor vector')
end
if isscalar(OP{2})
    yv = incvec(yex, OP{2});                 %--- Y-Coords of the bincenters
elseif ~isscalar(OP{2}) && isvector(OP{2})
    yv = OP{2};                              %--- Y-Coords of the bincenters
else
    error('second precision is neither scalar nor vector')
end

% average distance between bin centers, exact if they're equally scaled
if max(size(OP{1})) == 1
    adx = (xex(2)-xex(1))/OP{1};
else
    adx = (xex(2)-xex(1))/mean(diff(OP{1}));
end    
if max(size(OP{2})) == 1
    ady = (yex(2)-yex(1))/OP{2};
else
    ady = (yex(2)-yex(1))/mean(diff(OP{2}));
end

dx = cell2mat(OP{6});
dy = cell2mat(OP{7});

% check the size of the X-Coords &  Y-Coords vectors. These vallues should
% be the same as OP{1} and OP{2}
nx = length(xv);
ny = length(yv);
% create a temporary result matrix
pmt = zeros(nx, ny);

% look at window sizes. If windows are too small datapoints might be lost
switch strmatch(OP{4}, {'box', 'circ'})
    case {1}
        if cell2mat(OP{6}) <= adx/2 || cell2mat(OP{7}) <= ady/2
            error('specified bin dimensions are too small, datapoints might be lost');
        end
    case {2}
        if cell2mat(OP{6}) < adx/2 || cell2mat(OP{7}) < ady/2
            error('specified bin dimensions are too small, datapoints might be lost');
        end
end

%% ---- Begin creation of 2d-PDM

for i = 1:nx
    xc = xv(i);
    for j = 1:ny
        yc = yv(j);
        %--- Find indices within sliding window
        aoii = dataindi(xd, yd, xc, yc, dx, dy, OP{4});
        %--- attribute statistical weight to aoii
        waoi = weightmat(xd(aoii), yd(aoii), xc, yc, dx, dy, OP{9}, aoii);
        %--- calculate local value of ksdensity
        pmt(j,i) = sum(aoii.*waoi);
    end %--- End of 'j' loop

end %--- End of 'i' loop

%% ---- Normalization of 2d-PDM
if strmatch(OP{11}, 'none');
    rxv = pmt;
else
    rxv = norming(pmt, OP{11}, OP{13});
end
%% ---- Output
if nargout == 0
    pcolor(xv, yv, rxv), shading interp;
elseif nargout == 1
    varargout = rxv;
else
    varargout(1) = {xv};
    varargout(2) = {yv};
    varargout(3) = {rxv};
end
%% ---- Additional Functions
    function [av] = incvec(extr, s)
        % creates a vector between extr(1) and extr(2) with s equalspaced values
        dv = (extr(2)-extr(1))/(s);
        av = (extr(1)+dv/2):dv:extr(2);
    end

    function [indv] = dataindi(XD, YD, XC, YC, DX, DY, Me)
        % find indices of dataset evaluated in current step
        % find all indices in box as first approximation
        indvt = (XC-DX/2 <= XD & XD <= XC+DX/2) & (YC-DY/2 <= YD & YD <= YC+DY/2);
        switch strmatch(Me, {'box', 'circ'})
            case{1}
                indv = indvt;
            case{2} % find indices in circular/elliptical area if 'Method' is 'circ'
                k = length(indvt);
                dmat = zeros(1,k);
                dmat(indvt) = ((XD(indvt)-XC)/(0.5*DX)).^2 + ((YD(indvt)-YC)/(0.5*DY)).^2;
                indv = indvt & dmat <= 1;
        end
    end

    function [wam] = weightmat(XD, YD, XC, YC, DX, DY, Me, aoi)
        % calculate statistical weight of all events inside the sliding window
        wam = zeros(size(aoi));
        switch strmatch(Me, {'flat', 'lin', 'dist', 'gauss'})
            case{1}
                wam = ones(size(aoi));
            case{2}
                wam(aoi) = 1 - (abs(XD-XC)/DX+abs(YD-YC)/DY)/2;
            case{3}
                wam(aoi) = 1 - ((abs(XD-XC)/DX).^2+(abs(YD-YC)/DY).^2)/2;
            case{4}
                wam(aoi) = exp(-((abs(XD-XC)/DX*exp(1)).^2+(abs(YD-YC)/DY*exp(1)).^2)/2);
        end
    end

    function [rma] = norming(trm, nm, nt)
        % calculate the normalisation of the pdm
        rma = zeros(size(trm));
        switch strmatch(nm, {'global', 'x', 'y'})
            case{1}
                if strmatch(nt, 'int')
                    nv = sum(sum(trm));
                    if nv ~= 0
                        rma = trm./nv;
                    end
                else
                    nv = max(max(trm));
                    if nv ~= 0
                        rma = trm./nv;
                    end
                end
            case{2}
                if strmatch(nt, 'int')
                    for k = 1:size(trm,2)
                        nv = sum(trm(k,:));
                        if nv ~= 0
                            rma(k,:) = trm(k,:)./nv;
                        end
                    end
                else
                    for k = 1:size(trm,2)
                        nv = max(trm(k,:));
                        if nv ~= 0
                            rma(k,:) = trm(k,:)./nv;
                        end
                    end
                end
            case{3}
                if strmatch(nt, 'int')
                    for k = 1:size(trm,1)
                        nv = sum(trm(:,k));
                        if nv ~= 0
                            rma(:,k) = trm(:,k)./nv;
                        end
                    end
                else
                    for k = 1:size(trm,1)
                        nv = max(trm(:,k));
                        if nv ~= 0
                            rma(:,k) = trm(:,k)./nv;
                        end
                    end
                end
        end
    end
%% ---- End of Function
end