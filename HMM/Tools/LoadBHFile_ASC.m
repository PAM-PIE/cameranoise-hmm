function [dataFCS, dataMCS] = LoadBHFile_ASC(filename)

numHeadlinesFCS = 6;
numHeadlinesMCS = 2;

if isempty(dir(filename))
    disp('File does not exists!');
else
   fid = fopen(filename,'r');
   bFCS = 0;
   bMCS = 0;
   dataFCS = [];
   dataMCS = [];
   while 1
       tline = fgetl(fid);
       if ~ischar(tline), break, end
       if strmatch('*END', tline), bFCS=0; bMCS=0; end
       if bFCS>1, bFCS=bFCS-1; end
       if bMCS>1, bMCS=bMCS-1; end
       if bFCS==1
           dataFCS = [dataFCS; strread(tline,'%f','delimiter',' ')'];
       end
       if bMCS==1
           dataMCS = [dataMCS; strread(tline,'%f','delimiter',' ')'];
       end
       if (~isempty(findstr('*BLOCK', tline)))&(~isempty(findstr('Fcs', tline))), bFCS=numHeadlinesFCS; end
       if (~isempty(findstr('*BLOCK', tline)))&(~isempty(findstr('Mcs', tline))), bMCS=numHeadlinesMCS; end
   end
   fclose(fid);
end