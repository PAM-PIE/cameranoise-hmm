
path='H:\Nawid\2006-05-16\';
%{
filename = { ...
    'atto655_200uw_1e6_a.asc', ...
    'atto655_strept_200uw_1e6_a.asc'...
    };
%}
filearr=dir([path,'atto655_x_*a.asc']);
filename={};
for i=1:length(filearr)
    filename{i} = filearr(i).name;
end
%{
filename = {...
    'cy5_1zu10_010uw_a.asc',...
    'cy5_1zu10_020uw_a.asc',...
    'cy5_1zu10_050uw_a.asc',...
    'cy5_1zu10_100uw_a.asc',...
    'cy5_1zu10_200uw_a.asc',...
    'cy5_1zu10_500uw_a.asc',...
    'cy5_1zu10_750uw_a.asc'...
    };
%}
[fcs, mcs] = LoadBHFile_ASC([path, filename{1}]);
hold off;
PlotFCS(fcs, hsv2rgb([1 1 0.5]));
hold on;

for i=2:length(filename)
    disp(sprintf('load %d of %d files',[i,length(filename)]));
    [fcs, mcs] = LoadBHFile_ASC([path, filename{i}]);
    PlotFCS(fcs, hsv2rgb([1 1 0.5+0.5*i/length(filename)]));
end

ylim([0 inf]);
filename = strrep(filename, '_', '\_');
legend(filename);

path = strrep(path, '_', '\_');
path = strrep(path, '\', '\\');
title(path);
