function [SubArr] = StufenFilterFast(StufenArr, sFilter, bAdjacent);
%function [SubArr] = StufenFilterFast(StufenArr, sFilter, bAdjacent);
%FilterDef:
%'1': only one Step per Burst, Creates an 1-Column-Array
%'2': only two Steps per Burst, Creates an 2-Column-Array
%'3': only three Steps per Burst, Creates an 3-Column-Array
%'2+': a Bursts with i.e. 3 Steps results in two Doublets, Creates an 2-Column-Array
% This 3-Step-Burst results in two lines, the second step occures 2 times!

if sFilter(end)=='+'
  sFilter=sFilter(1:end-1);
  bPlus=true;
else
  bPlus=false;
end
AnzStufen=str2num(sFilter);
sFields=fieldnames(StufenArr);

if bPlus
  IdxLastStufe=find(StufenArr.iStufe>=AnzStufen);
else
  IdxLastStufe=find(StufenArr.iStufe==AnzStufen);
end

for i=1:AnzStufen
  for iField=1:length(sFields)
    SubArr(i).(sFields{iField})=StufenArr.(sFields{iField})(IdxLastStufe-AnzStufen+i);
  end
end

if bAdjacent
  diff=zeros(AnzStufen-1,length(SubArr(1).Start));
  for i=1:AnzStufen-1
    diff(i,:)=SubArr(i+1).Start-SubArr(i).End;
  end
  idx=mod(find(diff(:)>1)-1,length(SubArr(1).Start))+1;
  for i=1:AnzStufen
    for iField=1:length(sFields)
      SubArr(i).(sFields{iField})(idx)=[];
    end
  end
end
