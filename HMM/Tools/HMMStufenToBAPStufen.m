function [StufenBAP] = HMMStufenToBAPStufen(StufenHMM, r0, gamma);
%[StufenBAP] = HMMStufenToBAPStufen(StufenHMM, r0, gamma);

StufenBAP = [];
for iBurst = 1:length(StufenHMM)
    for iStufe = 1:length(StufenHMM{iBurst})
        StufenBAP(end+1).Anzahl = length(StufenHMM{iBurst});
        StufenBAP(end).iPos = iStufe;
        StufenBAP(end).Start = StufenHMM{iBurst}(iStufe).Start;
        StufenBAP(end).End = StufenHMM{iBurst}(iStufe).End;
        StufenBAP(end).Length = StufenHMM{iBurst}(iStufe).End-StufenHMM{iBurst}(iStufe).Start;
        StufenBAP(end).ProxFact = StufenHMM{iBurst}(iStufe).MW;
        StufenBAP(end).ProxFactErr = 0;
        StufenBAP(end).SumIntens = 0; %20*StufenBAP(end).Length;
        StufenBAP(end).SumPeaks = 0;
        StufenBAP(end).Efret = CalcEfretFromPF(StufenHMM{iBurst}(iStufe).MW, gamma);
        StufenBAP(end).r = CalcDistFromPF(StufenHMM{iBurst}(iStufe).MW, r0, gamma);
    end
end
