function [StufenArr] = StufenHMM2StufenArrFast(StufenHMM);

if length(StufenHMM)==0
  StufenArr=[];
else
  %Variablen initialisieren
  TempArr=[StufenHMM{:}];
  AnzBursts=length(StufenHMM);
  AnzStufen=length(TempArr);
  
  %Alle Daten übernehmen
  sFields=fieldnames(TempArr);
  for iField=1:length(sFields)
    StufenArr.(sFields{iField})=[TempArr.(sFields{iField})];
  end
  
  %iBurst ermitteln (BurstNr)
  Burst_AnzStufen=zeros(1,AnzBursts);
  for iBurst=1:AnzBursts
    Burst_AnzStufen(iBurst)=length(StufenHMM{iBurst});
  end  
  IdxStufe1=cumsum([1,Burst_AnzStufen(1:end-1)]);
  StufenArr.iBurst=zeros(1,AnzStufen);
  StufenArr.iBurst(IdxStufe1)=1;
  StufenArr.iBurst=cumsum(StufenArr.iBurst);
end
