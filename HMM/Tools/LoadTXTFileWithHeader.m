function [values] = LoadTXTFileWithHeader(filename,headerlines);
%for Header info type "fieldnames(values)"

if ~exist('headerlines','var'), headerlines=2; end;
fid = fopen(filename, 'rt');
tline = fgetl(fid);
header = strread(tline,'%s','delimiter','\t');
%filter = ''; for i=1:size(header,1), filter = [filter,'%s']; end
filter = repmat('%s',1,size(header,1));
values = textscan(fid, filter, 'delimiter', '\t');
values = cell2struct(values, header', headerlines);
fclose(fid);

fnames = fieldnames(values);
for i=1:size(fnames,1)
    dummy = str2num(char(values.(char(fnames(i)))(:)));
    if size(dummy)==size(values.(char(fnames(i)))(:))
        values.(char(fnames(i))) = dummy;
    end;
end

%varargout(1)={values};
%if nargout>=2, varargout(2)={header}; end

