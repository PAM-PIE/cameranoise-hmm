function [StufenArr] = StufenHMM2StufenArrPhot(StufenHMM,BurstInt,r0,gamma);
%[StufenArr] = StufenHMM2StufenArrPhot(StufenHMM,BurstInt,r0,gamma);
%Diese Function konvertiert die HMM-Stufen in das StufenArr, das mit
%den bereits existierenden Matlab-Auswerte-Scripts kompatibel ist.

num=length([StufenHMM{:}]);
fprintf(1,'%d Stufen-Eigenschaften erstellen: %6d',[num,0]); 
%Init
Stufe.iPos = 0;
Stufe.Start = 0;
Stufe.End = 0;
Stufe.Anzahl = 0;
Stufe.Length = 0;
Stufe.SumIntens = 0;
Stufe.SumPeaks = 0;
Stufe.ProxFact = 0;
%Stufe.ProxFactErr = 0;
Stufe.Efret = 0;
Stufe.r = 0;
Stufe.LL = 0;
Stufe.Intens = zeros(4,1);
Stufe.Peak = zeros(4,1);
Stufe.P = 0;
Stufe.S = 0;
Stufe.PFclass = 0;
Stufe.iBurst = 0;
StufenArr(1) = Stufe;
StufenArr = repmat(StufenArr,num,1);
%calc
iStufe=0;
tic;
for i=1:numel(StufenHMM)
    iPos = 0;
    iMark = iStufe;
    for j=1:numel(StufenHMM{i})
        iStufe=iStufe+1;
        iPos=iPos+1;
        Stufe.iPos = iPos;
        Stufe.Start = min(length(BurstInt{1,i}),fix(StufenHMM{i}(j).Start)+1);
        Stufe.End = min(length(BurstInt{1,i}),fix(StufenHMM{i}(j).End)+1);
        Stufe.Anzahl = numel(StufenHMM{i});
        Stufe.Length = Stufe.End - Stufe.Start;
        Stufe.SumIntens = sum(abs(BurstInt{1,i}(Stufe.Start:Stufe.End))+abs(BurstInt{2,i}(Stufe.Start:Stufe.End)));
        Stufe.SumPeaks = max(BurstInt{1,i}(Stufe.Start:Stufe.End)+BurstInt{2,i}(Stufe.Start:Stufe.End));
        Stufe.ProxFact = StufenHMM{i}(j).MW;
        %Stufe.ProxFactErr = StufenHMM{i}(j).ProxFactErr;
        Stufe.Efret = CalcEfretFromPF(Stufe.ProxFact, gamma);
        Stufe.r = CalcRFromEfret(Stufe.Efret, r0);
        Stufe.LL = StufenHMM{i}(j).LL;
        Stufe.Intens = zeros(4,Stufe.End-Stufe.Start+1);
        for itrace=1:size(BurstInt,1)
            Stufe.Intens(itrace,:) = BurstInt{itrace,i}(Stufe.Start:Stufe.End);
        end
        Stufe.Peak = max(Stufe.Intens,[],2);
        Stufe.P = sum(Stufe.Intens(2,:))./max(1,sum(Stufe.Intens(1,:)+Stufe.Intens(2,:)));
        Stufe.S = sum((Stufe.Intens(1,:)+Stufe.Intens(2,:)))./max(1,sum(Stufe.Intens(1,:)+Stufe.Intens(2,:)+Stufe.Intens(3,:)));
        if exist('obsvect')
            Stufe.PFclass = obsvect(StufenHMM{i}(j).PFclass);
        else
            Stufe.PFclass = StufenHMM{i}(j).PFclass;
        end
        Stufe.iBurst = i;
        StufenArr(iStufe) = Stufe;
        if toc >= 0.2, fprintf(1,'\b\b\b\b\b\b%6d',[iStufe]); tic; end
    end
    for idummy=iMark+1:iStufe
        StufenArr(idummy).Anzahl=iPos;
    end
end
StufenArr(iStufe+1:end)=[];
fprintf(1,'\b\b\b\b\b\bok.\n'); 
