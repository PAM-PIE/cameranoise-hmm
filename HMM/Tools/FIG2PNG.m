function FIG2PNG(pathpic);
%FIG2PNG(path);
%Converts FIG-Files in a given Path to PNG-files

%pathpic = 'D:\Thomas\2007-04-23\HMM\Auswertung 2h\';

%filefig = dir([pathpic,'*.fig']);
filesfig = dir(pathpic);
[pathpic,name,ext,versn] = fileparts(pathpic);
pathpic=[pathpic,'\'];
fprintf('Process %d files...\n',length(filesfig));

for i=1:length(filesfig)
    [pathstr,name,ext,versn] = fileparts(filesfig(i).name);
    filefig = fullfile(pathpic, filesfig(i).name);
    filepng = fullfile(pathpic,[name,'.png']);
    fprintf('#%d: %s',i,filefig);
    open(filefig);
    fprintf(' => ');
    drawnow;
    fprintf('%s',filepng);
    fh=gcf;
    set(fh,'PaperPositionMode','auto');    % <-- Ohne diese Anweisung druckt der SaveAs-Befehl nur Mist!
    saveas(fh,filepng,'png');
    close(fh);
    fprintf(' ok.\n');
end
fprintf('All files processed.\n');
