function dataInt=FuncBinPhotArr(dataFRETdt,dataFRETch,dataALEXdt,cTimeBase,cTimeBin);
%dataInt=FuncBinPhotArr(dataFRETdt,dataFRETch,dataALEXdt,cTimeBase,cTimeBin);
%
%dataInt:    Ausgabe als 3xAnzBurst-Cell-Array
%            1: Donor, 2: Acceptor: 3: ALEX
%dataFRETdt: Interphotonenabstände der FRET-Photonen in der Einheit cTimeBase
%dataFRETch: Detektornummer des jeweiligen Photons
%dataALEXdt: Interphotonenabstände der ALEX-Photonen in der Einheit cTimeBase
%cTimeBase:  z.B. 50e-9 Sekunden (b&h), 100e-9 Sekunden (Picoquant)
%cTimeBin:   Zeitauflösung der gebinnten Trajektorie (z.B. 1e-3 Sekunden)

for iBurst=1:length(dataFRETdt)
    %zeitl. Interphotonenabstände in abs Photoenzeiten umrechnen und runden
    PhotArrFRET=floor(cumsum(dataFRETdt{iBurst})*cTimeBase/cTimeBin+1);
    if ~isempty(dataALEXdt{iBurst})
      PhotArrALEX=floor(cumsum(dataALEXdt{iBurst})*cTimeBase/cTimeBin+1);
    end
    %Photonen der FRET-Kanäle trennen
    PhotArr1=PhotArrFRET(find(dataFRETch{iBurst}==1));
    PhotArr2=PhotArrFRET(find(dataFRETch{iBurst}==2));
    %BurstLänge bestimmen
    if ~isempty(dataALEXdt{iBurst})
      BurstLen=max(PhotArrFRET(end),PhotArrALEX(end));
    else
      BurstLen=PhotArrFRET(end);
    end
    %Trajektorie binnen
    dataInt{1,iBurst}=accumarray(PhotArr1',1,[BurstLen 1])';
    dataInt{2,iBurst}=accumarray(PhotArr2',1,[BurstLen 1])';
    if ~isempty(dataALEXdt{iBurst})
      dataInt{3,iBurst}=accumarray(PhotArrALEX',1,[BurstLen 1])';
    end
end

end