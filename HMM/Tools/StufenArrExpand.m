function [StufenArr] = StufenArrExpand(StufenArr,dataInt,dataE);
%function [StufenArr] = StufenArrExpand(StufenArr,dataInt,dataE);

%Variablen initialisieren
AnzStufen=length(StufenArr.PFclass);
AnzBursts=length(dataE);

%Anzahl ermitteln (Anzahl Stufen pro Burst)
StufenArr.AnzStufen=zeros(1,AnzStufen);
IdxStufe1=find([1,diff(StufenArr.iBurst)]>0);
Burst_AnzStufen=[diff(IdxStufe1),length(StufenArr.PFclass)-IdxStufe1(end)+1];
StufenArr.AnzStufen(IdxStufe1)=[Burst_AnzStufen(1),diff(Burst_AnzStufen)];
StufenArr.AnzStufen=cumsum(StufenArr.AnzStufen);

%iPos ermitteln (StufenNr im Burst)
StufenArr.iStufe=ones(1,AnzStufen);
StufenArr.iStufe(IdxStufe1(2:end))=-Burst_AnzStufen(1:end-1)+1;
StufenArr.iStufe=cumsum(StufenArr.iStufe);

%Length ermitteln (Duration)
StufenArr.Length=StufenArr.End-StufenArr.Start+1;

%neu durchnummerieren
StufenArr.ID=1:length(StufenArr.PFclass);

%Eigenschaften aus Zusatzinfos nachrechnen
%Initialisierung
if (~isempty(dataE)) | (~isempty(dataInt))
  Burst_Duration=zeros(1,AnzBursts);
  for iBurst=1:AnzBursts
    Burst_Duration(iBurst)=length(dataE{iBurst});
  end
  Burst_IdxFirstBin=cumsum([1,Burst_Duration(1:end-1)]);
  StartAbs=StufenArr.Start+Burst_IdxFirstBin(StufenArr.iBurst)-1;
  EndAbs=StufenArr.End+Burst_IdxFirstBin(StufenArr.iBurst)-1;
end

if ~isempty(dataE)
  TempArr=[dataE{:}];
  StufenArr.Efret=zeros(1,AnzStufen);
  for iStufe=1:AnzStufen
    StufenArr.Efret(iStufe)=mean(TempArr(StartAbs(iStufe):EndAbs(iStufe)));
  end
end

if ~isempty(dataInt)
  TempArr=[dataInt{1,:}];
  StufenArr.Iavg1=zeros(1,AnzStufen);
  for iStufe=1:AnzStufen
    StufenArr.Iavg1(iStufe)=mean(TempArr(StartAbs(iStufe):EndAbs(iStufe)));
  end

  TempArr=[dataInt{2,:}];
  StufenArr.Iavg2=zeros(1,AnzStufen);
  for iStufe=1:AnzStufen
    StufenArr.Iavg2(iStufe)=mean(TempArr(StartAbs(iStufe):EndAbs(iStufe)));
  end
end
end
