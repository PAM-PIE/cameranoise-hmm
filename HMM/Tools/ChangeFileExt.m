function [newfilename]=ChangeFileExt(oldfilename,newext)
%function [newfilename]=ChangeFileExt(oldfilename,newext)

ipos = strfind(oldfilename, '.');
if length(ipos)>0
  newfilename=[oldfilename(1:ipos(end)-1),newext];
else
  newfilename=[oldfilename,newext];
end
