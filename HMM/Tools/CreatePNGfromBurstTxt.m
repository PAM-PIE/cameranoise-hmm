function CreatePNGfromBurstTxt(filenamearg)

    if ~isempty(strfind(filenamearg, '*'))
        filenameArr = dir(filenamearg);
        [pathstr,name,ext,versn] = fileparts(filenamearg);
        if ~isempty(pathstr), pathstr=[pathstr,'\']; end
        filenameArr = {filenameArr.name}';
        filenameArr = strcat(pathstr, filenameArr);
    else
        filenameArr{1} = filenamearg;
    end
        
    for ifilename=1:length(filenameArr)
        
        filename=filenameArr{ifilename};
        
        [data, Header] = LoadBurstFile(filename);
        if isempty(data), continue, end
        disp(['Processing "',filename,'"...']);

        colInt = [...
            0 0 1; ...
            0 0.5 0; ...
            1 0 0; ...
            0 0.8 0.8; ...
        ];

        hold off
        ix = strmatch('Zeit [ms]', Header);
        data(:,ix) = data(:,ix) - data(1,ix);

        ax(1) = subplot(3,1,3, 'position', [0.1 0.1 0.8 0.3]);
        iy = strmatch('Zeitspur 1', Header);
        plot(data(:,ix), data(:,iy), 'color', colInt(1,:));
        hold on
        iy = strmatch('Zeitspur 2', Header);
        plot(data(:,ix), data(:,iy), 'color', colInt(2,:));
        iy = strmatch('Zeitspur 3', Header);
        plot(data(:,ix), data(:,iy), 'color', colInt(3,:));
        iy = strmatch('Zeitspur 4', Header);
        plot(data(:,ix), data(:,iy), 'color', colInt(4,:));
        xlabel('time / ms');
        ylabel('Intensity');

        ax(2) = subplot(3,1,2, 'position', [0.1 0.45 0.8 0.2]);
        iy = strmatch('Prox.Fact', Header);
        plotwithcol(data(:,ix), data(:,iy), data(:,iy+3), [1 0.5 0]);
        set(gca, 'ylim', [0 1]);
        ylabel('ProxFact');
        set(gca,'xticklabel',{});
        
        iy1 = strmatch('Zeitspur 1', Header);
        iy2 = strmatch('Zeitspur 2', Header);
        for i=1:size(data,1), datatemp(i) = data(i,iy2)/(data(i,iy2)+data(i,iy1)+1); end
        plotwithcol(data(:,ix), datatemp(:), data(:,iy+3), [0 0.5 1]);

        %{
        ax(3) = subplot(3,1,1, 'position', [0.1 0.7 0.8 0.2]);
        iy = strmatch('Frettrace', Header);
        plotwithcol(data(:,ix), data(:,iy), max(data(:,iy+1))-data(:,iy+1), [0 0 1]);
        %hold on
        %plot(data(:,ix), data(:,iy)-data(:,iy+1));
        %plot(data(:,ix), data(:,iy)+data(:,iy+1));
        set(gca, 'ylim', [3.5 6.5]);
        ylabel('Frettrace');
        set(gca,'xticklabel',{});
        %}
        ax(3) = subplot(3,1,1, 'position', [0.1 0.7 0.8 0.2]);
        iy = strmatch('Frettrace', Header);
        m = [];
        for i=1:size(data,1)
            y = pdf('Normal', 0:0.1:10, data(i,iy), data(i,iy+1));
            m = cat(2, m, y');
        end                
%        m = m'; 
        image(1:size(data,1), 0:0.1:10, m, 'CDataMapping', 'scaled');
        linkaxes(ax,'x');
        xlim([data(1,ix) data(end,ix)]);
        set(gca, 'YDir', 'normal');
        ylim([1.5 6.5]);
        ylabel('Frettrace');
        set(gca,'xticklabel',{});

        [pathstr,name,ext,versn] = fileparts(filename);
        if ~isempty(pathstr), pathstr=[pathstr,'\']; end        
        filenamepic = [pathstr,name,'.png'];
        name = strrep(name, '_', '\_');
        title([name,ext]);

        %saveas(gcf,filenamepic,'png');
    end
end


%------------------------------------------------

function [data, Header] = LoadBurstFile(filename)

Header = [];
data = [];
if isempty(dir(filename))
    disp(['File "',filename,'" does not exist!']);
else
   fid = fopen(filename,'r');
   tline = fgetl(fid);
   Header = strread(tline,'%s','delimiter','\t');
   while 1
       tline = fgetl(fid);
       if ~ischar(tline), break, end
       tline = strrep(tline, ',', '.');
       data = [data; strread(tline,'%f','delimiter','\t')'];
   end
   fclose(fid);   
end

end
