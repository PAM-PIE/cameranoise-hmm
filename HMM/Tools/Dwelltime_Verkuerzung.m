
Anz=10000;
tau=10;
len1 = exprnd(tau,Anz,1);
len2 = len1.*rand(Anz,1);

subplot(2,1,1);
Fit_HistExp1(len1,0:1:40,0,false);

subplot(2,1,2);
Fit_HistExp1(len2,0:1:40,0,false);
