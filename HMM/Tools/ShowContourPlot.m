function [data]=ShowContourPlot(x,y, binx, biny, ArrAreasMin, ArrAreasMax, bShowGrid);
%function [data]=ShowContourPlot(x,y, binx, biny, ArrAreasMin, ArrAreasMax, bShowGrid);

z=[x(:),y(:)];
data=hist3(z,{binx biny})';
contourf(binx, biny, data, 100, 'LineStyle','none');
xrange = [min(binx) max(binx)];
yrange = [min(biny) max(biny)];
xlim(xrange); ylim(yrange);
%h=hist3(z,{binmin:binwidth:binmax binmin:binwidth:binmax})';
%contourf(binmin:binwidth:binmax, binmin:binwidth:binmax, h, 100, 'LineStyle','none');
%xlim([binmin binmax]); ylim([binmin binmax]);
set(gca, 'PlotBoxAspectRatio', [1 1 1]);
%xlabel(gca,'proximity factor 1');
%ylabel(gca,'proximity factor 2');
colorbar;

if bShowGrid
    hold on;
    ShowGrid(xrange, yrange, [ArrAreasMin(:); ArrAreasMax(:)], [ArrAreasMin(:); ArrAreasMax(:)], [1 1 1]);
    hold off;
end
