%Alex-Plot

filepath = 'I:\Thomas\2007-04-23\HMM\';
filename = 'atp_b';
MaxAnzBurst = inf;

[dataInt1, dataInt2, dataInt3, IDFile, IDBurst, dataStartMS] = ...
    BinStep_LoadBurstBinV2b([filepath,filename],MaxAnzBurst);

AnzBursts=length(dataInt1);
I1_=zeros(AnzBursts,1);
I2_=zeros(AnzBursts,1);
I3_=zeros(AnzBursts,1);
for i=1:AnzBursts
    I1_(i)=sum(dataInt1{i});
    I2_(i)=sum(dataInt2{i});
    I3_(i)=sum(dataInt3{i});
end
I1_=I1_';
I2_=I2_';
I3_=I3_';

cbin=1;
I1noise=0;
I2noise=0;
I3noise=0;
binxy=-0.1:0.04:1.1;
binz=1:1:1000;

if cbin>1
    I1=I1_(1:cbin*div(length(I1_),cbin));
    I2=I2_(1:cbin*div(length(I2_),cbin));
    I3=I3_(1:cbin*div(length(I3_),cbin));
    I1=sum(reshape(I1,cbin,[]));
    I2=sum(reshape(I2,cbin,[]));
    I3=sum(reshape(I3,cbin,[]));
else
    I1=I1_';
    I2=I2_';
    I3=I3_';
end
I1noise=I1noise*cbin;
I2noise=I2noise*cbin;
I3noise=I3noise*cbin;

idx=find(((I1-3+I2+I3)>=0*cbin)&((I1-3+I2+I3)<=inf*cbin));
% idx=find(...
%     (I1>=3*cbin)&(I1<=35*cbin) & ...
%     (I2>=0*cbin)&(I2<=35*cbin) & ...
%     (I3>=5*cbin)&(I3<=35*cbin));
I3=I3*1.5;
fprintf('calc %d bins...',length(idx));
if length(idx)<2000000
    S=(I1(idx)-I1noise+I2(idx)-I2noise)./(I1(idx)-I1noise+I2(idx)-I2noise+I3(idx)-I3noise);
    P=(I2(idx)-I2noise)./(I1(idx)-I1noise+I2(idx)-I2noise);

    if 1
        ShowContourWithHist(P, S, binxy, binxy, binz, true, 'P', 'S', '', '', (0:0.2:1), (0:0.2:1));
    else
        z=hist3([S',P'],{binxy binxy});
        surf(binxy,binxy,z);
        shading interp
        colorbar
        xlabel('P');
        ylabel('S');
        view([0 90]);
    end

    fprintf('ok\n');
else
    fprintf('dauert zu lange!\n');
end
