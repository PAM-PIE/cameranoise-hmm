function FastDensPlot(x,y,sizex,sizey);

data=hist3([x(:),y(:)],{sizex sizey})';
imagesc(sizex,sizey,data);
set(gca,'YDir','normal');
