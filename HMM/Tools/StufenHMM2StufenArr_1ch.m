function [StufenArr] = StufenHMM2StufenArr_1ch(StufenHMM,BurstInt);
%function [StufenArr] = StufenHMM2StufenArr_1ch(StufenHMM,BurstInt);
%Diese Function konvertiert die HMM-Stufen in das StufenArr, das mit
%den bereits existierenden Matlab-Auswerte-Scripts kompatibel ist.

num=length([StufenHMM{:}]);
fprintf(1,'%d Stufen-Eigenschaften erstellen: %6d',[num,0]); 
%Init
Stufe.iPos = 0;
Stufe.Start = 0;
Stufe.End = 0;
Stufe.Anzahl = 0;
Stufe.Length = 0;
Stufe.SumIntens = 0;
Stufe.SumPeaks = 0;
Stufe.LL = 0;
Stufe.Intens = zeros(4,1);
Stufe.Peak = zeros(4,1);
Stufe.PFclass = 0;
Stufe.iBurst = 0;
StufenArr(1) = Stufe;
StufenArr = repmat(StufenArr,num,1);
%calc
iStufe=0;
tic;
for i=1:numel(StufenHMM)
    iPos = 0;
    iMark = iStufe;
    for j=1:numel(StufenHMM{i})
        iStufe=iStufe+1;
        iPos=iPos+1;
        Stufe.iPos = iPos;
        Stufe.Start = StufenHMM{i}(j).Start;
        Stufe.End = StufenHMM{i}(j).End;
        Stufe.Anzahl = numel(StufenHMM{i});
        Stufe.Length = Stufe.End - Stufe.Start;
        Stufe.Intens = BurstInt{i}(Stufe.Start:Stufe.End);
        Stufe.SumIntens = sum(abs(BurstInt{i}(Stufe.Start:Stufe.End)));
        Stufe.SumPeaks = max(BurstInt{i}(Stufe.Start:Stufe.End));
        Stufe.LL = StufenHMM{i}(j).LL;
        Stufe.Peak = max(Stufe.Intens,[],2);
        if exist('obsvect')
            Stufe.PFclass = obsvect(StufenHMM{i}(j).PFclass);
        else
            Stufe.PFclass = StufenHMM{i}(j).PFclass;
        end
        Stufe.iBurst = i;
        StufenArr(iStufe) = Stufe;
        if toc >= 0.2, fprintf(1,'\b\b\b\b\b\b%6d',[iStufe]); tic; end
    end
    for idummy=iMark+1:iStufe
        StufenArr(idummy).Anzahl=iPos;
    end
end
StufenArr(iStufe+1:end)=[];
fprintf(1,'\b\b\b\b\b\bok.\n'); 
