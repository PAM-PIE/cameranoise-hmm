function [ergfit] = cfTool_auto1D(datax,datay)
%CFTOOL_AUTO1D    Create plot of datasets and fits
%   CFTOOL_AUTO1D(DATAX,DATAY)
%   Creates a plot, similar to the plot in the main curve fitting
%   window, using the data that you provide as input.  You can
%   apply this function to the same data you used with cftool
%   or with different data.  You may want to edit the function to
%   customize the code and this help message.
%
%   Number of datasets:  1
%   Number of fits:  1

 
% Data from dataset "test":
%    X = datax:
%    Y = datay:
%    Unweighted
%
% This function was automatically generated on 10-Feb-2006 15:48:18

% Set up figure to receive datasets and fits
bBild = false;
if bBild
    f_ = clf;
    figure(f_);
    legh_ = []; legt_ = {};   % handles and text for legend
    xlim_ = [Inf -Inf];       % limits of x axis
    ax_ = subplot(1,1,1);
    set(ax_,'Box','on');
    axes(ax_); hold on;
end
 
% --- Plot data originally in dataset "test"
datax = datax(:);
datay = datay(:);
if bBild 
    h_ = line(datax,datay,'Parent',ax_,'Color',[0.333333 0 0.666667],...
         'LineStyle','none', 'LineWidth',1,...
         'Marker','.', 'MarkerSize',12);
    xlim_(1) = min(xlim_(1),min(datax));
    xlim_(2) = max(xlim_(2),max(datax));
    legh_(end+1) = h_;
    legt_{end+1} = 'test';
    %set(ax_,'XScale','log');
    % Nudge axis limits beyond data limits
    if all(isfinite(xlim_))
        xlim_ = xlim_ + [-1 1] * 0.01 * diff(xlim_);
        set(ax_,'XLim',xlim_)
    end
end


% --- Create fit "fit 1"
fo_ = fitoptions('method','NonlinearLeastSquares','Algorithm','Trust-Region','DiffMaxChange',0.001,'MaxFunEvals',10000,'MaxIter',1000,'TolFun',1e-008,'TolX',1e-008,'Robust','on');
st_ = [0.5 0.8 0.1 ];
set(fo_,'Startpoint',st_);
set(fo_,'Lower',[0 0 0]);
set(fo_,'Upper',[inf inf inf]);
DiffModel = @(x, Nf, omegaZ, tauDiff) (1/Nf).*((1+x./tauDiff).^(-1)).*((1+x./(omegaZ*omegaZ*tauDiff)).^(-0.5));
ft_ = fittype('(1/Nf)*(1/(1+x/tauDiff))*((1/(1+omegaZ*omegaZ*x/tauDiff))^0.5)',...
     'dependent',{'y'},'independent',{'x'},...
     'coefficients',{'Nf', 'omegaZ', 'tauDiff'});

% Fit this model using new data
cf_ = fit(datax,datay,ft_ ,fo_);

% Or use coefficients from the original fit:
if 0
   cv_ = {5.152225622911, -1.769724738159e-005, 0.6237945221059};
   cf_ = cfit(ft_,cv_{:});
end

% Calc Erg-Array
ergy = datay;
for i=1:length(datax)
    ergy(i) = DiffModel(datax(i), cf_.Nf, cf_.omegaZ, cf_.tauDiff);
end

if bBild
    % Plot this fit
    %orig: h_ = plot(cf_,'fit',0.95);
    h_ = semilogx(datax, ergy);
    legend off;  % turn off legend from plot method call
    set(h_(1),'Color',[1 0 0],...
         'LineStyle','-', 'LineWidth',2,...
         'Marker','none', 'MarkerSize',6);
    legh_(end+1) = h_(1);
    legt_{end+1} = 'fit 1';

    hold off;
    legend(ax_,legh_, legt_);
end

ergfit = cf_;
