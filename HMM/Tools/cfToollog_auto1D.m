function cfToollog_auto1D(sourcex,sourcey)
%CFTOOL_AUTO1D    Create plot of datasets and fits
%   CFTOOL_AUTO1D(sourcex,sourcey)
%   Creates a plot, similar to the plot in the main curve fitting
%   window, using the data that you provide as input.  You can
%   apply this function to the same data you used with cftool
%   or with different data.  You may want to edit the function to
%   customize the code and this help message.
%
%   Number of datasets:  1
%   Number of fits:  1

 
% Data from dataset "test":
%    X = sourcex:
%    Y = sourcey:
%    Unweighted


% --- Create fit "fit 1"
fo_ = fitoptions('method','NonlinearLeastSquares','Algorithm','Levenberg-Marquardt','DiffMaxChange',0.1,'MaxFunEvals',10000,'MaxIter',1000,'TolFun',1e-008,'TolX',1e-008);
st_ = [4.0 0.0 0.3 ];
set(fo_,'Startpoint',st_);
DiffModel = @(x, Nf, omegaZ, tauDiff) (1/Nf)*(1/(1+x/tauDiff))*((1/(1+omegaZ*omegaZ*x/tauDiff))^0.5);
ft_ = fittype('(1/Nf)*(1/(1+x/tauDiff))*((1/(1+omegaZ*omegaZ*x/tauDiff))^0.5)',...
     'dependent',{'y'},'independent',{'x'},...
     'coefficients',{'Nf', 'omegaZ', 'tauDiff'});

% Fit this model using new data
cf_ = fit(sourcex,sourcey,ft_ ,fo_);

% Or use coefficients from the original fit:
if 0
   cv_ = {5.152225622911, -1.769724738159e-005, 0.6237945221059};
   cf_ = cfit(ft_,cv_{:});
end

% Calc Erg-Array
ergy = sourcey;
for i=1:length(sourcex)
    ergy(i) = DiffModel(sourcex(i), cf_.Nf, cf_.omegaZ, cf_.tauDiff);
end

% Plot this fit
%orig: h_ = plot(cf_,'fit',0.95);
h_ = semilogx(sourcex, sourcey);
hold on;
h_ = semilogx(sourcex, ergy, 'color', 'red');
xlabel('Tau/ms');
ylabel('CCF12');
title('test');
s = sprintf('COF = %3.3f   %3.3f   %3.3f', cf_.Nf, cf_.tauDiff, cf_.omegaZ);
text(1, max(sourcey)*0.9, s);
disp(' Nf  tauDiff  omegaZ');
disp([cf_.Nf cf_.tauDiff cf_.omegaZ]);
%{
	s = sprintf('COF = %3.3f   %3.3f', c, offset);
	text(max(t)/2,v(4)-0.05*(v(4)-v(3)),s);
	s = ['AMP = '];
	A = A/sum(A);
	for i=1:length(A)
		s = [s sprintf('%1.3f',A(i)) '   '];
	end
	text(max(t)/2,v(4)-0.12*(v(4)-v(3)),s);
	s = ['TAU = '];
	for i=1:length(tau)
		s = [s sprintf('%3.3f',tau(i)) '   '];
	end
	text(max(t)/2,v(4)-0.19*(v(4)-v(3)),s);
%}
%legend off;  % turn off legend from plot method call
%set(h_(1),'Color',[1 0 0],...
%     'LineStyle','-', 'LineWidth',2,...
%     'Marker','none', 'MarkerSize',6);
%legh_(end+1) = h_(1);
%legt_{end+1} = 'fit 1';

hold off;
%legend(ax_,legh_, legt_);
