function [T]=TextureArrow(col,row);

T=zeros(col,row);
col2=ceil(col/2);
row2=ceil(row/2);
for j=1:row2
  Z=mod(div((1:col)-j,row2),2); 
  T(:,j)=Z; 
  T(:,row-j+1)=Z; 
end
