clear
close all

%%%%%%%%%%%%%%%%%%%Einstellen%%%%%%%%%%%%%%
AnzinDir=5;
peakzahl=1;
linienpos='4';
linienpos1='2';
linienpos2='3';

peakposvar=10;

peakbreitemin1=2;
peakbreitemax1=15;
fitbreite1=120;

peakbreitemin2=5;
peakbreitemax2=25;
fitbreite2=200;

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

dir_name = 'C:\user\ss3\Horsereddish\Monomere\Mol17\processed\';
files = dir(dir_name);
x = size(files);
daten=[];
gesamt=[];

for i = 1:x(1,1)-AnzinDir
    load_structure = load(strcat(dir_name,'gesamt',num2str((i-1)*30),'.dat'));
    if i==1
        xAchse=1./load_structure(:,1)*1e+007;
        load_structure(:,1)=[];
        spekprowink(i)=size(load_structure,2);
        daten=[daten load_structure];
        gesamt(i,:)=sum(load_structure(:,1:size(load_structure,2))')/(size(load_structure,2));
    else
        load_structure(:,1)=[];
        daten=[daten load_structure];
        gesamt(i,:)=sum(load_structure(:,1:size(load_structure,2))')/(size(load_structure,2));
        spekprowink(i)=size(load_structure,2);
    end
    clear('load_structure');
end
clear x;

yAchse=1:size(daten,2);
figure
imagesc(xAchse, yAchse, daten');
colormap(gray);
colorbar;
xlabel(dir_name);
waitforbuttonpress;

if peakzahl==1
    
    

    fitparams=zeros(4,size(daten,2));
    
    temp=0;
    for j=1:length(spekprowink);
    

    for index = temp+1:temp+spekprowink(j)
    
        close;
        Xpoint=0;
        now = daten(:,index);
        figure
        xlabel(num2str(30*(j-1)));
        hold on;
        p = plot(xAchse,now, 'blue');crosshair;
        waitfor(p);

        
        if Xpoint>0
		X = evalin('base','Xpoint');
		Y = evalin('base','Ypoint');

        peak1(index) = XYhist.set1.data(1,2);
 
        clear XYhist;
        clear p;
        warning('off','all');
    
        p = plot(xAchse,now, 'blue');
    
        hold on;
    

        ftype = fittype('y0+(2*a1/pi)*c1/((4*(x-b1)^2)+c1^2)');%+(-2*a2/pi)*c2/((4*(x-b2)^2)+c2^2)');%+(-2*a3/pi)*c3/((4*(x-b3)^2)+c3^2)+(-2*a4/pi)*c4/((4*(x-b4)^2)+c4^2)');   %Lorentzfunktion
        opts = fitoptions('y0+(2*a1/pi)*c1/((4*(x-b1)^2)+c1^2)');%+(-2*a2/pi)*c2/((4*(x-b2)^2)+c2^2)');%+(-2*a3/pi)*c3/((4*(x-b3)^2)+c3^2)+(-2*a4/pi)*c4/((4*(x-b4)^2)+c4^2)'); %Lorentzfunktion
        opts.Upper = [Inf peak1(index)+0.5*peakposvar peakbreitemax1 Inf];  %moegliche obere Grenzen fuer a1-4,b1-4,c1-4,y0 
        opts.Lower = [0 peak1(index)-0.5*peakposvar peakbreitemin1 0];       %moegliche untere Grenzen fuer a1-4,b1-4,c1-4,y0 
        opts.MaxFunEvals = 1000;
        
        difb=abs(xAchse-peak1(index)+0.5*fitbreite1); %Fitbereich
        [dump2,ende]=min(difb);
        difc=abs(xAchse-peak1(index)-0.5*fitbreite1); %Fitbereich 
        [dump3,start]=min(difc);
    
        [fitspec,gof,output]=fit(xAchse(start:ende), daten(start:ende,index), ftype, opts); %Fitten .. Variablen enthalten Ergebnisse des Fits
        fitparams(1,index)=fitspec.a1;
        fitparams(2,index)=fitspec.b1;
        fitparams(3,index)=fitspec.c1;
        fitparams(4,index)=fitspec.y0; 
    
        plot(xAchse,fitparams(4,index)+(2*fitparams(1,index)/pi)*fitparams(3,index)./((4*(xAchse(:,1)-fitparams(2,index)).^2)+fitparams(3,index)^2),'r');%+(-2*fitparams(2,1)/pi)*fitparams(2,3)./((4*(x(:,1)-fitparams(2,2)).^2)+fitparams(2,3)^2),'red', 'LineWidth', 1.5);%+(-2*fitparams(3,1)/pi)*fitparams(3,3)./((4*(x(:,1)-fitparams(3,2)).^2)+fitparams(3,3)^2)+(-2*fitparams(4,1)/pi)*fitparams(4,3)./((4*(x(:,1)-fitparams(4,2)).^2)+fitparams(4,3)^2),'red', 'LineWidth', 1.5);
        waitforbuttonpress;
        
        end
        
        prompt  = {'Fit erfolgreich? J/N'};
        title   = '';
        lines= 1;
        if Xpoint>0
            def     = {'J'};
        else
            def     = {'N'};
        end
        answer  = inputdlg(prompt,title,lines,def);
        assignin('base','ent',answer{1});
        
        if ent=='N'
            fitparams(:,index)=0;
        end
        
        
    end
    temp=temp+spekprowink(j);
    end

    for index=1:size(fitparams,2)
        Kurve(:,index)=((2*fitparams(1,index)/pi)*fitparams(3,index)./((4*(xAchse(:,1)-fitparams(2,index)).^2)+fitparams(3,index)^2));
    end

    temp=0;
    clear yAchse;
    figure
    clims=[0 max(max(Kurve))];

    for j=1:length(spekprowink)
        subplot(length(spekprowink),1,j); 
        yAchse=temp+1:temp+spekprowink(j);
        imagesc(xAchse,yAchse,Kurve(:,(temp+1):(temp+spekprowink(j)))',clims);colormap(gray);
        temp=temp+spekprowink(j);
        if j<length(spekprowink)
            set(gca,'xTickLabel',{' '});
        end
        
    end
    clear j;
    
    temp=0;
    for j=1:length(spekprowink)
        pol=[];
        for k=temp+1:temp+spekprowink(j)
            if fitparams(:,k)~=0
                pol=[pol fitparams(:,k)];
            end
        end
        temp=temp+spekprowink(j);
        mkdir(dir_name,linienpos);
        save(strcat(dir_name,linienpos,'\fitpol',num2str((j-1)*30),'.dat'),'pol','-ascii');
    end
    clear pol,i;
    save(strcat(dir_name,linienpos,'\xAchse.dat'),'xAchse','-ascii');     

elseif peakzahl==2
    
    
    fitparams1=zeros(4,size(daten,2));
    fitparams2=zeros(4,size(daten,2));
    
    temp=0;
    for j=1:length(spekprowink);

    for index = temp+1:temp+spekprowink(j)
 
        close;
        Xpoint=0;
        figure
        xlabel(num2str(30*(j-1)));
        now = daten(:,index);
        hold on;
        p = plot(xAchse,now, 'blue');crosshair;
        waitfor(p);
        
        if Xpoint>0
		X = evalin('base','Xpoint');
		Y = evalin('base','Ypoint');
        
        peak1(index) = XYhist.set1.data(1,2);
        peak2(index) = XYhist.set1.data(2,2);
    %   peak3 = XYhist.set1.data(3,2);
    %   peak4 = XYhist.set1.data(4,2);
  
        clear XYhist;
        clear p;
        warning('off','all');
    
        p = plot(xAchse,now, 'blue');
    
        hold on;
    

        ftype = fittype('y0+(2*a1/pi)*c1/((4*(x-b1)^2)+c1^2)+(2*a2/pi)*c2/((4*(x-b2)^2)+c2^2)');%+(-2*a3/pi)*c3/((4*(x-b3)^2)+c3^2)+(-2*a4/pi)*c4/((4*(x-b4)^2)+c4^2)');   %Lorentzfunktion
        opts = fitoptions('y0+(2*a1/pi)*c1/((4*(x-b1)^2)+c1^2)+(2*a2/pi)*c2/((4*(x-b2)^2)+c2^2)');%+(-2*a3/pi)*c3/((4*(x-b3)^2)+c3^2)+(-2*a4/pi)*c4/((4*(x-b4)^2)+c4^2)'); %Lorentzfunktion
        opts.Upper = [Inf Inf peak1(index)+0.5*peakposvar peak2(index)+0.5*peakposvar peakbreitemax1 peakbreitemax2 Inf];  %moegliche obere Grenzen fuer a1-4,b1-4,c1-4,y0 
        opts.Lower = [0 0 peak1(index)-0.5*peakposvar peak1(index)-0.5*peakposvar peakbreitemin1 peakbreitemin2 0];       %moegliche untere Grenzen fuer a1-4,b1-4,c1-4,y0 
        opts.MaxFunEvals = 1000;
        
        difb=abs(xAchse-(peak1(index)+peak2(index))/2+0.5*fitbreite2); %Fitbereich
        [dump2,ende]=min(difb);
        difc=abs(xAchse-(peak1(index)+peak2(index))/2-0.5*fitbreite2); %Fitbereich 
        [dump3,start]=min(difc);
    
        [fitspec,gof,output]=fit(xAchse(start:ende), daten(start:ende,index), ftype, opts); %Fitten .. Variablen enthalten Ergebnisse des Fits
        fitparams1(1,index)=fitspec.a1;
        fitparams1(2,index)=fitspec.b1;
        fitparams1(3,index)=fitspec.c1;
        fitparams1(4,index)=fitspec.y0; 
        fitparams2(1,index)=fitspec.a2;
        fitparams2(2,index)=fitspec.b2;
        fitparams2(3,index)=fitspec.c2;
        fitparams2(4,index)=fitspec.y0; 
        
    
        plot(xAchse,fitparams1(4,index)+(2*fitparams1(1,index)/pi)*fitparams1(3,index)./((4*(xAchse(:,1)-fitparams1(2,index)).^2)+fitparams1(3,index)^2)+(2*fitparams2(1,index)/pi)*fitparams2(3,index)./((4*(xAchse(:,1)-fitparams2(2,index)).^2)+fitparams2(3,index)^2),'red', 'LineWidth', 1.5);%+(-2*fitparams(3,1)/pi)*fitparams(3,3)./((4*(x(:,1)-fitparams(3,2)).^2)+fitparams(3,3)^2)+(-2*fitparams(4,1)/pi)*fitparams(4,3)./((4*(x(:,1)-fitparams(4,2)).^2)+fitparams(4,3)^2),'red', 'LineWidth', 1.5);
        waitforbuttonpress;
        end
        
        prompt  = {'Fit erfolgreich? J/N'};
        title   = '';
        lines= 1;
        if Xpoint>0
            def     = {'J'};
        else
            def     = {'N'};
        end
        answer  = inputdlg(prompt,title,lines,def);
        assignin('base','ent',answer{1});
        
        if ent=='N'
            fitparams1(:,index)=0;
            fitparams2(:,index)=0;
        end
    end
    temp=temp+spekprowink(j);
    end

    for index=1:size(fitparams1,2)
        Kurve(:,index)=((2*fitparams1(1,index)/pi)*fitparams1(3,index)./((4*(xAchse(:,1)-fitparams1(2,index)).^2)+fitparams1(3,index)^2)+(2*fitparams2(1,index)/pi)*fitparams2(3,index)./((4*(xAchse(:,1)-fitparams2(2,index)).^2)+fitparams2(3,index)^2));
    end

    temp=0;
    clear yAchse;
    figure
    clims=[0 max(max(Kurve))];

    for j=1:length(spekprowink)
        subplot(length(spekprowink),1,j); 
        yAchse=temp+1:temp+spekprowink(j);
        imagesc(xAchse,yAchse,Kurve(:,(temp+1):(temp+spekprowink(j)))',clims);colormap(gray);
        temp=temp+spekprowink(j);
        if j<length(spekprowink)
            set(gca,'xTickLabel',{' '});
        end
    end
    clear j;
    
    temp=0;
    for j=1:length(spekprowink)
        pol1=[];
        pol2=[];
        for k=temp+1:temp+spekprowink(j)
            if fitparams1(:,k)~=0
                pol1=[pol1 fitparams1(:,k)];
                pol2=[pol2 fitparams2(:,k)];
            end
        end
        temp=temp+spekprowink(j);
        mkdir(dir_name,linienpos1);
        mkdir(dir_name,linienpos2);
        save(strcat(dir_name,linienpos1,'\fitpol',num2str((j-1)*30),'.dat'),'pol1','-ascii');
        save(strcat(dir_name,linienpos2,'\fitpol',num2str((j-1)*30),'.dat'),'pol2','-ascii');
    end
    clear pol1,i,pol2;

    save(strcat(dir_name,linienpos1,'\xAchse.dat'),'xAchse','-ascii');
    save(strcat(dir_name,linienpos2,'\xAchse.dat'),'xAchse','-ascii'); 
    
end
   
   
    

