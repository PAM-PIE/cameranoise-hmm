function [StufenArr]=LifetimeArr2StufenArr(LifetimeArr);

num=length(LifetimeArr.iStufe);
fprintf(1,'%d Stufen-Eigenschaften erstellen: %6d',[num,0]); 
%Init
Stufe.iPos = 0;
Stufe.Start = 0;
Stufe.End = 0;
Stufe.Anzahl = 0;
Stufe.Length = 0;
Stufe.SumIntens = 0;
Stufe.SumPeaks = 0;
Stufe.ProxFact = 0;
%Stufe.ProxFactErr = 0;
Stufe.Efret = 0;
Stufe.r = 0;
Stufe.LL = 0;
Stufe.Intens = zeros(4,1);
Stufe.Peak = zeros(4,1);
Stufe.P = 0;
Stufe.S = 0;
Stufe.PFclass = 0;
Stufe.iBurst = 0;
StufenArr(1) = Stufe;
StufenArr = repmat(StufenArr,num,1);
%calc
iStufe=0;
tic;
for iStufe=1:length(LifetimeArr.iStufe)
  StufenArr(iStufe).iPos=LifetimeArr.iStufe(iStufe);
  StufenArr(iStufe).Start=LifetimeArr.Start(iStufe);
  StufenArr(iStufe).End=LifetimeArr.Start+LifetimeArr.Length(iStufe);
  StufenArr(iStufe).Anzahl=LifetimeArr.StepsCount(iStufe);
  StufenArr(iStufe).Length=LifetimeArr.Length(iStufe);
  StufenArr(iStufe).SumIntens=...
    LifetimeArr.Intens1(iStufe)+...
    LifetimeArr.Intens2(iStufe)+...
    LifetimeArr.Intens3(iStufe)+...
    LifetimeArr.Intens4(iStufe);
  StufenArr(iStufe).SumPeaks=0;
  StufenArr(iStufe).ProxFact=LifetimeArr.ProxFact(iStufe);
  StufenArr(iStufe).Efret=LifetimeArr.EFRET(iStufe);
  StufenArr(iStufe).Intens=[...
    LifetimeArr.Intens1(iStufe),
    LifetimeArr.Intens2(iStufe),
    LifetimeArr.Intens3(iStufe),
    LifetimeArr.Intens4(iStufe)];
  StufenArr(iStufe).PFclass=LifetimeArr.StepsClass(iStufe);
  StufenArr(iStufe).iBurst=LifetimeArr.iBurst(iStufe);
  if toc >= 0.2, fprintf(1,'\b\b\b\b\b\b%6d',[iStufe]); tic; end
end

StufenArr(iStufe+1:end)=[];
fprintf(1,'\b\b\b\b\b\bok.\n'); 
