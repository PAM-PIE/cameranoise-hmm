function ShowTransmat(transmat0,transmat1);

subplot(1,2,1);
image(transmat0,'CDataMapping','scaled');
subplot(1,2,2);
image(transmat1,'CDataMapping','scaled');

load('usermap'); colormap(usermap);
set(gca,'CLim',[0 1]);
colorbar;
