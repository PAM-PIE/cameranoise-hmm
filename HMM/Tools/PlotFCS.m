function PlotFCS(dataFCS, col);

semilogx(dataFCS(:,1)/1000, dataFCS(:,2)-1, 'color', col);
xlabel('\tau/ms');
ylabel('G(\tau)-1');
