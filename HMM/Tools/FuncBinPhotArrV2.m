function dataInt=FuncBinPhotArr(dataDonor,dataAcc,dataALEX,cTimeBase,cTimeBin);
%function dataInt=FuncBinPhotArr(dataDonor,dataAcc,dataALEX,cTimeBase,cTimeBin);
%
%dataInt:    Ausgabe als 3xAnzBurst-Cell-Array
%            1: Donor, 2: Acceptor: 3: ALEX
%dataDonor: Interphotonenabstände der Donor-Photonen in der Einheit cTimeBase
%dataAcc:   Interphotonenabstände der Akzeptor-Photonen in der Einheit cTimeBase
%dataALEX:  Interphotonenabstände der ALEX-Photonen in der Einheit cTimeBase
%cTimeBase:  z.B. 50e-9 Sekunden (b&h), 100e-9 Sekunden (Picoquant)
%cTimeBin:   Zeitauflösung der gebinnten Trajektorie (z.B. 1e-3 Sekunden)

for iBurst=1:length(dataDonor)
    %zeitl. Interphotonenabstände in abs Photoenzeiten umrechnen und runden
    PhotArr1=floor(dataDonor{iBurst}*cTimeBase/cTimeBin+1);
    PhotArr2=floor(dataAcc{iBurst}*cTimeBase/cTimeBin+1);
    if ~isempty(dataALEX{iBurst})
      PhotArrALEX=floor(dataALEX{iBurst}*cTimeBase/cTimeBin+1);
    end
    %BurstLänge bestimmen
    BurstLen=max(PhotArr1(end),PhotArr2(end));
    if ~isempty(dataALEX{iBurst})
      BurstLen=max(BurstLen,PhotArrALEX(end));
    else
    BurstLen=BurstLen+1;
    
    end
    %Trajektorie binnen
    dataInt{1,iBurst}=accumarray(PhotArr1',1,[BurstLen 1])';
    dataInt{2,iBurst}=accumarray(PhotArr2',1,[BurstLen 1])';
    if ~isempty(dataALEX{iBurst})
      dataInt{3,iBurst}=accumarray(PhotArrALEX',1,[BurstLen 1])';
    end
end

end