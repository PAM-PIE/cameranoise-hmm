%% Import Data from Burst Analyzer

function [dataInt, BurstStart, BurstLength] = ImportBAbin(searchpath, searchfilter, savefilename)

filelist = dir(fullfile(searchpath, searchfilter));
savefilename = fullfile(searchpath, [savefilename, '.mat']);
dataInt = {};
BurstCount = length(filelist);
BurstStart = zeros(1,BurstCount);
BurstLength = zeros(1,BurstCount);

if BurstCount>0
    for iBurst = 1:BurstCount
        if ~filelist(iBurst).isdir
            f1 = filelist(iBurst).name;
            f1 = fullfile(searchpath, f1);
            d = importdata(f1, '\t', 1);
            dataInt{1,iBurst} = d.data(:,2)';
            dataInt{2,iBurst} = d.data(:,3)';
            dataInt{3,iBurst} = d.data(:,4)';
            BurstStart(iBurst) = d.data(1,1);
            BurstLength(iBurst) = d.data(end,1)-d.data(1,1)+1;
            delete(f1);
        end    
    end
    save(savefilename, 'dataInt', 'BurstStart', 'BurstLength');
else
    if exist(savefilename,'file')
        disp('Files already converted!');
    else
        disp('No files found!');
    end
end

%#ok<*AGROW>
