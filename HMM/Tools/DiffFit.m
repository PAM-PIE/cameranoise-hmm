%open masterfile with all files to fit
function [] = DiffFit(masterfile)
%masterfile = 'D:\Monika\2006-02-10\c-mit-glucose-a05-77-3kanel-5myw\list_cf1.txt';

bild = 0;    %keine bilder erzeugen

if length(dir(masterfile))==0
    errordlg(sprintf('Masterfile not found!\n%s\n\nProgram execution terminated.',masterfile), 'Auto Diff-FIT');
    return;
end;

[masterfpath,masterfname,masterfext,masterfversn] = fileparts(masterfile);

%Make3D_Plot([masterfpath,'\',masterfname,'_fit.txt'], '', '');

disp(['masterfilename: ',masterfname,masterfext]);
disp(['masterfilepath: ',masterfpath]);
%declare result zipfilename
AppWinzip = ''; %winqueryreg('HKEY_LOCAL_MACHINE', 'SOFTWARE\Microsoft\Windows\CurrentVersion\App Paths\winzip32.exe');
reszipfile = [masterfpath, '\', masterfname, '.zip'];
resFile = [masterfpath, '\', masterfname, '_FitResults.txt'];
fitfilelistname = [masterfpath,'\',masterfname,'_fit.txt'];
fitfilelist = '';

%load all files to fit
ffiles = fopen(masterfile, 'r');
files = textscan(ffiles, '%s',  'delimiter', '\t');  %Dateinamen mit Leerzeichen funktionieren so
files = files{:,1};
fclose(ffiles);

%delete result zipfile, if it exists
if length(dir(reszipfile)) > 0
    delete(reszipfile);
end

disp(sprintf('\nstart calculations of %d files...', length(files)));

fres = fopen(resFile, 'w');
%fprintf(fres, 'File\tNf\tAnteilD1\ttauDiff1\ttauDiff2\tomegaZ\r\n');
for ifile=1:length(files)
    disp(sprintf('File %d of %d',ifile,length(files)));

    %declare filenames and pathes
    [picpath,picname,picext,picversn] = fileparts(files{ifile});
    if strcmp(picpath, '')
        picpath = masterfpath;
    end;
    
    %open datafile
    fid = fopen([picpath, '\', files{ifile}], 'r');
    keystr = '';
    while ~(strcmp(keystr, '[Data]'))
        [keystr, keyval] = strtok(fgetl(fid), '=');
        keyval = keyval(2:length(keyval));
        if strcmp(keystr,'IntensAVG') Iavg = keyval; end
        if strcmp(keystr,'Dauer') tfile = keyval; end
    end
    data = textscan(fid, '%f %f', 'headerlines', 1);
    sourcex = data{:,1};
    sourcey = data{:,2};
    fclose(fid);

    %fit datafiles
    if length(sourcex) >= 2
        [res, ergy, prmval, prmstr] = cfToollog_auto1B2D_fest(sourcex, sourcey, masterfile, files{ifile});
    else
        res = 0;
        ergy = 0;
        prmval = 0;
        prmstr = 0;
    end;
    

    %write header if fit for the first time
    if ftell(fres)==0
        sdummy = 'file\tIavg\ttfile';
        for i=1:length(prmstr)
            sdummy = [sdummy,'\t',prmstr{i}];
        end
        sdummy = [sdummy,'\r\n'];
        fprintf(fres, sdummy);
    end
    
    %write fit result to summery file
    sdummy = files{ifile};
    sdummy = [sdummy,'\t',num2str(Iavg)];
    sdummy = [sdummy,'\t',num2str(tfile)];
    for i=1:length(prmstr)
        sdummy = [sdummy,'\t',num2str(prmval(i))];
    end
    sdummy = [sdummy,'\r\n'];
    fprintf(fres, sdummy);
   
    if bild
        % Plot this fit
        h_ = semilogx(sourcex, sourcey);
        hold on;
        h_ = semilogx(sourcex, ergy, 'color', 'red');
        xlabel('tau/ms');
        ylabel(picext(2:length(picext)));
        title(files{ifile});
        ylim = get(gca, 'YLim');    %1=min, 2=max
        for i=1:length(prmval)
            s = sprintf('%s',prmstr{i});
            text(10, (ylim(2)-ylim(1))*(1-0.05*i)+ylim(1), s);
            s = sprintf('= %3.3f',prmval(i));
            text(40, (ylim(2)-ylim(1))*(1-0.05*i)+ylim(1), s);
        end
        hold off;
    
        %save picture
        picfilename = [picpath, '\', picname, '_cf2.png'];
        I = getframe(gcf);
        imwrite(I.cdata, picfilename);
    end;
    
    %export fitcurve to asciifile
    fitfilename = [picpath, '\', picname, '_fit', picext];
    fitfilelist = [fitfilelist, picname, '_fit', picext, '\r\n'];

    ffunc = fopen(fitfilename, 'w');
    fprintf(ffunc, '%s\t%s\t%s\r\n', 'tau_ms', picext(2:length(picext)), 'fit');
    fclose(ffunc);
    dlmwrite(fitfilename, [sourcex sourcey ergy], '-append', 'delimiter', '\t', 'newline', 'pc');
    
    %move fitcurve and picfile to zip-archive
    if ~strcmp(AppWinzip, '')  %if WinZip is installed
        system([AppWinzip, ' -min -m "', reszipfile, '" "', picfilename, '"']);
        system([AppWinzip, ' -min -m "', reszipfile, '" "', fitfilename, '"']);
    end;    
end
fclose(fres);
if ~strcmp(AppWinzip, '')  %if WinZip is installed
    system([AppWinzip, ' -min -m "', reszipfile, '" "', resFile, '"']);
end

fres = fopen(fitfilelistname,'w');
fprintf(fres, fitfilelist);
fclose(fres);

disp('All calculations are finished.');
%Make3D_Plot(fitfilelistname, '', '');

