function [] = GenListFiles(sVerzStamm, sPattern)

listVerz = dir(sVerzStamm);
sResFile = 'dir_cf2.txt';

%Generate Res-FileList
ListResFiles = {};
sDirHeader = {};
for i=3:length(listVerz)
    if (listVerz(i).isdir==1)
        sVerz = [sVerzStamm, '\', listVerz(i).name];

        disp(sVerz);
        files = dir([sVerz, '\', sPattern]);
        filenames = {files(:).name}';

        ff = fopen([sVerz, '\', sResFile], 'w');
        fprintf(ff, '%s\r\n', filenames{:});
        fclose(ff);
    end;
end;

disp('Listfiles created.');
