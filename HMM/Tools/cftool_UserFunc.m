function [cf_] = cftool_UserFunc(datax,datay,sFunc,cellcoeffname,cellcoeffval,cellfixname,cellfixval);
%cftool_LifetimeFit2D    Create plot of datasets and fits

datax = datax(:);
datay = datay(:);

fo_ = fitoptions(...
    'method','NonlinearLeastSquares',...
    'Algorithm','Levenberg-Marquardt',...
    'DiffMaxChange',0.001,'MaxFunEvals',10000,'MaxIter',1000,'TolFun',1e-008,'TolX',1e-008);
st_ = [cellcoeffval{:}];
set(fo_,'Startpoint',st_);

ft_ = fittype(sFunc,...
      'dependent',{'y'},'independent',{'x'},...
      'coefficients',cellcoeffname,...
      'problem',cellfixname);

% Fit this model using new data
cf_ = fit(datax,datay,ft_ ,fo_,'problem',cellfixval);
