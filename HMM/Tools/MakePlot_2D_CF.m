%open masterfile with all files to fit
function [] = MakePlot_2D_CF(sVerz, sResFiles)

listVerz = dir(sVerz);

%Generate Res-FileList
ListResFiles = {};
sDirHeader = {};
PNGFile = {};
for i=3:length(listVerz)
    if (listVerz(i).isdir==1)
        NewFile = [sVerz, '\', listVerz(i).name, '\', sResFiles];
        if ~isempty(dir(NewFile))
            ListResFiles = {ListResFiles{:}, NewFile};
            sDirHeader = {sDirHeader{:}, listVerz(i).name};
            PNGFile = {PNGFile{:}, [sVerz, '\', listVerz(i).name, '\', 'Ausw2D_']};
            disp(NewFile);
        end;
    end;
end;


%load files and create diags
for i=1:length(ListResFiles)
    ff = fopen(ListResFiles{1,i}, 'r');
    resDataRows = textscan(ff, '%s',  'delimiter', '\n');
    fclose(ff);
    resDataRows = resDataRows{1,1};
    resData = [];
    resDataHeader = strread(resDataRows{1,1}, '%s', 'delimiter', '\t')';
    for j=2:length(resDataRows)
        [resDataRow] = strread(resDataRows{j,1}, '%s', 'delimiter', '\t')';
        resData = [resData; resDataRow];
    end;
    colSkalLight = 0.2;
    hold off;

    %Diag t Int
    colX = 0;
    colY = 2;
    sourcey = [str2double({resData{:,colY}}')];
    sourcex = [1:size(sourcey,1)]'*10;
    plot(sourcex, sourcey);
    hold on;
    lim = get(gca, 'YLim');    %1=min, 2=max
    set(gca, 'YLim', [0 lim(2)]);    %1=min, 2=max
    %set(gca, 'XLim', [0 3]);
    %set(gca, 'YLim', [0 1]);  
    xlabel('time/s');
    ylabel([resDataHeader{colY},'/kcps']);
    title(sDirHeader(i));
    aPNGFile = [PNGFile{i}, 't', '_', resDataHeader{colY}, '.png'];
    %save picture
    I = getframe(gcf);
    imwrite(I.cdata, aPNGFile);
    hold off;
    fprintf('%d/%d: %s saved.\n', [i, length(ListResFiles), aPNGFile]);

    %Diag t Nf
    colX = 0;
    colY = 5;
    sourcey = [str2double({resData{:,colY}}')];
    sourcex = [1:size(sourcey,1)]'*10;
    plot(sourcex, sourcey);
    hold on;
    lim = get(gca, 'YLim');    %1=min, 2=max
    set(gca, 'YLim', [0 lim(2)]);    %1=min, 2=max
    %set(gca, 'XLim', [0 3]);
    %set(gca, 'YLim', [0 1]);  
    xlabel('time/s');
    ylabel(resDataHeader{colY});
    title(sDirHeader(i));
    aPNGFile = [PNGFile{i}, 't', '_', resDataHeader{colY}, '.png'];
    %save picture
    I = getframe(gcf);
    imwrite(I.cdata, aPNGFile);
    hold off;
    fprintf('%d/%d: %s saved.\n', [i, length(ListResFiles), aPNGFile]);
    
    %Diag t A2 (A1)
    colX = 0;
    colY = 6;
    sourcey = [str2double({resData{:,colY}}')];
    sourcex = [1:size(sourcey,1)]'*10;
    plot(sourcex, sourcey, 'color', [1-colSkalLight 1-colSkalLight 1]);
    hold on;
    sourcey = [1-str2double({resData{:,colY}}')];
    sourcex = [1:size(sourcey,1)]'*10;
    plot(sourcex, sourcey, 'color', [0 0 1]);
    %set(gca, 'XLim', [0 3]);
    set(gca, 'YLim', [0 1]);  
    xlabel('time/s');
    ylabel('A2');
    title(sDirHeader(i));
    aPNGFile = [PNGFile{i}, 't', '_', 'A2', '.png'];
    %save picture
    I = getframe(gcf);
    imwrite(I.cdata, aPNGFile);
    hold off;
    fprintf('%d/%d: %s saved.\n', [i, length(ListResFiles), aPNGFile]);
    
    %Diag t tau2 (tau1)
    colX = 0;
    colY = 8;
    sourcey = [log10(str2double({resData{:,colY-1}}'))];
    sourcex = [1:size(sourcey,1)]'*10;
    plot(sourcex, sourcey, 'color', [1-colSkalLight 1-colSkalLight 1]);
    hold on;
    sourcey = [log10(str2double({resData{:,colY}}'))];
    sourcex = [1:size(sourcey,1)]'*10;
    plot(sourcex, sourcey, 'color', [0 0 1]);
    sourceA1 = [str2double({resData{:,6}}')];
    sourceTau1 = [log10(str2double({resData{:,7}}'))];
    sourceTau2 = [log10(str2double({resData{:,8}}'))];
    for j=1:length(sourcex)
        sourcey(j) = sourceA1(j)*sourceTau1(j)+(1-sourceA1(j))*sourceTau2(j);
    end;
    sourcex = [1:size(sourcey,1)]'*10;
    plot(sourcex, sourcey, 'color', [0.5 0 1]);
    
    %set(gca, 'XLim', [0 3]);
    set(gca, 'YLim', [-2 3]);  
    xlabel('time/s');
    ylabel(resDataHeader{colY});
    title(sDirHeader(i));
    aPNGFile = [PNGFile{i}, 't', '_', resDataHeader{colY}, '.png'];
    %save picture
    I = getframe(gcf);
    imwrite(I.cdata, aPNGFile);
    hold off;
    fprintf('%d/%d: %s saved.\n', [i, length(ListResFiles), aPNGFile]);    
    
    %Diag A2 log tau2
    colX = 8;
    colY = 6;
    sourcex = [log10(str2double({resData{:,colX}}'))];
    sourcey = [1-str2double({resData{:,colY}}')];
    scatter(sourcex, sourcey);
    hold on;
    set(gca, 'XLim', [-2 3]);
    set(gca, 'YLim', [0 1]);  
    xlabel(['log10(',resDataHeader{colX},'/ms)']);
    ylabel('A2');
    title(File2Klartext(sVerz,sDirHeader(i)));
    aPNGFile = [PNGFile{i}, resDataHeader{colX}, '_', 'A2', '.png'];
    %save picture
    I = getframe(gcf);
    imwrite(I.cdata, aPNGFile);
    hold off;
    fprintf('%d/%d: %s saved.\n', [i, length(ListResFiles), aPNGFile]);
end;

disp('All diagrams created.');

