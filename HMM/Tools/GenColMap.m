function [cmap] = GenColMap(nodes, hues)
% Thsi function generates a colormap on nodes
% nodes is a m dim vector, colors is a m-by-3 matrix

for i=1:length(nodes)
    icol(i)=fix(Skal(nodes(i),nodes(1),nodes(end),1,length(colormap)));
end
huemap = interp1(icol, hues, 1:length(colormap));
huemap = [huemap; 1*ones(1,length(huemap)); 1*ones(1,length(huemap))];
cmap = hsv2rgb(huemap');

end

function [y] = Skal(x,x1,x2,y1,y2);
y=(x-x1)/(x2-x1)*(y2-y1)+y1;
end
