function [sText] = File2Klartext(sVerz, aFilename)

FilenameKlartext = [sVerz, '\', 'File2Klartext.txt'];
if isempty(dir(FilenameKlartext))
    disp(['File "', FilenameKlartext, '" not found.']);
    finish;
end
    
ffiles = fopen(FilenameKlartext, 'r');
files = textscan(ffiles, '%s %s',  'delimiter', '\t');  %Dateinamen mit Leerzeichen funktionieren so
files = [files{1,:}];
fclose(ffiles);
sText = '';

for i=1:size(files,1)
    if strcmpi(char(files(i,1)), aFilename)
        sText=char(files(i,2));
    end
end

if isempty(sText)
    sText=aFilename;
end
