function cfTool_auto2D(sourcex,sourcey)
%CFTOOL_AUTO2D    Create plot of datasets and fits
%   CFTOOL_AUTO2D(SOURCEX,SOURCEY)
%   Creates a plot, similar to the plot in the main curve fitting
%   window, using the data that you provide as input.  You can
%   apply this function to the same data you used with cftool
%   or with different data.  You may want to edit the function to
%   customize the code and this help message.
%
%   Number of datasets:  1
%   Number of fits:  1

 
% Data from dataset "Source":
%    X = sourcex:
%    Y = sourcey:
%    Unweighted
%
% This function was automatically generated on 10-Feb-2006 18:05:52

% Set up figure to receive datasets and fits
f_ = clf;
figure(f_);
legh_ = []; legt_ = {};   % handles and text for legend
xlim_ = [Inf -Inf];       % limits of x axis
ax_ = subplot(1,1,1);
set(ax_,'Box','on');
axes(ax_); hold on;

 
% --- Plot data originally in dataset "Source"
sourcex = sourcex(:);
sourcey = sourcey(:);
h_ = line(sourcex,sourcey,'Parent',ax_,'Color',[0.333333 0 0.666667],...
     'LineStyle','none', 'LineWidth',1,...
     'Marker','.', 'MarkerSize',12);
xlim_(1) = min(xlim_(1),min(sourcex));
xlim_(2) = max(xlim_(2),max(sourcex));
legh_(end+1) = h_;
legt_{end+1} = 'Source';

% Nudge axis limits beyond data limits
if all(isfinite(xlim_))
   xlim_ = xlim_ + [-1 1] * 0.01 * diff(xlim_);
   set(ax_,'XLim',xlim_)
end


% --- Create fit "fit 1"
fo_ = fitoptions('method','NonlinearLeastSquares','Robust','On','Lower',[0 0 0 0 0 ],'Upper',[1 100 1 1000 1000 ],'DiffMaxChange',1,'MaxFunEvals',1293,'MaxIter',888);
st_ = [0.5 4 0.1 0.1 0.3 ];
set(fo_,'Startpoint',st_);
ft_ = fittype('(1/Nf)*(AnteilD1*((1/(1+x/tauDiff1))*(1/(1+omegaZ*omegaZ*x/tauDiff1))^0.5)+(1-AnteilD1)*((1/(1+x/tauDiff2))*(1/(1+omegaZ*omegaZ*x/tauDiff2))^0.5))' ,...
     'dependent',{'y'},'independent',{'x'},...
     'coefficients',{'AnteilD1', 'Nf', 'omegaZ', 'tauDiff1', 'tauDiff2'});

% Fit this model using new data
cf_ = fit(sourcex,sourcey,ft_ ,fo_);

% Or use coefficients from the original fit:
if 0
   cv_ = {0.7953642099933, 4.773682854151, 2.220455107498e-014, 0.1965567189935, 1000};
   cf_ = cfit(ft_,cv_{:});
end

% Plot this fit
h_ = plot(cf_,'fit',0.95);
legend off;  % turn off legend from plot method call
set(h_(1),'Color',[1 0 0],...
     'LineStyle','-', 'LineWidth',2,...
     'Marker','none', 'MarkerSize',6);
legh_(end+1) = h_(1);
legt_{end+1} = 'fit 1';

hold off;
legend(ax_,legh_, legt_);
