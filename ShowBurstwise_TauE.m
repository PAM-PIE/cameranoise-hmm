%Auswertung Burstwise Tau �ber E
%clear variables; 

filepath_='D:\Schluesche\Auswertung\';
bSave=true;
bLargeFonts=true;
bMWline=false;
bGlobalStates=true;
Q=1;

%if bEinzelPlots, close all; else clf; end

bLoad=false;
if ~exist('iSample_','var'), bLoad=true; end
if ~exist('iState','var'), bLoad=true; end
if ~bLoad
  if (iSample_~=iSample) | (iState_~=iState), bLoad=true; end
end

if bLoad
  filepath=['D:\Schluesche\MainData\',sSample{iSample},'\',sState{iState},'FRET\'];
  if ~isnan(Q)
    filename=sprintf('Ausw%s_%s_m%02d.mat','24',sBurstSel,Q);
    load([filepath,filename],'mu1','dwelltime1','gamma');
  end
  filename=sprintf('Ausw%s_%s_m%02d.mat',sAuswVer,sBurstSel,Anz);
  load([filepath,filename],'MultiQData','MaxAnzBurst','AlldataInt','AlldataPF','R0','gamma','TimeBin');
  iSample_=iSample;
  iState_=iState;
  AllStufen=[];
  AnzBursts=length(MultiQData{1});
  for iBurst=1:AnzBursts
    AllStufen{iBurst}=MultiQData{1}(iBurst).AllStufen;
    dataLL{iBurst}=[];
  end
  load('Colormap_plot3.mat'); 
end

StufenArr=StufenHMM2StufenArrFast(AllStufen);

iStufe0=1;
for iStufe=2:length(StufenArr.MW)
  if (abs(StufenArr.MW(iStufe)-StufenArr.MW(iStufe0))<=DeltaPF) & ...
      (StufenArr.iBurst(iStufe)==StufenArr.iBurst(iStufe0))
    StufenArr.End(iStufe0)=StufenArr.End(iStufe);
    StufenArr.MW(iStufe)=NaN;
  else
    iStufe0=iStufe;
  end
end
idx=find(isnan(StufenArr.MW));
sFields=fieldnames(StufenArr);
for iField=1:length(sFields)
  StufenArr.(sFields{iField})(idx)=[];
end
StufenArr=StufenArrExpand(StufenArr,[],AlldataPF);
DoubleArr=StufenFilterFast(StufenArr,'2+',true);
TripleArr=StufenFilterFast(StufenArr,'3+',true);

if bEinzelPlots, close all; figure; else subplot(3,1,1); end
colormap(cmap);
ylim_=log10([5 5000]);
xlim_=[0 1];
img=ShowPlot2D([StufenArr.Efret],log10(max(1,StufenArr.Length*TimeBin)),xlim_,ylim_,[101 101],[3 6],1);
hold on

if bMWline
  dwellx=ylim_(1):diff(ylim_)/(size(img,1)-1):ylim_(2);
  dwell=(img'*(10.^dwellx'))./(sum(img',2)+(sum(img',2)==0));
  dwell(find(dwell==0))=NaN;
  Ex=xlim_(1):diff(xlim_)/(size(img,2)-1):xlim_(2);
  plot(Ex',log10(dwell),'w','LineWidth',2);
end
%mu1=MetaQData{iSample,iState}{4}.mu1;
%dwelltime1=MetaQData{iSample,iState}{4}.dwelltime1;
if bGlobalStates
  E=CalcEfretFromPF(mu1,gamma);
  E12=nchoosek(E,2);
  E12=[[E12(:,1) E12(:,2)];[E12(:,2) E12(:,1)];[E(:) E(:)]];
  plot(E,log10(dwelltime1),'+w','MarkerSize',12,'LineWidth',4);
  for q=1:length(mu1)
    text(E(q),log10(dwelltime1(q))+3/10,int2str(q),...
      'Color','w',...
      'FontWeight','bold',...
      'FontSize',22,...
      'HorizontalAlignment','center',...
      'VerticalAlignment','middle');
  end
end
set(gca,'PlotBoxAspectRatio',[1 1 1]);
title(sprintf('%s %s (%s V%s)',sSample{iSample},sState{iState},sBurstSel,sAuswVer));
ylabel(['state duration/ms \Delta\mu=',sprintf('%0.2f',DeltaPF)]);
if ~bLargeFonts
  set(gca,'YTick',log10([5 10 20 50 100 200 500 1000 2000 5000]));
  set(gca,'YTickLabel',[5 10 20 50 100 200 500 1000 2000 5000]);
else
  set(gca,'YTick',log10([10 100 1000]));
  set(gca,'YTickLabel',[10 100 1000]);
  set(gca,'FontSize',22);
%   set(gcf,'toolbar','none');
%   set(gcf,'menubar','none');
end
box on;
set(gcf,'InvertHardcopy','off');
set(gcf,'PaperPositionMode','auto');
filename_=sprintf('Ausw%s_%s %s_%s TauE_dmu0-%02.0f',...
  sAuswVer,sBurstSel,sSample{iSample},sState{iState},DeltaPF*100);
if bLargeFonts, filename_=[filename_,'_LF']; end
if bSave
  saveas(gcf,[filepath_,filename_],'png');
  dlmwrite([filepath_,filename_,'a.txt'], img, 'delimiter', '\t')
  if bMWline, dlmwrite([filepath_,filename_,'b.txt'], [Ex',dwell], 'delimiter', '\t'); end
  dlmwrite([filepath_,filename_,'c.txt'], [StufenArr.Efret',log10(max(1,StufenArr.Length'*TimeBin))], 'delimiter','\t');
  save([filepath_,filename_],'StufenArr','TimeBin');
end


if bEinzelPlots, figure; else subplot(3,1,2); end
colormap(cmap);
data2DMain=ShowPlot2D([DoubleArr(1).Efret],[DoubleArr(2).Efret],[0 1],[0 1],[101 101],[1 1]*2);
hold on
if bGlobalStates
  plot(E12(:,1),E12(:,2),'+w','MarkerSize',12,'LineWidth',4);
end
set(gca,'PlotBoxAspectRatio',[1 1 1]);
title(sprintf('%s %s (%s V%s)',sSample{iSample},sState{iState},sBurstSel,sAuswVer));
ylabel(['HMM Burstwise diff=1 \Delta\mu=',sprintf('%.2f',DeltaPF)]);
set(gca,'FontSize',22);
set(gca,'YTick',[0 0.5 1]);
set(gcf,'InvertHardcopy','off');
set(gcf,'PaperPositionMode','auto');
filename_=sprintf('Ausw%s_%s %s_%s Trans2D_diff1_dmu0-%02.0f',...
  sAuswVer,sBurstSel,sSample{iSample},sState{iState},DeltaPF*100);
if bLargeFonts, filename_=[filename_,'_LF']; end
if bSave
  saveas(gcf,[filepath_,filename_],'png');
  dlmwrite([filepath_,filename_,'a.txt'], data2DMain, 'delimiter', '\t')
end


if bEinzelPlots, figure; else subplot(3,1,3); end
colormap(cmap);
data2DMain=ShowPlot2D([TripleArr(1).Efret],[TripleArr(3).Efret],[0 1],[0 1],[101 101],[1 1]*2);
hold on
if bGlobalStates
  plot(E12(:,1),E12(:,2),'+w','MarkerSize',12,'LineWidth',4);
end
set(gca,'PlotBoxAspectRatio',[1 1 1]);
title(sprintf('%s %s (%s V%s)',sSample{iSample},sState{iState},sBurstSel,sAuswVer));
ylabel(['HMM Burstwise diff=2 \Delta\mu=',sprintf('%.2f',DeltaPF)]);
set(gca,'FontSize',22);
set(gca,'YTick',[0 0.5 1]);
set(gcf,'InvertHardcopy','off');
set(gcf,'PaperPositionMode','auto');
filename_=sprintf('Ausw%s_%s %s_%s Trans2D_diff2_dmu0-%02.0f',...
  sAuswVer,sBurstSel,sSample{iSample},sState{iState},DeltaPF*100);
if bLargeFonts, filename_=[filename_,'_LF']; end
if bSave
  saveas(gcf,[filepath_,filename_],'png');
  dlmwrite([filepath_,filename_,'a.txt'], data2DMain, 'delimiter', '\t')
end
