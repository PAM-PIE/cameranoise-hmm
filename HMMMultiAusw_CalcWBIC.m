%HMMMultiAusw_CalcWBIC
sSample={'DNA3','DNA12','DNA18','DNA19','Sim1','Sim2'};
sState={'Dynamic','Steady'};

Anz=7;
bShowLL=false;
bSim=~isempty(strfind(sSample{iSample},'Sim'));
if ~bSim
  filepath1=['D:\Schluesche\Maindata\',sSample{iSample},'\',sState{1},'FRET\'];
  filepath2=['D:\Schluesche\Maindata\',sSample{iSample},'\',sState{2},'FRET\'];
  filename=sprintf('Ausw%s_%s_m%02d',sAuswVer,sBurstSel,Anz);
else
  filepath1=['D:\Schluesche\Maindata\',sSample{iSample},'\'];
  filename=sprintf('Ausw%s_%s_m%02d',sAuswVer,sSample{iSample},Anz);
end
[MultiQ,Lzero1,H1]=CalcHessLogLx([filepath1,filename]);
if ~bSim
  [MultiQ,Lzero2,H2]=CalcHessLogLx([filepath2,filename]);
end
%Lzero2=Lzero1; H2=H1;
% AnzN
% DNA3dyn V20 Burst50 12339
% Sim1 V20 15000
%AnzN=12339;
for iMulti=1:length(MultiQ)
  %AnzParam=2*iMulti+iMulti*(iMulti-1);
  %BIC1(iMulti)=-2*Lzero1(iMulti)+log(det(-H1{iMulti}));
  %BIC2(iMulti)=-2*Lzero2(iMulti)+AnzParam*log(AnzN);
  BIC1(iMulti)=2*Lzero1(iMulti)-log(det(-H1{iMulti}));
  if ~bSim
    BIC2(iMulti)=2*Lzero2(iMulti)-log(det(-H2{iMulti}));
  end
end

save(['D:\Schluesche\Auswertung\',...
  sprintf('Ausw%s_%s %s SIC',...
  sAuswVer,sBurstSel,sSample{iSample})],'MultiQ','Lzero1','Lzero2','H1','H2','BIC1','BIC2');

OrigExport=[MultiQ',Lzero1',Lzero2',BIC1',BIC2'];
wMax=[]; iMax=[];
[wMax(1) iMax(1)]=max(BIC1);
if ~bSim
  [wMax(2) iMax(2)]=max(BIC2);
end

subplot(2,1,2);
if bShowLL, subplot(Anz,Anz,[Anz*2+1 Anz*Anz]); end
hold off
plot(MultiQ,BIC1,'.-','DisplayName',sState{1});
hold on
if ~bSim
  plot(MultiQ,BIC2,'.-','color',[0 .5 0],'DisplayName',sState{2});
  legend(sState,'Location',[.5 .49 .4 .05]);
end
plot(iMax,wMax,'or');
xlabel('number of states');
ylabel('SIC');
%ylabel('\Sigma\chi�');
title(sprintf('%s (%s V%s)',sSample{iSample},sBurstSel,sAuswVer));
%ylim([0 2]);
xlim([0 Anz+1]);

set(gcf,'InvertHardcopy','off');
set(gcf,'PaperPositionMode','auto');
saveas(gcf,['D:\Schluesche\Auswertung\',...
  sprintf('Ausw%s_%s %s SIC',...
  sAuswVer,sBurstSel,sSample{iSample})],'png');
