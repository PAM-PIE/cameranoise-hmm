function [datanew]=AddErrInfo(data,dataerr)

datanew=data;
for iMol=1:length(data)
  ixAcc=find(data(iMol).xcoordAcc==[dataerr(:).xcoordAcc]);
  iyAcc=find(data(iMol).ycoordAcc==[dataerr(:).ycoordAcc]);
  iMolErr=[];
  for i=1:length(ixAcc)
    j=find(ixAcc(i)==iyAcc);
    if length(j)==1, iMolErr=ixAcc(i); end
  end
  if ~isempty(iMolErr)
    datanew(iMol).AccErr=dataerr(iMolErr).AccErr;
    datanew(iMol).DonorErr=dataerr(iMolErr).DonorErr;
    datanew(iMol).AccKorr=dataerr(iMolErr).Acc;
    datanew(iMol).DonorKorr=dataerr(iMolErr).Donor;
  else
    fprintf('F�r Mol #%d wurden keine Rauschinformationen gefunden!\n',iMol);
  end
end
