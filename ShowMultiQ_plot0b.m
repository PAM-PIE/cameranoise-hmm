% Show RawData Variance Analysis
%clear variables;
sSample={'DNA3','DNA12','DNA18','DNA19','Donor_Only','Acceptor_Only'};
sState={'Dynamic','Steady'};

iSample=1;
iState=1;

MaxAnzBurst = Inf;
sBurstSel='All';
cIntDivider=1;
Ivaradd=0; %0.002;
len=50;
TimeBin=5;
TimeBinNew=TimeBin;
HMMVersion;

filepath=['D:\Schluesche\Maindata\',sSample{iSample},'\'];
if iSample<5
  filepath=[filepath,sState{iState},'FRET\'];
end
filename='TBPDNA_*.txt';
if exist('filepath_')
  if ~strcmp(filepath,filepath_)
    HMMLoadData;
  end
else
  HMMLoadData;
end
filepath_=filepath;

AnzBursts=length(AlldataPF);
dataMean1=[];
dataMean2=[];
dataVar1=[];
dataVar2=[];
for iBurst=1:AnzBursts
  I1=max(-Inf,AlldataInt{1,iBurst}-IntNoise{1,iBurst});
  I2=max(-Inf,AlldataInt{2,iBurst}-IntNoise{2,iBurst});
  I1=I1/cIntDivider; I2=I2/cIntDivider;
  I1=I1(1:div(length(I1),len)*len);
  I2=I2(1:div(length(I2),len)*len);
  I1=reshape(I1,len,[]);
  I2=reshape(I2,len,[]);
  dataMean1=[dataMean1,mean(I1)];
  dataVar1=[dataVar1,var(I1)];
  dataMean2=[dataMean2,mean(I2)];
  dataVar2=[dataVar2,var(I2)];
end
hold off
axmax=min(max([dataMean1,dataMean2]),max([dataVar1,dataVar2]));
axmax=5e3;
%Fitx=1:axmax; %lin-spaced x-values
Fitx=exp(log(1):0.1:log(axmax)); %log-spaced x-values
Fity1=(Fitx+mean([IntNoise{1,:}]))*(2);
Fity2=(Fitx+mean([IntNoise{2,:}]))*(2);
loglog(Fitx,Fity1,'color',[0 .5 0]);
hold on
loglog(Fitx,Fity2,'color',[1 0 0]);
loglog(dataMean1,dataVar1,'.','color',[0 .5 0]);
loglog(dataMean2,dataVar2,'r.');
axis([1 axmax 1 axmax]);

stitle=sSample{iSample};
if iSample<5, stitle=[stitle,' ',sState{iState}]; end
stitle=[stitle,' ',sBurstSel]; 
title(stitle,'Interpreter','none');
xlabel('mean');
ylabel('variance');

OrigExport=[];
OrigExport=[dataMean1(:),dataVar1(:),dataMean2(:),dataVar2(:)];
OrigExport(1:length(Fitx),5)=Fitx;
OrigExport(1:length(Fitx),6)=Fity1;
OrigExport(1:length(Fitx),7)=Fity2;

% set(gcf,'InvertHardcopy','off');
% set(gcf,'PaperPositionMode','auto');
% saveas(gcf,['PhotStatistik ',stitle],'png');
