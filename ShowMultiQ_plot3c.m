%ShowMultiQ_plot3c
%Transition rates
sSample={'DNA3','DNA12','DNA18','DNA19'};
sState={'Dynamic','Steady'};

% sAuswVer='13';
% sBurstSel='All'; %'Burst50';
% iState=1;
% iSample1=3;
% iSample2=4;
% cShow=1;
AnzArr=6:-1:3;
cShowArr={'mu','seq','RMS'};
NSeq=3; %Sequence Depth
bShowInitSeq=false;
cLabelCount=6;
%1: Mu
%2: Seq-Histogramme
%3: RMS-Verteilung

for iAnz=1:length(AnzArr)
  Anz=AnzArr(iAnz);
  TimeBin=5;
  xhistMu=0:0.02:1;
  dxhistMu=xhistMu(2)-xhistMu(1);
  K={};
  W={};
  Mu1={};
  yhistMu={};
  yhistSeq={};
  index={};
  Test1=[];
  subplot(length(AnzArr),1,iAnz);
  cla; hold on;
  for iSample=[iSample1,iSample2]

    if ~exist('MetaQData')
      filepath_=['D:\Schluesche\Maindata\'];
      load([filepath_,'Ausw',sAuswVer,'_',sBurstSel,'_MetaData.mat']);
      load([filepath_,'Ausw',sAuswVer,'_',sBurstSel,'_MetaStufen.mat']);
    end
	  filepath_=['D:\Schluesche\Maindata\',sSample{iSample},'\',sState{iState},'FRET\'];
  	load([filepath_,'Ausw',sAuswVer,'_',sBurstSel,'_m0',int2str(Anz),'.mat'],'StufenArr');
    %StufenArr=MetaStufenArr{iSample,iState,Anz};

    MultiQData=MetaQData{iSample,iState};
    transmat1=MultiQData{Anz}.transmat1;
    mu1=MultiQData{Anz}.mu1;
    Q=length(diag(transmat1));
    Qo=length(mu1);
    %K{end+1}=TimeBin./logm(transmat1(1:Qo,1:Qo));
    K{end+1}=(transmat1(1:Qo,1:Qo));
    Mu1{end+1}=mu1;

    DoubleArr=StufenFilter(StufenArr,'2+',true);
    W{end+1}=hist3([[DoubleArr(:,1).PFclass]',[DoubleArr(:,2).PFclass]'],{1:Q,1:Q})';
    W{end}=W{end}+eye(Q).*repmat(hist([StufenArr.PFclass],1:Q),Q,1);

    if NSeq==2
      MultiArr=DoubleArr;
    else
      MultiArr=StufenFilter(StufenArr,[int2str(NSeq),'+'],true);
    end
    index{end+1}=reshape([MultiArr.PFclass]-1,[],NSeq);

    idx=index{end}*(Q.^((NSeq-1):-1:0))';
    xhistSeq=0:1:Q^NSeq-1;
    yhistSeq{end+1}=hist(idx,xhistSeq);
    %yhistMu{end+1}=hist([StufenArr.ProxFact],xhistMu);
    y_=max(0,min(1,[StufenArr.ProxFact]));
    w_=[StufenArr.Length];
    idx_=round(y_*(length(xhistMu)-1))+1;
    yhistMu{end+1}=accumarray(idx_',w_,size(xhistMu'))';    
  end

  iperms=perms(1:Q);
  for q=1:Q, Test1(:,q)=q+10*(1:Q); end
  idx1=index{1};

  yhist1=yhistSeq{1};
  diff=[];
  fprintf('\n');
  for i=1:size(iperms,1)
    ip=iperms(i,:);
    idx2=index{2};
    for ni=1:NSeq
      idx2(:,ni)=ip(idx2(:,ni)+1)'-1;
    end
    idx=idx2*(Q.^((NSeq-1):-1:0))';
    yhist2=hist(idx,xhistSeq);
    diff(i)=sqrt(sum((yhist1-yhist2).^2));
  end
  [w,j]=sort(diff);
  imax=10;
  if length(j)<imax
    imax=length(j)
  else
    fprintf('  ');
    fprintf('%d',iperms(j(end),:));
    fprintf(': %.2f\n',diff(j(end)));
    fprintf('  ...\n');
  end
  for i=imax:-1:1
    fprintf('  ');
    fprintf('%d',iperms(j(i),:));
    fprintf(': %.2f\n',diff(j(i)));
  end
  for i=1:Q
    fprintf('  %.2f -> %.2f\n',Mu1{1}(i),Mu1{2}(iperms(j(1),i)));
  end

  if bShowInitSeq
    ip=1:Q;
  else
    ip=iperms(j(1),:);
  end
  switch cShow
    case 1
      yhist1=yhistMu{1};
      yhist2=yhistMu{2};
      yhist1=yhist1./max(yhist1);
      yhist2=yhist2./max(yhist2);
      stairs(xhistMu-dxhistMu/2,Anz+.35*yhist1);  %hist erzeugt zentrierte bins
      stairs(xhistMu-dxhistMu/2,Anz+1-.35*yhist2);
      for i=1:Q
        x1=Mu1{1}(i);
        x2=Mu1{2}(iperms(j(1),i));
        [w1,i1]=min(abs(xhistMu-x1)); %i1=find(xhistMu<=x1,1,'last');
        [w2,i2]=min(abs(xhistMu-x2)); %i2=find(xhistMu+dxhistMu/2<=x2,1,'last');
        plot([x1 x1],Anz+[.35*yhist1(i1) .4],':','color',[1 1 1]*.3);
        plot([x2 x2],Anz+1-[.35*yhist2(i2) .4],':','color',[1 1 1]*.3);
        plot(x1,Anz+.35*yhist1(i1),'r.');
        plot(x2,Anz+1-.35*yhist2(i2),'r.');
        plot([x1 x2],Anz+[.4 1-.4],'r');
      end
      box on
      axis tight;
      set(gca,'YTick',Anz);
    case 2
      idx2=index{2};
      for ni=1:NSeq
        idx2(:,ni)=ip(idx2(:,ni)+1)'-1;
      end
      idx=idx2*(Q.^((NSeq-1):-1:0))';
      yhist2=hist(idx,xhistSeq);
      %yhist2=yhistSeq{2};
      yhist1=yhistSeq{1};
      yhist1=yhist1./max(yhist1);
      yhist2=yhist2./max(yhist2);
      bar(xhistSeq,Anz+.4*yhist1,'EdgeColor','b','FaceColor',[.9 .9 1],...
        'BarWidth',1,'BaseValue',Anz);
      bar(xhistSeq,Anz+1-.4*yhist2,'EdgeColor','b','FaceColor',[.9 .9 1],...
        'BarWidth',1,'BaseValue',Anz+1);
      %stairs(xhistSeq-(xhistSeq(2)-xhistSeq(1))/2,Anz+.4*yhist1);
      %stairs(xhistSeq-(xhistSeq(2)-xhistSeq(1))/2,Anz+1-.4*yhist2);
      [w,i2]=sort(yhist1,'descend');
      for i=1:min(length(i2),cLabelCount) %length(xhistSeq)
        if yhist1(i2(i))>mean(yhist1)
          sdummy=FuncSeqPlus1(dec2base(xhistSeq(i2(i)),Q),NSeq);
          ytext=Anz+.4*yhist1(i2(i))+.05;
          if yhist1(i2(i))>.5, ytext=ytext-.1; end
          text(xhistSeq(i2(i)),ytext,sdummy,...
            'rotation',45,'color','b',...
            'HorizontalAlignment','center','VerticalAlignment','middle');
        end
      end
      [w,i2]=sort(yhist2,'descend');
      for i=1:min(length(i2),cLabelCount) %length(xhistSeq)
        if yhist2(i2(i))>mean(yhist2)
          sdummy=FuncSeqPlus1(dec2base(xhistSeq(i2(i)),Q),NSeq);
          for i3=1:length(sdummy)
            sdummy(i3)=num2str(ip(str2num(sdummy(i3))));
          end
          ytext=Anz+1-(.4*yhist2(i2(i))+.05);
          if yhist2(i2(i))>.5, ytext=ytext+.1; end
          text(xhistSeq(i2(i)),ytext,sdummy,...
            'rotation',-45,'color','b',...
            'HorizontalAlignment','center','VerticalAlignment','middle');
        end
      end
      box on
      axis tight;
      set(gca,'YTick',Anz);
    case 3
      plot(w,'.-');
      set(gca,'YTickMode','auto');
      axis tight;
    case 4
      I=[StufenArr(:).SumIntens]./([StufenArr(:).Length]+1);
      hist(I,0:20:500);
      axis tight;
  end
end

subplot(length(AnzArr),1,1);
title(sprintf('%s-%s (%s V%s)',sSample{iSample1},sSample{iSample2},sBurstSel,sAuswVer));

set(gcf,'InvertHardcopy','off');
set(gcf,'PaperPositionMode','auto');
saveas(gcf,['D:\Schluesche\Auswertung\',...
  sprintf('Ausw%s_%s %s_%s_%s Compare_%s',...
  sAuswVer,sBurstSel,sSample{iSample1},sSample{iSample2},sState{iState},cShowArr{cShow})],'png');
