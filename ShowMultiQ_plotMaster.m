%Master of ShowMultiQ_plot

HMMVersion;

if ~exist('MetaQData')
  filepath_=['D:\Schluesche\Maindata\'];
  load([filepath_,'Ausw',sAuswVer,'_',sBurstSel,'_MetaData.mat']);
  %load([filepath_,'Ausw',sAuswVer,'_',sBurstSel,'_MetaStufen.mat']);
end
TimeBin=5;

%for cAusw=[0,2,21,4,6,7,8,9,41,31]
for cAusw=[31]

switch cAusw
  case 0
    %FRET-Rawdata-Histogramme
    close(gcf); %set(gcf,'WindowStyle','docked');
    for iSample=1:4
      ShowMultiQ_plot0;
    end
  
  case 2
    %FRET-Histogramme
    clf; set(gcf,'WindowStyle','docked');
    for iSample=1:4
      for iState=1:2
        Anz=1;
        ShowMultiQ_plot2;
      end
    end

  case 21
    %Variance
    clf; set(gcf,'WindowStyle','docked');
    Anz=10;
    for iSample=1:4
      for iState=1:2
        ShowMultiQ_plot2b;
      end
    end    

  case 31
    %Transition plots
    close(gcf); %set(gcf,'WindowStyle','normal');
    for iSample=1:4
      for iState=1:2
        for Anz=2:10
          %figure frei
          ShowMultiQ_loaddata;
          diff=1;
          ShowMultiQ_plot3;
          drawnow;
          diff=2;
          ShowMultiQ_plot3;
          drawnow;
        end
      end
    end
    colormap('default');
    
  case 3
    %Promotor comparison
    clf; set(gcf,'WindowStyle','docked');
    for iState=1:1
      cShow=1;
      iSample1=1; iSample2=2; ShowMultiQ_plot3c; drawnow;
      iSample1=3; iSample2=4; ShowMultiQ_plot3c; drawnow;
      iSample1=1; iSample2=3; ShowMultiQ_plot3c; drawnow;
      iSample1=1; iSample2=4; ShowMultiQ_plot3c; drawnow;
      iSample1=2; iSample2=3; ShowMultiQ_plot3c; drawnow;
      iSample1=2; iSample2=4; ShowMultiQ_plot3c; drawnow;
      cShow=2;
      iSample1=1; iSample2=2; ShowMultiQ_plot3c; drawnow;
      iSample1=3; iSample2=4; ShowMultiQ_plot3c; drawnow;
      iSample1=1; iSample2=3; ShowMultiQ_plot3c; drawnow;
      iSample1=1; iSample2=4; ShowMultiQ_plot3c; drawnow;
      iSample1=2; iSample2=3; ShowMultiQ_plot3c; drawnow;
      iSample1=2; iSample2=4; ShowMultiQ_plot3c; drawnow;
    end
    
  case 4
    %dwelltimes
    clf; set(gcf,'WindowStyle','docked');
    for iSample=1%:4
      for iState=1%:2
        for Anz=4%2:10
          %ShowMultiQ_loaddata;
          ShowMultiQ_plot4;
          drawnow;
        end
      end
    end
    
  case 41
    %3-state kinetics
    clf; set(gcf,'WindowStyle','docked');
    iState=1;
    Anz=7;
    for iSample=1:4
      ref1=1;
      ref2=2;
      ShowBurstwise_plot4e;
      drawnow;
%       ref1=3;
%       ref2=4;
%       ShowBurstwise_plot4e;
%       drawnow;
    end
    
  case 5
    %Correlation functions
    clf; set(gcf,'WindowStyle','docked');
    for iSample=1:4
      for iState=1:1
        for Anz=3:6
          ShowMultiQ_loaddata;
          ShowMultiQ_plot5;
          drawnow;
        end
      end
    end
    
  case 6
    %Kinetic-Plots 3D
    clf; set(gcf,'WindowStyle','docked');
    for iSample=1:4
      for iState=1:2
        ShowMultiQ_plot6;
        drawnow;
      end
    end
    colormap('default');

  case 7
    %trajLL
    clf; set(gcf,'WindowStyle','docked');
    for iSample=1:4
      ShowMultiQ_plot7;
      drawnow;
    end
    
  case 8
    %BIC-Plots
    clf; set(gcf,'WindowStyle','docked');
    for iSample=1:4
      ShowBIC;
      drawnow;
    end

  case 9
    %ChiRed-Plots
    clf; set(gcf,'WindowStyle','docked');
    for iSample=1:4
      HMMMultiAusw_CalcChiSquare;
      drawnow;
    end
    
  case 10
    % Tracewise Gamma-Correction
    clf; set(gcf,'WindowStyle','docked');
    for iSample=1:6
      ShowMultiQ_plotGamma;
      drawnow;
    end

  case 11
    % Enhanced BIC
    clf; set(gcf,'WindowStyle','docked');
    for iSample=1:4
      HMMMultiAusw_CalcWBIC;
      drawnow;
    end
    
end

end
