
% Generiert Stufensequenz-Plot

StufenArr=StufenHMM2StufenArrPF(AllStufen,dataInt,5,1,obsvect,true);
DoubleArr=StufenFilter(StufenArr,'2+',true);
mu1sort=sort(mu1);
gridlines=[0,(mu1sort(2:end)+mu1sort(1:end-1))/2,1];
ShowContourPlot([DoubleArr(:,1).ProxFact]', [DoubleArr(:,2).ProxFact]', 0:0.05:1, 0:0.05:1, gridlines, gridlines, true);

transnum=hist3([[DoubleArr(:,1).PFclass]',[DoubleArr(:,2).PFclass]'],{1:Q,1:Q})';

for i=Q:-1:1
  fprintf('%6d',transnum(i,:));
  fprintf('\n');
end