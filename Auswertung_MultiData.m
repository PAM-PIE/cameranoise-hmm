
MultiData=MultiQData{5};
Qo=length(MultiData(1).mu1);
TimeBin = 5;

cAuswTyp = 1;
bCombineFigure = true;

%Daten vorbereiten
datacount=length(MultiData);
arr_mu1=reshape([MultiData(:).mu1],Qo,datacount)';
arr_mu1var=reshape([MultiData(:).mu1var],Qo,datacount)';
arr_dwelltime1=reshape([MultiData(:).dwelltime1],Qo,datacount)';
arr_StepCount=reshape([MultiData(:).StepCount],Qo,datacount)';
%Stufenweise rechnen
StufenLen={};
Efret={};
Efret_std={};
for i=1:length(MultiData)
    if ~isempty(MultiData(i).StepCount)
        StufenLen{i}=([MultiData(i).AllStufen.End]-[MultiData(i).AllStufen.Start])*TimeBin;
        Efret{i}=([MultiData(i).AllStufen.PFmean]);
        Efret_std{i}=([MultiData(i).AllStufen.PFstd]);
    else
        StufenLen{i}=[];
        Efret{i}=[];
        Efret_std{i}=[];
    end
end

if ~bCombineFigure, close all; end

switch cAuswTyp
  case 1
    %HMM-Parameter darstellen
    if bCombineFigure, subplot(2,1,1); else figure; end;
    hold off; cla;
    xcount=100;
    y=arr_mu1(:);
    yw=sqrt(arr_mu1var(:)); %variance -> standard deviation
    yc=arr_StepCount(:);
    len=min(1e4,arr_dwelltime1(:));
    datax=[0,(1:1:xcount)/xcount];
    datay=zeros(length(y),xcount+1);
    for i=1:length(y)
      if (yc(i)>0) & (yw(i)>0) & (yw(i)<.15) & (len(i)>0)
        datay(i,:)=yc(i)*len(i)*normpdf(datax,y(i),yw(i))/TimeBin;
      end
    end
    bar(datax,sum(datay),'EdgeColor','none','BarWidth',1); axis tight;
    plotRelMax(datax,smooth(sum(datay),1));
    xlabel('E_F_R_E_T');
    ylabel('#levels');
    title('HMM-Param');

    %Stufenweise darstellen
    if bCombineFigure, subplot(2,1,2); else figure; end;
    hold off; cla;
    xcount=100;
    y=[Efret{:}]';
    yw=[Efret_std{:}]';
    len=[StufenLen{:}]';
    datax=[0,(1:1:xcount)/xcount];
    datay=zeros(length(y),xcount+1);
    for i=1:length(y)
      if (yw(i)>0) & (yw(i)<.15) & (len(i)>0)
        datay(i,:)=len(i)*normpdf(datax,y(i),yw(i));
      end
    end
    bar(datax,sum(datay),'EdgeColor','none','BarWidth',1); 
    plotRelMax(datax,smooth(sum(datay),1));
    axis tight;
    xlabel('E_F_R_E_T');
    ylabel('#levels');    
    title('HMM-level');
   
  case 2
    %HMM-Parameter darstellen
    if bCombineFigure, subplot(2,1,1); else figure; end;
    xbin=0:1/50:1;
    ybin=(0:1/20:1)*10;
    x=arr_mu1(:);
    xw=arr_mu1var(:);
    y=arr_dwelltime1(:);
    y(find(~isfinite(y)))=0;
    y=log10(y);
    image(xbin,ybin,hist3([x y],{xbin ybin})'); 
    xlabel('E_F_R_E_T');
    ylabel('log_1_0(\tau_0/ms)');
    %set(gca,'ytick',0:2:10);
    %set(gca,'yticklabel',10.^(0:2:10));
    colorbar;
    title('HMM-Param');  
  
    %Stufenweise darstellen
    if bCombineFigure, subplot(2,1,2); else figure; end;
    xbin=0:1/50:1;
    ybin=(0:1/20:1)*10;
    x=[Efret{:}]';
    y=[StufenLen{:}]';
    y(find(~isfinite(y)))=0;
    y=log10(y);
    image(xbin,ybin,hist3([x y],{xbin ybin})'); 
    xlabel('E_F_R_E_T');
    ylabel('log_1_0(\tau_0/ms)');
    %set(gca,'ytick',0:2:10);
    %set(gca,'yticklabel',10.^(0:2:10));
    colorbar;
    title('HMM-level');  
    
  case 3
    %HMM-Parameter darstellen
    %if bCombineFigure, subplot(2,1,1); else figure; end;
    figure; 
    hold off; cla;
    AllStufen={};
    for i=1:length(MultiData)
      AllStufen{i}=MultiData(i).AllStufen;
    end
    StufenArr=StufenHMM2StufenArrPF(AllStufen,AlldataInt,5,1,obsvect,true);
    DoubleArr=StufenFilter(StufenArr,'2+',true);
   	[xg,yg,m]=mdpdhist([DoubleArr(:,1).ProxFact],[DoubleArr(:,2).ProxFact],100,100,...
      'Method','circ','Dim',.09,.09,'Weight','flat');
	  surf(xg, yg, m); 
    shading interp;
    camlight left;
    lighting gouraud;
    %bin=0:0.05:1;
    %ShowContourPlot([DoubleArr(:,1).ProxFact]', [DoubleArr(:,2).ProxFact]', bin, bin, [], [], false);
    %xlabel('proximity factor 1','FontSize',20); ylabel('proximity factor 2','FontSize',20);        
    %set(gca,'FontSize',20);
    xlabel('E_F_R_E_T 1');
    ylabel('E_F_R_E_T 2');
    title('HMM-level');

end
