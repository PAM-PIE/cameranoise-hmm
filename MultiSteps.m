% Theoretische Betrachtung, wie viele Stufen durch die begrenzter Zeitaufl�sung
% und kurzen Dwelltime nicht gesehen werden k�nnen und dadurch
% f�lschlicherweise als multiple Spr�nge interpretiert werden.

tDwell = 24.7;       % Dwelltime der Stufenl�nge in ms
tStufeMinLen = 5;   % K�rzeste Stufenl�nge, um als Stufe erkannt zu werden in ms
tResol = 5;         % Zeitaufl�sung der Daten in ms

bEinzeln=false;
bSave=false;
% THEORIE
% Wahrscheinlichkeitsdichtefunktion (PDF, probability density function) der
% Aufenthaltsdauer wird als monoexponentielle Funktion angenommen:
%   p(t)=a*exp(-t/tDwell)
% Unbestimmtes Integral davon:
%   P(t) = -a*tDwell*exp(-t/tDwell)
% Anteil der nicht detektierten Stufen:
%   P(tStufeMinLen)-P(0) = a*tDwell*(1-exp(-t/tDwell))
% Anteil aller Stufen:
%   P(+Inf)-P(0) = 1    % Logisch, ist ja eine Cumulative
%   Verteilungsfunktion (CDF, cumulative distribution funtion)
% Anteil der detektierten Stufen:
%   P(+Inf)-P(tStufeMinLen) = 1-P(tStufeMinLen)

% RECHNUNG
% Amplitude so w�hlen, dass das Integral der Wahrscheinlichkeitsdichtefunktion 1 ergibt
a = 1/tDwell;
% Ab jetzt kann der Vorfaktor a*tDwell weggelassen werden

% Anteil der nicht detektierten Stufen:
pMissed = 1-exp(-tStufeMinLen/tDwell);
% Menge der zu zeigenden �bersprungenen Stufen:
MissedSteps = 0:1:4;
% Wahrscheinlichkeiten multiplizieren sich:
AnzMultiSteps = (1-pMissed).*pMissed.^(MissedSteps);
%             Stufe --^          ^-- Stufe      ^--- So oft
%           detektiert            nicht detektiert

% Verweildauer-Histogramm:
t=0:tResol:4*tDwell;
y=a*exp(-t./tDwell); %PDF
% Index finden, bis zu dem Stufen nicht detektiert werden k�nnen
i1=find(t>=tStufeMinLen,1,'first');

% VISUALISIERUNG
% Wahrscheinlichkeitsdichtefunktion zeichnen:
% Roter Bereich: nicht detektierbar, markiert durch rote Linie
% Blauer Bereich: detektierbar
% Blaue Linie markiert den 1/e-Punkt, also die Dwelltime
fh=[];
if bEinzeln, close all; else clf; end
if bEinzeln, fh(1)=figure; else subplot(2,1,1); end
cla; hold on; box on
if bEinzeln, set(gca,'FontSize',21); end
%stairs(t(1:i1+1)-.5,y(1:i1+1),':');
%stairs(t(i1+1:end)-.5,y(i1+1:end),'-');
bar(t(1:i1),y(1:i1),1,'FaceColor',[1 0 0]);
bar(t(i1+1:end),y(i1+1:end),1,'FaceColor',[0 0 .5]);
alpha(.2);
plot([1 1]*tDwell,[0 a],':','color',[0 0 1]);
plot([1 1]*tStufeMinLen,[0 a],'r');
xlabel('dwelltime (ms)');
ylabel('probability');
set(gca,'XTick',0:5*tResol:t(end));
axis tight;

% Anzahl der �bersprungenen Stufen zeigen
% Bei einem 10c-Ring sind das 36� pro Stufe
if bEinzeln, fh(2)=figure; else subplot(2,1,2); end
box on;
if bEinzeln, set(gca,'FontSize',21); end
bar(MissedSteps,AnzMultiSteps);
ylim([0 1]);
xlim([MissedSteps(1)-1 MissedSteps(end)+1]);
xlabel('number of missed steps');
ylabel('probability');
for i=1:length(MissedSteps)
  h=text(MissedSteps(i),AnzMultiSteps(i),sprintf('%2.0f%%',100*AnzMultiSteps(i)),...
    'VerticalAlignment','bottom','HorizontalAlignment','center');
  if bEinzeln, set(h,'FontSize',21); else set(h,'FontSize',8); end
  %text(MissedSteps(i),0.9,sprintf('%d�',(MissedSteps(i)+1)*36),...
  %    'VerticalAlignment','bottom','HorizontalAlignment','center','FontSize',8);
end
% Nawid Zarrabi, 12. M�rz 2010

if bSave
  if bEinzeln
    for i=1:length(fh)
      set(fh(i),'InvertHardcopy','off');
      set(fh(i),'PaperPositionMode','auto');
      saveas(fh(i),sprintf('MultiSteps%d.png',i));
    end
  else
    set(gcf,'InvertHardcopy','off');
    set(gcf,'PaperPositionMode','auto');
    saveas(gcf,'MultiSteps.png');
  end
end

pMissed