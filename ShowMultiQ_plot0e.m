% PFsigma plots
sSample={'DNA3','DNA12','DNA18','DNA19'};
sState={'Dynamic','Steady'};

sAuswVer='8';
sBurstSel='All'; %'Burst50';

title('loading...'); drawnow;
clf;
Anz=1;

iState=1;
cAusw=3;

cmap=get(gca,'ColorOrder');

switch cAusw
  case 1
    PFsigmax={};
    PFsigmay={};
    for iSample=1:4
      filepath=['D:\Schluesche\Maindata\',sSample{iSample},'\',sState{iState},'FRET\'];
      filename1=['Ausw',sAuswVer,'_',sBurstSel,'_m0',int2str(Anz),'.mat'];
      %load([filepath,filename1],'dataPFsigma');
      hold on
      PFsigmay{iSample}=[dataPFsigma{:}];
      PFsigmax{iSample}=(1:length(PFsigmay{iSample}))/length(PFsigmay{iSample});
      stairs(PFsigmax{iSample},sort(PFsigmay{iSample}),'color',cmap(iSample,:));
      slegend{end+1}=sSample{iSample};
    end
    xlim([.998 1]);
    legend(slegend,'location','North');

  case 2
    PFsigmax={};
    PFsigmay={};
    slegend={};
    hold on
    for iSample=3
      filepath=['D:\Schluesche\Maindata\',sSample{iSample},'\',sState{iState},'FRET\'];
      filename1=['Ausw',sAuswVer,'_',sBurstSel,'_m0',int2str(Anz),'.mat'];
      load([filepath,filename1],'dataPFsigma','dataPF','dataInt','IntNoise');
      fprintf('%s: %.3f %.3f\n',sSample{iSample},mean([dataInt{1,:}]),mean([dataInt{2,:}]));
      %       tempPFsigma={};
      %       PFsigmay=[dataPFsigma{:}];
      %       PFsigmax=[tempPFsigma{:}];
      PF=[];
      for iBurst=1:length(dataPF)
        PF(iBurst)=mean(dataPF{iBurst});
      end
      %       PFsigmay=[dataPFsigma{:}];
      %       PFsigmax=[dataInt{1,:}]+[dataInt{2,:}];
      %plot(PFsigmax,PFsigmay,'.','color',cmap(iSample,:));
      stairs(0:0.02:1,hist([PF],0:0.02:1),'color',cmap(iSample,:));
      slegend{end+1}=sSample{iSample};
      disp(length(find(PF>.8))/length(PF));
    end
    xlim([0 1]);
    legend(slegend,'location','North');
    
  case 3
    slegend={};
    clen=5;
    for iSample=1:4
      filepath=['D:\Schluesche\Maindata\',sSample{iSample},'\',sState{iState},'FRET\'];
      filename1=['Ausw',sAuswVer,'_',sBurstSel,'_m0',int2str(Anz),'.mat'];
      load([filepath,filename1],'dataPF');
      idx=[];

      subplot(4,1,iSample);
      hold on;
      
      for iBurst=1:length(dataPF), idx(iBurst)=length(dataPF{iBurst}); end
      yhist=hist(idx,xhist);
      bar(xhist,yhist/sum(yhist),'FaceColor',[.8 .8 .8],'EdgeColor','none','BarWidth',1);
      idx=[];
      
      for iBurst=1:length(dataPF)
        j=div(length(dataPF{iBurst}),clen);
        PF=reshape(dataPF{iBurst}(1:j*clen),clen,[]);
        PF=var(PF);
        temp=find(sort(PF)>=.01,1);
        if isempty(temp), temp=length(PF); end
        idx(iBurst)=temp*clen;
      end
      xhist=(0:1/40:1)*800; xhist(end+1)=xhist(end)+xhist(2);
      yhist=hist(idx,xhist);
      yhist=yhist./sum(yhist);
      
      stairs(xhist,yhist,'color',cmap(iSample,:));
      slegend{end+1}=sSample{iSample};
      legend(sSample{iSample},'location','NorthEast');
      axis tight;
    end
    %axis tight;
    %legend(slegend,'location','NorthEast');
end