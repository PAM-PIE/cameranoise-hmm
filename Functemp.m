clf;

subplot(3,1,1); cla; hold on; box on
set(gca,'YScale','log');
plot(log2(x),y);
plot(log2(gamma)*[1 1],ylim,'color',[1 .5 0]);

subplot(3,1,2); cla; hold on; box on
set(gca,'YScale','lin');
plot(log2(x(1:end-1)),diff(log2(y)));
plot(log2(gamma)*[1 1],ylim,'color',[1 .5 0]);
grid

subplot(3,1,3); cla; hold on; box on
set(gca,'YScale','lin');
plot(log2(x(1:end-2)),diff(diff(log2(y))));
plot(log2(gamma)*[1 1],ylim,'color',[1 .5 0]);
grid