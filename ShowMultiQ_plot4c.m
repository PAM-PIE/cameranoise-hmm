% State Duration Histogram
sSample={'DNA3','DNA12','DNA18','DNA19'};
sState={'Dynamic','Steady'};

% title('loading...'); drawnow;
clf;
% xhist=0:0.02:1;
% yhist=1:1:55;
% TimeBin=5;
%
% filepath_=['D:\Schluesche\Maindata\',sSample{iSample},'\',sState{iState},'FRET\'];
% filename1=['Ausw',sAuswVer,'_',sBurstSel,'_m0',int2str(Anz),'.mat'];
% %load([filepath_,filename1],'MultiQData','StufenArr','mu1','dwelltime1','TimeBin');
% load([filepath_,filename1],'StufenArr','mu1','dwelltime1','TimeBin');

MultiQData=MetaQData{iSample,iState};

% filepath=['D:\Schluesche\Maindata\',sSample{iSample},'\',sState{iState},'FRET\'];
% load([filepath,'Ausw',sAuswVer,'_',sBurstSel,'_m0',int2str(Anz),'.mat'],'StufenArr','TimeBin');
%   load([filepath,'Ausw',sAuswVer,'_',sBurstSel,'_m0',int2str(Anz),'.mat']);
%   HMMMarkBursts;

xbin=TimeBin*5;

OrigExp=[];
tauHMM=NaN*zeros(Anz);
tauMean=NaN*zeros(Anz);
tauStd=NaN*zeros(Anz);

subplot(2,1,1); cla; hold on; box on;
iAnz=1:Anz;
cmap=colormap;
imap=round(interp1([0 1],[1 length(cmap)],((1:Anz)-1)/(Anz-1)));
cmap2=cmap(imap,:);

set(gca,'ClimMode','manual');
set(gca,'clim',[0 1]);

for Q=iAnz
  %y(q)=sum(MultiQData{q}.mu1var(:));
  %y=NaN*zeros(Q,1);

  StufenArr=StufenHMM2StufenArrFast(MultiQData{Q}.AllStufen);
  mu1=MultiQData{Q}.mu1;
  mu1var=MultiQData{Q}.mu1var;
  %Q=length(mu1);
  mu0=MultiQData{Q}.mu0;
  %mu0=(1:Q)*1/(Q+1)+1/(2*(Q+1));
  StepCount=MultiQData{Q}.StepCount;
  dwelltime1=MultiQData{Q}.dwelltime1;

  for q=1:Q
    idx=find([StufenArr.PFclass]==q);
    data=(StufenArr.End(idx)-StufenArr.Start(idx)+1)*TimeBin;
    if numel(data)>0
      tauMean(iAnz,q)=mean(data);
      tauStd(iAnz,q)=sqrt(std(data));
    end
    tauHMM(iAnz,q)=dwelltime1(q);

    y=tauMean(Q,q);
    y1=max(0.0001,tauStd(Q,q));
    if ~isnan(tauMean(Q,q))
      imap=round(interp1([0 1],[1 length(cmap)],mu1(q)));
      rectangle('Position',[Q-.4,y-y1/2,.8,y1],...
        'FaceColor',cmap(imap,:),'EdgeColor','none');
      text(Q,y,sprintf('%.2f',mu1(q)),'HorizontalAlignment','center');
    end
  end
end

xlim([0 Anz]+.5);
ylim([0 600]);
xlabel('#states');
ylabel('dwelltime/ms');
title(sprintf('%s %s (%s V%s)',sSample{iSample},sState{iState},sBurstSel,sAuswVer));
colorbar('location','SouthOutside');
