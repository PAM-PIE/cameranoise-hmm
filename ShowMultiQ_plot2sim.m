% FRET Histograms from the data and not from distributions
sSample={'DNA3','DNA12','DNA18','DNA19','Donor_Only','Acceptor_Only','Sim1'};
sState={'Dynamic','Steady',''};
filepathStamm=['D:\Schluesche\Maindata\',sSample{iSample},'\'];
if iSample<=4
  filepathStamm=[filepathStamm,'\',sState{iState},'FRET\'];
end

%load MetaData first
%MultiQData=MetaQData{iSample,iState};
load([filepathStamm,'\Ausw16_Sim1_m07.mat'],'MultiQData');

bShowData=true;  %true: hist StufenArr; false: hist mu1

x=0:0.02:1;
y=1:2:100;
xhist=x;
yhist=y;
dxhist=x(2)-x(1);
hold off; clf;
% I=80;

for i=1:length(MultiQData)

  mu1=MultiQData{i}.mu1;
  mu1var=MultiQData{i}.mu1var;
  Q=length(mu1);
  mu0=MultiQData{i}.mu0;
  %mu0=(1:Q)*1/(Q+1)+1/(2*(Q+1));
  StepCount=MultiQData{i}.StepCount;
  dwelltime1=MultiQData{i}.dwelltime1;
  lenLL(i)=length(MultiQData{i}.trajLL);

  y=zeros(1,length(x));
  ix0=zeros(1,Q);
  ix1=zeros(1,Q);
  if bShowData
    load([filepathStamm,'Ausw',sAuswVer,'_',sBurstSel,'_m0',int2str(Q),'.mat'],'StufenArr');
    %y=hist([StufenArr.ProxFact],xhist);
    y_=max(0,min(1,[StufenArr.ProxFact]));
    w_=[StufenArr.Length];
    idx_=round(y_*(length(xhist)-1))+1;
    y=accumarray(idx_',w_,size(xhist'))';
%     StufenArr=StufenHMM2StufenArr(MultiQData{i}.AllStufen,dataInt,5,1);
%     y=hist([StufenArr.ProxFact],xhist);
  else
    for q=1:Q
      N=StepCount(q)*dwelltime1(q);
      if ~isfinite(N), N=1; end
%       a=mu1(q)*I;
%       b=(1-mu1(q))*I;
      %y1=betapdf(x,a,b);
      y1=N*normpdf(x,mu1(q),sqrt(mu1var(q)));
      ix0(q)=find(x>=mu0(q),1);
      ix1(q)=find(x>=mu1(q),1);
      y=y+y1;
    end
    y=y/max(y);
  end
  for q=1:Q
    [w0,ix0(q)]=min(abs(mu0(q)-x));
    [w1,ix1(q)]=min(abs(mu1(q)-x));
  end
  y(find(~isfinite(y)))=NaN;
  y=y/max(y)*.8;
  hold on
  y0=i;
  plot(x,y0,'k');
  plot(x,y0+.8,'--k');
  stairs(x-dxhist/2,y0+y);
  %stem(x(ix),y0+y(ix),'BaseValue',Q);
  plot(mu0,y0+y(ix0),'o','MarkerSize',4,'color',[0 .5 0]);
  plot(mu1,y0+y(ix1),'.r');
  for q=1:Q
    text(x(ix1(q)),y0+y(ix1(q)),sprintf('%.2f',mu1(q)),...
      'HorizontalAlignment','center','VerticalAlignment','bottom');
  end
  drawnow;
  xlim([0 1]);
end
ylim([1 y0+.999]);
title(sprintf('%s %s (%s V%s)',sSample{iSample},sState{iState},sBurstSel,sAuswVer));

set(gcf,'InvertHardcopy','off');
set(gcf,'PaperPositionMode','auto');
saveas(gcf,['D:\Schluesche\Auswertung\',...
  sprintf('Ausw%s_%s %s_%s mu1',...
  sAuswVer,sBurstSel,sSample{iSample},sState{iState})],'png');
