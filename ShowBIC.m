sSample={'DNA3','DNA12','DNA18','DNA19','Sim1','Sim2'};
sState={'Dynamic','Steady'};

% sAuswVer='9';
% sBurstSel='All'; %'Burst50';
% iSample=1;

LL=[];
BIC=[];
States=[];
q=1:10;
NumParam=q.*(q-1)+2*q;  %Transmat+mu+mu1var (prior wird nicht gelernt)
bsave=true;

if ~exist('dataNumTraces')
  filepath_=['D:\Schluesche\Maindata\'];
  load([filepath_,'DataNumTracesAll.mat']);
end
if ~exist('dataNumBins')
  filepath_=['D:\Schluesche\Maindata\'];
  load([filepath_,'DataNumBinsAll.mat']);
end

subplot(2,1,1); cla;
for q=1:length(MetaQData{iSample,1})
  LL(q,1)=(MetaQData{iSample,1}{q}.trajLL(end));
  LL(q,2)=(MetaQData{iSample,2}{q}.trajLL(end));
  States(q)=length(MetaQData{iSample,1}{q}.mu1);
  %   dataNumBins(q,1)=sum(MetaQData{iSample,1}{q}.StepCount);
  %   dataNumBins(q,2)=sum(MetaQData{iSample,2}{q}.StepCount);
  %BIC(q,1)=2*LL(q,1)*dataNumBins(iSample,1)-NumParam(States(q))'.*log(dataNumBins(iSample,1));
  %BIC(q,2)=2*LL(q,2)*dataNumBins(iSample,2)-NumParam(States(q))'.*log(dataNumBins(iSample,2));
  BIC(q,1)=2*LL(q,1)-NumParam(States(q))'.*log(dataNumBins(iSample,1));
  BIC(q,2)=2*LL(q,2)-NumParam(States(q))'.*log(dataNumBins(iSample,2));
  %   BIC(q,1)=2*LL(q,1)/dataNumTraces(iSample,1)*dataNumBins(iSample,1)-NumParam(States(q))'.*log(dataNumBins(iSample,1));
  %   BIC(q,2)=2*LL(q,2)/dataNumTraces(iSample,2)*dataNumBins(iSample,2)-NumParam(States(q))'.*log(dataNumBins(iSample,2));
end
OrigHeader={'MultiQ','LL1','LL2','BIC1','BIC2'};
OrigExport=[States',LL(:,1),LL(:,2),BIC(:,1),BIC(:,2)];

plot(States,LL,'.-');
title(sprintf('%s (%s V%s)',sSample{iSample},sBurstSel,sAuswVer));
ylabel('logL');

subplot(2,1,2); cla;
plot(States,BIC,'.-');
ylabel('BIC');

[w,i]=max(BIC);
hold on
plot(States(i),w,'or');
box on

%legend(sState,'Location','SouthEast');
legend(sState,'Location',[.5 .49 .4 .05]);

if bsave
  set(gcf,'InvertHardcopy','off');
  set(gcf,'PaperPositionMode','auto');
  saveas(gcf,['D:\Schluesche\Auswertung\',...
    sprintf('Ausw%s_%s %s BIC',...
    sAuswVer,sBurstSel,sSample{iSample})],'png');

  fid=fopen(['D:\Schluesche\Auswertung\',...
    sprintf('Ausw%s_%s %s BIC',...
    sAuswVer,sBurstSel,sSample{iSample}),'.txt'],'w');
  for i=1:length(OrigHeader), fprintf(fid,'%s\t',[OrigHeader{i}]); end
  fprintf(fid,'\r\n');
  fprintf(fid,'%d\t%f\t%f\t%f\t%f\r\n',OrigExport');
  fclose(fid);
end
