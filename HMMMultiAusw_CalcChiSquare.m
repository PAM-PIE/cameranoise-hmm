%HMMMultiAusw_CalcChiRed2
sSample={'DNA3','DNA12','DNA18','DNA19'};
sState={'Dynamic','Steady'};

bsave=true;

Anz=10;
filepath1=['D:\Schluesche\Maindata\',sSample{iSample},'\',sState{1},'FRET\'];
filepath2=['D:\Schluesche\Maindata\',sSample{iSample},'\',sState{2},'FRET\'];
filename=sprintf('Ausw%s_%s_m%02d.mat',sAuswVer,sBurstSel,Anz);
[MultiQ,LL1,LLBIC1,ChiSquare1,ChiSquare1Red,ChiBIC1]=CalcChiSquare3([filepath1,filename]);
[MultiQ,LL2,LLBIC2,ChiSquare2,ChiSquare2Red,ChiBIC2]=CalcChiSquare3([filepath2,filename]);
OrigHeader={'MultiQ','LL1','LL2','LLBIC1','LLBIC2','ChiSquare1','ChiSquare2','ChiSquare1Reduced','ChiSquare2Reduced','ChiBIC1','ChiBIC2'};
OrigExport=[MultiQ',LL1',LL2',LLBIC1',LLBIC2',ChiSquare1',ChiSquare2',ChiSquare1Red',ChiSquare2Red',ChiBIC1',ChiBIC2'];

subplot(3,1,1); cla; box on;
hold off
%plot(MultiQ,1+0*MultiQ,'color',[1 1 1]*.7);
hold on
plot(MultiQ,ChiSquare1,'.-','DisplayName',sState{1});
plot(MultiQ,ChiSquare2,'.-','color',[0 .5 0],'DisplayName',sState{2});
xlabel('number of states');
ylabel('\langle\chi�\rangle');
%ylabel('\Sigma\chi�');
title(sprintf('%s (%s V%s)',sSample{iSample},sBurstSel,sAuswVer));
%ylim([0 2]);
xlim([MultiQ(1) MultiQ(end)]);

subplot(3,1,2); cla; box on;
hold off
plot(MultiQ,1+0*MultiQ,'color',[1 1 1]*.7);
hold on
plot(MultiQ,ChiSquare1Red,'.-','DisplayName',sState{1});
plot(MultiQ,ChiSquare2Red,'.-','color',[0 .5 0],'DisplayName',sState{2});
xlabel('number of states');
ylabel('\langle\chi�_R_e_d_u_c_e_d\rangle');
%ylabel('\Sigma\chi�');
title(sprintf('%s (%s V%s)',sSample{iSample},sBurstSel,sAuswVer));
%ylim([0 2]);
xlim([MultiQ(1) MultiQ(end)]);

subplot(3,1,3); cla; box on;
hold off
%plot(MultiQ,1+0*MultiQ,'color',[1 1 1]*.7);
hold on
% plot(MultiQ(2:7),diff(ChiSquare1),'.-','DisplayName',sState{1});
% plot(MultiQ(2:7),diff(ChiSquare2),'.-','color',[0 .5 0],'DisplayName',sState{2});
plot(MultiQ,ChiBIC1,'.-','DisplayName',sState{1});
plot(MultiQ,ChiBIC2,'.-','color',[0 .5 0],'DisplayName',sState{2});
[wex,iex]=min(ChiBIC1);
plot(MultiQ(iex),ChiBIC1(iex),'or');
[wex,iex]=min(ChiBIC2);
plot(MultiQ(iex),ChiBIC2(iex),'or');
xlabel('number of states');
ylabel('BIC');
title(sprintf('%s (%s V%s)',sSample{iSample},sBurstSel,sAuswVer));
xlim([MultiQ(1) MultiQ(end)]);

legend(sState,'Location',[.5 .005 .4 .05]);

if bsave
  set(gcf,'InvertHardcopy','off');
  set(gcf,'PaperPositionMode','auto');
  saveas(gcf,['D:\Schluesche\Auswertung\',...
    sprintf('Ausw%s_%s %s ChiSquareRed',...
    sAuswVer,sBurstSel,sSample{iSample})],'png');

  fid=fopen(['D:\Schluesche\Auswertung\',...
    sprintf('Ausw%s_%s %s ChiSquareRed',...
    sAuswVer,sBurstSel,sSample{iSample}),'.txt'],'w');
  for i=1:length(OrigHeader), fprintf(fid,'%s\t',[OrigHeader{i}]); end
  fprintf(fid,'\r\n');
  fprintf(fid,'%d\t%f\t%f\t%f\t%f\t%f\t%f\t%f\t%f\t%f\t%f\r\n',OrigExport');
  fclose(fid);
end

beep
