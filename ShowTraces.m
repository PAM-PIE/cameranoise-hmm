function [] = ShowTraces(data);
%function [] = ShowTraces(data);
%this function visualizes the trace data loaded from a "TracesPro"-File.
bHist=false;

if bHist
    ax(1) = subplot(2,2,1);
else
    ax(1) = subplot(2,1,1);
end
hold off
plot(data.FRET,'color',[0 0 1]);
ylim([0 1]);
title(['Particle No ',int2str(data.Mol)]);

if bHist
    ax(2) = subplot(2,2,3);
else
    ax(2) = subplot(2,1,2);
end
hold off
plot(data.Donor,'color',[0 0.5 0]);
hold on
plot(data.Acc,'color',[1 0 0]);
ylim_=ylim;
ylim([0 ylim_(2)]);

linkaxes(ax,'x');
xlim([1 length(data.FRET)]);

if bHist
    ax(3) = subplot(2,2,2);
    histbinx=0:0.05:1;
    hold off
    hdata=hist(data.FRET,histbinx);
    hdata(1)=0;
    hdata(end)=0;
    bar(histbinx,hdata);
    xlim([0 1]);
end
