%HMMMultiAusw_CalcChiRed SIMLUTAION-Data
sSample={'DNA3','DNA12','DNA18','DNA19','Donor_Only','Acceptor_Only','Sim1'};
sState={'Dynamic','Steady',''};
filepathStamm=['D:\Schluesche\Maindata\',sSample{iSample},'\'];
if iSample<=4
  filepathStamm=[filepathStamm,'\',sState{iState},'FRET\'];
end

Anz=7;
bShowLL=false;
filename=sprintf('Ausw%s_%s_m%02d.mat',sAuswVer,sBurstSel,Anz);
[MultiQ,ChiSquare1,ChiSquare1red]=CalcChiSquare([filepathStamm,filename]);
OrigExport=[MultiQ',ChiSquare1',ChiSquare1red'];

subplot(2,1,1);
if bShowLL, subplot(Anz,Anz,[Anz*2+1 Anz*Anz]); end
hold off
plot(MultiQ,ChiSquare1,'.-');
xlabel('number of states');
ylabel('\Sigma\chi�');
title(sprintf('%s (%s V%s)',sSample{iSample},sBurstSel,sAuswVer));
%ylim([0 2]);
xlim([MultiQ(1) MultiQ(end)]);
  
subplot(2,1,2);
if bShowLL, subplot(Anz,Anz,[Anz*2+1 Anz*Anz]); end
hold off
plot(MultiQ,1+0*MultiQ,'color',[1 1 1]*.7);
hold on
plot(MultiQ,ChiSquare1red,'.-');
xlabel('number of states');
ylabel('\Sigma\chi_r_e_d�');
title(sprintf('%s (%s V%s)',sSample{iSample},sBurstSel,sAuswVer));
ylim(1+.1*[-1 1]);
xlim([MultiQ(1) MultiQ(end)]);

if bShowLL

  load([filepath1,filename],'MultiQData','MultiQ');
  for q=1:Anz
    subplot(Anz,Anz,q);
    plotLL(MultiQData{q}.trajLL);
    if q==1, ylabel(sState{1}); end
  end

  load([filepath2,filename],'MultiQData','MultiQ');
  for q=1:Anz
    subplot(Anz,Anz,q+Anz);
    plotLL(MultiQData{q}.trajLL);
    if q==1, ylabel(sState{2}); end
  end

end

set(gcf,'InvertHardcopy','off');
set(gcf,'PaperPositionMode','auto');
saveas(gcf,['D:\Schluesche\Auswertung\',...
  sprintf('Ausw%s_%s %s ChiRed',...
  sAuswVer,sBurstSel,sSample{iSample})],'png');
