function [tBleach] = FindTrajEnd(dataInt,paramLevel,paramSmooth);
%function [tBleach] = FindTrajEnd(dataInt);
%this function searches the intensity rajectory for the moment,
%where the intensity goes to 0

if iscell(dataInt)
    tBleach = length(dataInt{1})*ones(length(dataInt),1);
    for iTrace=1:length(dataInt)
        temp=find(smooth(dataInt{iTrace}(10:end),paramSmooth)<paramLevel,1,'first');
        if ~isempty(temp), tBleach(iTrace)=temp; end
    end
else
    tBleach = length(dataInt);
    temp=find(smooth(dataInt(10:end),paramSmooth)<paramLevel,1,'first');
    if ~isempty(temp), tBleach=max(1,temp-1+10); end
end
