% Show Ekorr verteilung
sSample={'DNA3','DNA12','DNA18','DNA19','Sim1','Sim2'};
sState={'Dynamic','Steady'};

iState=1;
MaxAnzBurst=Inf;

for iSample=1:1

  filepath=['D:\Schluesche\MainData\',sSample{iSample},'\',sState{iState},'FRET\'];
  if strcmp(sSample{iSample},'Sim1')
    filepath='D:\Schluesche\Maindata\Sim1\';
    filename='Sim_*.txt';
  end
  if strcmp(sSample{iSample},'Sim2')
    filepath='D:\Schluesche\Maindata\Sim2\';
    filename='Sim_*.txt';
  end
  
  load([filepath,'EkorrV',sAuswVer,'.mat']);
  HMMLoadData;
  
  AnzBursts=length(data);
  idx=nchoose2(1:AnzBursts);
  I=[data(:).DonorAvg]+[data(:).AccAvg];
  diffE=abs(I(idx(:,2))-I(idx(:,1)));
  diffx=[data(idx(:,2)).xcoordDonor]-[data(idx(:,1)).xcoordDonor];
  diffy=[data(idx(:,2)).ycoordDonor]-[data(idx(:,1)).ycoordDonor];
  diffr=sqrt(diffx.^2+diffy.^2);
  
  clf
  %ShowPlot2D(diffr,diffE,[0 250],[0 400],[1]*250,[3 3],.3);
  img=ShowPlot2D(diffr,diffE,[0 250],[0 1],[1 1]*251,[1 1]*4,1);
  y=0:1/250:1;
  img=ShowPlot2D(diffr,diffE,[0 250],[0 400],[1 1]*251,[1 1*4],1);
  y=(0:1/250:1)*400;
  hold on
  plot(y*img./sum(img),'w');
  hold off
end
