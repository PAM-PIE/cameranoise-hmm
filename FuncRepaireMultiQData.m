function [MultiQData]=FuncRepaireMultiQData(xxfilename,XXQ,XXMultiQData)

load(xxfilename);
clear xxfilename
for XXq=1:XXQ-1
  MultiQData{XXq}=XXMultiQData{XXq};
end
if XXQ>1
  clear XXq XXQ XXMultiQData;
  save(sprintf('%sAusw%s_%s_m%02d',filepath,sAuswVer,sBurstSel,Q));
end
