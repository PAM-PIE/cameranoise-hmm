%Auswertung Burstwise 2D-plot

sSample={'DNA3','DNA12','DNA18','DNA19'};
sState={'Dynamic','Steady'};
HMMVersion;

Anz=5;
iSample=2;
iState=1;

%   for iSample=1:numel(sSample)
%     for iState=1:numel(sState)
      filepath=['D:\Schluesche\MainData\',sSample{iSample},'\',sState{iState},'FRET\'];
      filename=sprintf('Ausw%s_%s_m%02d.mat',sAuswVer,sBurstSel,Anz);
      load([filepath,filename],'MultiQData','MaxAnzBurst','AlldataInt','R0','gamma');
      %save([filepath,sprintf('Ausw%s_%s_MetaData',sAuswVer,sBurstSel)],'MetaQData');
%     end    
%   end


  AllStufen=[];
  for iBurst=1:MaxAnzBurst
    AllStufen{iBurst}=MultiQData{1}(iBurst).AllStufen;
  end
  StufenArr=StufenHMM2StufenArr(AllStufen,AlldataInt,R0,gamma);
  DoubleArr=StufenFilter(StufenArr,'2+',true);
  ShowPlot2D([DoubleArr(:,1).Efret],[DoubleArr(:,2).Efret],[0 1],[0 1],[100 100],[1 1]*2);
  set(gca,'PlotBoxAspectRatio',[1 1 1]);
  