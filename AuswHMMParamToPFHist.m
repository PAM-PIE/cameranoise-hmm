%HMMParamToPFHist;
filepath='D:\Nawid\Schluesche\2008-01-17\TBPDNA3_200610\';
filenameAusw='gesamt_ausw_m16';
load([filepath,filenameAusw,'.mat']);

bFigEinzeln=true;
bFigSave=true;

if 1
    [w,idx]=sort(mu1);
    fprintf('%8.0f',idx); fprintf('\tindex \n');
    fprintf('%8.3f',mu1(idx)); fprintf('\tmu1 \n');
    fprintf('%8.3f',mu1var(idx)); fprintf('\tmu1var \n');
    fprintf('%8.0f',dwelltime1(idx)); fprintf('\ttau/ms \n');
    fprintf('%8.0f',StepCount(idx)); fprintf('\t#steps \n');
end
x=0:0.01:1; y=[]; A=[];

%avgStufeLen=mean([StufenArr(:).Length])*TimeBin;
%temp=zeros(AnzBursts,1);
%for i=1:AnzBursts, temp(i)=length(AlldataPF{i}); end;
%avgBurstLen=mean(temp)*TimeBin;

StufenBins=[];
iFilter=[];
for i=1:AnzStufen
    iBurst=StufenArr(i).iBurst;
    if 1%(StufenArr(i).ProxFactErr<0.1) & (StufenArr(i).Length>=10)
        iFilter=[iFilter,i];
        aStufeBin=AlldataPF{iBurst}(StufenArr(i).Start:StufenArr(i).End);
        for j=1:0
            aStufeBin=smooth(aStufeBin,'moving',15)';
        end
        StufenBins=[StufenBins,aStufeBin];
    end
end
if bFigEinzeln, close all; end
if bFigEinzeln, figure; else subplot(2,1,1); end
hold off;
%hist([StufenArr(:).ProxFact],x);
bar(x,hist(StufenBins,x),'BarWidth',1,'FaceColor',[1 1 1]*.7,'EdgeColor','none');
hold on;
for i=1:length(mu1)
	idx=find([StufenArr(iFilter).PFclass]==i);
    if length(idx)>0
        A(i)=mean([StufenArr(idx).Length]);
        %mu1mean=mean([StufenArr(idx).ProxFact]);
        mu1mean(i)=mu1(i);
        mu1std(i)=mean([StufenArr(idx).ProxFactErr]);
        %mu1std(i)=sqrt(mu1var(i));
        y(i,:)=...
            length(StufenBins)/length([AlldataPF{:}])*...
            StepCount(i)*A(i)*...
            normpdf(x,mu1mean(i),mu1std(i))/length(x);
    else
        y(i,:)=zeros(1,length(x));
    end
end
plot(x,sum(y),'r','LineWidth',2);
i=find(isfinite(dwelltime1));
if length(i)>0, plot(x,y(i,:),'color',[0 .5 0],'LineWidth',.5); end %gr�n: kurze Dwelltimes
i=find(~isfinite(dwelltime1));
if length(i)>0, plot(x,y(i,:),'color',[0 0 1],'LineWidth',.5); end%blau: inf Dwelltimes
axis tight;
title('frame wise');
xlabel('FRET Efficiency');
ylabel('Occurences');
saveas(gcf,[filepath,filenameAusw,'_fw_all.png']);

if bFigEinzeln, figure; else subplot(2,1,2); end
hold off;
bar(x,hist([StufenArr(iFilter).ProxFact],x),'BarWidth',1,'FaceColor',[1 1 1]*.7,'EdgeColor','none');
hold on
for i=1:length(mu1)
	idx=find([StufenArr(iFilter).PFclass]==i);
    if length(idx)>0
        A(i)=1;
        %mu1mean=mean([StufenArr(idx).ProxFact]);
        mu1mean(i)=mu1(i);
        %mu1std=mean([StufenArr(idx).ProxFactErr]);
        mu1std(i)=sqrt(mu1var(i))/5;
        y(i,:)=...
            length(StufenBins)/length([AlldataPF{:}])*...
            StepCount(i)*A(i)*...
            normpdf(x,mu1mean(i),mu1std(i))/length(x);
    else
        y(i,:)=zeros(1,length(x));
    end
end
plot(x,sum(y),'r','LineWidth',2);
i=find(isfinite(dwelltime1));
if length(i)>0, plot(x,y(i,:),'color',[0 .5 0],'LineWidth',.5); end %gr�n: kurze Dwelltimes
i=find(~isfinite(dwelltime1));
if length(i)>0, plot(x,y(i,:),'color',[0 0 1],'LineWidth',.5); end%blau: inf Dwelltimes
axis tight;
title('step wise');
xlabel('FRET Efficiency');
ylabel('Occurences');
saveas(gcf,[filepath,filenameAusw,'_sw_all.png']);
