%HMMMarkBursts;
cMinLL = 0; %0.001;
cMinInt1 = 0;
cMinInt2 = 0;
cMinInt3 = 0;
cMinLen = 0;

%AnzBursts=1;
if 0
  fid = fopen([filepath,filename, '_hmmout.txt'], 'wt');
  fprintf(fid,['HMM StepFinder Version ',sVers,'\n\n']);
  fprintf(fid,'Bursts\t%d\n',[AnzBursts]);
  fprintf(fid,'LogLik\t%g\n\n',[loglik(length(loglik))]);
  fprintf(fid,'ID\tStart\tEnd\tClass\tmean\n');
end

AnzStufen = 0;
if bShowParam
  disp('Starte Burst-Markierung...');
  fprintf(1,'%6d Bursts markieren: %6d',[AnzBursts,0]);
end

dataLL={};

if (iscell(mu1))
  %#2018
  mu1_multi=mu1;
  mu1var_multi=mu1var;
  Sigma1_multi=Sigma1;
  iBurst=1;
  mu1=mu1{FileListIdx(iBurst)};
  mu1var=mu1var{FileListIdx(iBurst)};
  Sigma1=Sigma1{FileListIdx(iBurst)};
end

for iBurst=1:AnzBursts

  B = mixgauss_prob_kn(dataPF{iBurst}, dataPFsigma{iBurst}, mu1, Sigma1);
  Bobs = ExpandB(B,obsvect);
  path = viterbi_path(prior1, transmat1, Bobs);
  llpath = prob_path(prior1, transmat1, Bobs, path);
  LL = sum(llpath)/length(llpath);
  Stufen = Path2StufenPF(path, 1:Q, cMinLen, dataPF{iBurst}, dataInt{1,iBurst}, dataInt{2,iBurst}, ...
    llpath, cMinLL, cMinInt1,cMinInt2);  %Nur Stufen 1..Q-1 auswählen, min. Stufenlänge
  Stufen_ = Path2StufenPF(obsvect(path), 1:Qo, cMinLen, dataPF{iBurst}, dataInt{1,iBurst}, dataInt{2,iBurst}, ...
    llpath, cMinLL, cMinInt1,cMinInt2);          %Nur Stufen 1..Q-1 auswählen, min. Stufenlänge
  AllStufen{iBurst} = Stufen;
  dataLL{iBurst} = llpath;
  if numel(Stufen)>0
    AllLoglik(iBurst) = mean([Stufen(:).LL]);
  else
    AllLoglik(iBurst) = 0;
  end
  AnzStufen = AnzStufen + numel(Stufen);
  if bShowParam
    if mod(iBurst,100)==0, fprintf(1,'\b\b\b\b\b\b%6d',[iBurst]); end
  end

end
if bShowParam, fprintf(1,'\b\b\b\b\b\bok.\n'); end

%if bSave, fclose(fid); end

%close(gcf);
%disp(sprintf('%d Stufen created.',[AnzStufen]));

if 0
  StufenArr=StufenHMM2StufenArrPF(AllStufen,dataInt,5,1);
  DoubleArr=StufenFilter(StufenArr,'2+',true);
  mu1sort=sort(mu1);
  gridlines=[0,(mu1sort(2:end)+mu1sort(1:end-1))/2,1];
  ShowContourPlot([DoubleArr(:,1).ProxFact]', [DoubleArr(:,2).ProxFact]', 0:0.05:1, 0:0.05:1, gridlines, gridlines, true);
end
if 0
  StufenArr=StufenHMM2StufenArrPF(AllStufen,dataInt,5,1);
  DoubleArr=StufenFilter(StufenArr,'2',true);
  TripleArr=StufenFilter(StufenArr,'3+',true);
  HMMCreatePFChart({DoubleArr, TripleArr}, false, 5);
end
if 0 %bMultiQ
  LL=[]; LLanz=[];
  for i=MultiQ
    LL(i)=MultiQ_LL{i}(end);
    LLanz(i)=length(MultiQ_AnzBurst{i});
  end
  ShowLL(LL,LLanz);
end
if ~isempty(AllStufen)
  StufenArr=StufenHMM2StufenArr(AllStufen,dataInt,R0,gamma);
  StepCount=hist(obsvect([StufenArr(:).PFclass]),1:1:Qo);
else
  StepCount=[];
end
if bShowParam
  fprintf('\n    *** RESULT ***\n');
  [w,idx]=sort(mu1);
  fprintf('%8.0f',idx); fprintf('\tindex \n');
  fprintf('%8.3f',mu1(idx)); fprintf('\tmu1 \n');
  fprintf('%8.3f',mu1var(idx)); fprintf('\tmu1var \n');
  fprintf('%8.0f',dwelltime1(idx)); fprintf('\ttau/ms \n');
  fprintf('%8.0f',StepCount(idx)); fprintf('\t#steps \n');
end

if bLearnBurstwise
  MultiQData{iMulti}(idata).mu0 = mu0;
  MultiQData{iMulti}(idata).mu0var = mu0var;
  MultiQData{iMulti}(idata).Sigma0 = Sigma0;
  MultiQData{iMulti}(idata).transmat0 = transmat0;
  MultiQData{iMulti}(idata).dwelltime0 = dwelltime0;
  MultiQData{iMulti}(idata).mu1 = mu1;
  MultiQData{iMulti}(idata).Sigma1 = Sigma1;
  MultiQData{iMulti}(idata).mu1var = mu1var;
  MultiQData{iMulti}(idata).transmat1 = transmat1;
  MultiQData{iMulti}(idata).dwelltime1 = dwelltime1;
  MultiQData{iMulti}(idata).StepCount = StepCount;
  MultiQData{iMulti}(idata).trajLL = trajLL;
  MultiQData{iMulti}(idata).AllStufen = Stufen;
  MultiQData{iMulti}(idata).AllStufen_ = Stufen_;
else
  MultiQData{iMulti}.mu0 = mu0;
  MultiQData{iMulti}.mu0var = mu0var;
  MultiQData{iMulti}.Sigma0 = Sigma0;
  MultiQData{iMulti}.transmat0 = transmat0;
  MultiQData{iMulti}.dwelltime0 = dwelltime0;
  MultiQData{iMulti}.mu1 = mu1;
  MultiQData{iMulti}.Sigma1 = Sigma1;
  MultiQData{iMulti}.mu1var = mu1var;
  MultiQData{iMulti}.transmat1 = transmat1;
  MultiQData{iMulti}.dwelltime1 = dwelltime1;
  MultiQData{iMulti}.StepCount = StepCount;
  MultiQData{iMulti}.trajLL = trajLL;
  MultiQData{iMulti}.AllStufen = AllStufen;
  %MultiQData{iMulti}.AllStufen_ = Stufen_;
end

