% Show RawData Variance Analysis
%clear variables;
sSample={'DNA3','DNA12','DNA18','DNA19','Donor_Only','Acceptor_Only'};
sState={'Dynamic','Steady'};

for iSample=1:4
iState=1

MaxAnzBurst = Inf;
sBurstSel='All';
len=25;
xhist=0:2:40;

subplot(4,1,iSample);

if 1
  filepath=['D:\Schluesche\Maindata\',sSample{iSample},'\'];
  if iSample<5
    filepath=[filepath,sState{iState},'FRET\'];
  end
  filename='TBPDNA_*.txt';
  HMMLoadData;
end

AnzBursts=length(AlldataPF);
dataMean1=[];
% dataMean2=[];
dataVar1=[];
% dataVar2=[];
for iBurst=1:AnzBursts
%   I1=max(-Inf,0*AlldataInt{1,iBurst}+1*IntNoise{1,iBurst});
%   I2=max(-Inf,0*AlldataInt{2,iBurst}+1*IntNoise{2,iBurst});
%   I1=I1(1:div(length(I1),len)*len);
%   I2=I2(1:div(length(I2),len)*len);
%   I1=reshape(I1,len,[]);
%   I2=reshape(I2,len,[]);
%   dataMean1=[dataMean1,mean(I1)];
%   dataVar1=[dataVar1,var(I1)];
%   dataMean2=[dataMean2,mean(I2)];
%   dataVar2=[dataVar2,var(I2)];
  
  dataMean1=[dataMean1,mean(AlldataPF{iBurst})];
  dataVar1=[dataVar1,var(AlldataPF{iBurst})];
end
hold off
% yhist1=hist(dataVar1./dataMean1,xhist);
% yhist2=hist(dataVar2./dataMean2,xhist);
yhist1=hist(dataMean1./dataVar1,xhist);
% yhist2=hist(dataMean2./dataVar2,xhist);
stairs(xhist(1:end-1),yhist1(1:end-1),'color',[0 .5 0]);
hold on
% stairs(xhist(1:end-1),yhist2(1:end-1),'r');
xlim([min(xhist) max(xhist)]);

stitle=sSample{iSample};
if iSample<5, stitle=[stitle,' ',sState{iState}]; end
stitle=[stitle,' ',sBurstSel]; 
title(stitle,'Interpreter','none');
xlabel('mean/variance');
ylabel('#');

drawnow;

end

% set(gcf,'InvertHardcopy','off');
% set(gcf,'PaperPositionMode','auto');
% saveas(gcf,sprintf('PhotStatistik VarMean %s %s',sState{iState},sBurstSel),'png');
% 
