function [vary1]=FuncEstimateVar(a,b,va,vb,fa,fb)
% function [vary1,x1,y1]=FuncEstimateVar(a,b,va,vb,fa,fb)

vary1=zeros(1,length(a));
for j=1:length(a)
  I=a(j)+b(j);
  sa=sqrt((a(j)+va(j))*fa);
  sb=sqrt((b(j)+vb(j))*fb);
  x1=-.5:0.02:1.5; dx=x1(2)-x1(1); x1=x1+dx/2;
  x_=(-1:1/200:1)*5*I; dx_=x_(2)-x_(1); x_=x_+dx_/2;
  y1=zeros(1,length(x1),1);
  for i=1:length(x1)
    u=x1(i);
    z=1/u-1;
    y1(i)=1./(2*pi*sa*sb).*sum(abs(x_).*exp(-(1/2).*((z.*x_-b(j)).^2./sb^2+(x_-a(j)).^2./sa^2)).*dx_);
    y1(i)=1/u^2*y1(i);
  end
  vary1(j)=sum(y1.*dx.*(x1-a(j)/(a(j)+b(j))).^2);
end
%vary1=max(6e-5,vary1);
