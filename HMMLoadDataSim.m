%HMMLoadDataSim
if isfinite(MaxAnzBurst)
  tempfilename=sprintf('%sdata_Burst%d',filepath,MaxAnzBurst);
else
  tempfilename=sprintf('%sdata_gesamt',filepath);
end

if exist([tempfilename,'.mat'],'file') & 0
  load(tempfilename);
  fprintf(1,'%d traces loaded.\n',length(data));
else
  %load SimParam
  files=dir([filepath,strrep(filename,'.txt','param.mat')]);
  load([filepath,files(1).name],'PF','IntNoise1','IntNoise2','ttimetrace');
  SimParam.mu1=PF(:)'; 
  SimParam.DonorErr=fix(IntNoise1*ttimetrace);
  SimParam.AccErr=fix(IntNoise2*ttimetrace);
  clear PF IntNoise1 IntNoise2 ttimetrace;
  %load data
  files=dir([filepath,filename]);
  data=[]; SimStufen=[];
  fprintf('Load files...\n');
  if length(files)==0, error('search path is empty!'); end
  for ifile=1:length(files)
    filename1=files(ifile).name;
    fprintf(1,'Load file "%s"...',[filepath,filename1]);
    if exist([filepath,filename1],'file')
      [data1,SimStufen1] = LoadTracesSim([filepath,filename1],SimParam);
      %[data1, header] = LoadTracesProV51([filepath,filename1]);
      %dataerr = LoadTracesProErr(ChangeFileExt([filepath,filename1],'.err'));
      %data1 = AddErrInfo(data1,dataerr);
    else
      error('file not found!');
    end
    fprintf(1,'%d traces.\n',length(data1));
    data=[data,data1];
    SimStufen{length(data)}=SimStufen1;
    if length(data)>=MaxAnzBurst, break; end
  end
  save(tempfilename,'data');
  
end
if (MaxAnzBurst<inf) && (length(data)>MaxAnzBurst)
  data=data(1:MaxAnzBurst);
end

clear dataInt;
clear dataPF;
clear dataPFsigma;
AlldataInt = {};
AlldataPF = {};
AlldataPFsigma = {};
IntNoise = {};
%tBleachInt=[];
%tBleachFRET=[];
for i=1:length(data)
%   j=data(i).StartAnalysis;
%   while (data(i).Donor(j)+data(i).Acc(j)<40)&(j<data(i).StopAnalysis), j=j+1; end
%   data(i).StartAnalysis=j;
%   j=data(i).StopAnalysis;
%   while (data(i).Donor(j)+data(i).Acc(j)<40)&(j>data(i).StartAnalysis), j=j-1; end
%   data(i).StopAnalysis=j;
  
  if data(i).StopAnalysis-data(i).StartAnalysis<2, data(i).StopAnalysis=data(i).StartAnalysis; end

  AlldataPF{i}=max(0,min(1,(data(i).FRET(data(i).StartAnalysis:data(i).StopAnalysis))'));
  IntNoise{1,i}=data(i).DonorErr(data(i).StartAnalysis:data(i).StopAnalysis)';
  IntNoise{2,i}=data(i).AccErr(data(i).StartAnalysis:data(i).StopAnalysis)';
  AlldataInt{1,i}=max(0,data(i).Donor(data(i).StartAnalysis:data(i).StopAnalysis)');
  AlldataInt{2,i}=max(0,data(i).Acc(data(i).StartAnalysis:data(i).StopAnalysis)');

  k=2*(1+(IntNoise{1,i}+IntNoise{2,i})./(AlldataInt{1,i}+AlldataInt{2,i}));
  AlldataPFsigma{i}=max(1,(AlldataInt{1,i}+AlldataInt{2,i})./k);
%   k1=2*max(1,min(5,1+IntNoise{1,i}./AlldataInt{1,i})); 
%   k2=2*max(1,min(5,1+IntNoise{2,i}./AlldataInt{2,i}));
%   AlldataPFsigma{i}=max(1,(AlldataInt{1,i}./k2+AlldataInt{2,i}./k1));
    
end
fprintf('\n');
%HMMSelectBursts2;
%HMMSelectBursts3;
