function [data,SimStufen] = LoadTracesSim(filename,SimParam);
%Einladen von Timetraces aus einer MonteCarlo-Simulation

%Daten:
[data.Donor,data.Acc] = textread(filename,'%d\t%d','headerlines',2);

data.Donor=data.Donor-SimParam.DonorErr;
data.Acc=data.Acc-SimParam.AccErr;

data.FRET=data.Acc./(data.Donor+data.Acc+(data.Donor+data.Acc==0));
data.StartAnalysis=1;
data.StopAnalysis=length(data.Donor);

data.DonorErr=SimParam.DonorErr*ones(size(data.Donor));
data.AccErr=SimParam.AccErr*ones(size(data.Acc));

%Stufen
[pathstr,name,ext,versn] = fileparts(filename);
[SimStufen.Start,SimStufen.PFclass] = textread([pathstr,'\',name,'.state1'],'%d\t%d');
SimStufen.End=SimStufen.Start(2:end)-1;
SimStufen.Start(end)=[];
SimStufen.PFclass(end)=[];
SimStufen.End(end)=SimStufen.End(end)+1;
