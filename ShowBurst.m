
%iBurst=1;
%Qo=BestModel(iBurst);
if 0
  iBurst=iBurst+1;
  clf; hold on
  plot(data(iBurst).Donor+data(iBurst).DonorErr,'color',[.7 .7 1]);
  plot(data(iBurst).Acc+data(iBurst).AccErr,'color',[.5 .7 .5]);
  Start=data(iBurst).StartAnalysis;
  Stop=data(iBurst).StopAnalysis;
  plot([Start:Stop],AlldataInt{1,iBurst},'color',[0 0 1]);
  plot([Start:Stop],AlldataInt{2,iBurst},'color',[0 .5 0]);
  plot(Start*[1 1],[0 300],'r');
  plot(Stop*[1 1],[0 300],'r');
  title(sprintf('Burst %d',iBurst));
  axis tight;
  ylim([0 300]);
  return
end
%---------------------

if 1
    [data, header] = LoadTracesProV51([filepath,filename]);
    datacount=length(data);
    clear dataInt;
    clear dataPF;
    for i=1:length(data)
        if 1
            tBleachInt = FindTrajEnd(data(i).Donor+data(i).Acc,1000,10)-1;
            tBleachFRET = FindTrajEnd(data(i).FRET,-0.5,1)-1;
            data(i).StopAnalysis = min(tBleachFRET,tBleachInt);
            if data(i).StopAnalysis-data(i).StartAnalysis<2, data(i).StopAnalysis=data(i).StartAnalysis; end
        end

        dataInt{i}=data(i).Donor+data(i).Acc; 
        dataInt{i}=max(1,...
            dataInt{i}(data(i).StartAnalysis:data(i).StopAnalysis)'/TimeBin);
        dataPF{i}=max(0.01,min(0.99,...
            data(i).FRET(data(i).StartAnalysis:data(i).StopAnalysis)))';
        %dataInt{i}=max(1,...
        %    (data(i).Donor(data(i).StartAnalysis:data(i).StopAnalysis)+...
        %    data(i).Acc(data(i).StartAnalysis:data(i).StopAnalysis))'/500);
    end
end

if 1
    Qn=1;
        
    Q=Qn*Qo;
    obsvect = zeros(Qo,Qn);
    for o=1:Qo, obsvect(o,:) = o; end
    obsvect = reshape(obsvect',1,Q);
    
    mu1 = MultiData{Qo}(iBurst).mu1;
    Sigma1 = MultiData{Qo}(iBurst).Sigma1;
    mu1var = MultiData{Qo}(iBurst).mu1var;
    transmat1 = MultiData{Qo}(iBurst).transmat1;
    dtrans = diag(MultiData{Qo}(iBurst).transmat1);
    dwelltime1 = [-TimeBin./log(dtrans(Qn*(1:Qo)))]';
    mixmat1 = ones(Q,Mcount);
    prior1 = zeros(Q,1);
    for o=1:Qo, prior1((o-1)*Qn+1)=1/Qo; end
else
    mu1 = mu0;
    Sigma1 = Sigma0;
    mu1var = mu0var;
    transmat1 = transmat0;
end

IDBurst=[data(:).Mol];
dataStartMS=ones(1,length(data));

if 0
    if bUseBetaFunc
        B = beta_prob(dataPF{iBurst}, dataInt{iBurst}, mu1, Sigma1, mixmat1);
    else
        B = mixgauss_prob(dataPF{iBurst}, mu1, Sigma1);
    end
    Bobs = ExpandB(B,obsvect);
    path = viterbi_path(prior1, transmat1, Bobs);
    llpath = prob_path(prior1, transmat1, Bobs, path);
    LL = sum(llpath)/length(llpath);
    Stufen = Path2Stufen(path, 1:Q, 1, dataPF{iBurst}, dataInt{iBurst}, llpath, cMinLL);  %Nur Stufen 1..Q-1 auswählen, min. Stufenlänge
end

ShowBurstWithStufen(dataPF{iBurst},dataInt{iBurst},mu1,Sigma1,...
    mixmat1,transmat1,prior1,obsvect,2*bUseBetaFunc,...
    IDBurst(iBurst),dataStartMS(iBurst),TimeBin,-Inf,1:Q);

[mu1;dwelltime1]
