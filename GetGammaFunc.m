function [y]=GetGammaFunc(I1,I2,gamma,icase);
%function [y]=GetGammaFunc(I1,I2,gamma,icase);

Isumavg=max(1,I1*gamma+I2);
c=1000;
switch icase
  case 0
%     for i=1:size(j,1)
%       [R,P]=corrcoef(I2./Isumavg,Isumavg(j(i,:)));
%       ydummy(i)=diag(P,1);
%     end
%     y=mean(ydummy);
    [R,P]=corrcoef(I2./Isumavg,Isumavg);
    %[R,P]=corrcoef(I2./Isumavg,Isumavg(j));
    y=diag(P,1);
  case 1
    R=[];
    for i=1:c
      [w,idx]=sort(rand(1,length(I1)));
      j=idx(1:end-9);
      R(i)=diag(corrcoef(I2(j)./max(1,Isumavg(j)),max(1,Isumavg(j))),1);
    end
    y=std(R)*100;
end

% Isumavg=max(1,I1*gamma+I2);
% [R,P,RLO,RUP]=corrcoef(I2./Isumavg,Isumavg);
% switch icase
%   case 0
%     y=diag(P,1);
%   case 1
%     y=diag(RUP-RLO,1);
% end
