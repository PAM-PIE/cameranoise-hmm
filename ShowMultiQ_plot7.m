%ShowMultiQ_plot7
sSample={'DNA3','DNA12','DNA18','DNA19'};
sState={'Dynamic','Steady'};

% iSample=1;
% sAuswVer='10';
% sBurstSel='All'; %'Burst50';
Anz=10;
filepath1=['D:\Schluesche\Maindata\',sSample{iSample},'\',sState{1},'FRET\'];
filepath2=['D:\Schluesche\Maindata\',sSample{iSample},'\',sState{2},'FRET\'];
filename=sprintf('Ausw%s_%s_m%02d.mat',sAuswVer,sBurstSel,Anz);

% subplot(1,1,1);
% if bShowLL, subplot(Anz,Anz,[Anz*2+1 Anz*Anz]); end
% hold off
% plot(MultiQ,ChiSquare1,'.-','DisplayName',sState{1});
% hold on
% plot(MultiQ,ChiSquare2,'.-','color',[0 .5 0],'DisplayName',sState{2});
% legend show
% xlabel('number of states');
% ylabel('\Sigma\chi�');
% title(sprintf('%s (%s V%s)',sSample{iSample},sBurstSel,sAuswVer));

load([filepath1,filename],'MultiQData','MultiQ');
for q=1:length(MultiQ)
  subplot(Anz,2,(Anz-q)*2+1);
  plotLL(MultiQData{q}.trajLL);
  %text(length(MultiQData{q}.trajLL)+1,MultiQData{q}.trajLL(end),int2str(q));
  ylabel(int2str(q));
  box on
  if q==1, xlabel(sState{1}); end
  if q==7, text(0.8,1.5,sprintf('%s (%s V%s)',sSample{iSample},sBurstSel,sAuswVer),'Units','normalized'); end
end

load([filepath2,filename],'MultiQData','MultiQ');
for q=1:length(MultiQ)
  subplot(Anz,2,(Anz-q)*2+2);
  plotLL(MultiQData{q}.trajLL);
  box on
  if q==1, xlabel(sState{2}); end
  %if q==7, title(sprintf('(%s V%s)',sBurstSel,sAuswVer)); end
end

set(gcf,'InvertHardcopy','off');
set(gcf,'PaperPositionMode','auto');
saveas(gcf,['D:\Schluesche\Auswertung\',...
  sprintf('Ausw%s_%s %s trajLL',...
  sAuswVer,sBurstSel,sSample{iSample})],'png');
