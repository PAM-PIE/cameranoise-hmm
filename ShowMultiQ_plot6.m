%ShowMultiQ_plot6
%Transition rates (circle)
sSample={'DNA3','DNA12','DNA18','DNA19'};
sState={'Dynamic','Steady'};

% sAuswVer='10';
% sBurstSel='All'; %'Burst50';
% iSample=1;
% iState=2;

AnzArr=6:-1:2;
clf;
%set(gcf,'color','w');
cmin=0;
cmax=20;
clog=false;
ax={};

for iAnz=1:length(AnzArr)
  ax{iAnz}=subplot(length(AnzArr),1,iAnz);
  %ax{iAnz}=subplot('Position',[0.1,0.1+0.8*(length(AnzArr)-iAnz)/length(AnzArr),.8,0.2]);
  cla; hold on
  %if AnzArr(iAnz)~=4, continue; end
  
  if ~exist('MetaQData')
    filepath_=['D:\Schluesche\Maindata\'];
    load([filepath_,'Ausw',sAuswVer,'_',sBurstSel,'_MetaData.mat']);
    %load([filepath_,'Ausw',sAuswVer,'_',sBurstSel,'_MetaStufen.mat']);
  end
  
  Anz=AnzArr(iAnz);
  TimeBin=5;
  AnzStufen=[];
  MultiQData=MetaQData{iSample,iState};
  transmat1=MultiQData{Anz}.transmat1;
  mu1=MultiQData{Anz}.mu1;
  Q=length(diag(transmat1));
  Qo=length(mu1);
  T=transmat1(1:Qo,1:Qo);
  K=1./(TimeBin*1e-3).*logm(T);
  r0=1;

  filepath_=['D:\Schluesche\Maindata\',sSample{iSample},'\',sState{iState},'FRET\'];
  load([filepath_,'Ausw',sAuswVer,'_',sBurstSel,'_m0',int2str(Anz),'.mat'],'StufenArr');
  %StufenArr=MetaStufenArr{iSample,iState,Anz};
  
  DoubleArr=StufenFilter(StufenArr,'2+',true);
  if size(DoubleArr,1)>0
    W=hist3([[DoubleArr(:,1).PFclass]',[DoubleArr(:,2).PFclass]'],{1:Q,1:Q})';
  else
    W=zeros(Q);
  end
  
  x=r0*cos((0:0.01:1)*2*pi);
  y=r0*sin((0:0.01:1)*2*pi);
  plot3(x,y,0*x,'color',[1 1 1]*.5);
  
  x0=r0*cos(3/2*pi-mu1*2*pi);
  y0=r0*sin(3/2*pi-mu1*2*pi);
  for q=1:Anz 
    idx=find([StufenArr.PFclass]==q);
    AnzStufen(q)=length(idx);
    plot3([x0(q) x0(q)],[y0(q) y0(q)],[0 1]);
    text(x0(q),y0(q),1.2,sprintf('%.2f',mu1(q)),...
      'color',[1 1 1]*.5,...
      'HorizontalAlignment','center',...
      'VerticalAlignment','middle');
  end
  
  %transitions
  [wsort,isort]=sort(W(:),'descend');
  Wsum=cumsum(W(isort));
  istart=mod(isort-1,Q)+1;
  iend=div(isort-1,Q)+1;
  x1=x0(istart);
  y1=y0(istart);
  x2=x0(iend);
  y2=y0(iend);
  z1=[0;Wsum(1:end-1)./sum(W(:))]';
  z2=[Wsum./sum(W(:))]';
  istep=sign(iend-istart);
  c1=cmin;
  c2=K(isort);
  if clog
    c1=log10(c1);
    c2=log10(c2);
  end

  view([-10,50]);
  Wsum=[0;Wsum];
  for i=1:length(isort)
		%plot3([x1(i) x2(i)],[y1(i) y2(i)],[Wsum(i)./sum(W(:)) Wsum(i+1)./sum(W(:))],'r');
    X=[x1(i) x2(i);x1(i) x2(i)];
    Y=[y1(i) y2(i);y1(i) y2(i)];
    Z=[z1(i) z1(i);z2(i) z2(i)];
    C=[c1 c2(i);c1 c2(i)];
%     x=x0(istart(i):istep(i):iend(i));
%     y=y0(istart(i):istep(i):iend(i));
%     X=[x;x]; 
%     Y=[y;y];
%     Z=[repmat(z1(i),1,abs(iend(i)-istart(i))+1);repmat(z2(i),1,abs(iend(i)-istart(i))+1)];
    
    surf(X,Y,Z,C,...
        'EdgeColor','none',...
        'FaceColor','interp',...
         'CDataMode','auto',...
         'CDataMapping','scaled');
%         'FaceAlpha',0.2,...
%         'AlphaDataMapping','none');
    colormap('default');
    cmap=colormap;
    cmap=[.7 .7 .9;cmap];
    colormap(cmap);
    %colormap([.5 .5 .7;0.1 0.1 1]);
  end
  axis([-1 1 -1 1 0 1.5]);
  if clog
    set(gca,'clim',log10([cmin cmax]));
  else
    set(gca,'clim',[cmin cmax]);
  end
  %axis tight
  set(gca,'XTick',[]);
  set(gca,'YTick',[]);
  set(gca,'ZTick',[]);
%  zlabel(int2str(q),'Rotation',0);
  set(gca,'xcolor','w');
  set(gca,'ycolor','w');
  set(gca,'zcolor','w');
  box off

  drawnow
end
subplot(ax{1});
title(sprintf('%s %s (%s V%s)',sSample{iSample},sState{iState},sBurstSel,sAuswVer));
subplot(ax{end});
h=colorbar('SouthOutside');
% set(h,'XTick',[0 1 2 3 4],...
%   'XTickLabel',...
%   {'1','10','100','1000','10000'});
set(h,'xlim',[4/64 max(get(gca,'clim'))]);


set(gcf,'InvertHardcopy','off');
set(gcf,'PaperPositionMode','auto');
saveas(gcf,['D:\Schluesche\Auswertung\',...
  sprintf('Ausw%s_%s %s_%s KineticA',...
  sAuswVer,sBurstSel,sSample{iSample},sState{iState})],'png');
