%Auswertung Burstwise Tau �ber E
%clear variables;

if 1
  sSample={'DNA3','DNA12','DNA18','DNA19'};
  sState={'Dynamic','Steady'};
  HMMVersion;
  %sBurstSel='Burst50';
  %sBurstSel='All';

  bEinzelPlots=true;

  Anz=10;
  for iSample=1:4
    for iState=2%1:2
      for DeltaPF=0%:0.05:0.5

        ShowBurstwise_TauE
        drawnow;
        
        Dwell(:,iSample)=dwelltime1;

      end
    end
  end

else

  filepath='D:\Schluesche\Maindata\DNA3\DynamicFRET\';
  filename='Ausw22_All_m10.mat';
  load([filepath,filename],'AlldataPF','AlldataInt','AlldataPFsigma',...
    'bLearnBurstwise','bUseBetaFunc','cMinLL','Q',...
    'AnzBursts','MultiQData','TimeBin','AllStufen','BurstID','BurstGamma','BurstGammaW');
  dataPF=AlldataPF;
  dataInt=AlldataInt;
  dataPFsigma=AlldataPFsigma;
  
  AnzBursts=size(dataInt,2);
  AllStufen=[];
  dataLL=[];
  for iBurst=1:AnzBursts
    AllStufen{iBurst}=MultiQData{1}(iBurst).AllStufen;
    dataLL{iBurst}=ones(size(dataPF{iBurst}));
  end

  iBurst=1;
  HMMShowBurst;

end
