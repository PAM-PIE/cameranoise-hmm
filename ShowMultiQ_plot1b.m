sSample={'DNA3','DNA12','DNA18','DNA19'};
sState={'Dynamic','Steady'};

sVers='16';
sBurstSel='All';
iSample=2;
iState=1;
Anz=8;

%load(['D:\Schluesche\Maindata\Ausw',sVers,'_Sel1_MetaData.mat']);
%load(['D:\Schluesche\Maindata\Ausw',sVers,'_Sel1_MetaStufen.mat']);
%filepath1=['D:\Schluesche\Maindata\',sSample{iSample},'\SteadyFRET\'];
filepath=['D:\Schluesche\Maindata\',sSample{iSample},'\',sState{iState},'FRET\'];
% load([filepath,'Ausw',sVers,'_',sBurstSel,'_m0',int2str(Anz),'.mat'],'MultiQData');
% load([filepath,'Ausw',sVers,'_',sBurstSel,'_MetaStufen.mat']);

%filename=sprintf('Ausw2_Burst50_m%02d.mat',Anz);
%load([filepath1,filename],'MultiQData');
%MultiQData=MetaQData{iSample,iState};

x=0:0.01:1;
y=1:2:100;
xhist=x;
yhist=y;
hold off; clf;

for i=1:length(MultiQData)

  mu1=MultiQData{i}.mu1;
  Q=length(mu1);
  mu0=(1:Q)*1/(Q+1);
  StepCount=MultiQData{i}.StepCount;
  
  y=zeros(1,length(x));
  ix0=zeros(1,Q);
  ix1=zeros(1,Q);
  for q=1:Q
    N=StepCount(q);
    %a=mu1(q)*I;
    %b=(1-mu1(q))*I;
    %y1=betapdf(x,a,b);
    %y1=N*normpdf(x,mu1(q),0.01);
    %y=y+N*y1/max(y1);
    ix0(q)=find(x>=mu0(q),1);
    ix1(q)=find(x>=mu1(q),1);
  end
  %y=hist([MetaStufenArr{iSample,iState,i}.ProxFact],xhist);
  y=hist([MetaStufenArr{i}.ProxFact],xhist);
  y(find(~isfinite(y)))=NaN;
  y=y/max(y)*.8;
  hold on
  plot(x,Q,'k');
  plot(x,Q+.8,'--k');
  plot(x,Q+y);
  %stem(x(ix),Q+y(ix),'BaseValue',Q);
  plot(x(ix0),Q+y(ix0),'.','color',[0 .5 0]);
  plot(x(ix1),Q+y(ix1),'.r');
  for q=1:Q
    text(x(ix1(q)),Q+y(ix1(q)),sprintf('%.2f',mu1(q)),...
      'HorizontalAlignment','center','VerticalAlignment','bottom');
  end
end
ylim([1 Q+.999]);
xlim([0 1]);
title(sprintf('%s %s',sSample{iSample},sState{iState}));

%saveas(gcf,ChangeFileExt(filename,['_',sSample,'_mu1']),'png');
