%HMMShowBurst

%iBurst = 0;

iBurst = iBurst + 1;

figure(1);
AllStufen=AllStufen_1;
dataLL=dataLL_1;
mu1=mu1_1;
Q = Q_1;

ShowBurstWithStufen(...
  dataInt{1,iBurst}, dataInt{2,iBurst}, [], IntNoise{1,iBurst}, IntNoise{2,iBurst}, [], ...
  iBurst, 0, TimeBin, cMinLL, 1:Q, AllStufen{iBurst}, dataLL{iBurst}, mu1, [], []);

figure(2);
AllStufen=AllStufen_2;
dataLL=dataLL_2;
mu1=mu1_2;
Q = Q_2;

ShowBurstWithStufen(...
  dataInt{1,iBurst}, dataInt{2,iBurst}, [], IntNoise{1,iBurst}, IntNoise{2,iBurst}, [], ...
  iBurst, 0, TimeBin, cMinLL, 1:Q, AllStufen{iBurst}, dataLL{iBurst}, mu1, [], []);
  
I1=dataInt{1,iBurst}; I2=dataInt{2,iBurst}; var(I2./(I1+I2));
