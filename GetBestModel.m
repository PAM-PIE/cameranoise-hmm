
%i=strfind(filename,'.');
%filenamestamm=filename(1:i(end)-1);
%load([filepath,filenamestamm,'_c.mat']);
clear LL;
clear AnzStufen;
clear NoParams;

Omax=6;
for o=1:Omax
for iMol=1:length(MultiData)
    LL(o,iMol)=MultiData{o}(iMol).trajLL(end);
    AnzStufen(o,iMol)=length(MultiData{o}(iMol).AllStufen);
end
end
%Nuller durch Einser ersetzen, damit der log nicht scheitert!
AnzStufen(find(AnzStufen==0))=1;

Nmax=Omax;
NoParams=repmat((1:Nmax)'.*((1:Nmax)'+2),1,length(MultiData));
BIC=2*LL-NoParams.*log(AnzStufen);

[temp,BestModel]=max(BIC);

mu1=[];
dwell1=[];
for iMol=1:length(MultiData)
    o=max(1,BestModel(iMol)-0);
    %o=2;
    mu1=[mu1,MultiData{o}(iMol).mu1];
    dwell1=[dwell1,MultiData{o}(iMol).dwelltime1];
    dwell1(find(dwell1==inf))=1e10;
    dwell1(find(dwell1==-inf))=1e10;
end
hold off
semilogy(mu1,dwell1,'.');
hold on
plot([0 1],TimeBin*[1,1],'--r','LineWidth',2);
