% Show RawData Variance Analysis, Tracewise
sSample={'DNA3','DNA12','DNA18','DNA19','Donor_Only','Acceptor_Only'};
sState={'Dynamic','Steady'};

iSample=2;
iState=2;

MaxAnzBurst = 5;
sBurstSel='Burst50';
cIntDivider=1;
Ivaradd=0; %0.002;

filepath=['D:\Schluesche\Maindata\',sSample{iSample},'\'];
if iSample<5
  filepath=[filepath,sState{iState},'FRET\'];
end
filename='TBPDNA_*.txt';
if exist('filepath_')
  if ~strcmp(filepath,filepath_)
    HMMLoadData;
  end
else
  HMMLoadData;
end
filepath_=filepath;

AnzBursts=length(AlldataPF);
I1mean=zeros(1,AnzBursts);
I2mean=zeros(1,AnzBursts);
I1var=zeros(1,AnzBursts);
I2var=zeros(1,AnzBursts);

for iBurst=1:AnzBursts
  I1=AlldataInt{1,iBurst}-IntNoise{1,iBurst};
  I2=AlldataInt{2,iBurst}-IntNoise{2,iBurst};
  Inoise1=IntNoise{1,iBurst};
  Inoise2=IntNoise{2,iBurst};
  I1mean(iBurst)=mean(I1);
  I2mean(iBurst)=mean(I2);
  I1var(iBurst)=((mean(I1+Inoise1)))*(2);
  I2var(iBurst)=((mean(I2+Inoise2)))*(2);
  [s1 v1]=normfit(I1);
  [s2 v2]=normfit(I2);

  I1mean(iBurst)=s1;
  I2mean(iBurst)=s2;
  I1var(iBurst)=v1^2;
  I2var(iBurst)=v2^2;
end
clf; hold off
axmax=min(max([I1mean,I2mean]),max([I1var,I2var]));
Fitx=exp(log(1):0.1:log(axmax)); %log-spaced x-values
Fity1=(Fitx+mean([IntNoise{1,:}]))*(2);
Fity2=(Fitx+mean([IntNoise{2,:}]))*(2);
plot(Fitx,Fity1,'color',[0 .5 0]);
hold on
plot(Fitx,Fity2,'color',[1 0 0]);
plot(I1mean,I1var,'.','color',[0 .5 0]);
plot(I2mean,I2var,'.','color',[1 0 0]);

stitle=sSample{iSample};
if iSample<5, stitle=[stitle,' ',sState{iState}]; end
stitle=[stitle,' ',sBurstSel]; 
title(stitle,'Interpreter','none');
xlabel('mean');
ylabel('variance');

set(gcf,'InvertHardcopy','off');
%set(gcf,'PaperPositionMode','auto');
%saveas(gcf,['PhotStatistik ',stitle],'png');
