function [sresult]=FuncSeqPlus1(seq,N);
%function [sresult]=FuncSeqPlus1(seq,N);

for i=(length(seq)+1):N
  seq=['0',seq];
end
sresult=seq;
for i=1:length(seq)
  sresult(i)=int2str(str2num(seq(i))+1);
end
