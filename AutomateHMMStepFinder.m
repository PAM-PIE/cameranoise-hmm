clear variables;
bAutomate=true;
%!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
%"clear variables" und "filepath=..." in HMMStepFinder auskommentieren!
%!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!

%Automate HMMStepfinder
%sendmail('n.zarrabi@physik.uni-stuttgart.de','calculations started','Calculations startet...');
%sendmail('nawid.zarrabi@gmx.de','calculations started','Calculations startet...');

%filepathBase = 'D:\Schluesche';

filepath='F:\Maindata\DNA3\DynamicFRET\';
HMMStepFinder;
filepath='F:\Maindata\DNA12\DynamicFRET\';
HMMStepFinder;
filepath='F:\Maindata\DNA18\DynamicFRET\';
HMMStepFinder;
filepath='F:\Maindata\DNA19\DynamicFRET\';
HMMStepFinder;
if 0
  filepath='D:\Schluesche\Maindata\DNA3\SteadyFRET\';
  HMMStepFinder;
  filepath='D:\Schluesche\Maindata\DNA12\SteadyFRET\';
  HMMStepFinder;
  filepath='D:\Schluesche\Maindata\DNA18\SteadyFRET\';
  HMMStepFinder;
  filepath='D:\Schluesche\Maindata\DNA19\SteadyFRET\';
  HMMStepFinder;
else
  filepath='D:\Schluesche\Maindata\DNA3\SteadyFRET\';
  for q=1:7
    sDummy=sprintf('copy "%sAusw%s_All_m%02d.mat" "%sAusw%s_All_m%02d.mat"',filepath,'16',q,filepath,sAuswVer,q);
    system(sDummy);
  end
  filepath='D:\Schluesche\Maindata\DNA12\SteadyFRET\';
  for q=1:7
    sDummy=sprintf('copy "%sAusw%s_All_m%02d.mat" "%sAusw%s_All_m%02d.mat"',filepath,'16',q,filepath,sAuswVer,q);
    system(sDummy);
  end
  filepath='D:\Schluesche\Maindata\DNA18\SteadyFRET\';
  for q=1:7
    sDummy=sprintf('copy "%sAusw%s_All_m%02d.mat" "%sAusw%s_All_m%02d.mat"',filepath,'16',q,filepath,sAuswVer,q);
    system(sDummy);
  end
  filepath='D:\Schluesche\Maindata\DNA19\SteadyFRET\';
  for q=1:7
    sDummy=sprintf('copy "%sAusw%s_All_m%02d.mat" "%sAusw%s_All_m%02d.mat"',filepath,'16',q,filepath,sAuswVer,q);
    system(sDummy);
  end
end
% filepath='D:\Schluesche\Maindata\Donor_Only\';
% HMMStepFinder;

%sendmail('n.zarrabi@physik.uni-stuttgart.de','calculations finished','All calculations successfully finished');
%sendmail('nawid.zarrabi@gmx.de','calculations finished','All calculations successfully finished');
% system('c:\windows\system32\shutdown.exe -s -t 00');
system('c:\windows\system32\shutdown.exe -s -t 00');
return

clear variables
fclose all;
CreateMetaQData;
ShowMultiQ_plotMaster;
%HMMShowBurst_Master;
%ShowBurstwise_Master

system('c:\windows\system32\shutdown.exe -s -t 00');
