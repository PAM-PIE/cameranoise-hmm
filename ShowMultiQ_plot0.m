% Show RawData EFret Histogram
%clear variables;
sSample={'DNA3','DNA12','DNA18','DNA19'};
sState={'Dynamic','Steady'};

%MaxAnzBurst = Inf;
%sBurstSel='All';
%cIntDivider=1;
%Ivaradd=0; %0.002;
xhist=0:0.015:1; dxhist=xhist(2)-xhist(1);
xhist=[xhist(1)-dxhist,xhist,xhist(end)+dxhist];

iState=1;
filepath=['D:\Schluesche\Maindata\',sSample{iSample},'\',sState{iState},'FRET\'];
filename='TBPDNA_*.txt';
load(sprintf('%sAusw%s_%s_m%02d',filepath,sAuswVer,sBurstSel,1),'MaxAnzBurst','TimeBin','TimeBinNew');
HMMLoadData;
%dataPF1=[AlldataInt{2,:}]./([AlldataInt{1,:}]+[AlldataInt{2,:}]+([AlldataInt{1,:}]+[AlldataInt{2,:}]==0));
dataPF1=[AlldataPF{:}];
fprintf('%s %s: %d Traces\n',sSample{iSample},sState{iState},length(AlldataPF));

iState=2;
filepath=['D:\Schluesche\Maindata\',sSample{iSample},'\',sState{iState},'FRET\'];
filename='TBPDNA_*.txt';
HMMLoadData;
%dataPF2=[AlldataInt{2,:}]./([AlldataInt{1,:}]+[AlldataInt{2,:}]+([AlldataInt{1,:}]+[AlldataInt{2,:}]==0));
dataPF2=[AlldataPF{:}];
fprintf('%s %s: %d Traces\n',sSample{iSample},sState{iState},length(AlldataPF));

yhist1=hist(dataPF1,xhist); 
yhist2=hist(dataPF2,xhist); 

xhist=xhist(2:end-1);
yhist1=yhist1(2:end-1);
yhist2=yhist2(2:end-1);
hold off
stairs(xhist,yhist1,'b');
hold on
stairs(xhist,yhist2,'r');

title(sprintf('%s %s',sSample{iSample},sBurstSel));

ASCIIexport=[xhist(:) yhist1(:) yhist2(:) yhist1(:)/sum(yhist1) yhist2(:)/sum(yhist2)];
filepath_='D:\Schluesche\Auswertung\';
filename_=sprintf('Ausw%s_%s %s FretHist',...
  sAuswVer,sBurstSel,sSample{iSample});

set(gcf,'InvertHardcopy','off');
set(gcf,'PaperPositionMode','auto');
% set(gcf,'PaperUnits','points');
% set(gcf,'PaperSize',[560 420]);
saveas(gcf,[filepath_,filename_],'png');
dlmwrite([filepath_,filename_,'.txt'], ASCIIexport, 'delimiter', '\t')
