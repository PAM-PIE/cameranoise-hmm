clear bUpdate
if ~exist('bUpdate'), bUpdate=true; end
if bUpdate
    IDBurst=[data(:).Mol];
    dataStartMS=ones(1,length(data));
    
    fprintf(1,'%6d Stufen-Eigenschaften rechnen: %6d',[length(AllStufen),0]); 
    NoStufen=zeros(numel(AllStufen),1);
    BurstLen=zeros(numel(AllStufen),1);
    BurstIavg=zeros(numel(AllStufen),1);
    for i=1:length(AllStufen)
        NoStufen(i)=numel(AllStufen{i}); 
        if NoStufen(i)>0
            BurstLen(i)=length(dataInt{i}); %AllStufen{i}(end).End-AllStufen{i}(1).Start+1; 
            BurstIavg(i)=mean(dataInt{i});
        else
            BurstLen(i)=0;
            BurstIavg(i)=0;
        end        
        if mod(i,100)==0, fprintf(1,'\b\b\b\b\b\b%6d',[i]); end
    end
    fprintf(1,'\b\b\b\b\b\b%6d',[i]);
    %SimStufen
    if exist('filenamesim')
        SimParam=LoadSimParam(filenamesim);
        [SimStufen]=LoadSimStufen(filenamesim, dataStartMS, BurstLen, IDFile, SimParam.AnzFree+1, SimParam);
    else
        SimStufen={};
    end
    %Fertig
    fprintf(1,'\b\b\b\b\b\bok.\n'); 
    bUpdate=false;
end

if 0
    subplot(2,1,1);
    hist(NoStufen,1:max(NoStufen));
    ylabel('#events');
    xlabel('#Stufen per Burst');
    box on;

    subplot(2,1,2);
    hist(BurstLen,1:max(BurstLen));
    ylabel('#events');
    xlabel('BurstLen (TimeSteps)');
    box on;
end

if 1
    for iBurst=1:length(BurstLen)
        if length(dataInt{iBurst})<=1, continue; end
        if exist('filenamesim')
            ShowBurstWithStufen(...
                dataPF{iBurst},dataInt{iBurst},mu1,Sigma1,mixmat1,transmat1,prior1,obsvect,bUseBetaFunc*2,...
                IDBurst(iBurst),dataStartMS(iBurst),TimeBin,-Inf,1:Q,SimStufen{iBurst},SimParam);
        else
            ShowBurstWithStufen(...
                dataPF{iBurst},dataInt{iBurst},mu1,Sigma1,mixmat1,transmat1,prior1,obsvect,bUseBetaFunc*2,...
                IDBurst(iBurst),dataStartMS(iBurst),TimeBin,-Inf,1:Q);
        end
        fprintf('\t%5d\t%4d\t%4.1f\t%4d\n',...
            [[iBurst]',...
            [BurstLen(iBurst)]',...
            [BurstIavg(iBurst)]',...
            [NoStufen(iBurst)]']'); 
        if iBurst<length(BurstLen), waitforbuttonpress; end
    end
    %ShowHMMResults(AllStufen,dataInt,Q);
else
    ShowBurst=10;
    [sortw, sorti]=sort(NoStufen);
    disp([[sorti(end:-1:end-ShowBurst)]', [sortw(end:-1:end-ShowBurst)]', [BurstLen(sorti(end:-1:end-ShowBurst))]']); 
    %[sortw, sorti]=sort(AllLoglik);
    %disp({'i','LL','Len','Iavg','#Stufen'});
    i=numel(sorti);
    iBurstArr=[];
    while (length(iBurstArr)<ShowBurst)&(i>1);
        if NoStufen(sorti(i))>2
            iBurstArr(end+1)=sorti(i);
            fprintf('\t%5d\t%4f\t%4d\t%4.1f\t%4d\n',...
                [[sorti(i)]',...
                [sortw(i)]',...
                [BurstLen(sorti(i))]',...
                [BurstIavg(sorti(i))]',...
                [NoStufen(sorti(i))]']'); 
        end
        i=i-1;
    end
    fprintf('\n');

    
    for i=1:length(iBurstArr)
        iBurst=iBurstArr(i);
        if isempty(SimStufen)
            ShowBurstWithStufen(dataPF{iBurst},dataInt{iBurst},mu1,Sigma1,...
                mixmat1,transmat1,prior1,obsvect,...
                IDBurst(iBurst),dataStartMS(iBurst),TimeBin,-Inf,1:Q);
        else
            ShowBurstWithStufen(dataPF{iBurst},dataInt{iBurst},mu1,Sigma1,...
                mixmat1,transmat1,prior1,obsvect,...
                IDBurst(iBurst),dataStartMS(iBurst),TimeBin,-Inf,1:Q,...
                SimStufen{iBurst},SimParam);
        end
        waitforbuttonpress
    end
    
    %iBurst=308;
    %iBurst=201;
    %ShowBurstWithStufen(dataPF{iBurst},dataInt{iBurst},mu1,Sigma1,mixmat1,transmat1,prior1,IDBurst(iBurst),dataStartMS(iBurst),TimeBin,cMinLL,1:Q);
end
clear bUpdate
