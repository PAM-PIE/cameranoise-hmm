function [StufenArr]=ShrinkStufen(StufenArr)
%function [StufenArr]=ShrinkStufen(StufenArr)

Len=StufenArr.End-StufenArr.Start+1;
for iStufe=2:length(StufenArr.PFclass)
  if (StufenArr.PFclass(iStufe)==StufenArr.PFclass(iStufe-1)) & ...
      (StufenArr.iBurst(iStufe)==StufenArr.iBurst(iStufe-1))
    StufenArr.Start(iStufe)=StufenArr.Start(iStufe-1);
    StufenArr.MW(iStufe)=...
      (Len(iStufe-1)*StufenArr.MW(iStufe-1)+Len(iStufe)*StufenArr.MW(iStufe))/...
      (Len(iStufe-1)+Len(iStufe));
    StufenArr.MWstd(iStufe)=sqrt(...
      (Len(iStufe-1)*(StufenArr.MWstd(iStufe-1)).^2+Len(iStufe)*(StufenArr.MWstd(iStufe))).^2/...
      (Len(iStufe-1)+Len(iStufe)));
    StufenArr.Iavg1(iStufe)=StufenArr.Iavg1(iStufe-1)+StufenArr.Iavg1(iStufe);
    StufenArr.Iavg2(iStufe)=StufenArr.Iavg1(iStufe-1)+StufenArr.Iavg2(iStufe);
    StufenArr.LL(iStufe)=NaN;
    StufenArr.PFclass(iStufe-1)=0;
  end  
end

idx=find(StufenArr.PFclass>0);
sFields=fieldnames(StufenArr);
for iField=1:length(sFields)
  StufenArr.(sFields{iField})=StufenArr.(sFields{iField})(idx);
end
