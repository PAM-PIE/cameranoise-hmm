sSample={'DNA3','DNA12','DNA18','DNA19'};
sState={'Dynamic','Steady'};

% sAuswVer='7';
% sBurstSel='Burst50';
% iSample=1;
% iState=1;
% Anz=5;

clear dataPF;

filepath=['D:\Schluesche\Maindata\',sSample{iSample},'\',sState{iState},'FRET\'];
load([filepath,sprintf('Ausw%s_%s_m%02d.mat',sAuswVer,sBurstSel,Anz)]);
HMMVersion;
% if ~exist('dataPF')
%   for iBurst=1:AnzBursts
%     dataPF{iBurst}=(data(iBurst).FRET(data(iBurst).StartAnalysis:data(iBurst).StopAnalysis))';
%     dataPF{iBurst}=max(0,min(1,dataPF{iBurst}));
%   end
% end
for q=1:length(MultiQData)
  if ~isfield(MultiQData{q},'mu0')
    load([filepath,sprintf('Ausw%s_%s_m%02d.mat',sAuswVer,sBurstSel,length(MultiQData{q}.mu1))],'mu0');
    MultiQData{q}.mu0=mu0; 
  end  
end

return
ShowMemo;
