%Recalc Stufen
return

clear variables

XXsSample={'DNA3','DNA12','DNA18','DNA19'};
XXsState={'Dynamic','Steady'};
XXAnz=7;
XXsAusw='8';
%sBurstSel='Burst50';
XXsBurstSel='All';

XXMultiQData={};
for XXiSample=1:numel(XXsSample)
  for XXiState=1:numel(XXsState)
    for XXq=1:XXAnz
      XXfilepath=['D:\Schluesche\MainData\',XXsSample{XXiSample},'\',XXsState{XXiState},'FRET\'];
      XXfilename=sprintf('Ausw%s_%s_m%02d.mat',XXsAusw,XXsBurstSel,XXq);
      fprintf('%s %s (%d)...',XXsSample{XXiSample},XXsState{XXiState},XXq);
      %FuncRecalcStufen([XXfilepath,XXfilename]);
      XXMultiQData=FuncRepaireMultiQData([XXfilepath,XXfilename],XXq,XXMultiQData);
      fprintf('ok\n');
    end
  end
end
