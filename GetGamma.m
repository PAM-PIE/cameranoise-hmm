% Estimate Gamma-Factor for every trace individually
clear variables;

sSample={'DNA3','DNA12','DNA18','DNA19','Donor_Only','Acceptor_Only','Sim1','Sim2'};
sState={'Dynamic','Steady'};

MaxAnzBurst = Inf;
TimeBin=5;
TimeBinNew=TimeBin;
HMMVersion;

bsave=true;
iSelBurst=2;

for iSample=1%1:4
  for iState=1%:2

    clear Ekorr Ekorrw
    if iSample<7
      filepath=['D:\Schluesche\Maindata\',sSample{iSample},'\'];
      if iSample<5
        filepath=[filepath,sState{iState},'FRET\'];
      end
      filename='TBPDNA_*.txt';
      if exist('filepath_')
        if ~strcmp(filepath,filepath_)
          HMMLoadData;
        end
      else
        HMMLoadData;
      end
    else
      %filepath='D:\Nawid\Tools\MonteCarloFCS\Sim_Schluesche_1\';
      filepath=['D:\Schluesche\Maindata\',sSample{iSample},'\'];
      filename='Sim_*.txt';
      if exist('filepath_')
        if ~strcmp(filepath,filepath_)
          HMMLoadDataSim;
        end
      else
        HMMLoadDataSim;
      end      
    end
    filepath_=filepath;

    AnzBursts=length(AlldataPF);
    if bsave
      BurstGamma=zeros(1,AnzBursts);
      BurstGammaW=zeros(1,AnzBursts);
    end

    if bsave, fprintf('Estimate Gamma, Burst %4d',0); end

    if bsave
      iBurstArr=1:AnzBursts;
    else
      iBurstArr=iSelBurst;
    end
    for iBurst=iBurstArr
      %jBurst=iSelBurst;
      jBurst=iBurst;
      istart=1;
      iend=length(AlldataInt{1,jBurst});
      I1=AlldataInt{1,jBurst}(istart:iend);
      I2=AlldataInt{2,jBurst}(istart:iend);
      Inoise1=IntNoise{1,jBurst};
      Inoise2=IntNoise{2,jBurst};
      PF=AlldataPF{jBurst}(istart:iend);

      %Isumavg=@(x)smooth(I1*x+I2,20)';
      %Isumavg=@(x)mean(I1*x+I2)*ones(size(I1));
      switch cAuswVer
        case 18
          Isumavg=@(x)I1*x+I2;
          fhelp=@(x)var(Isumavg(x));
        case 19
          Isumavg=@(x)I1*x+I2;
          fhelp=@(x)var(Isumavg(x))/mean(Isumavg(x));
%         case 20
%           Isumavg=@(x)max(1,I1*x+I2);
%           [w,idx]=sort(rand(1,length(I1)));
%           j=1:length(I1);
%           %j(idx(1:10))=[];
%           I1=smooth(I1(j),1)'+1*randn(size(I1(j))).*2.*sqrt(I1(j));
%           I2=smooth(I2(j),1)'+1*randn(size(I2(j))).*2.*sqrt(I2(j));
%           fhelp=@(x)1-GetGammaFunc(I1,I2,x,0);
        case {20,21,22}
          Isumavg=@(x)max(1,I1*x+I2);
          fh=@(x)corrcoef(I2./Isumavg(x),Isumavg(x));
          fhelp=@(x)diag(fh(x),1);
          fhelpw=@(x)1-fhelp(x);
      end

      %fhelp=@(x)max(diff(smooth(Isumavg(x),20)))/mean(Isumavg(x));
      %fhelp=@(x)var(Isumavg(x));
      %fhelp=@(x)var(I1*x+I2);
      %gamma=fminsearch(fhelp,1);
      if cAuswVer<=19
        gamma=fminbnd(fhelp,eps,2);
      else
        if (fhelp(1/4)>0) & (fhelp(4)<0)
          gamma=fzero(fhelp,[1/4 4]);
          %gamma=data(1).GammaFactor;
          %gamma=1;
          gammaw=fminbnd(fhelpw,2*eps,4);
          gammaw=1-fhelpw(gammaw);
          gammaw=sum((Isumavg(gamma)-Isumavg(1)).^2)./length(Isumavg(1));
        else
          gamma=mean([data.GammaFactor]);
          gammaw=0;
        end
      end

      BurstGamma(iBurst)=gamma;

      Isum=I1*gamma+I2;

      x=2.^(-2:0.02:2);
      y=zeros(size(x));
      for i=1:length(x)
        y(i)=fhelp(x(i));
      end

      if cAuswVer<=19
        [w,i]=min(y);
        temp=diff(diff(log2(y)));
        i=min(i,length(temp));
        BurstGammaW(iBurst)=temp(i);
      elseif cAuswVer==20
        BurstGammaW(iBurst)=gammaw;
%        BurstGammaW(iBurst)=GetGammaFunc(I1,I2,gamma,1);
      elseif cAuswVer>=21
        BurstGammaW(iBurst)=gammaw;
      end

      if ~bsave
        clf; ax=[];

        ax(1)=subplot(4,1,1); cla; hold on; box on
        plot(I1+I2,'color',[1 .4 .7]);
        plot((Isum));
        plot(Isumavg(gamma),'color',[1 .5 0]);
        axis tight;
        ylim([0 300]);
        ylabel('Isum');
        title(sprintf('Burst %d',iBurst));

        ax(2)=subplot(4,1,2); cla, hold on; box on
        E=I2./(I1.*gamma+I2+(I1.*gamma+I2==0));
        plot(PF,'color',[1 .4 .7]);
        plot(E);
        axis tight;
        %title(sprintf('Burst %d: \\gamma=%.2f',iBurst,gamma));
        ylim([0 1]);
        ylabel('E');
        grid on

        ax(3)=subplot(4,1,3); cla, hold on; box on
        plot(I1*gamma,'color',[0 .5 0]);
        plot(I2,'r');
        plot(Inoise1*gamma,'color',[0 .5 0]);
        plot(Inoise2,'r');
        axis tight;
        ylim([0 150]);
        ylabel('count rates');

        subplot(4,1,4); cla; hold on; box on
        set(gca,'XScale','log');
        if cAuswVer<=19, set(gca,'YScale','log'); end
        plot(x,y);
        plot(gamma*[1 1],ylim,'color',[1 .5 0]);
        axis tight;
        set(gca,'XTick',2.^[log2(min(x)):log2(max(x))]);
        if cAuswVer<=19
          title(sprintf('\\gamma=%.2f, w=%.1f',BurstGamma(iBurst),1e3*BurstGammaW(iBurst)));
        else
          title(sprintf('\\gamma=%.2f, w=%.2f',BurstGamma(iBurst),BurstGammaW(iBurst)));
        end
        xlabel('\gamma');
        ylabel('f(\gamma)');

        linkaxes(ax,'x');
        drawnow;
        %waitforbuttonpress;
      else
        fprintf('\b\b\b\b%4d',iBurst);
      end
    end

    if bsave
      idx=find((BurstGamma>=0.5)&(BurstGamma<=1.5));
      Ekorr=ones(size(BurstGamma));
      EkorrW=zeros(size(BurstGamma));
      Ekorr(idx)=BurstGamma(idx);
      EkorrW(idx)=BurstGammaW(idx);
      save([filepath,'EkorrV',sAuswVer,'.mat'],'Ekorr','EkorrW','BurstGamma','BurstGammaW');
    end
    if bsave, fprintf(' ok.\n'); end

  end
end
