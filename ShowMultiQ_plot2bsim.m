% FRET Histograms from the data and not from distributions
sSample={'DNA3','DNA12','DNA18','DNA19','Donor_Only','Acceptor_Only','Sim1'};
sState={'Dynamic','Steady',''};
filepathStamm=['D:\Schluesche\Maindata\',sSample{iSample},'\'];
if iSample<=4
  filepathStamm=[filepathStamm,'\',sState{iState},'FRET\'];
end

%load MetaData first
%MultiQData=MetaQData{iSample,iState};
load([filepathStamm,'\Ausw16_Sim1_m07.mat'],'MultiQData');

subplot(2,1,1); cla; hold on
x=1:Anz; 
cmap=colormap;
imap=round(interp1([0 1],[1 length(cmap)],((1:Anz)-1)/(Anz-1)))
cmap2=cmap(imap,:);
for Q=x
  %y(q)=sum(MultiQData{q}.mu1var(:));
  %y=NaN*zeros(Q,1);
  for q=1:Q
    y=MultiQData{Q}.mu1(q);
    y1=max(0.0001,sqrt(MultiQData{Q}.mu1var(q)));
    %plot(Q,y,'.','MarkerSize',20,'color',cmap2(q,:));
    rectangle('Position',[Q-.4,y-y1/2,.8,y1],...
         'FaceColor',cmap2(q,:));
  end
end

%set(gca,'ColorOrder',cmap2);
xlabel('states');
ylabel('\mu \pm \sigma');
set(gca,'XTick',1:Anz);
axis([0 Anz+1 0 1]);
title(sprintf('%s %s (%s V%s)',sSample{iSample},sState{iState},sBurstSel,sAuswVer));
box on;

subplot(2,1,2);
y=zeros(Anz); 
x=1:Anz; 
for Q=x
  %y(q)=sum(MultiQData{q}.mu1var(:));
  for q=1:Q
    y(Q,q)=MultiQData{Q}.mu1var(q);%.*MultiQData{Q}.StepCount(q)/sum(MultiQData{Q}.StepCount)*Q;
  end
end
bar(x,y,'stacked');
xlabel('states');
ylabel('\Sigma\sigma^2');
ylim([0 0.06]);
box on;

set(gcf,'InvertHardcopy','off');
set(gcf,'PaperPositionMode','auto');
saveas(gcf,['D:\Schluesche\Auswertung\',...
  sprintf('Ausw%s_%s %s_%s var1',...
  sAuswVer,sBurstSel,sSample{iSample},sState{iState})],'png');
