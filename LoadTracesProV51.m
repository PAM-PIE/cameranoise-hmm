function [data, header] = LoadTracesPro(filename);
%function [data] = LoadTracesPro(filename);
%function [data, header] = LoadTracesPro(filename);
%This function opens TXT-Files from the DataAnalysis Software "TracesPro"
%by Peter Schl�sche, Ludwig-Maximillins-Universit�t M�nchen, 
%Center for NanoScience, CUP, Prof. C. Br�uchle, Group Don Lamb

datainfo={};
fid = fopen(filename);
%read header lines
head1 = fgetl(fid);
head2 = fgetl(fid);
%reader file info
while 1
    temp = fgetl(fid);
    sep = regexp(temp,'\t');
    if sep(1)==1, break, end
    datainfo{end+1} = temp(1:sep(1)-1);
end
%get number of traces
temp = datainfo{strmatch('# of Traces',datainfo)};
TracesCount = str2num(temp(regexp(temp,':')+1:end));
%get number of frames per trace
temp = datainfo{strmatch('# of Frames',datainfo)};
FramesCount = str2num(temp(regexp(temp,':')+1:end));
%get shape of data file
ColCount = length(regexp(head1,'\t'))+1;
%HeadCount = ColCount-3*TracesCount-3;

temp=findstr(head1,sprintf('\t'));
temp=[0,temp,length(head1)+1];
header1={};
for i=1:length(temp)-1
    header1{i}=deblank(head1(temp(i)+1:temp(i+1)-1));
end
temp=findstr(head2,sprintf('\t'));
temp=[0,temp,length(head2)+1];
header2={};
for i=1:length(temp)-1
    header2{i}=deblank(head2(temp(i)+1:temp(i+1)-1));
end
%for i=1:length(header1)
%    header{i}=[header1{i},'_',header2{i}];
%end
header=[header1;header2];

HeadCount=strmatch('Frames',header1)-3;

%read header info in cell array
% fseek(fid,0,'bof');
% [header1a] = textscan(fid, '%s', HeadCount+2, 'delimiter', '\t', 'whitespace', '');
% [header2a] = textscan(fid, '%s', 3*TracesCount+1, 'delimiter', '\t', 'whitespace', '');
% [header1b] = textscan(fid, '%s', HeadCount+2, 'delimiter', '\t', 'whitespace', '');
% [header2b] = textscan(fid, '%s', 3*TracesCount+1, 'delimiter', '\t', 'whitespace', '');
% fclose(fid);
% header = [header1a{:}';header1b{:}'];

datahead = dlmread(filename,'\t',[2 1 2+TracesCount-1 1+HeadCount-1]);
dataframes = dlmread(filename,'\t',[2 HeadCount+3 2+FramesCount-1 ColCount-1]);

%'-1.#IND00'

for iMol = 1:TracesCount
    for iField = 1:size(datahead,2)
        fieldname = [char(header(1,iField+1)),char(header(2,iField+1))];
        fieldname = strrep(fieldname,'#','');
        fieldname = strrep(fieldname,'-','');
        fieldname = strrep(fieldname,' ','');
        if ~isempty(fieldname)
            %fieldname = ['header_',fieldname];
            data(iMol).(fieldname) = datahead(iMol,iField);
        end
    end
end

for iField = 1:size(dataframes,2)
    iMol = str2num(char(regexp(header1{iField+HeadCount+3},'\d+','match')));
    idata = find([data.Mol]==iMol);
    fieldname = [char(header2(iField+HeadCount+3))];
    data(idata).(fieldname) = [dataframes(:,iField)];
end

if exist('header'), header = datainfo'; end

