sSample={'DNA3','DNA12','DNA18','DNA19'};
sState={'Dynamic','Steady'};

HMMVersion;
Anz=1;

for iSample=1:4
  for iState=1:2

    filepath=['D:\Schluesche\Maindata\',sSample{iSample},'\',sState{iState},'FRET\'];
    load([filepath,'Ausw',sAuswVer,'_',sBurstSel,'_m0',int2str(Anz),'.mat'],'AlldataInt','TimeBin');

    AnzBursts=size(AlldataInt,2);
    Imean=[];
    Ivar=[];
    for i=1:AnzBursts
      Imean(i)=mean(AlldataInt{1,i}+AlldataInt{2,i});
      Ivar(i)=var(AlldataInt{1,i}+AlldataInt{2,i});
    end

    xhist=0:20:400;
    yhist=hist(Imean,xhist);
    OrigExp(:,1)=xhist;
    OrigExp(:,iState+1)=yhist;
    
    subplot(2,1,iState);
    hist(Imean,xhist);
    axis tight;
    title(sprintf('%s %s (%s V%s)',sSample{iSample},sState{iState},sBurstSel,sAuswVer));
    xlabel('sum intensity');
    ylabel(sprintf('total #: %d',AnzBursts));
  end
  drawnow;

  set(gcf,'InvertHardcopy','off');
  set(gcf,'PaperPositionMode','auto');
  filepath_='D:\Schluesche\Auswertung\';
  filename_=sprintf('Ausw%s_%s %s Imean',...
    sAuswVer,sBurstSel,sSample{iSample});
  saveas(gcf,[filepath_,filename_],'png');
  
  fid=fopen([filepath_,filename_,'.txt'],'w');
  fprintf(fid,'Isum\tdynamic\tsteady\n');
  fclose(fid);
  dlmwrite([filepath_,filename_,'.txt'],OrigExp,'-append','delimiter','\t');
end
