
AnzBursts=length(AlldataInt);

iBurst_=0;
iDel=[];
IntNoise_=[];
AlldataInt_=[];
AlldataPF_=[];
AlldataPFsigma_=[];
FileListIdx_=[];
BurstID=1:AnzBursts;
for iBurst=1:AnzBursts
  if cAuswVer<=20
    bBurstOk=EkorrW(iBurst)>5e-4;
  else
    bBurstOk=EkorrW(iBurst)>.65;
  end
  if bBurstOk
    iBurst_=iBurst_+1;
    IntNoise_{1,iBurst_}=IntNoise{1,iBurst};
    IntNoise_{2,iBurst_}=IntNoise{2,iBurst};
    AlldataInt_{1,iBurst_}=AlldataInt{1,iBurst};
    AlldataInt_{2,iBurst_}=AlldataInt{2,iBurst};
    AlldataPF_{iBurst_}=AlldataPF{iBurst};
    AlldataPFsigma_{iBurst_}=AlldataPFsigma{iBurst};
    FileListIdx_(iBurst_)=FileListIdx(iBurst);
  else
    iDel=[iDel iBurst];
  end
end
IntNoise=IntNoise_;
AlldataInt=AlldataInt_;
AlldataPF=AlldataPF_;
AlldataPFsigma=AlldataPFsigma_;
FileListIdx=FileListIdx_;
data(iDel)=[];
BurstID(iDel)=[];
clear IntNoise_ AlldataInt_ AlldataPF_ AlldataPFsigma_ FileListIdx_ i AnzBursts temp istart iend iDel
