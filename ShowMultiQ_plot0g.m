sSample={'DNA3','DNA12','DNA18','DNA19'};
sState={'Dynamic','Steady'};

sAuswVer='10';
sBurstSel='All'; %'Burst50';
iSample=2;
iState=2;
Anz=1;

filepath_=['D:\Schluesche\Maindata\',sSample{iSample},'\',sState{iState},'FRET\'];
load([filepath_,'Ausw',sAuswVer,'_',sBurstSel,'_m0',int2str(Anz),'.mat'],'AlldataInt','IntNoise');

AnzBursts=length(AlldataInt);
I1var_exp=zeros(1,AnzBursts);
I2var_exp=zeros(1,AnzBursts);
I1mean=zeros(1,AnzBursts);
I2mean=zeros(1,AnzBursts);
I1var=zeros(1,AnzBursts);
I2var=zeros(1,AnzBursts);

clf;
for iBurst=1:AnzBursts

  I1=AlldataInt{1,iBurst}-IntNoise{1,iBurst};
  I2=AlldataInt{2,iBurst}-IntNoise{2,iBurst};
  Inoise1=IntNoise{1,iBurst};
  Inoise2=IntNoise{2,iBurst};

  I1var_exp(iBurst)=(mean(I1+Inoise1))*(4);
  I2var_exp(iBurst)=(mean(I2+Inoise2))*(4);
  I1mean(iBurst)=mean(I1);
  I2mean(iBurst)=mean(I2);
  I1var(iBurst)=var(I1);
  I2var(iBurst)=var(I2);

end

i=find([I1var./I1var_exp]>4); I1var(i)=[]; I1mean(i)=[]; I1var_exp(i)=[];
i=find([I2var./I2var_exp]>4); I2var(i)=[]; I2mean(i)=[]; I2var_exp(i)=[];
  
subplot(2,1,1); hold on;
plot(I1mean,I1var,'.','color',[.7 .9 .7]);
plot(I1mean,I1var_exp,'.','color',[0 .5 0]);

subplot(2,1,2); hold on;
plot(I2mean,I2var,'.','color',[1 .8 .8]);
plot(I2mean,I2var_exp,'.','color',[1 0 0]);

fprintf('  %s_%s   %.4f    %.4f\n',sSample{iSample},sState{iState},mean(I1var./I1var_exp),mean(I2var./I2var_exp));


