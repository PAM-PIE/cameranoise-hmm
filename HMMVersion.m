%HMMVersion

cAuswVer=24;

switch cAuswVer
  case 16
    sVers='22.01.2009';
    sAuswVer=int2str(cAuswVer);
    sBurstSel='All';
    %sBurstSel='Burst50';
  case 17
    sVers='10.03.2009';
    sAuswVer=int2str(cAuswVer);
    sBurstSel='All';
  case 18
    sVers='31.03.2009';
    sAuswVer=int2str(cAuswVer);
    sBurstSel='All';
  case 19
    sVers='01.04.2009';
    sAuswVer=int2str(cAuswVer);
    sBurstSel='All';
  case 20
    sVers='10.04.2009';
    sAuswVer=int2str(cAuswVer);
    %sBurstSel='Sim1';
    sBurstSel='Burst50';
  case 21
    sVers='02.04.2009';
    sAuswVer=int2str(cAuswVer);
    sBurstSel='All';
  case 22
    sVers='24.04.2009';
    sAuswVer=int2str(cAuswVer);
    sBurstSel='All';
  case 23 %test
    sVers='18.05.2009';
    sVers='29.05.2009';
    sAuswVer=int2str(cAuswVer);
    sBurstSel='All';
  case 24
    sVers='29.05.2009';
    sAuswVer=int2str(cAuswVer);
    sBurstSel='All';
  case 25
    sVers='31.05.2009';
    sAuswVer=int2str(cAuswVer);
    sBurstSel='All';
  case 26
    sVers='03.10.2009';
    sAuswVer=int2str(cAuswVer);
    sBurstSel='All';
  case 27
    sVers='08.12.2009';
    sAuswVer=int2str(cAuswVer);
    sBurstSel='All';
end
