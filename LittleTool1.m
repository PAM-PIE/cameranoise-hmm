

%load('D:\Schluesche\Maindata\Ausw24_All_MetaData.mat');
for iState=1%:2
  fprintf('\n');
  for Q=1:10
    NumberOfFrames=zeros(4,Q);
    for iSample=1:4
      mu1=MetaQData{iSample,iState}{Q}.mu1;
      mu1std=sqrt(MetaQData{iSample,iState}{Q}.mu1var);
      AllStufen=MetaQData{iSample,iState}{Q}.AllStufen;
      StepCount=MetaQData{iSample,iState}{Q}.StepCount;
      StufenLength=zeros(1,sum(StepCount));
      StufenPFclass=zeros(1,sum(StepCount));
      i=1;
      for iBurst=1:length(AllStufen)
        icount=length(AllStufen{iBurst});
        StufenLength(i:i+icount-1)=[AllStufen{iBurst}(:).End]-[AllStufen{iBurst}(:).Start]+1;
        StufenPFclass(i:i+icount-1)=[AllStufen{iBurst}(:).PFclass];
        i=i+icount;
      end
      NumberOfFrames(iSample,:)=accumarray(StufenPFclass',StufenLength,[Q 1])';
    end
    for q=1:Q
      fprintf('%d',Q);
      for iSample=1:4
        fprintf('\t%.2f�%.2f\t%.0f%%',[MetaQData{iSample,iState}{Q}.mu1(q);sqrt(MetaQData{iSample,iState}{Q}.mu1var(q));100*NumberOfFrames(iSample,q)/sum(NumberOfFrames(iSample,:))]);
      end
      fprintf('\n');
    end
  end
end
