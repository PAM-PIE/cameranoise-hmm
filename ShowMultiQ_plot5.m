% Calc Corr Func

for q=1:Q
  dataQ{q}=[];
end
StatesCount=zeros(1,Q);
fprintf('Calc Burst %3d',0);
for iBurst=1:AnzBursts
%   if ~exist('dataPF')
%     dataPF{iBurst}=dataInt{2,iBurst}./(dataInt{1,iBurst}+dataInt{2,iBurst}+...
%       (dataInt{1,iBurst}+dataInt{2,iBurst}==0));
%   end
  for iStufe=1:numel(AllStufen{iBurst})
    q=AllStufen{iBurst}(iStufe).PFclass;
    i=[AllStufen{iBurst}(iStufe).Start:...
      AllStufen{iBurst}(iStufe).End];
    if i(end)>length(dataPF{iBurst}), i(end)=[]; end;
    datatemp=[dataPF{iBurst}(i)];
    dataQ{q}=[dataQ{q},datatemp];
    StatesCount(q)=StatesCount(q)+1;
  end
  fprintf('\b\b\b%3d',iBurst);
end
fprintf('\b\b\bok\n');

for q=1:Q
  subplot(Q,1,Q-q+1); cla; xlim([0 1]);
  if length(dataQ{q})>0
    FI=fft(dataQ{q}-mean(dataQ{q}));
    FC=FI.*conj(FI);
    C=real(ifft(FC))/real(mean(dataQ{q})^2);
    plot(0*C,'k'); hold on
    %plot(dataQ{q}-mean(dataQ{q}));
    plot(C);
  end
  ylabel(sprintf('�=%.2f\ntau=%2.1f',mu1(q),dwelltime1(q)));
  stitle=sprintf('%d steps, %d bins',StatesCount(q),length(dataQ{q}));
  if q==Q, stitle=sprintf('%s %s %s V%s\n%s',sSample{iSample},sState{iState},sBurstSel,sAuswVer,stitle); end
  title(stitle);
  %ylim([-1 1]*max(abs(C(200:600))));
  axis tight;
  %xlim([0 min(500,length(C))]);
  xlim([0 500]);
  ylim([-1 1]*20);
  if q==1, xlabel('\tau/Frames'); end
  drawnow;
end

set(gcf,'InvertHardcopy','off');
set(gcf,'PaperPositionMode','auto');
saveas(gcf,['D:\Schluesche\Auswertung\',...
  sprintf('Ausw%s_%s %s_%s CorrFunc_Q%d',...
  sAuswVer,sBurstSel,sSample{iSample},sState{iState},Q)],'png');
