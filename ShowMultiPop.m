function []=ShowMultiPop(MultiStufen,AnzRow,AnzCol);

AnzMulti=length(MultiStufen);
for iMulti=1:AnzMulti-1
    subplot(AnzRow,AnzCol,iMulti);
    %hist([MultiStufen{iMulti}(:).MW],0:0.01:1); axis tight;
    hist([MultiStufen{iMulti}(:).PFclass],iMulti); axis tight;
    
    title(int2str(iMulti));
end
