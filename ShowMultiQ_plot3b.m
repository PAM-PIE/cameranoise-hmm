%ShowMultiQ_plot3b
%Transition rates
sSample={'DNA3','DNA12','DNA18','DNA19'};
sState={'Dynamic','Steady'};

sAuswVer='8';
sBurstSel='All'; %'Burst50';
iState=1;
AnzArr=6:-1:3;

for iAnz=1:length(AnzArr)
  Anz=AnzArr(iAnz);
  TimeBin=5;
  xhist=0:0.02:1;
  K={};
  W={};
  Mu1={};
  yhist={};
  Test1=[];
  figure(Anz-3); cla; hold on;
  for iSample=1:2

    if ~exist('MetaQData')
      filepath_=['D:\Schluesche\Maindata\'];
      load([filepath_,'Ausw',sAuswVer,'_',sBurstSel,'_MetaData.mat']);
      load([filepath_,'Ausw',sAuswVer,'_',sBurstSel,'_MetaStufen.mat']);
    end
    StufenArr=MetaStufenArr{iSample,iState,Anz};
    DoubleArr=StufenFilter(StufenArr,'2+',true);
    TripleArr=StufenFilter(StufenArr,'3+',true);

    MultiQData=MetaQData{iSample,iState};
    transmat1=MultiQData{Anz}.transmat1;
    mu1=MultiQData{Anz}.mu1;
    Q=length(diag(transmat1));
    Qo=length(mu1);
    %K{end+1}=TimeBin./logm(transmat1(1:Qo,1:Qo));
    K{end+1}=(transmat1(1:Qo,1:Qo));
    Mu1{end+1}=mu1;

    W{end+1}=hist3([[DoubleArr(:,1).PFclass]',[DoubleArr(:,2).PFclass]'],{1:Q,1:Q})';
    W{end}=W{end}+eye(Q).*repmat(hist([StufenArr.PFclass],1:Q),Q,1);
    
    yhist{end+1}=hist([StufenArr.ProxFact],xhist);
  end

  for q=1:Q, Test1(:,q)=q+10*(1:Q); end
  iperms=perms(1:Q);
  K1=K{1};
  W1=W{1};
  W1=W1./sum(W1(:));
  diff=[];
  fprintf('\n');
  for i=1:size(iperms,1)
    K2=K{2}(iperms(i,:),:);
    K2=K2(:,iperms(i,:));
    W2=W{2}(iperms(i,:),:);
    W2=W2(:,iperms(i,:));
    W2=W2./sum(W2(:));
    Test2=Test1(iperms(i,:),:);
    Test2=Test2(:,iperms(i,:));
    diff(i)=sqrt(sum((W1(:).*K1(:)-W2(:).*K2(:)).^2));
  end
  [w,j]=sort(diff);
  imax=10;
  if length(j)<imax
    imax=length(j)
  else
    fprintf('  ');
    fprintf('%d',iperms(j(end),:));
    fprintf(': %.2f\n',diff(j(end)));
    fprintf('  ...\n');
  end
  for i=imax:-1:1
    fprintf('  ');
    fprintf('%d',iperms(j(i),:));
    fprintf(': %.2f\n',diff(j(i)));
  end
  for i=1:Q
    fprintf('  %.2f -> %.2f\n',Mu1{1}(i),Mu1{2}(iperms(j(1),i)));
  end

  yhist{1}=yhist{1}./max(yhist{1});
  yhist{2}=yhist{2}./max(yhist{2});
  plot(xhist,Anz+.4*yhist{1});
  plot(xhist,Anz+1-.4*yhist{2});
  for i=1:Q
    x1=Mu1{1}(i);
    x2=Mu1{2}(iperms(j(1),i));
    i1=find(xhist>=x1,1,'first');
    i2=find(xhist>=x2,1,'first');
    plot([x1 x1],Anz+[.4*yhist{1}(i1) .4],':','color',[1 1 1]*.7);
    plot([x2 x2],Anz+1-[.4*yhist{2}(i2) .4],':','color',[1 1 1]*.7);
    plot([x1 x2],Anz+[.4 .6],'r.-');
  end
  box on
  axis tight;
  set(gca,'YTick',Anz);
end
