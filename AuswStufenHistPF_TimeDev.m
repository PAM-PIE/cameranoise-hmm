HiddenStatesCount=6;
% Generiert Stufenhistogramm
filepath='D:\Nawid\Schluesche\2008-01-17\TBPDNA3_200610\';
for iMovie=1:3
    subplot(3,1,iMovie);
    load('-mat', sprintf('%sMov%02d_gesamt%02d.mat',filepath,iMovie,HiddenStatesCount));

    fprintf('#%i\t',iMovie); 
    StufenArr=StufenHMM2StufenArrPF(AllStufen,dataInt,5,1);
    
    plotLL(loglik(2:length(loglik))); pause;
    
    hist([StufenArr(:).ProxFact],0:1/50:1); axis tight
    title(sprintf('Movie %d',iMovie));
    ylim([0 35]);
    drawnow;

    fprintf('\t%5.2f',mu1); fprintf('\n');
    fprintf('\t%5.2f',mu1var); fprintf('\n');
    fprintf('\t%5.0f',dwelltime1); fprintf('\n');
    %Frame-weise:
    %hist([AlldataPF{:}],0:0.01:1); axis tight
end
fprintf('\n');
