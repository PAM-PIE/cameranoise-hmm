
MaxAnzBurst = 50;

AnzBursts=length(AlldataInt);
MaxAnzBurst=min(MaxAnzBurst,AnzBursts);
xhist=1:1:1000;
yhist=hist([AlldataInt{1,:}],xhist);
[w,i1]=max(smooth(yhist));
x1=xhist(i1);
yhist=hist([AlldataInt{2,:}],xhist);
[w,i2]=max(smooth(yhist));
x2=xhist(i2);
I1mean=zeros(1,AnzBursts);
I2mean=zeros(1,AnzBursts);
for iBurst=1:AnzBursts
  I1mean(iBurst)=mean(AlldataInt{1,iBurst});
  I2mean(iBurst)=mean(AlldataInt{2,iBurst});
end
dist=sqrt((I1mean-x1).^2+(I2mean-x2).^2);
[w,iBest]=sort(dist); iBest=iBest(1:MaxAnzBurst); w=w(1:MaxAnzBurst);

iBurst_=0;
iDel=[];
for iBurst=1:AnzBursts
  bBurstOk=~isempty(find(iBest==iBurst));
  if bBurstOk
    iBurst_=iBurst_+1;
    IntNoise_{1,iBurst_}=IntNoise{1,iBurst};
    IntNoise_{2,iBurst_}=IntNoise{2,iBurst};
    AlldataInt_{1,iBurst_}=AlldataInt{1,iBurst};
    AlldataInt_{2,iBurst_}=AlldataInt{2,iBurst};
    AlldataPF_{iBurst_}=AlldataPF{iBurst};
    AlldataPFsigma_{iBurst_}=AlldataPFsigma{iBurst};
  else
    iDel=[iDel iBurst];
  end
end
IntNoise=IntNoise_;
AlldataInt=AlldataInt_;
AlldataPF=AlldataPF_;
AlldataPFsigma=AlldataPFsigma_;
data(iDel)=[];
clear IntNoise_ AlldataInt_ i AnzBursts temp istart iend iDel
