clear variables;
bAutomate=true;
%!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
%"clear variables" und "filepath=..." in HMMStepFinder auskommentieren!
%!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!

%Automate HMMStepfinder
%sendmail('n.zarrabi@physik.uni-stuttgart.de','calculations started','Calculations startet...');
sendmail('nawid.zarrabi@gmx.de','calculations started','Calculations startet...');

clear variables; fclose all;
bAutomate=true;
filepath='D:\Schluesche\Maindata\DNA3\DynamicFRET\';
fmain=0.5:0.5:7.5;
for ifmain=1:length(fmain)
  fa=fmain(ifmain);
  fb=fmain(ifmain);
  HMMStepFinder;
end
sendmail('nawid.zarrabi@gmx.de','DNA3 finished','All calculations successfully finished');

clear variables; fclose all;
bAutomate=true;
filepath='D:\Schluesche\Maindata\DNA12\DynamicFRET\';
fmain=0.5:0.5:7.5;
for ifmain=1:length(fmain)
  fa=fmain(ifmain);
  fb=fmain(ifmain);
  HMMStepFinder;
end
sendmail('nawid.zarrabi@gmx.de','DNA12 finished','All calculations successfully finished');

clear variables; fclose all;
bAutomate=true;
filepath='D:\Schluesche\Maindata\DNA18\DynamicFRET\';
fmain=0.5:0.5:7.5;
for ifmain=1:length(fmain)
  fa=fmain(ifmain);
  fb=fmain(ifmain);
  HMMStepFinder;
end
sendmail('nawid.zarrabi@gmx.de','DNA18 finished','All calculations successfully finished');

clear variables; fclose all;
bAutomate=true;
filepath='D:\Schluesche\Maindata\DNA19\DynamicFRET\';
fmain=0.5:0.5:7.5;
for ifmain=1:length(fmain)
  fa=fmain(ifmain);
  fb=fmain(ifmain);
  HMMStepFinder;
end
sendmail('nawid.zarrabi@gmx.de','DNA19 finished','All calculations successfully finished');

clear variables; fclose all;
bAutomate=true;
filepath='D:\Schluesche\Maindata\DNA3\SteadyFRET\';
fmain=0.5:0.5:7.5;
for ifmain=1:length(fmain)
  fa=fmain(ifmain);
  fb=fmain(ifmain);
  HMMStepFinder;
end
sendmail('nawid.zarrabi@gmx.de','DNA3 finished','All calculations successfully finished');

clear variables; fclose all;
bAutomate=true;
filepath='D:\Schluesche\Maindata\DNA12\SteadyFRET\';
fmain=0.5:0.5:7.5;
for ifmain=1:length(fmain)
  fa=fmain(ifmain);
  fb=fmain(ifmain);
  HMMStepFinder;
end
sendmail('nawid.zarrabi@gmx.de','DNA12 finished','All calculations successfully finished');

clear variables; fclose all;
bAutomate=true;
filepath='D:\Schluesche\Maindata\DNA18\SteadyFRET\';
fmain=0.5:0.5:7.5;
for ifmain=1:length(fmain)
  fa=fmain(ifmain);
  fb=fmain(ifmain);
  HMMStepFinder;
end
sendmail('nawid.zarrabi@gmx.de','DNA18 finished','All calculations successfully finished');

clear variables; fclose all;
bAutomate=true;
filepath='D:\Schluesche\Maindata\DNA19\SteadyFRET\';
fmain=0.5:0.5:7.5;
for ifmain=1:length(fmain)
  fa=fmain(ifmain);
  fb=fmain(ifmain);
  HMMStepFinder;
end
sendmail('nawid.zarrabi@gmx.de','DNA19 finished','All calculations successfully finished');

sendmail('n.zarrabi@physik.uni-stuttgart.de','calculations finished','All calculations successfully finished');

system('c:\windows\system32\shutdown.exe -s -t 00');

return

clear variables
CreateMetaQData
ShowMultiQ_plotMaster
