%Check Traces

clen=10;
a1=zeros(1,AnzBursts);
a2=zeros(1,AnzBursts);
b1=zeros(1,AnzBursts);
b2=zeros(1,AnzBursts);

for iBurst=1:AnzBursts
  I1=dataInt{1,iBurst};
  I2=dataInt{2,iBurst};
  if length(I1)>clen*2
    a1(iBurst)=mean(I1(1:clen));
    a2(iBurst)=mean(I2(1:clen));
    b1(iBurst)=mean(I1(end-clen+1:end));
    b2(iBurst)=mean(I2(end-clen+1:end));
  else
    fprintf('Burst %d ist k�rzer als %d frames!',iBurst,2*cLen);
  end
  a=a1+a2;
  b=b1+b2;
end
