function [StufenArrOut]=StufenArr_CalcAnzahl(StufenArr);

maxAnzahl = max([StufenArr.iPos]);
for iAnz=1:maxAnzahl
    iStart=find([StufenArr.iPos]==iAnz);
    for i=1:length(iStart)
        for iDiff=1:iAnz
            StufenArr(iStart(i)-iDiff+1).Anzahl=iAnz;
        end
    end
end

StufenArrOut=StufenArr;

end