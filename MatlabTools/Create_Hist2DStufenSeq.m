%Auswertung 18.10.2007

gamma = 1.957;
r0 = 4.9; %nm
bCalcPF = false; %set this to false when loading old version bap-files


if ~exist('StufenArr','var')
    filepath = 'C:\Monika\2006\a-EGFP-c-Alexa568\Hydrolyse\2006-04-07\';
    filenameBAP='EFoF1-a-EGFP-c-Alexa568-1mMATP-150myW-b2.bap';
    %filepath = 'C:\Monika\2006\a-EGFP-c-Alexa568\F0\';
    %filenameBAP='ef0-a-egfp-c-alexa568-synthese-150myw-21-24-04-2006.bap';
    %filepath = 'C:\Monika\2006\a-EGFP-c-Alexa568\Synthese\';
    %filenameBAP='efof1-a-egfp-c-alexa568-synthese-150myw-2006-04-18-20-2005-09-12-13.bap';
    [BurstArr, StufenArr] = BAPLoadProject([filepath,filenameBAP], gamma, r0, bCalcPF);
    [pathstr,name,ext,versn] = fileparts(filenameBAP);
    filename_stamm = fullfile(filepath,name);
end
[DoubleArr] = StufenFilter(StufenArr, '2+');
[TripleArr] = StufenFilter(StufenArr, '3+');

close all;

BAPCreatePFChart36favorit([filepath,filenameBAP], {DoubleArr, TripleArr}, bCalcPF, r0, '36+72', '_Hist2DStufenSeq');
if bSaveDiag, saveas(gcf,[filename_stamm,'_Hist2DStufenSeq'],'png'); end
