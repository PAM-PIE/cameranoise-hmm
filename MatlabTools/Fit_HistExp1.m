function [yfit] = Fit_HistExp1(y,xbin,xstart,ylog)

bBild = nargout==0;
y=y(:);
xbin = xbin(:);
xPlus1 = xbin(2)-xbin(1)+xbin(end);
xbinPlus1 = [xbin;xPlus1];
ybinPlus1 = [hist([y;xPlus1],xbinPlus1)]';
ybin = ybinPlus1(1:end-1);
ixstart = round(xstart/(xbin(2)-xbin(1)));

if bBild
    hold off;
    xlim_ = [Inf -Inf];       
    ax_ = gca; %subplot(1,1,1);
    set(ax_,'Box','on');
    axes(ax_); 
end
 
ybin = ybin(:);
if bBild
    h_ = bar(xbin,ybin,'BarWidth',1,'FaceColor',0.9*[1 1 1]);
    xlim_(1) = min(xlim_(1),min(xbin));
    xlim_(2) = max(xlim_(2),max(xbin));
    if ylog
        set(h_,'BaseValue',0.1);
        set(ax_,'YScale','log');
    end
    hold on;

    % Nudge axis limits beyond data limits
    if all(isfinite(xlim_))
       xlim_ = xlim_ + [-1 1] * 0.01 * diff(xlim_);
       set(ax_,'XLim',xlim_)
    end
end

% --- Create fit
%fo_ = fitoptions('method','NonlinearLeastSquares','Robust','On','Algorithm','Levenberg-Marquardt','MaxFunEvals',5688,'MaxIter',3935,'TolFun',1e-007);
fo_ = fitoptions('method','NonlinearLeastSquares');
st_ = [max(y) -1./mean(y) ];
set(fo_,'Startpoint',st_);
yweight=ybin; yweight(1:max(1,ixstart-1))=0;
set(fo_,'Weight',yweight); 
ft_ = fittype('exp1' );

% Fit this model using new data
try
    cf_ = fit(xbin,ybin,ft_ ,fo_);

    % Plot this fit
    if bBild
        h_ = plot(xbin,cf_(xbin)); %,'fit',0.95);
        legend off;  % turn off legend from plot method call
        set(h_(1),'Color',[1 0 0],...
             'LineStyle','-', 'LineWidth',2,...
             'Marker','none', 'MarkerSize',6);
        hold off;
        xlabel('dwell time/ms');
        ylabel('#levels');
    else
        yfit = cf_;
    end    

    if ylog
        ylim([0.9 max(ybin)*1.1]);
    else
        ylim([0 max(ybin)*1.1]);
    end
    %disp(cf_);
    %fprintf(1,'\ttau1: %.2f\n',[-1/cf_.b]);
    %fprintf(1,'\ttau2: %.2f\n',[-1/cf_.d]);

    s = sprintf('%3.1f', cf_.a);
    text(0.7,0.9,['A = ',s],'units','normalized');
    s = sprintf('%3.1f ms', -1/cf_.b);
    text(0.7,0.8,['\tau = ',s],'units','normalized');

catch
    disp('Error during fit');
    yfit = [];
end
