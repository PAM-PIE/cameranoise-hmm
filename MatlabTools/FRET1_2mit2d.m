filepath='H:\Stefan\ATPase\2Laser\Synthese\Auswertung\';
filenameAuro='Synthese-Auswertung-ADP-e';
filenameATP='Synthese-Auswertung-Auro-b';
if 1
    [BurstArrATP, StufenArrATP]=BAPLoadProject([filepath,filenameATP,'.bap'],1.89,4.9,false);
    [BurstArrAuro, StufenArrAuro]=BAPLoadProject([filepath,filenameAuro,'.bap'],1.89,4.9,false);
end

AnzPlot=10;
bEinzeln=true;
close all;

if bEinzeln, figure; else subplot(AnzPlot,2,1); end
    idx=find(([StufenArrATP.iPos]>1)&([StufenArrATP.iPos]<[StufenArrATP.Anzahl]));
    Hist([StufenArrATP(idx).Length],1:20:200,5,false)
    Fit_HistExp1([StufenArrATP(idx).Length],1:10:200,5,false);
    AnzBurstsATP=length(BurstArrATP);
    title(sprintf('ATP (%d Bursts)',AnzBurstsATP),'Interpreter','none');
    LengthATP=([StufenArrATP(idx).Length]);
    V=hist(LengthATP,1:10:200)';
    
if bEinzeln, figure; else subplot(AnzPlot,2,2); end
    idx=find(([StufenArrAuro.iPos]>1)&([StufenArrAuro.iPos]<[StufenArrAuro.Anzahl]));
    Fit_HistExp1([StufenArrAuro(idx).Length],1:10:200,15,false);
    AnzBurstsAuro=length(BurstArrAuro);
    title(sprintf('Aurovertin (%d Bursts)',AnzBurstsAuro),'Interpreter','none');
    LengthAuro=([StufenArrAuro(idx).Length]);
    W=hist(LengthAuro,1:10:200)';
