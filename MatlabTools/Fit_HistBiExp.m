function [yfit] = Fit_HistBiExp(y)
%FIT_HISTBIEXP    Create plot of datasets and fits
%   FIT_HISTBIEXP(Y)
%   Creates a plot, similar to the plot in the main curve fitting
%   window, using the data that you provide as input.  You can
%   apply this function to the same data you used with cftool
%   or with different data.  You may want to edit the function to
%   customize the code and this help message.
%
%   Number of datasets:  1
%   Number of fits:  1

 
% Data from dataset "y":
%    Y = y:
%    Unweighted
%
% This function was automatically generated on 01-Jun-2006 13:34:19

% Set up figure to receive datasets and fits
f_ = clf;
figure(f_);
legh_ = []; legt_ = {};   % handles and text for legend
xlim_ = [Inf -Inf];       % limits of x axis
ax_ = subplot(1,1,1);
set(ax_,'Box','on');
axes(ax_); hold on;

 
% --- Plot data originally in dataset "y"
x_1 = (1:numel(y))';
y = y(:);
if nargout==0
    %h_ = line(x_1,y,'Parent',ax_,'Color',[0.333333 0 0.666667],...
    %     'LineStyle','none', 'LineWidth',1,...
    %     'Marker','.', 'MarkerSize',12);
    h_ = bar(y,'BarWidth',1,'FaceColor',0.9*[1 1 1]);
    xlim_(1) = min(xlim_(1),min(x_1));
    xlim_(2) = max(xlim_(2),max(x_1));
    legh_(end+1) = h_;
    legt_{end+1} = 'y';

    % Nudge axis limits beyond data limits
    if all(isfinite(xlim_))
       xlim_ = xlim_ + [-1 1] * 0.01 * diff(xlim_);
       set(ax_,'XLim',xlim_)
    end
end

% --- Create fit "fit 3"
fo_ = fitoptions('method','NonlinearLeastSquares','Robust','On','Algorithm','Levenberg-Marquardt','MaxFunEvals',5688,'MaxIter',3935,'TolFun',1e-007);
st_ = [-190.94084544299 -0.2594845732828 0.01909806713521 0.1783915516612 ];
set(fo_,'Startpoint',st_);
ft_ = fittype('exp2' );

% Fit this model using new data
cf_ = fit(x_1,y,ft_ ,fo_);

% Or use coefficients from the original fit:
if 0
   cv_ = {-512287566.8081, -9.40917545742, 50.6010118037, -0.3638327095087};
   cf_ = cfit(ft_,cv_{:});
end

% Plot this fit
if nargout==0
    h_ = plot(cf_,'fit',0.95);
    legend off;  % turn off legend from plot method call
    set(h_(1),'Color',[1 0 0],...
         'LineStyle','-', 'LineWidth',2,...
         'Marker','none', 'MarkerSize',6);
    legh_(end+1) = h_(1);
    legt_{end+1} = 'fit 3';
    hold off;
    legend(ax_,legh_, legt_);
else
    yfit = cf_;
end    

disp(cf_);
fprintf(1,'\ttau1: %.2f\n',[-1/cf_.b]);
fprintf(1,'\ttau2: %.2f\n',[-1/cf_.d]);
ylim([0 max(y)*1.1]);
