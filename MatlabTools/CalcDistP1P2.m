function [dist] = CalcDistP1P2(rc, d, h, phi)
%Berechnet den Abstand von zwei Punkten im Raum
%P2 befindet sich auf einem Kreis mit Radius rc um den Ursprung auf der xy-Ebene,
%P1 befindet sich links davon im Abstand -d, um h nach oben in z-Richtung verschoben.

if size(phi,1)==1, phi=phi'; end;

P1 = [rc*cos(phi), rc*sin(phi), zeros(length(phi),1)];
P2 = ones(length(phi),1)*[-d, 0, h];
dist = zeros(length(phi),1);

for i=1:length(phi)
    n = (P2(i,:)-P1(i,:))';
    n = n/norm(n);
    x0 = -P1(i,:)*n;
    dist(i) = P2(i,:)*n+x0;
end

end
