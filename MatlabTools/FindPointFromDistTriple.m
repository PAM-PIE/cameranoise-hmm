function [x, exitflag] = FindPointFromDistTriple(data, c1, c2, c3)

MyFunc = @(x) [
(x(1)-c1(1))^2+(x(2)-c1(2))^2+(x(3)-c1(3))^2 - data(1)^2; 
(x(1)-c2(1))^2+(x(2)-c2(2))^2+(x(3)-c2(3))^2 - data(2)^2; 
(x(1)-c3(1))^2+(x(2)-c3(2))^2+(x(3)-c3(3))^2 - data(3)^2];

options=optimset('Display','off');   % Option to display output
[x,fval,exitflag] = fsolve(MyFunc, [0 0 -1], options);
if exitflag~=1
    switch exitflag
        case 1
            disp('Function converged to a solution x');
        case 2
            disp('Change in x was smaller than the specified tolerance');
        case 3
            disp('Change in the residual was smaller than the specified tolerance');
        case 4
            disp('Magnitude of search direction was smaller than the specified tolerance');
        case 0 
            disp('Number of iterations exceeded options.MaxIter or number of function evaluations exceeded options.FunEvals.');
        case -1 
            disp('Algorithm was terminated by the output function.');
        case -2 
            disp('Algorithm appears to be converging to a point that is not a root.');
        case -3 
            disp('Trust radius became too small.');
        case -4 
            disp('Line search cannot sufficiently decrease the residual along the current search direction');
        otherwise 
            disp('?');
    end
end
