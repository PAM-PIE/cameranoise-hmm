%Auswertung 13.11.2008
%filename = 'C:\Programme\MATLAB7\work\Kdp-B\02-07\cysless-kcl-ATP-VO4-all.bap';
%filename = 'C:\Programme\MATLAB7\work\Kdp-B\02-07\cysless-kcl-ATP-MCS-all.bap';
%filename = 'C:\Programme\MATLAB7\work\Kdp-B\02-07\cysless-kcl-ATP.bap';
%filename = 'C:\Programme\MATLAB7\work\Kdp-B\02-07\cysless-kcl.bap';
%filename = 'C:\Programme\MATLAB7\work\Kdp-B\03-07\cysless-vo4.bap';
%filename = 'C:\Programme\MATLAB7\work\Kdp-B\03-07\cysless-mcs.bap';
%filename = 'C:\Programme\MATLAB7\work\Kdp-B\03-07\cysless-atp-aundb.bap';
%filename = 'C:\Programme\MATLAB7\work\Kdp-B\03-07\cysless-kcl-abc.bap';
%filename = 'C:\Programme\MATLAB7\work\Kdp-B\01-07\wt-vo4-12-korr.bap';
%filename = 'C:\Programme\MATLAB7\work\Kdp-B\01-07\wt-mcs12-korr.bap';
%filename = 'C:\Programme\MATLAB7\work\Kdp-B\01-07\wt-1zu500-korr.bap';
%filename = 'C:\Programme\MATLAB7\work\Kdp-B\28-06\6nm-tris.bap';
%filename = 'C:\Programme\MATLAB7\work\Kdp-B\28-06\6nm-mcs-all.bap';
%filename = 'C:\Programme\MATLAB7\work\Kdp-B\28-06\6nm-vo4-a.bap';
%filename = 'C:\PROJEKTE\Gary-Glick\2006-07-25\fof1-atp-abcde.bap';
%filename = 'C:\PROJEKTE\Gary-Glick\2006-07-25\fof1-aurovertin-abcd.bap';
%filename = 'C:\PROJEKTE\Gary-Glick\2006-07-25\fof1-amppnp-abcd.bap';
%filename = 'C:\PROJEKTE\Gary-Glick\2006-07-25\grc-data\fof1-aurovertin-ab-level-grc.bap';

%filename = 'C:\PROJEKTE\Gary-Glick\2006-07-25\grc-data\fof1-atp-a-level-grc.bap';
%filename = 'D:\Nawid\Burst Analyzer\Temp\TestdataHMM\fof1-amppnp-abcd.bap';
filepath = 'I:\Glick\';
filename='Combined-atp-v7-nov2008.bap';
%filename='Combined-aurovertin-v7-nov2008.bap';

%Filter
Filter.LengthMin=0;
Filter.LengthMax=inf;
Filter.iPosMin=1;  %iPos ist die Position der Stufe innerhalb des Bursts
Filter.iPosMax=inf;
Filter.iPosRelMax=0; %Min iPosRelMax vom Burst-Ende entfernt (0=deaktiviert)
Filter.AnzahlMin=1; %Anzahl der Stufen pro Burst
Filter.AnzahlMax=inf;
Filter.IntensMin(1)=0; %Photonenzahl
Filter.IntensMax(1)=inf;
Filter.IntensMin(2)=0;
Filter.IntensMax(2)=inf;
Filter.IntensMin(3)=0;
Filter.IntensMax(3)=inf;
Filter.IntensMin(4)=0;
Filter.IntensMax(4)=inf;
Filter.IntensMin(1)=0;
Filter.SumIntensMin=0;
Filter.SumIntensMax=inf;
Filter.IntensAVGMin(1)=0;
Filter.IntensAVGMax(1)=inf;    %Photonenzahl/L�nge => Z�hlrate
Filter.IntensAVGMin(2)=0;
Filter.IntensAVGMax(2)=inf;
Filter.IntensAVGMin(3)=0;
Filter.IntensAVGMax(3)=inf;
Filter.IntensAVGMin(4)=0;
Filter.IntensAVGMax(4)=inf;
Filter.SumIntensAVGMin=0;
Filter.SumIntensAVGMax=inf;
Filter.IntensPeakMin(1)=0;
Filter.IntensPeakMax(1)=inf;
Filter.IntensPeakMin(2)=0;
Filter.IntensPeakMax(2)=inf;
Filter.IntensPeakMin(3)=0;
Filter.IntensPeakMax(3)=inf;
Filter.IntensPeakMin(4)=0;
Filter.IntensPeakMax(4)=inf;
Filter.SumIntensPeakMin=0;
Filter.SumIntensPeakMax=inf;
Filter.ProxFactMin=0;
Filter.ProxFactMax=1;
Filter.ProxFactErrMin=0;
Filter.ProxFactErrMax=inf;

gamma=1;
r0=5;
bCalcPF=false;

if ~exist('StufenArr','var')
  [BurstArr, StufenArr] = BAPLoadProjectFast([filepath,filename], gamma, r0, bCalcPF);
end
%AlleStufen = StufenFilter(AlleStufen, '1');

%Filter

StufenArr.Length=StufenArr.Ende-StufenArr.Start+1;
StufenArr.SumIntens=StufenArr.Intens1+StufenArr.Intens2+StufenArr.Intens3+StufenArr.Intens4;
StufenArr.IntensAVG1=StufenArr.Intens1./StufenArr.Length;
StufenArr.IntensAVG2=StufenArr.Intens2./StufenArr.Length;
StufenArr.IntensAVG3=StufenArr.Intens3./StufenArr.Length;
StufenArr.IntensAVG4=StufenArr.Intens4./StufenArr.Length;
StufenArr.SumIntensAVG=StufenArr.IntensAVG1+StufenArr.IntensAVG2+StufenArr.IntensAVG3+StufenArr.IntensAVG4;
StufenArr.SumPeaks=StufenArr.PeakI1+StufenArr.PeakI2+StufenArr.PeakI3+StufenArr.PeakI4;
StufenArr.Efret = CalcEfretFromPF(StufenArr.ProxMW, gamma);
StufenArr.Dist = CalcRFromEfret(StufenArr.Efret, r0);

idx=find(...
  (StufenArr.Length>=Filter.LengthMin)&...
  (StufenArr.Length<=Filter.LengthMax)&...
  (StufenArr.iStufe>=Filter.iPosMin)&...
  (StufenArr.iStufe<=Filter.iPosMax)&...
  (StufenArr.AnzStufen-StufenArr.iStufe>=Filter.iPosRelMax)&...
  (StufenArr.AnzStufen>=Filter.AnzahlMin)&...
  (StufenArr.AnzStufen<=Filter.AnzahlMax)&...
  (StufenArr.Intens1>=Filter.IntensMin(1))&...
  (StufenArr.Intens1<=Filter.IntensMax(1))&...
  (StufenArr.Intens2>=Filter.IntensMin(2))&...
  (StufenArr.Intens2<=Filter.IntensMax(2))&...
  (StufenArr.Intens3>=Filter.IntensMin(3))&...
  (StufenArr.Intens3<=Filter.IntensMax(3))&...
  (StufenArr.Intens4>=Filter.IntensMin(4))&...
  (StufenArr.Intens4<=Filter.IntensMax(4))&...
  (StufenArr.SumIntens>=Filter.SumIntensMin)&...
  (StufenArr.SumIntens<=Filter.SumIntensMax)&...
  (StufenArr.IntensAVG1>=Filter.IntensAVGMin(1))&...
  (StufenArr.IntensAVG1<=Filter.IntensAVGMax(1))&...
  (StufenArr.IntensAVG2>=Filter.IntensAVGMin(2))&...
  (StufenArr.IntensAVG2<=Filter.IntensAVGMax(2))&...
  (StufenArr.IntensAVG3>=Filter.IntensAVGMin(3))&...
  (StufenArr.IntensAVG3<=Filter.IntensAVGMax(3))&...
  (StufenArr.IntensAVG4>=Filter.IntensAVGMin(4))&...
  (StufenArr.IntensAVG4<=Filter.IntensAVGMax(4))&...
  (StufenArr.SumIntensAVG>=Filter.SumIntensAVGMin)&...
  (StufenArr.SumIntensAVG<=Filter.SumIntensAVGMax)&...
  (StufenArr.PeakI1>=Filter.IntensPeakMin(1))&...
  (StufenArr.PeakI1<=Filter.IntensPeakMax(1))&...
  (StufenArr.PeakI2>=Filter.IntensPeakMin(2))&...
  (StufenArr.PeakI2<=Filter.IntensPeakMax(2))&...
  (StufenArr.PeakI3>=Filter.IntensPeakMin(3))&...
  (StufenArr.PeakI3<=Filter.IntensPeakMax(3))&...
  (StufenArr.PeakI4>=Filter.IntensPeakMin(4))&...
  (StufenArr.PeakI4<=Filter.IntensPeakMax(4))&...
  (StufenArr.SumPeaks>=Filter.SumIntensPeakMin)&...
  (StufenArr.SumPeaks<=Filter.SumIntensPeakMax)&...
  (StufenArr.ProxMW>=Filter.ProxFactMin)&...
  (StufenArr.ProxMW<=Filter.ProxFactMax)&...
  (StufenArr.ErrProxMW>=Filter.ProxFactErrMin)&...
  (StufenArr.ErrProxMW<=Filter.ProxFactErrMax));

OriginData=[];
xhist=0:0.02:1;
%yhist=hist(StufenArr.ProxMW(idx), xhist);
yhist=hist(StufenArr.Efret(idx), xhist);
%yhist=hist(StufenArr.Dist(idx), xhist);
OriginData(:,1)=xhist;
OriginData(:,2)=yhist;

bar(xhist,yhist,'BarWidth',1);
title(sprintf('%d Stufen (Gesamt: %d)',length(idx),length(StufenArr.Length)));
xlim([0 1]);
