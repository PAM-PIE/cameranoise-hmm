function [r] = CalcRFromEfret(Efret, r0)
%This function calculates the FRET-Distance from the FRET-Efficiency
%r0 is the F�rtser-Radius

r=[];
for i=1:length(Efret)
    nenner = (Efret(i));
    if nenner ~= 0
        if nenner==-1
            r(i)=nenner;
        else
            r(i) = r0*((1/Efret(i)-1)^(1/6));
        end
    else
        r(i) = +inf;
    end
end
