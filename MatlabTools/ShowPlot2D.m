function [imgdata]=ShowPlot2D(x,y,imgx,imgy,imgsize,psfsdv);

ix=fix((x-imgx(1))./(imgx(2)-imgx(1)).*imgsize(1))+1;
iy=fix((y-imgy(1))./(imgy(2)-imgy(1)).*imgsize(2))+1;
ix=max(1,min(imgsize(1),ix));
iy=max(1,min(imgsize(2),iy));

imgdata=accumarray([ix(:),iy(:)],1,imgsize)';
psf=customgauss([psfsdv(1)*10 psfsdv(2)*10],psfsdv(1),psfsdv(2),0,0,1,[0 0]);

imgconv=conv2(imgdata,psf);
xarr=imgx(1):diff(imgx)/imgsize(1):imgx(2);
yarr=imgy(1):diff(imgy)/imgsize(2):imgy(2);
imagesc(xarr,yarr,imgconv);
set(gca,'YDir','normal');
