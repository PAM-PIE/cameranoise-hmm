function [bOk, data] = CheckSequence(data, Seq);
%This function checkes the direction of rotation

[PF_low, iMin] = min(data);
data = circshift(data, 4-iMin);
[PF_high, iMax] = max(data);
jMax = strfind(Seq, 'H');

bOk = iMax == jMax;
