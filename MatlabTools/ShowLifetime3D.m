clear all;
load('\\pi3-pc102\Thomas\2007-04-23\Lifetime\FitResBiAuto.mat');
FitRes=FitResATP;
filepath='D:\Nawid\temp\';

Anz=10;
cla;
hold on
bSpeedMode=false;

data=[];
fit=[];

for iFile=1:Anz
%     filenameData{iFile} = sprintf('Gesamt Stufe%02d_van200_c_10BinStep.my1',iFile);
%     filenameFit{iFile} = sprintf('Gesamt Stufe%02d_van200_c_10BinStep_fit.my1',iFile);
%     dataraw=load([filepath,filenameData{iFile}]);
%     [time,dummy,fitraw]=textread([filepath,filenameFit{iFile}],'%f\t%f\t%f','headerlines',1);
    time=FitRes(iFile).time;
    dataraw=FitRes(iFile).Sample;
    fitraw=FitRes(iFile).FitArr;

    if bSpeedMode
        itime=(1:50:length(time));    
        time=time(itime);
        fitraw=fitraw(itime);
        dataraw=dataraw(itime);
    end
    
    data(iFile,:)=dataraw;
    fit(iFile,:)=fitraw;
    plot3(time,repmat(iFile,length(time)),data(iFile,:),'linewidth',2,'color',[1 1 1]*.7);
    plot3(time,repmat(iFile,length(time)),fit(iFile,:),'linewidth',.1,'color',[0 0 0]*0);
    fprintf('.');
end
fprintf('plot: ');
view([20 15]);
set(gca,'ydir','reverse');
set(gca,'zscale','log');
set(gca,'XMinorTick','on');
set(gca,'YMinorTick','on');
set(gca,'FontSize',20);
xlim([0 20]);
xbounds=xlim;
ybounds=ylim;
zbounds=zlim;
plot3([xbounds(1) xbounds(2)],[ybounds(1) ybounds(1)],[zbounds(1) zbounds(1)],'k');
plot3([xbounds(1) xbounds(1)],[ybounds(1) ybounds(2)],[zbounds(1) zbounds(1)],'k');
plot3([xbounds(1) xbounds(1)],[ybounds(1) ybounds(1)],[zbounds(1) zbounds(2)],'k');
plot3([xbounds(1) xbounds(2)],[ybounds(1) ybounds(1)],[zbounds(2) zbounds(2)],'k');
plot3([xbounds(1) xbounds(1)],[ybounds(1) ybounds(2)],[zbounds(2) zbounds(2)],'k');
plot3([xbounds(2) xbounds(2)],[ybounds(1) ybounds(1)],[zbounds(1) zbounds(2)],'k');
%xlabel('time/ns');
%ylabel('proximity factor');
%zlabel('number of photons');
hold off
fprintf('ok\n');
