function [dataperm] = GenStrPerm(data, Seq);

data=[data,data];
dataperm=[];
for i=1:length(Seq);
    j=strfind(data,Seq(i));
    if ~isempty(j)
        dataperm=data(j:j+length(data)/2-1);
        break;
    end        
end
