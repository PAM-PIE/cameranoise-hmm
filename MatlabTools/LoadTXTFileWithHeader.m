function [values] = LoadTXTFileWithHeader(filename);
%for Header info type "fieldnames(values)"

 if exist(filename,'file')==0
     error(['file not found!!! "',filename,'"']); 
     return;
 end

fid = fopen(filename, 'rt');
tline = fgetl(fid);
header = strread(tline,'%s','delimiter','\t');
%filter = ''; for i=1:size(header,1), filter = [filter,'%s']; end
filter = repmat('%s',1,size(header,1));
values = textscan(fid, filter, 'delimiter', '\t');
header = regexprep(header,'\W',''); %Alle Sonderzeichen l�schen!
values = cell2struct(values, header', 2);
fclose(fid);

fnames = fieldnames(values);
for i=1:size(fnames,1)
    for j=1:length(values.(char(fnames(i))))
        if isempty(values.(char(fnames(i))){j})
            values.(char(fnames(i))){j}='NaN';
        end;
    end;
    dummy = str2num(char(values.(char(fnames(i)))(:)));
    if size(dummy)==size(values.(char(fnames(i)))(:))
        values.(char(fnames(i))) = dummy;
    end;
end

%varargout(1)={values};
%if nargout>=2, varargout(2)={header}; end
