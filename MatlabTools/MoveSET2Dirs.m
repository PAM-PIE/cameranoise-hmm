function MoveSET2Dirs(sVerz)

SETFiles = dir([sVerz, '\*.set']);
for i=1:length(SETFiles)
    [SETVerz, SETName, SETExt, SETVers] = fileparts(SETFiles(i).name);
    SETName_bhext = SETName(length(SETName)-2: length(SETName));
    if ~isempty(str2num(SETName_bhext))
        SETName = SETName(1:length(SETName)-3);
    end;
    disp(['Directory ', int2str(i), ' from ', int2str(length(SETFiles)), ': ', SETName]);
    if isempty(dir([sVerz, '\', SETName]))
        mkdir(sVerz, SETName);
    end;
    movefile([sVerz, '\', SETName, '*.*'], [sVerz, '\', SETName]);
end
