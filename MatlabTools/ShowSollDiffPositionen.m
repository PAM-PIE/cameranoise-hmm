function ShowSollDiffPositionen(rc, d, h, dphi, phiofs, anzP)

hold on;
dphi1 = 0:pi/100:2*pi;
dphi2 = dphi/180*pi:pi/100:2*pi+dphi/180*pi;
d1 = CalcDistP1P2(rc,d,h,dphi1);
d2 = CalcDistP1P2(rc,d,h,dphi2);
d = d2 - d1;
plot(d1,d,'white',...
    'linewidth',2,...
    'DisplayName',num2str(dphi));

if anzP>0
    dphi1 = 0:2*pi/anzP:2*pi;
    dphi2 = dphi/180*pi:2*pi/anzP:2*pi+dphi/180*pi;
    d1 = CalcDistP1P2(rc,d,h,dphi1+phiofs/180*pi);
    d2 = CalcDistP1P2(rc,d,h,dphi2+phiofs/180*pi);
    d = d2 - d1;
    plot(d1,d,'ow',...
        'markersize',20,...
        'markerfacecolor','r',...
        'markeredgecolor','w',...
        'DisplayName',[num2str(phiofs),'�']);

    hold off;
end

end
