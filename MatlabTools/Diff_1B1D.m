
    %ftype = fittype('y0+(2*a1/pi)*c1/((4*(x-b1)^2)+c1^2)');
    %ftype = fittype('(1/Nf)*(1/(1+x/tauDiff))*((1/(1+omegaZ*omegaZ*x/tauDiff))^0.5)*(1-AmplBunching*(1-exp(-x/tauBunching)))');
    ftype = fittype('poly1');

    %opts = fitoptions('y0+(2*a1/pi)*c1/((4*(x-b1)^2)+c1^2)');
    %opts.Upper = [Inf peak1(index)+0.5*peakposvar peakbreitemax1 Inf];  %moegliche obere Grenzen fuer a1-4,b1-4,c1-4,y0 
    %opts.Lower = [0 peak1(index)-0.5*peakposvar peakbreitemin1 0];       %moegliche untere Grenzen fuer a1-4,b1-4,c1-4,y0 
    %opts.MaxFunEvals = 1000;

    %difb=abs(xAchse-peak1(index)+0.5*fitbreite1); %Fitbereich
    %[dump2,ende]=min(difb);
    %difc=abs(xAchse-peak1(index)-0.5*fitbreite1); %Fitbereich 
    %[dump3,start]=min(difc);

    [fresult,gof,output]=fit(data(:,1), data(:,2), 'poly1');
    %fitparams(1,index)=fitspec.Nf;
    %fitparams(2,index)=fitspec.tauDiff;
    %fitparams(3,index)=fitspec.omegaZ;
    %fitparams(4,index)=fitspec.AmplBunching; 
    %fitparams(5,index)=fitspec.TauBunching; 

    waitforbuttonpress;
