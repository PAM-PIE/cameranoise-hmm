function [SubArr] = StufenFilter(StufenArr, sFilter, bAdjacent);
%FilterDef:
%'1': only one Step per Burst, Creates an 1-Column-Array
%'2': only two Steps per Burst, Creates an 2-Column-Array
%'3': only three Steps per Burst, Creates an 3-Column-Array
%'2+': a Bursts with i.e. 3 Steps results in two Doublets, Creates an 2-Column-Array
% This 3-Step-Burst results in two lines, the second step occures 2 times!

fprintf(1,['Auf %d Stufen Filter "',sFilter,'" anwenden: %6d'],[length(StufenArr),0]); 

if sFilter(end)=='+'
    sFilter=sFilter(1:end-1);
    bPlus=true;
else
    bPlus=false;
end
AnzStufen = str2num(sFilter);

anzdata=hist([StufenArr.Anzahl],1:max([StufenArr.Anzahl]));
anzdata=anzdata./(1:length(anzdata));
if bPlus
    expectnum=AnzStufen*sum(((AnzStufen:length(anzdata))-AnzStufen+1).*(anzdata(AnzStufen:end)));
else
    expectnum=AnzStufen*anzdata(AnzStufen);
end
item=StufenArr(1);
SubArr=repmat(item,expectnum,1);
iSubArr=0;
for iArr = 1:length(StufenArr)
    if bPlus
        bTakeStufe=StufenArr(iArr).Anzahl>=AnzStufen;
    else
        bTakeStufe=StufenArr(iArr).Anzahl==AnzStufen;
    end
    if StufenArr(iArr).iPos==1, StufenRow=[]; end
    if bTakeStufe
        StufenRow = [StufenRow; StufenArr(iArr)];
        if StufenArr(iArr).iPos >= AnzStufen
            bAdjFlag=true;
            if bAdjacent
                for j=2:AnzStufen
                    if StufenRow(StufenArr(iArr).iPos-AnzStufen+j).Start > ...
                        StufenRow(StufenArr(iArr).iPos-AnzStufen+j-1).End+1
                        bAdjFlag=false;
                    end
                end
            end
            if bAdjFlag
                for j=1:AnzStufen
                    %Version alt: extrem langsam, da Array erweitert werden
                    %muss. Speicheranforderung nach jedem Element
                    % SubArr = [SubArr; StufenRow(StufenArr(iArr).iPos-AnzStufen+j)];
                    %da das Array bereits vorinitialisiert ist, wird hier
                    %kein neuer Speicher angefordert => Um Größenordnungen
                    %schnellere Verarbeitung!
                    iSubArr = iSubArr + 1;
                    SubArr(iSubArr) = StufenRow(StufenArr(iArr).iPos-AnzStufen+j);
                end
            end
        end
    end
    if mod(iArr,100)==0, fprintf(1,'\b\b\b\b\b\b%6d',[iArr]); end
end
SubArr=SubArr(1:iSubArr);
SubArr=reshape(SubArr,AnzStufen,[])';
fprintf(1,'\b\b\b\b\b\bok.\n'); 
