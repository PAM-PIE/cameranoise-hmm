function [x0 y0 xsigma ysigma]=fit2d(img);

[ny,nx]=size(img);
xdata=sum(img,1);
xdata=xdata(:);
ydata=sum(img,2);
ydata=ydata(:);

xi=(1:nx)';
yi=(1:ny)';

ft_ = fittype('A.*exp(-(x-mu).^2./(2.*sigma.^2))+offset',...
      'dependent',{'y'},'independent',{'x'},...
      'coefficients',{'A','mu','sigma','offset'});
fo_ = fitoptions(...
    'method','NonlinearLeastSquares',...
    'Algorithm','Trust-Region',...
    'DiffMaxChange',0.001,'MaxFunEvals',10000,'MaxIter',1000,'TolFun',1e-008,'TolX',1e-008);
% set(fo_,'Lower',[0,1,1,0]);
% set(fo_,'Upper',[0,5,5,5]);

% Fit this model using new data
st_ = [max(xdata),nx/2,nx/4,min(xdata)];
set(fo_,'Startpoint',st_);
resx = fit(xi,xdata,ft_ ,fo_);

st_ = [max(ydata),ny/2,ny/4,min(ydata)];
set(fo_,'Startpoint',st_);
resy = fit(yi,ydata,ft_ ,fo_);

x0 = resx.mu;
xsigma = resx.sigma;
y0 = resy.mu;
ysigma = resy.sigma;

