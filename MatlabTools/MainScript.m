function [] = FitVerz(sVerzStamm)

sResFiles = 'dir_cf2.txt';
listVerz = dir(sVerzStamm);

%Generate Res-FileList
ListResFiles = {};
for i=3:length(listVerz)
    if (listVerz(i).isdir==1)
        NewFile = [sVerzStamm, '\', listVerz(i).name, '\', sResFiles];
        if ~isempty(dir(NewFile))
            ListResFiles = {ListResFiles{:}, NewFile};
            disp(NewFile);
            DiffFit(NewFile);
        end;
    end;
end;

%DiffFit('C:\Nawid\HEFE\2006-03-22\Xa-v0-in-pbs-5myw-position-b01-41-03-\dir_cf1.txt');
%{
DiffFit('C:\Nawid\HEFE\2006-03-22\Xa-v0-in-pbs-5myw-position-c01-73-37-\dir_cf1.txt');
DiffFit('C:\Nawid\HEFE\2006-03-22\Xa-v0-in-pbs-5myw-position-c02-81-23-\dir_cf1.txt');
DiffFit('C:\Nawid\HEFE\2006-03-22\Xa-v0-in-pbs-5myw-position-c03-81-24-\dir_cf1.txt');
DiffFit('C:\Nawid\HEFE\2006-03-22\Xa-v0-in-pbs-5myw-position-d01-16-18-\dir_cf1.txt');
DiffFit('C:\Nawid\HEFE\2006-03-22\Xa-v0-in-pbs-5myw-position-d02-15-19-\dir_cf1.txt');
DiffFit('C:\Nawid\HEFE\2006-03-22\Xa-v0-in-pbs-5myw-position-d03-16-22-\dir_cf1.txt');
DiffFit('C:\Nawid\HEFE\2006-03-22\Xa-v0-in-pbs-5myw-position-d04-14-22-\dir_cf1.txt');
DiffFit('C:\Nawid\HEFE\2006-03-22\Xa-v0-in-pbs-5myw-position-e01-09-68-\dir_cf1.txt');
DiffFit('C:\Nawid\HEFE\2006-03-22\Xa-v0-in-pbs-5myw-position-f01-44-78-\dir_cf1.txt');
DiffFit('C:\Nawid\HEFE\2006-03-22\Xa-v0-in-pbs-5myw-position-h01-76-23-\dir_cf1.txt');
DiffFit('C:\Nawid\HEFE\2006-03-22\Xa-v0-in-pbs-5myw-position-j01-30-38-\dir_cf1.txt');
DiffFit('C:\Nawid\HEFE\2006-03-22\Xa-v0-in-pbs-5myw-position-j02-27-41-\dir_cf1.txt');
DiffFit('C:\Nawid\HEFE\2006-03-22\Xa-v0-in-pbs-5myw-position-j03-36-44-\dir_cf1.txt');
DiffFit('C:\Nawid\HEFE\2006-03-22\Xa-v0-in-pbs-5myw-position-k01-68-53-\dir_cf1.txt');
DiffFit('C:\Nawid\HEFE\2006-03-22\Xa-v0-in-pbs-5myw-position-k02-73-60-\dir_cf1.txt');
DiffFit('C:\Nawid\HEFE\2006-03-22\Xa-v0-in-pbs-5myw-position-k03-72-61-\dir_cf1.txt');
DiffFit('C:\Nawid\HEFE\2006-03-22\Xa-v0-in-pbs-5myw-position-m01-57-92-\dir_cf1.txt');
DiffFit('C:\Nawid\HEFE\2006-03-22\Xa-v0-in-pbs-5myw-position-n01-95-54-\dir_cf1.txt');
%}
%{
DiffFit('C:\Nawid\HEFE\2006-03-14\Xc--mit-fm4-488-1myw-10sec50myw-position-50-6-b\dir_cf1.txt');
DiffFit('C:\Nawid\HEFE\2006-03-14\Xc--mit-fm4-488-1myw-position-50-6-a\dir_cf1.txt');
DiffFit('C:\Nawid\HEFE\2006-03-14\Xc-mit-fm4-488-5myw-position-6-43-c\dir_cf1.txt');
DiffFit('C:\Nawid\HEFE\2006-03-14\Xc-mit-fm4-488-5myw-position-6-43-d\dir_cf1.txt');
DiffFit('C:\Nawid\HEFE\2006-03-14\Xc-mit-fm4-mit glucose-5myw-position-16-1-b\dir_cf1.txt');
DiffFit('C:\Nawid\HEFE\2006-03-14\Xc-mit-fm4-mit glucose-5myw-position-16-1-c\dir_cf1.txt');
DiffFit('C:\Nawid\HEFE\2006-03-14\Xc-mit-fm4-mit glucose-5myw-position-21-10-e\dir_cf1.txt');
DiffFit('C:\Nawid\HEFE\2006-03-14\Xc-mit-fm4-mit glucose-5myw-position-22-8-d\dir_cf1.txt');
DiffFit('C:\Nawid\HEFE\2006-03-14\Xc-mit-fm4-mit glucose-5myw-position-58-12-h\dir_cf1.txt');
DiffFit('C:\Nawid\HEFE\2006-03-14\Xc-mit-fm4-mit glucose-5myw-position-8-16-a\dir_cf1.txt');
DiffFit('C:\Nawid\HEFE\2006-03-14\Xc-mit-fm4-mit glucose-5myw-position42-76-l\dir_cf1.txt');
DiffFit('C:\Nawid\HEFE\2006-03-14\Xc-mit-fm4-mit glucose-5myw-position44-4-g\dir_cf1.txt');
DiffFit('C:\Nawid\HEFE\2006-03-14\Xc-mit-fm4-mit glucose-5myw-position69-18-i\dir_cf1.txt');
DiffFit('C:\Nawid\HEFE\2006-03-14\Xc-mit-fm4-mit glucose-5myw-position69-18-j\dir_cf1.txt');
DiffFit('C:\Nawid\HEFE\2006-03-14\Xc-mit-fm4-mit glucose-5myw-position69-9-k\dir_cf1.txt');
%}
%{
DiffFit('C:\Nawid\HEFE\2006-03-16\Xb-ohne-fm4-in-pbs-b01-position-55-10-x\dir_cf1.txt');
DiffFit('C:\Nawid\HEFE\2006-03-16\Xb-ohne-fm4-in-pbs-b01-position-56-6-x\dir_cf1.txt');
DiffFit('C:\Nawid\HEFE\2006-03-16\Xb-ohne-fm4-in-pbs-c01-position-33-08-x\dir_cf1.txt');
DiffFit('C:\Nawid\HEFE\2006-03-16\Xb-ohne-fm4-in-pbs-c02-position-29-11-x\dir_cf1.txt');
DiffFit('C:\Nawid\HEFE\2006-03-16\Xb-ohne-fm4-in-pbs-mit-glucose-f01-position-17-61-x\dir_cf1.txt');
DiffFit('C:\Nawid\HEFE\2006-03-16\Xb-ohne-fm4-in-pbs-mit-glucose-f02-position-21-58-x\dir_cf1.txt');
DiffFit('C:\Nawid\HEFE\2006-03-16\Xb-ohne-fm4-in-pbs-mit-glucose-f03-position-27-63-x\dir_cf1.txt');
DiffFit('C:\Nawid\HEFE\2006-03-16\Xb-ohne-fm4-in-pbs-mit-glucose-f04-position-23-51-x\dir_cf1.txt');
DiffFit('C:\Nawid\HEFE\2006-03-16\Xb-ohne-fm4-in-pbs-mit-glucose-o01-position-19-64-x\dir_cf1.txt');
DiffFit('C:\Nawid\HEFE\2006-03-16\Xb-ohne-fm4-in-pbs-mit-glucose-o02-position-18-64-x\dir_cf1.txt');
DiffFit('C:\Nawid\HEFE\2006-03-16\Xb-ohne-fm4-in-pbs-mit-glucose-o03-position-18-66-x\dir_cf1.txt');
DiffFit('C:\Nawid\HEFE\2006-03-16\Xb-ohne-fm4-in-pbs-mit-glucose-o04-position-05-58-x\dir_cf1.txt');
DiffFit('C:\Nawid\HEFE\2006-03-16\Xb-ohne-fm4-in-pbs-mit-glucose-p01-position-12-80-x\dir_cf1.txt');
DiffFit('C:\Nawid\HEFE\2006-03-16\Xb-ohne-fm4-in-pbs-mit-glucose-p02-position-10-80-x\dir_cf1.txt');
DiffFit('C:\Nawid\HEFE\2006-03-16\Xb-ohne-fm4-in-pbs-mit-glucose-q01-position-77-47-x\dir_cf1.txt');
DiffFit('C:\Nawid\HEFE\2006-03-16\Xb-ohne-fm4-in-pbs-mit-glucose-q02-position-78-44-x\dir_cf1.txt');
DiffFit('C:\Nawid\HEFE\2006-03-16\Xb-ohne-fm4-in-pbs-mit-glucose-r01-position-27-28-x\dir_cf1.txt');
DiffFit('C:\Nawid\HEFE\2006-03-16\Xb-ohne-fm4-in-pbs-mit-glucose-r02-position-28-28-x\dir_cf1.txt');
DiffFit('C:\Nawid\HEFE\2006-03-16\Xb-ohne-fm4-in-pbs-mit-glucose-r03-position-28-26-x\dir_cf1.txt');
DiffFit('C:\Nawid\HEFE\2006-03-16\Xb-ohne-fm4-in-pbs-ohne-glucose-j01-position-43-60-x\dir_cf1.txt');
DiffFit('C:\Nawid\HEFE\2006-03-16\Xb-ohne-fm4-in-pbs-ohne-glucose-j02-position-37-63-x\dir_cf1.txt');
DiffFit('C:\Nawid\HEFE\2006-03-16\Xb-ohne-fm4-in-pbs-ohne-glucose-j03-position-49-67-x\dir_cf1.txt');
DiffFit('C:\Nawid\HEFE\2006-03-16\Xb-ohne-fm4-in-pbs-ohne-glucose-k01-position-79-11-x\dir_cf1.txt');
DiffFit('C:\Nawid\HEFE\2006-03-16\Xb-ohne-fm4-in-pbs-ohne-glucose-k02-position-80-19-x\dir_cf1.txt');
DiffFit('C:\Nawid\HEFE\2006-03-16\Xb-ohne-fm4-in-pbs-ohne-glucose-k03-position-80-19-x\dir_cf1.txt');
DiffFit('C:\Nawid\HEFE\2006-03-16\Xb-ohne-fm4-in-pbs-ohne-glucose-k04-position-81-18-x\dir_cf1.txt');
DiffFit('C:\Nawid\HEFE\2006-03-16\Xb-ohne-fm4-in-pbs-ohne-glucose-l01-position-37-87-x\dir_cf1.txt');
DiffFit('C:\Nawid\HEFE\2006-03-16\Xb-ohne-fm4-in-pbs-ohne-glucose-l02-position-36-85-x\dir_cf1.txt');
DiffFit('C:\Nawid\HEFE\2006-03-16\Xb-ohne-fm4-in-pbs-ohne-glucose-l03-position-35-75-x\dir_cf1.txt');
%}
%{
DiffFit('C:\Nawid\HEFE\2006-03-20\Xa-v1-in-pbs-5myw-position-b01-24-04-\dir_cf1.txt');
DiffFit('C:\Nawid\HEFE\2006-03-20\Xa-v1-in-pbs-5myw-position-b02-27-10-\dir_cf1.txt');
DiffFit('C:\Nawid\HEFE\2006-03-20\Xa-v1-in-pbs-5myw-position-c01-26-21-\dir_cf1.txt');
DiffFit('C:\Nawid\HEFE\2006-03-20\Xa-v1-in-pbs-5myw-position-c02-31-34-\dir_cf1.txt');
DiffFit('C:\Nawid\HEFE\2006-03-20\Xa-v1-in-pbs-5myw-position-c03-29-36-\dir_cf1.txt');
DiffFit('C:\Nawid\HEFE\2006-03-20\Xa-v1-in-pbs-5myw-position-c05-27-37-\dir_cf1.txt');
DiffFit('C:\Nawid\HEFE\2006-03-20\Xa-v1-in-pbs-5myw-position-f01-22-55-\dir_cf1.txt');
DiffFit('C:\Nawid\HEFE\2006-03-20\Xa-v1-in-pbs-5myw-position-f02-26-64-\dir_cf1.txt');
DiffFit('C:\Nawid\HEFE\2006-03-20\Xa-v1-in-pbs-5myw-position-f03-35-59-\dir_cf1.txt');
DiffFit('C:\Nawid\HEFE\2006-03-20\Xa-v1-in-pbs-5myw-position-g01-45-67-\dir_cf1.txt');
DiffFit('C:\Nawid\HEFE\2006-03-20\Xa-v1-in-pbs-5myw-position-g02-53-51-\dir_cf1.txt');
DiffFit('C:\Nawid\HEFE\2006-03-20\Xa-v1-in-pbs-5myw-position-h01-65-65-\dir_cf1.txt');
DiffFit('C:\Nawid\HEFE\2006-03-20\Xa-v1-in-pbs-5myw-position-i01-70-71-\dir_cf1.txt');
DiffFit('C:\Nawid\HEFE\2006-03-20\Xa-v1-in-pbs-5myw-position-i02-78-78-\dir_cf1.txt');
%}
