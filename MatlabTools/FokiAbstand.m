
%Laser 1
filepath='H:\Stefan\ATPase\TripleFret\2008-12-03\';
filename1='488-645-beads-2blauerLaser-a.scan';

%Laser2
filepath='H:\Stefan\ATPase\TripleFret\2008-12-03\';
filename2='488-645-beads-2roterLaser-a0000.scan';

close all;
I={};
x0=[];
y0=[];
xsigma=[];
ysigma=[];
Scangroesse=10000; %Scangr��e in nanomatern
Pixel=200;         %Pixel je Zeile

[I{1} I{2}]=LoadScanFile([filepath,filename1]); %Bilder Laser1
[I{3} I{4}]=LoadScanFile([filepath,filename2]); %Bilder Laser2



for i=1:4
  [x0(i) y0(i) xsigma(i) ysigma(i)]=fit2d(I{i});

  subplot(3,3,i);
  imagesc(I{i});
  
  phi=(0:1/50:1)*2*pi;
  hold on

  x=1.5*xsigma(i)*cos(phi)+x0(i);
  y=1.5*ysigma(i)*sin(phi)+y0(i);

  plot(x,y,'k','LineWidth',2);
  plot(x0(i),y0(i),'k+','LineWidth',2);
end

%Fokiabstand
FA=x0(1)-x0(4);
FA=abs(FA);                     %Abstand in Pixeln in x
FAabs=FA*Scangroesse/Pixel      %Abszand in nm in x

FAy=y0(1)-y0(4);
FAy=abs(FAy);                   %Abstand in Pixeln in y
FAyabs=FAy*Scangroesse/Pixel    %Abstand in nm in y

subplot(2,4,2);
xlabel(sprintf('Abstand in x: %.0f nm\nAbstand in y: %.0f nm',FAabs,FAyabs));

