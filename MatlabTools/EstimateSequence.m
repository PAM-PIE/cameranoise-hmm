function [dataseq] = EstimateSequence(data, RangeMin, RangeMax, Seq);
%This function estimates a sequence of chars form a sequence of proxfacts

dataseq=[];
Seq = [Seq, '-'];
for i=1:size(data,1)
    jFound=length(Seq);
    for j=1:length(Seq)-1
        if (data(i)>=RangeMin(j))&(data(i)<=RangeMax(j))
            jFound=j;
        end
    end
    dataseq = [dataseq, Seq(jFound)];
end
