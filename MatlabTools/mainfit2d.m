
load('img','data');

[ny,nx]=size(data);
xdata=sum(data,1);
xdata=xdata(:);
ydata=sum(data,2);
ydata=ydata(:);

xi=(1:nx)';
yi=(1:ny)';

ft_ = fittype('A.*exp(-(x-mu).^2./(2.*sigma.^2))+offset',...
      'dependent',{'y'},'independent',{'x'},...
      'coefficients',{'A','mu','sigma','offset'});
fo_ = fitoptions(...
    'method','NonlinearLeastSquares',...
    'Algorithm','Trust-Region',...
    'DiffMaxChange',0.001,'MaxFunEvals',10000,'MaxIter',1000,'TolFun',1e-008,'TolX',1e-008);
% set(fo_,'Lower',[0,1,1,0]);
% set(fo_,'Upper',[0,5,5,5]);

% Fit this model using new data
st_ = [max(xdata),nx/2,nx/4,min(xdata)];
set(fo_,'Startpoint',st_);
resx = fit(xi,xdata,ft_ ,fo_);

st_ = [max(ydata),ny/2,ny/4,min(ydata)];
set(fo_,'Startpoint',st_);
resy = fit(yi,ydata,ft_ ,fo_);

% resx=fit(xi',xdata'-min(xdata),'gauss1');
% resy=fit(yi',ydata'-min(ydata),'gauss1');

imagesc(data);
phi=(0:1/50:1)*2*pi;
hold on

x=1.5*resx.sigma*cos(phi)+resx.mu;
y=1.5*resy.sigma*sin(phi)+resy.mu;

plot(x,y,'k','LineWidth',2);
plot(resx.mu,resy.mu,'k+','LineWidth',2);

return

%******************************************
%function [x0 y0 xsigma ysigma]=fit2d(img);
%******************************************


%******************************************
% Main prog
%******************************************

load('img','data');

[I1b_x0 y0 xsigm ysigm]=fit2d(data);

%zeichnen
imagesc(data);
phi=(0:1/50:1)*2*pi;
hold on

x=1.5*resx.sigma*cos(phi)+resx.mu;
y=1.5*resy.sigma*sin(phi)+resy.mu;

plot(x,y,'k','LineWidth',2);
plot(resx.mu,resy.mu,'k+','LineWidth',2);
