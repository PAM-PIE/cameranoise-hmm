function ShowContourWithHist(datax, datay, binx, biny, binz, filled, slabelx, slabely, slabelz, stitle, axislabelx, axislabely);
set(gcf,'PaperPositionMode','auto');    % <-- Ohne diese Anweisung druckt der SaveAs-Befehl nur Mist!
set(gcf,'PaperUnits','points');
set(gcf,'PaperSize',[300 300]);
set(gcf,'Position',[300 300 600 600]);

%Konstanten
cRandLeft = 0.1;
cRandBottom = 0.1;
cSizeContour = 0.6;
cSizeHist = 0.25;
%cColBar = [0.73 0.83 0.96];
cColBar = [0.9 0.9 1];
cFontSizeAxesValues = 14;
cFontSizeAxesLabels = 18;
cFontSizeTitle = 18;

%oberes Histogramm
subplot('Position', [cRandLeft+cSizeHist cRandBottom+cSizeContour cSizeContour cSizeHist]);
hdatax=hist(datax(:),binx);
bar(binx,hdatax,'BarWidth',1,'FaceColor',cColBar);
xlim([binx(1) binx(end)]);
set(gca, 'XTick', []);
set(gca, 'DefaultPatchFaceColor', cColBar);
set(gca,'YGrid','on');
set(gca,'FontSize', cFontSizeAxesValues);
TickDummy = get(gca, 'YTick');
set(gca, 'YTick', TickDummy(2:end));
hylabel=ylabel(slabelz,'FontSize',cFontSizeAxesValues);
set(hylabel,'Units','pixels');
ylabPosOld=get(hylabel,'Position');
title(stitle,'FontSize',cFontSizeTitle);

%linkes Histogramm
subplot('Position', [cRandLeft cRandBottom cSizeHist cSizeContour]);
hdatay=hist(datay(:),biny);
barh(biny,hdatay,'BarWidth',1,'FaceColor',cColBar);
ylim([biny(1) biny(end)]);
set(gca,'xdir','reverse');
set(gca,'XGrid','on');
TickDummy = get(gca, 'XTick');
set(gca, 'XTick', TickDummy(2:end));
set(gca, 'YTick', axislabely);
set(gca,'FontSize', cFontSizeAxesValues);
xlabel(slabelz,'FontSize',cFontSizeAxesValues);
h=ylabel(slabely,'FontSize',cFontSizeAxesValues);
set(h,'Units','pixels');
ylabPosNew=get(h,'Position');
set(h,'Units','pixels');
set(gca,'Units','pixels');
yplotPosNew=get(gca,'Position');
set(hylabel,'Position',[ylabPosNew(1)-yplotPosNew(3) ylabPosOld(2)]);

%contourplot
subplot('Position', [cRandLeft+cSizeHist cRandBottom cSizeContour cSizeContour]);
hdata=hist3([datay(:),datax(:)],{biny binx});
if filled
    contourf(binx, biny, hdata, binz, 'LineStyle', 'none')';
else
    contour(binx, biny, hdata, binz);
end
xlim([binx(1) binx(end)]); ylim([biny(1) biny(end)]);
set(gca, 'YAxisLocation', 'Right');
set(gca, 'XTick', axislabelx);
set(gca, 'YTick', axislabely);
set(gca, 'FontSize', cFontSizeAxesValues);
xlabel(slabelx,'FontSize',cFontSizeAxesValues);

%colorbar
hcolbar=colorbar('Position',[0.1 0.75 0.08 0.2]);
dummy=get(hcolbar, 'YLim');
set(hcolbar, 'YLim', [0 dummy(2)]);
set(gca,'FontSize', cFontSizeAxesValues);

end
