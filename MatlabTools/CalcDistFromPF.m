function [d] = CalcDistFromPF(aPF, r0, gamma)
%This function calculates the FRET-Distance from ProxFact

if aPF~=0
    d = r0 * (gamma*(1/aPF-1))^(1/6);
else
    d = +Inf;
end
