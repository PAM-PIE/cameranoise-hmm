function [StufenArr] = StufenHMM2StufenArr(StufenHMM,BurstInt,r0,gamma,obsvect);
%[StufenArr] = StufenHMM2StufenArr(StufenHMM,BurstInt,r0,gamma);
%Diese Function konvertiert die HMM-Stufen in das StufenArr, das mit
%den bereits existierenden Matlab-Auswerte-Codes kompatibel ist.

num=length([StufenHMM{:}]);
fprintf(1,'%d Stufen-Eigenschaften erstellen: %6d',[num,0]); 
%Init
Stufe.iPos = 0;
Stufe.Start = 0;
Stufe.End = 0;
Stufe.Anzahl = 0;
Stufe.Length = 0;
Stufe.SumIntens = 0;
Stufe.SumPeaks = 0;
Stufe.ProxFact = 0;
Stufe.ProxFactErr = 0;
Stufe.Efret = 0;
Stufe.r = 0;
Stufe.Intens = zeros(4,1);
Stufe.Peak = zeros(4,1);
Stufe.PFclass = 0;
Stufe.iBurst = 0;
StufenArr(1) = Stufe;
StufenArr = repmat(StufenArr,num,1);
%calc
iStufe=0;
for i=1:numel(StufenHMM)
    iPos = 0;
    iMark = iStufe;
    for j=1:numel(StufenHMM{i})
        bExtStufe=false;
        if (exist('obsvect'))&(j>1)
            if obsvect(StufenHMM{i}(j).PFclass)==obsvect(StufenHMM{i}(j-1).PFclass)
                bExtStufe=true;
            end
        end
        if bExtStufe
            lenneu = StufenHMM{i}(j).End-StufenHMM{i}(j).Start;
            lenalt = Stufe.Length;
            Stufe.End = StufenHMM{i}(j).End;
            Stufe.Length = Stufe.End - Stufe.Start;
            Stufe.SumIntens = Stufe.SumIntens+sum(BurstInt{i}(Stufe.Start:Stufe.End));
            Stufe.SumPeaks = max(Stufe.SumPeaks,max(BurstInt{i}(Stufe.Start:Stufe.End)));
            Stufe.ProxFact = (lenalt*Stufe.ProxFact+lenneu*StufenHMM{i}(j).MW)/(lenalt+lenneu+1);
            Stufe.ProxFactErr = (lenalt*Stufe.ProxFactErr+lenneu*StufenHMM{i}(j).LL)/(lenalt+lenneu+1);
            Stufe.Efret = (lenalt*Stufe.Efret+lenneu*CalcEfretFromPF(Stufe.ProxFact, gamma))/(lenalt+lenneu+1);
            Stufe.r = (lenalt*Stufe.r+lenneu*CalcRFromEfret(Stufe.Efret, r0))/(lenalt+lenneu+1);
        else
            iStufe=iStufe+1;
            iPos=iPos+1;
            Stufe.iPos = iPos;
            Stufe.Start = StufenHMM{i}(j).Start;
            Stufe.End = StufenHMM{i}(j).End;
            Stufe.Anzahl = numel(StufenHMM{i});
            Stufe.Length = Stufe.End - Stufe.Start;
            Stufe.SumIntens = sum(BurstInt{i}(Stufe.Start:Stufe.End));
            Stufe.SumPeaks = max(BurstInt{i}(Stufe.Start:Stufe.End));
            Stufe.ProxFact = StufenHMM{i}(j).MW;
            Stufe.ProxFactErr = StufenHMM{i}(j).LL;
            Stufe.Efret = CalcEfretFromPF(Stufe.ProxFact, gamma);
            Stufe.r = CalcRFromEfret(Stufe.Efret, r0);
            Stufe.Intens = zeros(4,1);
            Stufe.Peak = zeros(4,1);
            if exist('obsvect') 
                Stufe.PFclass = obsvect(StufenHMM{i}(j).PFclass);
            else
                Stufe.PFclass = StufenHMM{i}(j).PFclass;
            end
            Stufe.iBurst = i;
        end
        StufenArr(iStufe) = Stufe;
        if mod(iStufe,1000)==0, fprintf(1,'\b\b\b\b\b\b%6d',[iStufe]); end
    end
    for idummy=iMark+1:iStufe
        StufenArr(idummy).Anzahl=iPos;
    end
end
StufenArr(iStufe+1:end)=[];
fprintf(1,'\b\b\b\b\b\bok.\n'); 
