function [yfit] = Fit_HistGauss(x,y)
%Fit_HistGauss    Create plot of datasets and fits
%   Fit_HistGauss(x,y)
%   Creates a plot, similar to the plot in the main curve fitting
%   window, using the data that you provide as input.  You can
%   apply this function to the same data you used with cftool
%   or with different data.  You may want to edit the function to
%   customize the code and this help message.
%
%   Number of datasets:  1
%   Number of fits:  1

 
% Data from dataset "y":
%    Y = y:
%    Unweighted
%
% This function was automatically generated on 01-Jun-2006 13:34:19

% Set up figure to receive datasets and fits
f_ = gcf; %clf; figure(f_);
legh_ = []; legt_ = {};   % handles and text for legend
xlim_ = [Inf -Inf];       % limits of x axis
ax_ = gca; %subplot(1,1,1);
set(ax_,'Box','on');
axes(ax_); hold on;

 
% --- Plot data originally in dataset "y"
x_1 = x(:); %(1:numel(y))';
y = y(:);
if nargout==0
    %h_ = line(x_1,y,'Parent',ax_,'Color',[0.333333 0 0.666667],...
    %     'LineStyle','none', 'LineWidth',1,...
    %     'Marker','.', 'MarkerSize',12);
    %h_ = bar(x,y,'BarWidth',1,'FaceColor',0.9*[1 1 1]);
    h_ = stem(x,y);
    xlim_(1) = min(xlim_(1),min(x_1));
    xlim_(2) = max(xlim_(2),max(x_1));
    legh_(end+1) = h_;
    legt_{end+1} = 'data';

    % Nudge axis limits beyond data limits
    if all(isfinite(xlim_))
       xlim_ = xlim_ + [-1 1] * 0.01 * diff(xlim_);
       set(ax_,'XLim',xlim_)
    end
end

% --- Create fit "fit 3"
%fo_ = fitoptions('method','NonlinearLeastSquares','Robust','On','Algorithm','Levenberg-Marquardt','MaxFunEvals',5688,'MaxIter',3935,'TolFun',1e-007);
fo_ = fitoptions('method','NonlinearLeastSquares','Robust','On','Algorithm','trust-region','MaxFunEvals',5688,'MaxIter',3935,'TolFun',1e-007);
st_(1) = max(y);
st_(2) = sum(y.*x_1)/sum(y); %gewichteter Mittelwert
st_(3) = 1;
set(fo_,'Startpoint',st_);
set(fo_,'Weight',y);
set(fo_,'Lower',zeros(size(st_)));
ft_ = fittype('gauss1');

% Fit this model using new data
cf_ = fit(x_1,y,ft_ ,fo_);

% Or use coefficients from the original fit:
if 0
   cv_ = {-512287566.8081, -9.40917545742, 50.6010118037, -0.3638327095087};
   cf_ = cfit(ft_,cv_{:});
end

% Plot this fit
if nargout==0
    h_ = plot(cf_,'fit',0.95);
    legend off;  % turn off legend from plot method call
    set(h_(1),'Color',[1 0 0],...
         'LineStyle','-', 'LineWidth',2,...
         'Marker','none', 'MarkerSize',6);
    legh_(end+1) = h_(1);
    legt_{end+1} = 'gauss1';
    hold off;
    ylim([0 max(y)*1.1]);
    %legend(ax_,legh_, legt_);
    text(interp1([0 1],xlim,0.8), interp1([0 1],ylim,0.90), ['a: ',sprintf('%.2f',[cf_.a1])]);
    text(interp1([0 1],xlim,0.8), interp1([0 1],ylim,0.80), ['x_0: ',sprintf('%.2f',[cf_.b1])]);
    text(interp1([0 1],xlim,0.8), interp1([0 1],ylim,0.70), ['\sigma: ',sprintf('%.2f',[cf_.c1])]);
    xlabel('');
    ylabel('');
else
    yfit = cf_;
end    

disp(cf_);
%fprintf(1,'a1: %.2f\n',[cf_.a1]);
%fprintf(1,'b1: %.2f\n',[cf_.b1]);
%fprintf(1,'c1: %.2f\n',[cf_.c1]);
