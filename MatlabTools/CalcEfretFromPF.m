function [Efret] = CalcEfretFromPF(p, gamma)
%This function calculates the FRET-Efficiency from ProxFact

nenner = (gamma.*(1-p)+p);
Efret = p./nenner;
