function [bOk] = CheckRange(data, RangeMin, RangeMax);
%This function checkes ranges of data values

RangeMin=RangeMin(:);
RangeMax=RangeMax(:);
data=data(:);
if size(RangeMin,1) == 1, RangeMin=repmat(RangeMin,size(data,1),1); end
if size(RangeMin,2) == 1, RangeMin=repmat(RangeMin,1,size(data,2)); end
if sum(size(RangeMin)~=size(data))~=0, error('Bad Size RangeMin!!!'); end

if size(RangeMax,1) == 1, RangeMax=repmat(RangeMax,size(data,1),1); end
if size(RangeMax,2) == 1, RangeMax=repmat(RangeMax,1,size(data,2)); end
if sum(size(RangeMax)~=size(data))~=0, error('Bad Size RangeMax!!!'); end

bOk = ~(sum(sum(data<RangeMin))|sum(sum(data>RangeMax)));
