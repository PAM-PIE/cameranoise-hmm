function [] = MergeResults(sVerz, sResFiles)

listVerz = dir(sVerz);

%Generate Res-FileList
ListResFiles = {};
sDirHeader = {};
sVerzList = {};
for i=3:length(listVerz)
    if (listVerz(i).isdir==1)
        NewFile = [sVerz, '\', listVerz(i).name, '\', sResFiles];
        if ~isempty(dir(NewFile))
            ListResFiles = {ListResFiles{:}, NewFile};
            sVerzList = {sVerzList{:}, [sVerz, '\', listVerz(i).name]};
            sDirHeader = [sDirHeader, {listVerz(i).name; File2Klartext(sVerz,listVerz(i).name)}];
        end;
    end;
end;

%load all files and merge them
MergeData = {};
for i=1:length(ListResFiles)
    disp(sprintf('%d/%d %s',[i,length(ListResFiles),ListResFiles{1,i}]));
    ff = fopen(ListResFiles{1,i}, 'r');
    resDataRows = textscan(ff, '%s', 'delimiter', '\n');
    resDataRows = resDataRows{1,1};
    resData = [];
    for j=1:length(resDataRows)
        [resDataRow] = strread(resDataRows{j,1}, '%s', 'delimiter', '\t')';
        resData = [resData; resDataRow];
    end;
    xdata = {resData{:,3}}';
    xdata = str2double({xdata{2:length(xdata)}}');
    xdata = GenXDataFromDurationCol(xdata/1000);
    ydata = {resData{:,2}}';
    ydata = str2double({ydata{2:length(ydata)}}');
    [cf, I0, tBleach] = Fit_IntExp(xdata,ydata,[sVerzList{i},'\Fit_Int.png']);
    disp(sprintf('%f kcps   \t%f s\n',[I0; tBleach]));
    resData = {};
    resData{1,1}=num2str(tBleach(1));
    resData{2,1}=num2str(tBleach(2));
    resData{1,2}=num2str(I0(1));
    resData{2,2}=num2str(I0(2));
%    resData=num2cell([I0';tBleach']);
    %Überschriften hinzufügen
    HeaderLine = {};
    for j=1:size(resData,2)
        HeaderLine = [HeaderLine, {sDirHeader{:,i}}'];
    end;
    resData = [HeaderLine; resData];
    %Dimensionen anpassen
    DimDiff = size(resData,1) - size(MergeData,1);
    if DimDiff > 0
        MergeData = [MergeData; cell(DimDiff, size(MergeData,2))];
    end;
    if DimDiff < 0
        resData = [resData; cell(abs(DimDiff), size(resData,2))];
    end;
    % Hier werden die Spalten zusammengetragen:
    % Spalte 1: Dateiname
    % Spalte 6: A1
    % Spalte 8: tau2
    %MergeData = [MergeData, {resData{:,2}}'];
    MergeData = [MergeData, {resData{:,1}}', {resData{:,2}}'];
    %resData = resData{:,1};
    fclose(ff);
end;

%MergeData speichern
fres = fopen([sVerz,'\',sResFiles],'w');
for i=1:size(MergeData,1)
    for j=1:size(MergeData,2)
        if isempty(MergeData{i,j})
            MergeData{i,j} = '';
        end;
    end;           
    fprintf(fres, '%s\t', MergeData{i,:});
    fprintf(fres, '\r\n');
end;
fclose(fres);

disp('All Files processed.');

%###################################################

function [xdata] = GenXDataFromDurationCol(DurCol);
xdata = [];
t = 0;
for i=1:length(DurCol)
    t = t + DurCol(i);
    xdata=[xdata; t];
end
