function [cf, I0, tBleach] = Fit_IntExp(xdata,ydata,FilenamePNG)
%FIT_INTEXP    Create plot of datasets and fits
%   FIT_INTEXP(XDATA,YDATA)
%   Creates a plot, similar to the plot in the main curve fitting
%   window, using the data that you provide as input.  You can
%   apply this function to the same data you used with cftool
%   or with different data.  You may want to edit the function to
%   customize the code and this help message.
%
%   Number of datasets:  1
%   Number of fits:  1

 
% Data from dataset "Int":
%    X = xdata:
%    Y = ydata:
%    Unweighted
%
% This function was automatically generated on 05-Apr-2006 17:05:53

% Set up figure to receive datasets and fits
f_ = clf;
figure(f_);
legh_ = []; legt_ = {};   % handles and text for legend
xlim_ = [Inf -Inf];       % limits of x axis
ax_ = subplot(1,1,1);
set(ax_,'Box','on');
axes(ax_); hold on;

 
% --- Plot data originally in dataset "Int"
xdata = xdata(:);
ydata = ydata(:);
h_ = line(xdata,ydata,'Parent',ax_,'Color',[0.333333 0 0.666667],...
     'LineStyle','-', 'LineWidth',2,...
     'Marker','.', 'MarkerSize',12);
xlim_(1) = min(xlim_(1),min(xdata));
xlim_(2) = max(xlim_(2),max(xdata));
legh_(end+1) = h_;
legt_{end+1} = 'Int';

% Nudge axis limits beyond data limits
if all(isfinite(xlim_))
   xlim_ = xlim_ + [-1 1] * 0.01 * diff(xlim_);
   set(ax_,'XLim',xlim_)
end


% --- Create fit "fit 3"
%st_ = [179.7805515392 -0.02854000767857 64.12002253066 -0.004329398174549 ];
st_ = [100 -0.1 10 -0.01];
ft_ = fittype('exp2' );

% Fit this model using new data
cf_ = fit(xdata,ydata,ft_ ,'Startpoint',st_);

% Or use coefficients from the original fit:
if 0
   cv_ = {175.0049698464, -0.02550226491843, 59.9042454642, -0.004285336242934};
   cf_ = cfit(ft_,cv_{:});
end

cf = cf_;
I0(1) = cf_.a;
I0(2) = cf_.c;
tBleach(1) = -1/cf_.b;
tBleach(2) = -1/cf_.d;
if tBleach(1)>tBleach(2)
    flipud(I0);
    flipud(tBleach);
end
    
% Plot this fit
h_ = plot(cf_,'fit',0.95);
legend off;  % turn off legend from plot method call
set(h_(1),'Color',[1 0 0],...
     'LineStyle','-', 'LineWidth',0.5,...
     'Marker','none', 'MarkerSize',6);
legh_(end+1) = h_(1);
legt_{end+1} = 'fit bi-exp';

lim = get(gca, 'YLim');    %1=min, 2=max
set(gca, 'YLim', [0 lim(2)]);    %1=min, 2=max
lim = get(gca, 'XLim');    %1=min, 2=max
set(gca, 'XLim', [0 lim(2)]);    %1=min, 2=max
xlabel('time/s');
ylabel('Iavg/kcps');
%title('');

if ~strcmp(FilenamePNG,'')
    I = getframe(gcf);
    imwrite(I.cdata, FilenamePNG);
end

hold off;
legend(ax_,legh_, legt_);
