%Laser 1
filepath='H:\Stefan\ATPase\TripleFret\2008-12-03\';
filename1='488-645-beads-2blauerLaser-a.scan';

%Laser2
filepath='H:\Stefan\ATPase\TripleFret\2008-12-03\';
filename2='488-645-beads-2grünerLaser-a.scan';

%Laser3
filepath='H:\Stefan\ATPase\TripleFret\2008-12-03\';
filename3='488-645-beads-2roterLaser-a0000.scan';


close all;
I={};
x0=[];
y0=[];
xsigma=[];
ysigma=[];
Scangroesse=10000; %Scangröße in nanomatern
Pixel=200;         %Pixel je Zeile

[I{1} I{2} I{3}]=LoadScanFile([filepath,filename1]); %Bilder Laser1
[I{4} I{5} I{6}]=LoadScanFile([filepath,filename2]); %Bilder Laser2
[I{7} I{8} I{9}]=LoadScanFile([filepath,filename3]); %Bilder Laser3

j=[1,5,9];
for i=1:3
  [x0(i) y0(i) xsigma(i) ysigma(i)]=fit2d(I{j(i)});

  subplot(1,3,i);
  imagesc(I{j(i)});
  axis([0 200 0 200])
  axis 'equal';
  phi=(0:1/50:1)*2*pi;
  set(gca, 'PlotBoxAspectRatio', [1 1 1]);
  hold on

  x=1.5*xsigma(i)*cos(phi)+x0(i);
  y=1.5*ysigma(i)*sin(phi)+y0(i);

  plot(x,y,'k','LineWidth',2);
  plot(x0(i),y0(i),'k+','LineWidth',2);
end

%Fokiabstand
%Laser1 zu Laser2
FA=x0(1)-x0(2);
FA=abs(FA);                     %Abstand in Pixeln in x
FAabs=FA*Scangroesse/Pixel      %Abszand in nm in x

FAy=y0(1)-y0(2);
FAy=abs(FAy);                   %Abstand in Pixeln in y
FAyabs=FAy*Scangroesse/Pixel    %Abstand in nm in y

%Laser1 zu Laser3

FA1=x0(1)-x0(3);
FA1=abs(FA1);                     %Abstand in Pixeln in x
FAabs1=FA1*Scangroesse/Pixel      %Abszand in nm in x

FAy1=y0(1)-y0(3);
FAy1=abs(FAy1);                   %Abstand in Pixeln in y
FAyabs1=FAy1*Scangroesse/Pixel    %Abstand in nm in y


subplot(1,3,2);
xlabel(sprintf('Abstand in x: %.0f nm\nAbstand in y: %.0f nm',FAabs,FAyabs));

subplot(1,3,3);
xlabel(sprintf('Abstand in x: %.0f nm\nAbstand in y: %.0f nm',FAabs1,FAyabs1));
