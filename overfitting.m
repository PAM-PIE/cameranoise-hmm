%Example Overfitting

if 0
  numPeaks=5;
  numPoints=10000;
  muArr=rand(1,numPeaks)*2-1;
  varArr=rand(1,numPeaks)/10;
  data=[];
  for i=1:numPeaks
    data(i,:)=normrnd(muArr(i),varArr(i),1,numPoints);
  end
end

xdata=[-1:0.01:1]';
ydata=hist(data(:),xdata)';

chi=[];
for i=1:8
  sModel=sprintf('gauss%d',i);
  [fresult,gof,output] = fit(xdata,ydata,sModel,'MaxIter',20,'Display','off');
  fprintf('.');
  yfit=fresult(xdata);
  chi(i)=sum((yfit-ydata).^2)/length(ydata);
%  chi(i)=chi(i)/sqrt(numPoints);
  %chi(i)=gof.adjrsquare;
end
fprintf('ok\n');

clf;
% stairs(xdata,ydata,'LineWidth',2); 
% axis tight;
% plot(xdata,yfit,'r');
semilogy(chi,'.-'); grid;
chi
