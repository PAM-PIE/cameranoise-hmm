sSample={'DNA3','DNA12','DNA18','DNA19'};
sState={'Dynamic','Steady'};

HMMVersion;
Anz=7;
clf;

for iSample=1:4
  
  for iState=1:2

%     filepath=['D:\Schluesche\Maindata\',sSample{iSample},'\',sState{iState},'FRET\'];
%     load([filepath,'Ausw',sAuswVer,'_',sBurstSel,'_m0',int2str(Anz),'.mat'],'MultiQData','TimeBin');
    MultiQData=MetaQData{iSample,iState};
    OrigExp=-1*ones(Anz,2*Anz);
    
    subplot(2,1,iState); cla; hold on; box on;
    cmap=get(gca,'ColorOrder');
    for Q=1:Anz
      mu1=MultiQData{Q}.mu1;
      mu1var=MultiQData{Q}.mu1var;
      plot(mu1,mu1var+0.01*Q,'.-','color',cmap(Q,:));
      OrigExp(1:length(mu1),Q*2-1)=mu1;
      OrigExp(1:length(mu1var),Q*2)=mu1var;
    end
    xlim([0 1]); grid on
    title(sprintf('%s %s (%s V%s)',sSample{iSample},sState{iState},sBurstSel,sAuswVer));
    xlabel('�');
    ylabel('\sigma�');
    drawnow;
    
    filepath_='D:\Schluesche\Auswertung\';
    filename_=sprintf('Ausw%s_%s %s_%s VarOverMean',...
      sAuswVer,sBurstSel,sSample{iSample},sState{iState});
    fid=fopen([filepath_,filename_,'.txt'],'w');
    fprintf(fid,'mean%d\tvar%d',[1 1]);
    fprintf(fid,'\tmean%d\tvar%d',[2:Anz;2:Anz]);
    fprintf(fid,'\n');
    fclose(fid);
    dlmwrite([filepath_,filename_,'.txt'],OrigExp,'-append','delimiter','\t');
  end

  if 0
    set(gcf,'InvertHardcopy','off');
    set(gcf,'PaperPositionMode','auto');
    saveas(gcf,[filepath_,filename_],'png');
  end
end
