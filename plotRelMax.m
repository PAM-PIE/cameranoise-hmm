function plotRelMax(datax,datay);
%function plotRelMax(data);

hold on;
i=find((datay(1:end-2)<datay(2:end-1))&(datay(3:end)<datay(2:end-1)))+1;
x=datax(i);
y=datay(i);
%stem(x,y+.05*max(y),'Marker','v');
for i=1:length(y)
  text(x(i),y(i)+.1*max(y),sprintf('%.2f',x(i)),...
    'color',[1 0 0],... 
    'BackgroundColor',[1 1 .7],...
    'HorizontalAlignment','center',...
    'VerticalAlignment','bottom');
hold off;
end
