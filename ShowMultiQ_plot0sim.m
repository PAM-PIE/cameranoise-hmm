% Show RawData EFret Histogram
sSample={'DNA3','DNA12','DNA18','DNA19','Donor_Only','Acceptor_Only','Sim1'};
sState={'Dynamic','Steady',''};
filepathStamm=['D:\Schluesche\Maindata\',sSample{iSample},'\'];
if iSample<=4
  filepathStamm=[filepathStamm,'\',sState{iState},'FRET\'];
end

load([filepathStamm,'\Ausw16_Sim1_m07.mat'],'AlldataPF');

hold off
xhist=0:0.015:1;
yhist=hist([AlldataPF{:}],xhist); 
stairs(xhist,yhist,'b');

title(sprintf('%s %s (%s V%s)',sSample{iSample},sState{iState},sBurstSel,sAuswVer));

set(gcf,'InvertHardcopy','off');
set(gcf,'PaperPositionMode','auto');
saveas(gcf,['D:\Schluesche\Auswertung\',...
  sprintf('Ausw%s_%s %s_%s muraw',...
  sAuswVer,sBurstSel,sSample{iSample},sState{iState})],'png');
