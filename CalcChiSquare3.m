function [MultiQ,LL,LLBIC,ChiSquare,ChiSquareRed,ChiSquareBIC]=CalcChiSquare3(filename);

%load(filename,'MultiQData','dataPF','dataPFsigma','dataInt','MultiQ');
bBinWise=true;
load(filename,'MultiQData','dataPF','dataPFsigma','MultiQ');
ChiSquare=zeros(1,length(MultiQ))*NaN;
ChiSquareRed=zeros(1,length(MultiQ))*NaN;
for iMulti=1:length(MultiQData)
  mu1=MultiQData{iMulti}.mu1;
  mu1var=MultiQData{iMulti}.mu1var;
  transmat1=MultiQData{iMulti}.transmat1;
  Q=length(mu1);
  AllStufen=MultiQData{iMulti}.AllStufen;
  AnzBursts=length(AllStufen);
  %AnzStufen=length([AllStufen{:}]);
  ChiSquareBurst=zeros(1,AnzBursts);
  ChiSquareBurstVar=zeros(1,AnzBursts);
  NumBins=zeros(1,AnzBursts);
  NumSteps=zeros(1,AnzBursts);
  NumFreeParam=numel(transmat1)+numel(mu1);
  for iBurst=1:AnzBursts
    AnzStufen=length(AllStufen{iBurst});
    NumSteps(iBurst)=AnzStufen;
    StateTraj=[AllStufen{iBurst}(:).PFclass];
    MeanTraj=[AllStufen{iBurst}(:).MW];
    VarTraj=[AllStufen{iBurst}(:).MWstd].^2;
    StufenLen=[AllStufen{iBurst}(:).End]-[AllStufen{iBurst}(:).Start]+1;
    Yfit=mu1(StateTraj);
    YfitVar=mu1var(StateTraj);
    VarTrace=[];
    ChiSquareStufe=[];
    YdataVar=[];
    VarTraj=VarTraj./StufenLen.*(StufenLen-1);
    for iStufe=1:AnzStufen
      t1=AllStufen{iBurst}(iStufe).Start;
      t2=AllStufen{iBurst}(iStufe).End;
      ChiSquareStufe(iStufe)=sum((dataPF{iBurst}(t1:t2)-Yfit(iStufe)).^2);
      %ChiSquareStufe(iStufe)=sum((dataPF{iBurst}(t1:t2)-MeanTraj(iStufe)).^2);
      YdataVar(iStufe)=sum(Yfit(iStufe).*(1-Yfit(iStufe))./(dataPFsigma{iBurst}(t1:t2)));
    end
    if bBinWise
      YfitVar=YfitVar.*StufenLen;
      VarTraj=VarTraj.*StufenLen;
    else
      ChiSquareStufe=ChiSquareStufe./StufenLen;
      YdataVar=YdataVar./StufenLen;
    end
    ChiSquareBurst(iBurst)=sum(ChiSquareStufe);
    %ChiSquareBurstVar(iBurst)=sum(YfitVar+YdataVar);
    ChiSquareBurstVar(iBurst)=sum(VarTraj);
    NumBins(iBurst)=sum(StufenLen);
  end
  %ChiSquare(iMulti)=max(eps,sum(ChiSquareBurst)/(sum(NumBins)));
  ChiSquare(iMulti)=max(eps,sum(ChiSquareBurst)/(sum(NumBins)));
  ChiSquareRed(iMulti)=max(eps,sum(ChiSquareBurst)/(sum(ChiSquareBurstVar)));
  if bBinWise
    ChiSquareBIC(iMulti)=sum(NumBins)*log(sum(ChiSquareBurst)/sum(NumBins))+NumFreeParam*log(sum(NumBins));
  else
    ChiSquareBIC(iMulti)=sum(NumSteps)*log(sum(ChiSquareBurst)/sum(NumSteps))+NumFreeParam*log(sum(NumBins));
  end
  LL(iMulti)=MultiQData{iMulti}.trajLL(end);
  LLBIC(iMulti)=-2*LL(iMulti)+NumFreeParam*log(sum(NumBins));
end
