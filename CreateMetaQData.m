
sSample={'DNA3','DNA12','DNA18','DNA19','Sim1','Sim2'};
sState={'Dynamic','Steady'};
Anz=10;
HMMVersion;

if 1
  for iSample=1:4
    for iState=1:2
      if isempty(strfind(sBurstSel,'Sim'))
        filepath=['D:\Schluesche\MainData\',sSample{iSample},'\',sState{iState},'FRET\'];
        fprintf('%s %s...',sSample{iSample},sState{iState});
      else
        filepath=['D:\Schluesche\MainData\Sim1\'];
        fprintf('%s...',sSample{iSample});
      end
      filename=sprintf('Ausw%s_%s_m%02d.mat',sAuswVer,sBurstSel,Anz);
      if exist([filepath,filename],'file')
        load([filepath,filename],'MultiQData','dataInt');
      else
        MultiQData=[];
        dataInt={};
      end
      MetaQData{iSample,iState}=MultiQData;
      %for qq=1:3, MetaQData{iSample,iState}{7+qq}=MultiQData{qq}; end
      dataNumBins(iSample,iState)=length([dataInt{1,:}]);
      dataNumTraces(iSample,iState)=size(dataInt,2);
      fprintf('ok\n');
      %save([filepath,sprintf('Ausw%s_%s_MetaData',sAuswVer,sBurstSel)],'MetaQData');
    end
  end
  save(['D:\Schluesche\MainData\',sprintf('Ausw%s_%s_MetaData',sAuswVer,sBurstSel)],'MetaQData');
end

if 0
  MetaStufenArr={};
  for iSample=1:numel(sSample)
    for iState=1:numel(sState)
      for q=1:Anz
        filepath=['D:\Schluesche\MainData\',sSample{iSample},'\',sState{iState},'FRET\'];
        filename=sprintf('Ausw%s_%s_m%02d.mat',sAuswVer,sBurstSel,q);
        fprintf('%s %s (%d)...',sSample{iSample},sState{iState},q);
        if exist([filepath,filename],'file')
          load([filepath,filename],'StufenArr');
        else
          StufenArr=[];
        end
        MetaStufenArr{iSample,iState,q}=StufenArr;
        %MetaStufenArr{q}=StufenArr;
        fprintf('ok\n');
      end
      %save([filepath,sprintf('Ausw%s_%s_MetaStufen',sAuswVer,sBurstSel)],'MetaStufenArr');
    end
  end
  save(['D:\Schluesche\MainData\',sprintf('Ausw%s_%s_MetaStufen',sAuswVer,sBurstSel)],'MetaStufenArr');
end
