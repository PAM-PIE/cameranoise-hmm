function [datatxt]=ShowBurstWithStufenPF(...
  dataPF, dataPFsigma, dataDonor, dataAkzeptor, ...
  IDBurst, dataStartMS, TimeBin, cMinLL, Qremain, Stufen, ...
  llpath, mu1, Sigma1, SimStufen, SimParam);

cShowLL = false;
bShowVarTrace = true;
bCorrIntensities = false;
bIncStufenLen = true;  % only for binwise analysis
cConfidenceInterval = .95; %entspricht 1.96*sigma
% normcdf(3,0,1)-normcdf(-3,0,1)
% 3 Sigma w�ren cConfidenceInterval=.9973
% 2 Sigma w�ren cConfidenceInterval=.9545
% 1 Sigma w�ren cConfidenceInterval=.6827
if bShowVarTrace|cShowLL, AnzPlots=4; else AnzPlots=3; end

path=ones(length(dataPF),1);
mu1_=[NaN, mu1];
Sigma1_=[NaN, squeeze(Sigma1)'];
for i=1:numel(Stufen)
  istart=min(length(path),fix(Stufen(i).Start));
  iend=min(length(path),fix(Stufen(i).End));
  path(istart:iend)=Stufen(i).PFclass+1;
end

%Gau�
xmax=mu1_(path);
dataPFsigma=mu1_(path).*(1-mu1_(path))./dataPFsigma;
if isfinite(cConfidenceInterval)
  x1=norminv((1-cConfidenceInterval)/2,mu1_(path),sqrt(dataPFsigma+Sigma1_(path)));
  x2=norminv(1-(1-cConfidenceInterval)/2,mu1_(path),sqrt(dataPFsigma+Sigma1_(path)));
end

if nargout==0

  if bIncStufenLen
    for i=1:length(Stufen)
      Stufen(i).End=Stufen(i).End+1;
    end
  end
  
  hold off; clf;
  sLineStyle={};
  for i=1:length(Stufen),
    if (Stufen(i).LL>=cMinLL)&(sum(Stufen(i).PFclass==Qremain)),
      sLineStyle{i}='-';
    else
      sLineStyle{i}=':';
    end;
  end

  ax(1)=subplot(AnzPlots,1,1);
  %plot(dataPF,'b');
  %plotwithcol([1:length(dataPF)]',dataPF',dataInt',0.7,[0 0 1]);

  %   path=ones(length(dataPF),1);
  %   mu1_=[NaN, mu1];
  %   Sigma1_=[NaN, squeeze(Sigma1)'];
  %   for i=1:numel(Stufen)
  %     istart=min(length(path),fix(Stufen(i).Start));
  %     iend=min(length(path),fix(Stufen(i).End));
  %     path(istart:iend)=Stufen(i).PFclass+1;
  %   end
  %
  %   %Gau�
  %   xmax=mu1_(path);
  %   dataPFsigma=mu1_(path).*(1-mu1_(path))./dataPFsigma;
  %   if isfinite(cConfidenceInterval)
  %     x1=norminv((1-cConfidenceInterval)/2,mu1_(path),sqrt(dataPFsigma+Sigma1_(path)));
  %     x2=norminv(1-(1-cConfidenceInterval)/2,mu1_(path),sqrt(dataPFsigma+Sigma1_(path)));
  if isfinite(cConfidenceInterval)
    if 1
      h = area([x1;x2-x1]'); % Set BaseValue via argument
      set(h(1),'FaceColor','none','EdgeColor','none');
      set(h(2),'FaceColor',[1 .95 .9],'EdgeColor','none');
      %set(h,'LineStyle','-','EdgeColor',[1 .8 .7]); % Set all to same value
      hold on;
    end
    if 1
      plot(x1,'color',[1 .8 .7]);
      hold on;
      plot(x2,'color',[1 .8 .7]);
    end
  end

  hold on;
  set(gca,'LineStyleOrder',{'-'});
  plot(dataPF);

  if numel(Stufen)>0
    set(gca,'LineStyleOrder',sLineStyle);
    plot([[Stufen(:).Start];[Stufen(:).End]],...
      [mu1([Stufen(:).PFclass]);mu1([Stufen(:).PFclass])],...
      'color',[1 0.5 0],'LineWidth',2);
  end
  title([sprintf('Burst %d',[IDBurst]),': ',sprintf('%g ms (%d * [%g ms])',[dataStartMS,length(dataDonor),TimeBin])]);
  xlim([1 length(dataDonor)]);
  ylim([0 1]);
  ylabel('P');
  for i=1:length(Stufen)
    text((Stufen(i).Start+Stufen(i).End)/2, 0.9, int2str(Stufen(i).PFclass),...
      'HorizontalAlignment','center','Color',[1 0.5 0]);
  end
  % SimStufen
  if exist('SimStufen')
    for i=1:length(SimStufen.PFclass)
      text((SimStufen.Start(i)+SimStufen.End(i))/2, 0.1, int2str(SimStufen.PFclass(i)),...
        'HorizontalAlignment','center','Color',[0 0 0]);
    end
    stairs([SimStufen.Start;SimStufen.End(end)],...
      [SimParam.mu1(SimStufen.PFclass),SimParam.mu1(SimStufen.PFclass(end))],...
      'color',[0 0 0],'LineStyle',':');
    %plot([SimStufen.Start';SimStufen.End'],...
    %    [SimParam.mu1(SimStufen.PFclass);SimParam.mu1(SimStufen.PFclass)],...
    %    'color',[1 0 0]);
  end

  ax(3)=subplot(AnzPlots,1,3);
  set(gca,'LineStyleOrder',{'-'});
  if cShowLL
    plot(llpath);
    hold on;
    if numel(Stufen)>0
      set(gca,'LineStyleOrder',sLineStyle,'ColorOrder',[1 0.5 0]);
      plot([[Stufen(:).Start];[Stufen(:).End]],...
        [Stufen(:).LL;Stufen(:).LL],...
        'LineWidth',2);
    end
    if exist('SimStufen')
      verify=VerifyStufen({Stufen}, [length(dataDonor)], {SimStufen});
      title(sprintf('Vergleich: %.2f%%',100*verify));
    end
  else
    plot(dataDonor+dataAkzeptor);
    hold on;
    if numel(Stufen)>0
      %dataStufen = max(dataInt) * 0.95 * Stufen2Traj(Stufen);
      %plot([1:length(dataStufen)]+1,dataStufen,'color',[0.5 0.5 0]);
      dataStufenx=reshape([[Stufen.Start]',[Stufen.End]']',length(Stufen)*2,1);
      dataStufeny=repmat([max(dataDonor+dataAkzeptor)*0.95;0],length(Stufen),1);
      stairs(dataStufenx,dataStufeny,'color',[0.5 0.5 0]);
    end
  end
  xlim([1 length(dataDonor)]);
  if cShowLL
    ylim([1e-2 1e0]);
    set(gca,'YScale','log');
    ylabel('LL');
  else
    ylim([0 max(1,max(dataDonor+dataAkzeptor))]);
    set(gca,'YScale','lin');
    ylabel('total intensity');
  end

  ax(2)=subplot(AnzPlots,1,2);
  plot(dataDonor,'color',[0 0.5 0]);
  hold on;
  title(sprintf('%d FRET level',[length(Stufen)]));
  plot(dataAkzeptor,'color',[1 0 0]);
  if numel(Stufen)>0
    %dataStufen = max(dataInt) * 0.95 * Stufen2Traj(Stufen);
    %plot([1:length(dataStufen)]+1,dataStufen,'color',[0.5 0.5 0]);
    dataStufenx=reshape([[Stufen.Start]',[Stufen.End]']',length(Stufen)*2,1);
    dataStufeny=repmat([max(dataDonor+dataAkzeptor)*0.95;0],length(Stufen),1);
    stairs(dataStufenx,dataStufeny,'color',[0.5 0.5 0]);
  end
  %plot([1 length(dataDonor)],[1 1]*Inoise1,'-.','color',[0 .5 0]);
  %plot([1 length(dataDonor)],[1 1]*Inoise2,':','color',[1 0 0]);
  xlim([1 length(dataDonor)]);
  ylim([0 max(1,max(dataDonor+dataAkzeptor))]);
  ylabel(sprintf('counts / [%g ms]',TimeBin));

  ax(4)=subplot(AnzPlots,1,4); cla; box;
  hold off
  ShowVarTrace(dataDonor, dataAkzeptor, 10, [0 0 1]);
  hold on
  ShowVarTrace(dataDonor, dataAkzeptor, 50, [1 0 0]);
  semilogy([1 length(dataDonor)],[1 1],'k');
  xlim([1 length(dataDonor)]);
  ylim([1/3 3]);
  ylabel('var-trace');

  %Achsen verlinken
  linkaxes(ax,'x');
  %disp(sprintf('Burst %d von %d',[iBurst,AnzBursts]));
else
  %Stufen = Path2Stufen(path, Qremain, 1, dataDonor, dataAkzeptor, NoiseDonor, NoiseAkzeptor, llpath, cMinLL);  %Nur Stufen 1..Q-1 ausw�hlen, min. Stufenl�nge
  dataStufen = zeros(1,length(dataDonor));
  for i=1:length(Stufen), dataStufen(Stufen(i).Start:Stufen(i).End)=mu1(Stufen(i).PFclass); end;
  datatxt=NaN*ones(length(dataDonor),5);
  datatxt(1:length(dataDonor),1)=[1:length(dataDonor)]'-1;
  datatxt(1:length(dataDonor),2)=dataDonor;
  datatxt(1:length(dataAkzeptor),3)=dataAkzeptor;
  datatxt(1:length(dataPF),4)=dataPF;
  datatxt(1:length(dataStufen),5)=dataStufen;
  if exist('x1','var')
    datatxt(1:length(x1),6)=x1;
    datatxt(1:length(x1),7)=x2;
  end
end

  function ShowVarTrace(dataDonor, dataAkzeptor, d, col);
    x=1:length(dataDonor)-d;
    k=zeros(1,length(x));
    for i=x
      I1=dataDonor(i:i+d);
      I2=dataAkzeptor(i:i+d);
      k(i)=var(I1+I2)/(var(I1)+var(I2));
    end
    semilogy(x+d/2,k,'color',col); hold on
  end

end