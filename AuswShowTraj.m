
% Zeige Trajektorien

%iTraj=450;
iTraj=21;

if bLearnBurstwise
  iMulti = 1;
  idata = iTraj;
  mu1 = MultiQData{iMulti}(idata).mu1;
  Sigma1 = MultiQData{iMulti}(idata).Sigma1;
  mu1var = MultiQData{iMulti}(idata).mu1var;
  transmat1 = MultiQData{iMulti}(idata).transmat1;
  dwelltime1 = MultiQData{iMulti}(idata).dwelltime1;
  %StepCount = MultiQData{iMulti}(idata).StepCount;
  trajLL = MultiQData{iMulti}(idata).trajLL;
  %Stufen = MultiQData{iMulti}(idata).AllStufen;
  %Stufen_ = MultiQData{iMulti}(idata).AllStufen_;
end

if ~exist('bUpdate'), bUpdate=true; end
if bUpdate
    fprintf(1,'%6d Stufen-Eigenschaften rechnen: %6d',[length([AllStufen{:}]),0]); 
    NoStufen=zeros(numel(AllStufen),1);
    BurstLen=zeros(numel(AllStufen),1);
    BurstIavg=zeros(numel(AllStufen),1);
    tic;
    for i=1:length(AllStufen)
        NoStufen(i)=numel(AllStufen{i}); 
        if NoStufen(i)>=0
            BurstLen(i)=length(AlldataInt{i}); %AllStufen{i}(end).End-AllStufen{i}(1).Start+1; 
            BurstIavg(i)=mean(AlldataInt{i});
        else
            BurstLen(i)=0;
            BurstIavg(i)=0;
        end        
        if toc>0.2, fprintf(1,'\b\b\b\b\b\b%6d',[i]); tic; end
    end
    fprintf(1,'\b\b\b\b\b\b%6d',[i]);
    %Fertig
    fprintf(1,'\b\b\b\b\b\bok.\n'); 
    bUpdate=false;
end

if 0
    ShowBurst=20;
    [sortw, sorti]=sort(NoStufen,'descend');
    %[sortw, sorti]=sort(NoStufen,'ascend');
    %disp([[sorti(end:-1:end-ShowBurst)]', [sortw(end:-1:end-ShowBurst)]', [BurstLen(sorti(end:-1:end-ShowBurst))]']); 
    %[sortw, sorti]=sort(AllLoglik);
    %disp({'i','LL','Len','Iavg','#Stufen'});
    i=0;
    iBurstArr=[];
    fprintf('\t%5s\t%4s\t%4s\t%4s\n',...
         ['i'],...
        ['#'],...
        ['Len'],...
        ['Iavg']); 
    while (length(iBurstArr)<ShowBurst)&(i<numel(sorti));
        i=i+1;
        if NoStufen(sorti(i))>0
            iBurstArr(end+1)=sorti(i);
            fprintf('\t%5d\t%4.0f\t%4d\t%4.1f\n',...
                [sorti(i)]',...
                [sortw(i)]',...
                [BurstLen(sorti(i))]',...
                [BurstIavg(sorti(i))]'); 
        end
    end
    fprintf('\n');
end

ShowBurstWithStufen(AlldataPF{iTraj},AlldataInt{iTraj},...
    iTraj,data(iTraj).StartAnalysis,TimeBin,-Inf,1:Q,...
    AllStufen{iTraj},[],mu1);

