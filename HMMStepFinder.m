%StepFinder with a Hidden Markov Model
clear variables;
clf;
HMMVersion;

%sBurstSel='Sim1';
bSimData=strncmp(sBurstSel,'Sim',3);

if ~bSimData
  if ~exist('bAutomate')
    filepathBase = 'F:';
    filepath='\Maindata\DNA3\DynamicFRET\';
    %filepath='\Maindata\DNA3\SteadyFRET\';
    %filepath='\Maindata\DNA12\DynamicFRET\';
    %filepath='\Maindata\DNA12\SteadyFRET\';
    %filepath='\Maindata\DNA18\DynamicFRET\';
    %filepath='\Maindata\DNA18\SteadyFRET\';
    %filepath='\Maindata\DNA19\DynamicFRET\';
    %filepath='\Maindata\DNA19\SteadyFRET\';
    %filepath='\Maindata\Donor_Only\';
    %filepath='\Maindata\H2B\DynamicFRET_sub\';
    filepath=[filepathBase, filepath];
  end
  filename='TBPDNA*.txt';
else
  if strcmp(sBurstSel,'Sim1')
    filepath='\Maindata\Sim1\';
    filename='Sim_*.txt';
  end
  if strcmp(sBurstSel,'Sim2')
    filepath='\Maindata\Sim2\';
    filename='Sim_*.txt';
  end
end

AllStufen={};
AllLoglik=[];
loglik=[];
pmu0=[];
R0=5;
gamma=1;
%dbstop if warning;

blearn = true;
bAfterCrash = false;
bLearnBurstwise = false;
bLoad = true;
bMultiQ = true;
%MultiQ = 5*ones(1,10); %6:8; %1:9;
MultiQ = 4; %1:7; %1:10; %6:8; %1:9;
bUseExtHMM = true;
MaxAnzBurst = inf;
TimeBin = 5;
TimeBinNew = 5;

bBild = true;
bSave = false;
bShowParam = ~bLearnBurstwise;
bLowIntensStep = false;
bBurstMarkierung = true;
IterMax = 150;
%IterMax = 10;
IterUpdate = IterMax;
Threshold = 1E-4;
cMinLL = 0; %0.001;

bUpdate = true;

if ~bMultiQ, MultiQ=0; end
clear MultiQData;

if 1
  disp(' ');
  if ~bSimData
    HMMLoadData;
  else
    HMMLoadDataSim;
  end
else
  load([filepath,'ManuelleStufen.mat']);
  AlldataInt = {};
  AlldataPF = {};
  for i=1:length(fret)
    AlldataInt{i} = ones(1,length(fret{i}));
    AlldataPF{i} = fret{i}';
  end
end

if bAfterCrash
  AfterCrashQ=0;
  for q=MultiQ
    if exist(sprintf('%sAusw%s_%s_m%02d.mat',filepath,sAuswVer,sBurstSel,q),'file')
      AfterCrashQ=q;
    end
  end
  if (AfterCrashQ>0) && (AfterCrashQ<MultiQ(end))
    AfterCrashQBackup=AfterCrashQ;
    bAfterCrashBackup=bAfterCrash;
    AfterCrashMultiQ=MultiQ;
    load(sprintf('%sAusw%s_%s_m%02d',filepath,sAuswVer,sBurstSel,AfterCrashQ));
    AfterCrashQ=AfterCrashQBackup;
    bAfterCrash=bAfterCrashBackup;
    MultiQ=AfterCrashMultiQ;
    clear AfterCrashQBackup bAfterCrashBackup;
  end
end

idata=0;
while idata<size(AlldataInt,2)
  idata=idata+1;
  if bLearnBurstwise
    dataInt{1,1} = AlldataInt{1,idata};
    dataInt{2,1} = AlldataInt{2,idata};
    dataPF{1} = AlldataPF{idata};
    dataPFsigma{1} = AlldataPFsigma{idata};
  else
    dataInt = AlldataInt;
    dataPF = AlldataPF;
    dataPFsigma = AlldataPFsigma;
    idata=size(AlldataInt,2);
  end
  clear Memo;

  if ~bShowParam, fprintf(1,'  %d/%d traces',idata,size(AlldataInt,2)); end
  for iMulti=1:length(MultiQ)

    if (bAfterCrash) && (iMulti<=AfterCrashQ)
      fprintf('Skip "%s" Ausw%s Q=%d\n',filepath,sAuswVer,iMulti);
      continue;
    end

    if bLoad
      if ~bMultiQ
        TimeBin         = TimeBinNew;
        Qo              = 4;
        mu0             = (1:Qo)*1/(Qo+1);
        dwelltime0      = 200*ones(1,Qo);
        mu0var          = 0.01*ones(1,Qo);
        learn_mu        = ones(1,Qo);
        learn_muvar     = ones(1,Qo);
        learn_dwelltime = ones(1,Qo);
      else
        TimeBin         = 5;
        Qo              = MultiQ(iMulti);
        mu0             = (1:Qo)*1/(Qo+1);
        dwelltime0      = 200*ones(1,Qo);
        mu0var          = 0.01*ones(1,Qo);
        learn_mu        = ones(1,Qo);
        learn_muvar     = ones(1,Qo);
        learn_dwelltime = ones(1,Qo);
      end
      Qn=1;
      Q=Qn*Qo;
      obsvect = zeros(Qo,Qn);
      for o=1:Qo, obsvect(o,:) = o; end
      obsvect = reshape(obsvect,1,Q);
      if ~isempty(pmu0), mu0=pmu0; end
      Ocount = size(mu0,1);

      AnzBursts = size(dataInt,2);
      if bShowParam, fprintf(1,'%d traces found.\n',AnzBursts); end
    end

    nex = size(dataInt,2);
    Mcount = 1;          %Number of mixtures
    cov_type = 'diag';  %'full', 'diag' or 'spherical'

    Sigma0=zeros(Ocount,Ocount,Qo,Mcount);
    for q=1:Qo, for i=1:Ocount, Sigma0(i,i,q)=mu0var(:,q); end; end

    transmat0 = zeros(Q);
    if Qo==1
      transmat0=1;
    else
      for oi=1:Qo
        probii = exp(-TimeBin/dwelltime0(oi));
        probij = (1-probii)/(Qo-1);
        transmat0(oi,oi)=probii;
        if Qn>1
          for oj=2:Qo
            col = Qo + (mod(oi-1+oj-1,Qo)+1)*(Qn-1);
            transmat0(oi,col)=probij;
          end
          for n=2:Qn-1
            row = Qo+(oi-1)*(Qn-1)+n-1;
            col = row + 1;
            transmat0(row,col) = 1;
          end
          row = Qo+(oi-1)*(Qn-1)+Qn-1;
          col = oi;
          transmat0(row,col) = 1;
        else
          for oj=2:Qo
            col = mod(oi-1+oj-1,Qo)+1;
            transmat0(oi,col)=probij;
          end
        end
      end
    end
    transmat0 = mk_stochastic(transmat0);
    if bShowParam, transmat0, end

    prior0 = zeros(Q,1);
    for o=1:Qo
      prior0((o-1)*Qn+1)=1/Qo;
    end
    mixmat0 = ones(Q,Mcount);

    prior1 = prior0;
    transmat1 = transmat0;
    mu1 = mu0;
    Sigma1 = Sigma0;
    mixmat1 = mixmat0;
    LL=[];
    trajLL=[];
    iter = 0;
    Memo = [];

    if blearn
      if bShowParam, disp('learn...'); end
      %train HMM on burst
      while (isempty(LL))||(length(LL)==IterUpdate)

        if bUseExtHMM
          [LL, prior1, transmat1, mu1, Sigma1, mixmat1, Memo1] = ...
            m2hmm_em_kn(TimeBin, dataPF, dataPFsigma, prior1, transmat1, obsvect, mu1, Sigma1, mixmat1, ...
            'max_iter', IterUpdate, 'thresh', Threshold, 'verbose', ~bLearnBurstwise, 'cov_type', cov_type, ...
            'adj_mu', any(learn_mu), 'adj_Sigma', any(learn_muvar), ...
            'adj_trans', any(learn_dwelltime));
        else
          [LL, prior1, transmat1, mu1, Sigma1, mixmat1] = ...
            m2hmm_em(TimeBin, dataPF, prior1, transmat1, obsvect, mu1, Sigma1, mixmat1, ...
            'max_iter', IterUpdate, 'thresh', Threshold, 'verbose', ~bLearnBurstwise, 'cov_type', cov_type, ...
            'adj_mu', any(learn_mu), 'adj_Sigma', any(learn_muvar), ...
            'adj_trans', any(learn_dwelltime));
          Memo1.mu1{i}=mu1;
          Memo1.Sigma1{i}=Sigma1;
          Memo1.transmat1=transmat1;
          Memo1.lltrace=LL;
        end
        trajLL=[trajLL, LL];
        iter = iter+length(LL);

        %disp(['total number of iterations: ',int2str(iter)]);
        mu1var=[];
        for i=1:Ocount, [mu1var] = [mu1var(:)'; squeeze(Sigma1(i,i,:))']; end

        if bShowParam
          disp('');
          fprintf(1,'%7.3f',[mu1(1,:)]); fprintf('  ');
          if Mcount==2, fprintf(1,'%6.0f',[mu1(2,:)]); end; fprintf('\n');
          fprintf(1,'%7.3f',[mu1var(1,:)]); fprintf('  ');
          if Mcount==2, fprintf(1,'%6.0f',[mu1var(2,:)]); end; fprintf('\n');
        end
        warning off MATLAB:divideByZero;
        warning off MATLAB:log:logOfZero;
        dtrans=diag(transmat1);
        dwelltime1=[-TimeBin./log(dtrans(1:Qo))]';
        if bShowParam
          fprintf(1,'%7.1f',[dwelltime1(:)]); fprintf('  '); fprintf('\n');
        end

        Memo = [Memo, Memo1];

        warning on MATLAB:divideByZero;
        warning on MATLAB:log:logOfZero;

        if sum(isnan(LL))>0, break; end;
        if IterMax>0
          if iter >= IterMax break; end
        end

        plotLL(trajLL);
        drawnow;

      end
      loglik=trajLL;
    else
      mu1=mu0;
      mu1var=mu0var;
      transmat1=transmat0;
      prior1=prior0;
    end

    if bShowParam
      %if ~blearn, disp([reshape(mu1',1,Q*Ocount);reshape(mu1var',1,Q*Ocount)]); end
      transmat1
      dtrans=diag(transmat1);
      dwelltime1=-TimeBin./log(dtrans(1:Qo))'
    else
      fprintf(1,'  %3d iterations  ',length(trajLL));
      %plotLL((loglik(2:end)));
    end

    HMMMarkBursts;

    %BurstStatistiks;
    if bSave
      save(sprintf('%sAusw%s_%s_m%02d',filepath,sAuswVer,sBurstSel,Q));
    end

  end %iMulti

  if bSave && 0
    save(sprintf('%s$mol%03d',filepath,idata));
    if idata>1, delete(sprintf('%s$mol%03d',filepath,idata)); end
  end

end %idata

if bSimData
  BurstLen=[];
  for iBurst=1:AnzBursts
    %SimStufen{iBurst}.PFclass=SimStufen{iBurst}.PFclass-1;
    BurstLen(iBurst)=length(dataPF{iBurst});
  end
  VerifyStufen(AllStufen, BurstLen, SimStufen)
end

%clf;
%Auswertung_MultiData;
if ~bSimData, clear bSimData; end
if ~exist('bAutomate','var')
  if bLearnBurstwise
    AllStufen=[];
    AnzBursts = size(AlldataInt,2);
    for iBurst=1:AnzBursts
      AllStufen{iBurst}=MultiQData{1}(iBurst).AllStufen;
    end
    StufenArr=StufenHMM2StufenArr(AllStufen,AlldataInt,R0,gamma);
    DoubleArr=StufenFilter(StufenArr,'2+',true);
    ShowPlot2D([DoubleArr(:,1).Efret],[DoubleArr(:,2).Efret],[0 1],[0 1],[100 100],[1 1]*4);
  else
    iBurst = 5;
    HMMShowBurst
    %     iBurst=1;
    %     y1=data(iBurst).FRET;
    %     y2=AlldataPF{iBurst};
    %     clf;
    %     plot(y1(1:length(y2))-y2');
  end
end
