function [MultiQ,LL,ChiSquare,BIC]=CalcChiSquare3(filename);

%load(filename,'MultiQData','dataPF','dataPFsigma','dataInt','MultiQ');
load(filename,'MultiQData','dataPF','MultiQ');
ChiSquare=zeros(1,length(MultiQ))*NaN;
for iMulti=1:length(MultiQData)
  mu1=MultiQData{iMulti}.mu1;
  mu1var=MultiQData{iMulti}.mu1var;
  transmat1=MultiQData{iMulti}.transmat1;
  Q=length(mu1);
  AllStufen=MultiQData{iMulti}.AllStufen;
  AnzStufen=length([AllStufen{:}]);
  AnzBursts=size(dataPF,2);
  ChiSquareBurst=zeros(1,AnzBursts);
  NumBins=zeros(1,AnzBursts);
  NumSteps=zeros(1,AnzBursts);
  NumFreeParam=numel(transmat1)+numel(mu1);
  for iBurst=1:AnzBursts
    StateTraj=zeros(1,length(dataPF{iBurst}));
    MeanTraj=StateTraj;
    VarTraj=MeanTraj;
    AnzStufen=length(AllStufen{iBurst});
    %w=max(0,dataInt{1,iBurst}+dataInt{2,iBurst});
    w=ones(1,length(dataPF{iBurst}));
    for iStufe=1:AnzStufen
      t1=AllStufen{iBurst}(iStufe).Start;
      t2=AllStufen{iBurst}(iStufe).End;
      w(t1:t2)=1/(t2-t1+1);
      StateTraj(t1:t2)=AllStufen{iBurst}(iStufe).PFclass;
      MeanTraj(t1:t2)=AllStufen{iBurst}(iStufe).MW;
      VarTraj(t1:t2)=AllStufen{iBurst}(iStufe).MWstd.^2;
      %MeanTraj(t1:t2)=mean(dataPF{iBurst}(t1:t2));
      %VarTraj(t1:t2)=var(dataPF{iBurst}(t1:t2));
%       MeanTraj(t1:t2)=mu1(StateTraj(t1:t2));
%       VarTraj(t1:t2)=mu1var(StateTraj(t1:t2));
      %MeanTraj(t1:t2)=sum(dataPF{iBurst}(t1:t2).*dataPFsigma{iBurst}(t1:t2))/sum(dataPFsigma{iBurst}(t1:t2));
    end
    ii=find(StateTraj==0);
    if ~isempty(ii)
      fprintf('!!! Q=%d, Burst %d (',iMulti,iBurst);
      for iii=1:length(ii)
        fprintf('%d ',ii(iii));
        if ii(iii)>1
          StateTraj(ii(iii))=StateTraj(ii(iii)-1);
          MeanTraj(ii(iii))=MeanTraj(ii(iii)-1);
          VarTraj(ii(iii))=VarTraj(ii(iii)-1);
        else
          StateTraj(ii(iii))=StateTraj(ii(iii)+1);
          MeanTraj(ii(iii))=MeanTraj(ii(iii)+1);
          VarTraj(ii(iii))=VarTraj(ii(iii)+1);
        end        
      end
      fprintf('\b)\n');
    end
    Yfit=MeanTraj;
    YfitVar=VarTraj;
    %Yfit=mu1(StateTraj);
    %YfitVar=mu1var(StateTraj);
    Ydata=dataPF{iBurst};
    %YdataVar=Yfit.*(1-Yfit)./dataPFsigma{iBurst};
    %YdataVar=MeanTraj.*(1-MeanTraj)./dataPFsigma{iBurst};
    %w=1./YdataVar;
    %w=length(dataPF{iBurst});
    %ChiSquareBurst(iBurst)=sum(max(0,((Ydata-Yfit).^2)-YdataVar+0*YfitVar));
    %ChiSquareBurst(iBurst)=sum(max(0,((Ydata-Yfit).^2)./(YdataVar+YfitVar)));
    %ChiSquareBurst(iBurst)=sum(max(0,((Ydata-Yfit).^2-YdataVar)./max(1,dataInt{1,iBurst}+dataInt{2,iBurst})));
    ChiSquareBurst(iBurst)=sum(((Ydata-Yfit).^2).*w);
    ChiSquareBurstVar(iBurst)=sum(YfitVar.*w);
    %MeanVar(iBurst)=sum(w.*YdataVar);
    %NumBins(iBurst)=length(dataPF{iBurst});
    %NumBins(iBurst)=sum(1./ChiSquareBurst(iBurst));
    NumBins(iBurst)=numel(w);
    NumSteps(iBurst)=sum(w);
  end
  %ChiSquare(iMulti)=max(eps,sum(ChiSquareBurst)/(sum(NumBins)));
  ChiSquare(iMulti)=max(eps,sum(ChiSquareBurst)/(sum(ChiSquareBurstVar)));
  BIC(iMulti)=sum(NumSteps)*log(sum(ChiSquareBurst)/sum(NumSteps))+NumFreeParam*log(sum(NumBins));
  %BIC(iMulti)=sum(MeanVar)/sum(NumBins);
  LL(iMulti)=MultiQData{iMulti}.trajLL(end);
end
