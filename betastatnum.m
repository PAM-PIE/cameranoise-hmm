function [M,MS1,MS2]=betastatnum(A,B,c,alpha);
%[M,MS1,MS2]=betastatnum(A,B,c);
%M: Mittelwert
%MS1: Mittelwert-Sigma
%MS2: Mittelwert+Sigma
%A, B: Parameter der Beta-Verteilung, z.B. A=Acceptor, B=Donor
%c: Prozent des Integrals der PDF
%alpha: Verbreiterung der Verteilung (norm: 1)

aufl=1000;
x=0:1/aufl:1; x=x(2:end-1);
y=x;
M=zeros(length(A),1);
MS1=zeros(length(A),1);
MS2=zeros(length(A),1);
for i=1:length(A)
    if (A(i)>0)&(B(i)>0)
        y=1./(Beta(A(i),B(i))).*x.^(A(i)-1).*(1-x).^(B(i)-1);
        y=y.^(alpha);
        y=cumsum(y);
        y=y./y(end);
        M(i)=x(find(y>=0.5,1));
        MS1(i)=x(find(y>=0.5-c/2,1));
        MS2(i)=x(find(y>=0.5+c/2,1));
    else
        M(i)=0.5;
        MS1(i)=0;
        MS2(i)=1;
    end
end
