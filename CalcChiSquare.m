function [MultiQ,ChiSquare,ChiSquareRed]=CalcChiSquare(filename);

load(filename,'MultiQData','dataPF','dataPFsigma','MultiQ');
ChiSquare=zeros(1,length(MultiQ))*NaN;
for iMulti=1:length(MultiQData)
  mu1=MultiQData{iMulti}.mu1;
  mu1var=MultiQData{iMulti}.mu1var;
  transmat1=MultiQData{iMulti}.transmat1;
  Q=length(mu1);
  AllStufen=MultiQData{iMulti}.AllStufen;
  AnzStufen=length([AllStufen{:}]);
  AnzBursts=size(dataPF,2);
  ChiSquareBurst=zeros(1,AnzBursts);
  NumBins=zeros(1,AnzBursts);
  NumFreeParam=numel(transmat1)+numel(mu1);
  for iBurst=1:AnzBursts
    StateTraj=1+zeros(1,length(dataPF{1,iBurst}));
    AnzStufen=length(AllStufen{iBurst});
    for iStufe=1:AnzStufen
      t1=AllStufen{iBurst}(iStufe).Start;
      t2=AllStufen{iBurst}(iStufe).End;
      StateTraj(t1:t2)=AllStufen{iBurst}(iStufe).PFclass;
    end
    Yfit=mu1(StateTraj);
    YfitVar=mu1var(StateTraj);
    Ydata=dataPF{iBurst};
    YdataVar=Yfit.*(1-Yfit)./dataPFsigma{iBurst};
    ChiSquareBurst(iBurst)=sum((Ydata-Yfit).^2);
    ChiSquareBurstVar(iBurst)=sum(YdataVar+YfitVar);
    NumBins(iBurst)=length(dataPF{iBurst});
  end
  ChiSquare(iMulti)=sum(ChiSquareBurst)/(sum(NumBins));
  ChiSquareRed(iMulti)=sum(ChiSquareBurst)/(sum(ChiSquareBurstVar));
end
