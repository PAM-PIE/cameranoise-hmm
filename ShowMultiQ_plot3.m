%Transition plot
%ShowMultiQ_loaddata;
bShowRawData=false; %plot1
bShowHMMData=true; %plot2
%diff=2;

clf;
xhist=0:0.04:1;
HMMpos=nchoosek(mu1,2);
AnzBursts=length(dataInt);

if bShowRawData
  subplot(bShowRawData+bShowHMMData,1,1); hold on; cla;
  if 0
    %alt
    data2DMain=zeros(length(xhist));
    for iBurst=1:AnzBursts
      tempPF=dataPF{iBurst};
      dataArr=[];
      dataArr(1,:)=tempPF(1:end-1);
      dataArr(2,:)=tempPF(2:end);
      data2D=hist3(dataArr',{xhist;xhist})';
      data2DMain=data2DMain+data2D;
    end
    imagesc(xhist,xhist,data2DMain);
    set(gca,'YDir','normal');
  else
    %neu
    for iBurst=1:AnzBursts
      tempPF=[tempPF,dataPF{iBurst}(1:end-1)];
    end
    ShowPlot2D([DoubleArrFast(1).MW],[DoubleArrFast(2).MW],[0 1],[0 1],[101 101],[1 1]*2);
  end    
%   plot(HMMpos(:,1),HMMpos(:,2),'+w');
%   plot(HMMpos(:,2),HMMpos(:,1),'+w');
%   plot(mu1,mu1,'ow');
  ylabel('data');
  set(gca,'DataAspectRatio',[1 1 1]);
  axis tight;
end

if bShowHMMData
  subplot(bShowRawData+bShowHMMData,1,bShowRawData+1); hold on; cla;
  if 0
    %alt
    data2DMain=zeros(length(xhist));
    for iBurst=1:AnzBursts
      tempPF=[AllStufen{iBurst}.MW];
      dataArr=[];
      dataArr(1,:)=tempPF(1:end-diff);
      dataArr(2,:)=tempPF(1+diff:end);
      data2D=hist3(dataArr',{xhist;xhist})';
      data2DMain=data2DMain+data2D;
    end
    imagesc(xhist,xhist,data2DMain);
    set(gca,'YDir','normal');
  else
    %neu
    StufenArrFast=StufenHMM2StufenArrFast(AllStufen);
    StufenArrFast=StufenArrExpand(StufenArrFast,[],[]);
    if diff==1
      DoubleArrFast=StufenFilterFast(StufenArrFast,'2+',true);
      data2DMain=ShowPlot2D([DoubleArrFast(1).MW],[DoubleArrFast(2).MW],[0 1],[0 1],[1 1]*301,[1 1]*4);
      %data2DMain=ShowPlot2D([DoubleArrFast(1).MW],[DoubleArrFast(2).MW],[0 1],[0 1],[1 1]*101,[1 1]*2);
    end 
    if diff==2
      TripleArrFast=StufenFilterFast(StufenArrFast,'3+',true);
      data2DMain=ShowPlot2D([TripleArrFast(1).MW],[TripleArrFast(3).MW],[0 1],[0 1],[1 1]*301,[1 1]*4);
      %data2DMain=ShowPlot2D([TripleArrFast(1).MW],[TripleArrFast(3).MW],[0 1],[0 1],[1 1]*101,[1 1]*2);
    end
  end
  plot(HMMpos(:,1),HMMpos(:,2),'+w','LineWidth',3,'MarkerSize',12);
  plot(HMMpos(:,2),HMMpos(:,1),'+w','LineWidth',3,'MarkerSize',12);
  plot(mu1,mu1,'ow','LineWidth',3,'MarkerSize',12);
  ylabel(sprintf('HMM delta=%d Q=%d',diff,Q));
  set(gca,'DataAspectRatio',[1 1 1]);
  axis tight;
  set(gca,'XTick',[0:0.2:1]);
  set(gca,'YTick',[0:0.2:1]);
  set(gca,'TickDir','out');
  %set(gca,'TickLength',[.05 .025]);
  %set(gca,'TightInset',[0.2 0 0.01 0]);
  set(gca,'FontSize',22);
end

subplot(bShowRawData+bShowHMMData,1,1);
title(sprintf('%s %s (%s V%s)',sSample{iSample},sState{iState},sBurstSel,sAuswVer));
load('Colormap_plot3','cmap');
colormap(cmap);

set(gcf,'InvertHardcopy','off');
set(gcf,'PaperPositionMode','auto');
if ~bShowRawData & bShowHMMData
  filepath_='D:\Schluesche\Auswertung\';
  filename_=sprintf('Ausw%s_%s %s_%s Trans2D_diff%d_Q%d',...
    sAuswVer,sBurstSel,sSample{iSample},sState{iState},diff,Q);
  saveas(gcf,[filepath_,filename_],'png');
  dlmwrite([filepath_,filename_,'a.txt'], data2DMain, 'delimiter', '\t')
  %temp=[HMMpos;mu1',mu1'];
  dlmwrite([filepath_,filename_,'b.txt'], [HMMpos;circshift(HMMpos,[0 1]);mu1',mu1'], 'delimiter', '\t')
  %save([filepath_,filename_,'b.txt'],'temp','-ascii');
end
if bShowRawData & ~bShowHMMData
  filepath_='D:\Schluesche\Auswertung\';
  filename_=sprintf('Ausw%s_%s %s_%s Trans2D',...
    sAuswVer,sBurstSel,sSample{iSample},sState{iState});
  saveas(gcf,[filepath_,filename_],'png');
  save([filepath_,filename_],'data2DMain','-ascii');
end
