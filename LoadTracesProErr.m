function [dataerr]=LoadTracesProErr(filename)

temp = dlmread(filename,'\t');

dataerr={};
molCount=div(size(temp,2)-1,4);
for iMol=1:molCount
  dataerr(iMol).xcoordAcc = temp(1,1+(iMol-1)*4+1);
  dataerr(iMol).ycoordAcc = temp(1,1+(iMol-1)*4+2);
  dataerr(iMol).xcoordDonor = temp(1,1+(iMol-1)*4+3);
  dataerr(iMol).ycoordDonor = temp(1,1+(iMol-1)*4+4);
  dataerr(iMol).Acc = temp(2:end,1+(iMol-1)*4+1);
  dataerr(iMol).AccErr = temp(2:end,1+(iMol-1)*4+2);
  dataerr(iMol).Donor = temp(2:end,1+(iMol-1)*4+3);
  dataerr(iMol).DonorErr = temp(2:end,1+(iMol-1)*4+4);
end
