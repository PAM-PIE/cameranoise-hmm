%Show variance of the data
sSample={'DNA3','DNA12','DNA18','DNA19'};
sState={'Dynamic','Steady'};

sAuswVer='8';
sBurstSel='All'; %'Burst50';
iState=1;
Anz=1;

for iSample=1:4
  subplot(4,1,iSample);
  filepath=['D:\Schluesche\Maindata\',sSample{iSample},'\',sState{iState},'FRET\'];
  load([filepath,'Ausw',sAuswVer,'_',sBurstSel,'_m0',int2str(Anz),'.mat'],'dataPFsigma');
  s=[]; 
  for i=1:length(dataPFsigma)
    s(i)=mean(dataPFsigma{i}); 
  end
  hist(s,0:0.01:0.4);
  xlabel('\sigma_d_a_t_a');
  ylabel([sSample{iSample},' ',sState{iState}]);
  title(['\sigma_m_e_a_n=',sprintf('%.4f',mean(s))]);
  axis tight;
end
