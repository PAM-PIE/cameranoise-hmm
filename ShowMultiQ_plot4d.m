% State Duration Histogram
sSample={'DNA3','DNA12','DNA18','DNA19'};
sState={'Dynamic','Steady'};

% title('loading...'); drawnow;
clf;
bsave=false;
% xhist=0:0.02:1;
% yhist=1:1:55;
% TimeBin=5;
%
% filepath_=['D:\Schluesche\Maindata\',sSample{iSample},'\',sState{iState},'FRET\'];
% filename1=['Ausw',sAuswVer,'_',sBurstSel,'_m0',int2str(Anz),'.mat'];
% %load([filepath_,filename1],'MultiQData','StufenArr','mu1','dwelltime1','TimeBin');
% load([filepath_,filename1],'StufenArr','mu1','dwelltime1','TimeBin');

MultiQData=MetaQData{iSample,iState};

filepath=['D:\Schluesche\Maindata\',sSample{iSample},'\',sState{iState},'FRET\'];
%load([filepath,'Ausw',sAuswVer,'_',sBurstSel,'_m0',int2str(Anz),'.mat'],'StufenArr','TimeBin');
load([filepath,'Ausw',sAuswVer,'_',sBurstSel,'_m0',int2str(Anz),'.mat'],'TimeBin');
%  load([filepath,'Ausw',sAuswVer,'_',sBurstSel,'_m0',int2str(Anz),'.mat']);
%   HMMMarkBursts;

OrigExp=[];
durData=NaN*zeros(Anz);
durHMM=NaN*zeros(Anz);

subplot(2,1,1); cla; hold on; box on;
iAnz=1:Anz;
cmap=colormap;

set(gca,'ClimMode','manual');
set(gca,'clim',[0 1]);

for Q=iAnz
  %y(q)=sum(MultiQData{q}.mu1var(:));
  %y=NaN*zeros(Q,1);

  StufenArr=StufenHMM2StufenArrFast(MultiQData{Q}.AllStufen);
  mu1=MultiQData{Q}.mu1;
  mu1var=MultiQData{Q}.mu1var;
  %Q=length(mu1);
  mu0=MultiQData{Q}.mu0;
  %mu0=(1:Q)*1/(Q+1)+1/(2*(Q+1));
  StepCount=MultiQData{Q}.StepCount;
  dwelltime1=MultiQData{Q}.dwelltime1;

  for q=1:Q
    idx=find([StufenArr.PFclass]==q);
    data=(StufenArr.End(idx)-StufenArr.Start(idx)+1)*TimeBin;
    durData(Q,q)=sum(data);
    durHMM(Q,q)=dwelltime1(q).*StepCount(q);
    durHMM(find(isinf(durHMM)))=NaN;
  end
  y1=durData(Q,:);
  y=[0,cumsum(y1)];
  
  y1=y1/max(y);
  y=y/max(y);
  for q=1:Q
    if ~isnan(y1(q))
      imap=round(interp1([0 1],[1 length(cmap)],mu1(q)));
      rectangle('Position',[Q-.4,y(q),.8,y1(q)],...
        'FaceColor',cmap(imap,:),'EdgeColor','none');
      text(Q,y(q)+y1(q)/2,sprintf('%.2f',mu1(q)),'HorizontalAlignment','center');
    end
  end
  
  if Q==1
    fprintf('%s %s (%s V%s)\n',sSample{iSample},sState{iState},sBurstSel,sAuswVer);
    t1=MultiQData{2}.transmat1; 
    %r=expm(t1)/(TimeBin*1e-3);  falsch!!!
    r=1./(TimeBin*1e-3).*logm(t1);
    r1(Q)=r(1,2);
    r2(Q)=r(2,1);
    fprintf('***  L->H: %5.2f 1/s\tH->L: %5.2f 1/s\n',r1(Q),r2(Q));
  else
    obsvect=zeros(1,Q);
    for q=1:Q      
      [w,i]=min(abs(mu1(q)-MetaQData{iSample,1}{2}.mu1));
      obsvect(q)=i;
    end
    StufenArrNew=StufenArr;
    StufenArrNew.PFclass=obsvect(StufenArrNew.PFclass);
    StufenArrNew=ShrinkStufen(StufenArrNew);
    StufenArrNew=StufenArrExpand(StufenArrNew,[],[]);
    DoubleArr=StufenFilterFast(StufenArrNew,'2+',false);
    idx1=find(DoubleArr(1).PFclass==1);
    idx2=find(DoubleArr(1).PFclass==2);
    r1(Q)=length(idx1)/sum(DoubleArr(1).Length(idx1)*TimeBin*1e-3);
    r2(Q)=length(idx2)/sum(DoubleArr(1).Length(idx2)*TimeBin*1e-3);
    fprintf('Q=%d, L->H: %5.2f 1/s    H->L: %5.2f 1/s    #1: %4d, #2: %4d\n',Q,r1(Q),r2(Q),length(idx1),length(idx2));
    
    rectangle('Position',[Q-.4,0,.8,sum(y1(find(obsvect==1)))],...
      'FaceColor','none','EdgeColor','k');
  end
end
fprintf('\n');
xlim([0 Anz]+.5);
%ylim([0 600]);
xlabel('#states');
ylabel('dwelltime/ms');
title(sprintf('%s %s (%s V%s)',sSample{iSample},sState{iState},sBurstSel,sAuswVer));
ax=colorbar('location','SouthOutside');
set(ax,'XTick',(0:0.2:1)*size(cmap,1));
set(ax,'XTickLabel',(0:0.2:1));

subplot(2,1,2);
% xhist=interp1([0 20],[1 3.5],0:1:20);
% yhist1=hist(log2(r1(2:end)),xhist);
% yhist2=hist(log2(r2(2:end)),xhist);
% stairs(2.^xhist,yhist1);
% hold on
% stairs(2.^xhist,yhist2,'r');
hold off
plot(r1(2:end),2:7,'b*');
hold on
plot(r2(2:end),2:7,'r*');

set(gca,'XScale','log');
set(gca,'XTick',2:10);
xlim([2 10]);
ylim([1.5 7.5]);

OrigExp=[];
OrigExp(:,1)=1:7;
OrigExp(:,2)=r1;
OrigExp(:,3)=r2;

if bsave
  set(gcf,'InvertHardcopy','off');
  set(gcf,'PaperPositionMode','auto');
  filepath_='D:\Schluesche\Auswertung\';
  filename_=sprintf('Ausw%s_%s %s_%s rates_Q%d',...
    sAuswVer,sBurstSel,sSample{iSample},sState{iState},Q);
  saveas(gcf,[filepath_,filename_],'png');
  
  fid=fopen([filepath_,filename_,'.txt'],'w');
  fprintf(fid,'#states\trLH\trHL\n');
  fclose(fid);
  dlmwrite([filepath_,filename_,'.txt'],OrigExp,'-append','delimiter','\t');
end
