clear variables;
sSample={'DNA3','DNA12','DNA18','DNA19','Donor_Only','Acceptor_Only','Sim1'};
sState={'Dynamic','Steady',''};

bSimData=true;
HMMVersion;

%iBurstArr=[1,5; 1,13; 2,20; 2,30; 3,99; 3,119; 4,126; 4,20]; %All
%iBurstArr=[1,5; 1,13; 2,20; 2,30; 3,9; 3,19; 4,26; 4,20]; %Burst50
iBurstArr=[7,1];
iState=3;
sBurstSel='Sim1';

cMinLL=0;
clf; set(gcf,'WindowStyle','docked');

for iLoop=1:size(iBurstArr,1)
  iSample=iBurstArr(iLoop,1);
  iBurst=iBurstArr(iLoop,2);

  %filepath=['D:\Schluesche\Maindata\',sSample{iSample},'\',sState{iState},'FRET\'];
  filepath=['D:\Schluesche\Maindata\',sSample{iSample},'\'];
  load([filepath,'Ausw',sAuswVer,'_',sBurstSel,'_m0',int2str(1),'.mat'],...
    'dataPF', 'dataPFsigma', 'dataInt','bUseBetaFunc','TimeBin');

  for q=2:6
    filepath=['D:\Schluesche\Maindata\',sSample{iSample},'\'];
    load([filepath,'Ausw',sAuswVer,'_',sBurstSel,'_m0',int2str(q),'.mat'],...
      'Q', 'AllStufen', 'dataLL', 'mu1', 'Sigma1','SimParam','SimStufen');
    HMMShowBurst;
    xlabel(sprintf('%s %s (%s V%s Q%d)',sSample{iSample},sState{iState},sBurstSel,sAuswVer,q));
    if 1
      set(gcf,'InvertHardcopy','off');
      set(gcf,'PaperPositionMode','auto');
      saveas(gcf,['D:\Schluesche\Auswertung\',...
        sprintf('Ausw%s_%s %s_%s Q%d Burst%d',...
        sAuswVer,sBurstSel,sSample{iSample},sState{iState},Q,iBurst)],'png');
    end
  end

end