HiddenStatesCount=6;
% Generiert Stufenhistogramm
filepath='D:\Nawid\Schluesche\2008-01-17\TBPDNA3_200610\';
colarr={};
colarr{1}=[0 0 1];
colarr{2}=[0 .5 0];
colarr{3}=[1 0 0];
cla;
hold off;
for iMovie=1:3
    load('-mat', sprintf('%sMov%02d_gesamt%02d.mat',filepath,iMovie,HiddenStatesCount));

    fprintf('#%i\t',iMovie); 
    StufenArr=StufenHMM2StufenArrPF(AllStufen,dataInt,5,1);
    
    fprintf('\t%5.2f',mu1); fprintf('\n');
    fprintf('\t%5.2f',mu1var); fprintf('\n');
    fprintf('\t%5.0f',dwelltime1); fprintf('\n');
    
    plot(mu1,dwelltime1,'o','color',colarr{iMovie});
    hold on;
end
fprintf('\n');
