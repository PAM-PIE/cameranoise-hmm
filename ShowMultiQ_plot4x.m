% State Duration Histogram
sSample={'DNA3','DNA12','DNA18','DNA19'};
sState={'Dynamic','Steady'};

% title('loading...'); drawnow;
clf;
% xhist=0:0.02:1;
% yhist=1:1:55;
% TimeBin=5;
%
% filepath_=['D:\Schluesche\Maindata\',sSample{iSample},'\',sState{iState},'FRET\'];
% filename1=['Ausw',sAuswVer,'_',sBurstSel,'_m0',int2str(Anz),'.mat'];
% %load([filepath_,filename1],'MultiQData','StufenArr','mu1','dwelltime1','TimeBin');
% load([filepath_,filename1],'StufenArr','mu1','dwelltime1','TimeBin');

MultiQData=MetaQData{iSample,iState};

mu1=MultiQData{Anz}.mu1;
mu1var=MultiQData{Anz}.mu1var;
Q=length(mu1);
mu0=MultiQData{Anz}.mu0;
%mu0=(1:Q)*1/(Q+1)+1/(2*(Q+1));
StepCount=MultiQData{Anz}.StepCount;
dwelltime1=MultiQData{Anz}.dwelltime1;

%filepath=['D:\Schluesche\Maindata\',sSample{iSample},'\',sState{iState},'FRET\'];
%load([filepath,sprintf('Ausw%s_%s_m%02d.mat',sAuswVer,sBurstSel,Q)],'StufenArr','TimeBin');
%   load([filepath,'Ausw',sAuswVer,'_',sBurstSel,'_m0',int2str(Anz),'.mat']);
%   HMMMarkBursts;

if 0
  filepath=['D:\Schluesche\Maindata\'];
  filename1=['Ausw',sAuswVer,'_',sBurstSel,'_MetaData.mat'];
  if exist([filepath,filename1])
    load([filepath,filename1]);
  else
    filepath=['D:\Schluesche\Maindata\',sSample{iSample},'\',sState{iState},'FRET\'];
    filename1=[sprintf('Ausw%s_%s_m%02d.mat',sAuswVer,sBurstSel,Anz)];
    if ~exist('filename1_') | ~strcmp(filename1_,[filepath,filename1])
      load([filepath,filename1],'MultiQData','StufenArr','mu1','dwelltime1');
      filename1_=[filepath,filename1];
    end
  end

  filepath=['D:\Schluesche\Maindata\'];
  filename2=['Ausw',sAuswVer,'_',sBurstSel,'_MetaStufen.mat'];
  if exist([filepath,filename2])
    load([filepath,filename2]);
  else
    filename2=['Ausw',sAuswVer,'_',sBurstSel,'_MetaStufen.mat'];
    if ~exist('filename2_') | ~strcmp(filename2_,[filepath,filename2])
      load([filepath,filename2]);
      filename2_=[filepath,filename2];
    end
  end
end

if 0

  for Q=1:Anz
    subplot(8,1,Anz-Q+1); cla; hold on
    filename=sprintf('Ausw%s_%s_m02d.mat',sAuswVer,sBurstSel,Q);
    %load([filepath,filename],'StufenArr','mu1','dwelltime1');
    StufenArr=MetaStufenArr{Q};
    mu1=MultiQData{Q}.mu1;
    dwelltime1=MultiQData{Q}.dwelltime1;
    data2Dhist=hist3([[StufenArr.ProxFact]' log(TimeBin*[StufenArr.Length]')],{xhist,yhist})';
    imagesc(xhist,log(yhist),data2Dhist);
    %axis([0 1 0 120]);
    axis tight;
    plot(mu1,log(dwelltime1),'+w');
    ylabel(int2str(Q));
    %drawnow;
  end
  subplot(Q,1,Q);
  xlabel('\mu');
  subplot(Q,1,1);
  title(sprintf('%s %s (V%s)',sSample{iSample},sState{iState},sAuswVer));

else

  xbin=TimeBin*5;
  x=0:xbin:1500;

  OrigExp=[];
  tauHMM=NaN*zeros(1,Q);
  tauMean=NaN*zeros(1,Q);
  for q=1:Q
    subplot(Q,1,Q-q+1);
    idx=find([StufenArr.PFclass]==q);
    if q==Q, idx=find(([StufenArr.PFclass]==3)|([StufenArr.PFclass]==4)); end
    y=([StufenArr(idx).Length]+1)*TimeBin;
    tauHMM(q)=dwelltime1(q);
    yfitHMM=numel(y)*xbin/tauHMM(q)*exp(-x/tauHMM(q));
    if numel(y)>0
      tauMean(q)=mean(y);
      yfitMean=numel(y)*xbin/tauMean(q)*exp(-x/tauMean(q));
    end
    if 1
      x_=[x,x(end)+xbin];
      y_=hist(y,x_);
      y_(end)=[];
      semilogy(x,y_,'.');
      hold on
      if numel(y)>0, plot(x,yfitMean,':b'); end
      plot(x,yfitHMM,'r','LineWidth',2);
      if numel(y)>1, ylim([1 numel(y)]); end
      %ylabel(int2str(q));
      ylabel(sprintf('�=%.02f',mu1(q)));
      text(60,numel(y)*.4,['\tau_H_M_M=',sprintf('%.1f ms',dwelltime1(q))],'color','r');
      if numel(y)>0
        text(x(end)-60,numel(y)*.4,['<\tau>=',sprintf('%.1f ms (#%d)',mean(y),numel(y))],...
          'HorizontalAlignment','right','color','b');
      end
      %text(80,numel(y)*.2,['# ',sprintf('%d',numel(y))]);
      xlim([x(1) x(end)]);
      OrigExp(:,4*(q-1)+1)=x;
      OrigExp(:,4*(q-1)+2)=y_;
      OrigExp(:,4*(q-1)+3)=yfitHMM;
      OrigExp(:,4*(q-1)+4)=yfitMean;
    else
      %plot((1:length(y))/length(y),sort(y));
      cdfplot(y);
      stairs(sort(y),(1:length(y))/length(y));
      axis([1 max(y) 0 1]);
    end
  end
  subplot(Q,1,Q);
  xlabel('\tau/ms');
  subplot(Q,1,1);
  title(sprintf('%s %s (%s V%s)',sSample{iSample},sState{iState},sBurstSel,sAuswVer));

  if 0
    set(gcf,'InvertHardcopy','off');
    set(gcf,'PaperPositionMode','auto');
    filepath_='D:\Schluesche\Auswertung\';
    filename_=sprintf('Ausw%s_%s %s_%s Dwelltime_Q%d',...
      sAuswVer,sBurstSel,sSample{iSample},sState{iState},Q);
    saveas(gcf,[filepath_,filename_],'png');
    fid=fopen([filepath_,filename_,'.txt'],'w');
    fprintf(fid,'mu\t%f\t\t\t',mu1); fprintf(fid,'\n');
    fprintf(fid,'tau\t%f\t\t\t',dwelltime1); fprintf(fid,'\n');
    fprintf(fid,'x\ty\tyHMM\tymean\t'); fprintf(fid,'\n');
    fclose(fid);
    dlmwrite([filepath_,filename_,'.txt'],OrigExp,'-append','delimiter','\t');
  end
end
