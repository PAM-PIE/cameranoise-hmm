
for ill=1:length(Memo)
  if bUseBetaFunc
    %B = beta_prob(dataPF{iBurst}, dataInt{iBurst}, mu1, Sigma1, mixmat1);
    B = beta_prob(dataInt{1,iBurst}, dataInt{2,iBurst}, IntNoise{1,iBurst}, IntNoise{2,iBurst}, Memo(ill).mu1, Memo(ill).Sigma1, mixmat1);
  else
    B = mixgauss_prob(dataPF{iBurst}, Memo(ill).mu1, Memo(ill).Sigma1);
  end
  Bobs = ExpandB(B,obsvect);
  path = viterbi_path(prior1, Memo(ill).transmat1, Bobs);
  llpath = prob_path(prior1, Memo(ill).transmat1, Bobs, path);
  LL = sum(llpath)/length(llpath);
  %   Stufen = Path2StufenPF(path, 1:Q, 1, dataPF{iBurst}, dataInt{iBurst}, llpath, cMinLL);  %Nur Stufen 1..Q-1 auswählen, min. Stufenlänge
  %   Stufen_ = Path2StufenPF(obsvect(path), 1:Qo, 1, dataPF{iBurst}, dataInt{iBurst}, llpath, cMinLL);  %Nur Stufen 1..Q-1 auswählen, min. Stufenlänge
  Stufen = Path2Stufen(path,1:Q,cMinLen,dataInt{1,iBurst},dataInt{2,iBurst},[],...
    IntNoise{1,iBurst},IntNoise{2,iBurst},[],llpath,-Inf,cMinInt1,cMinInt2,cMinInt3);
  Stufen_ = Path2Stufen(obsvect(path),1:Q,cMinLen,dataInt{1,iBurst},dataInt{2,iBurst},[],...
    IntNoise{1,iBurst},IntNoise{2,iBurst},[],llpath,-Inf,cMinInt1,cMinInt2,cMinInt3);

  tempmu1var=[];
  for i=1:Ocount, [tempmu1var] = [tempmu1var(:)'; squeeze(Memo(ill).Sigma1(i,i,:))']; end

  ShowBurstWithStufen(...
    dataInt{1,iBurst}, dataInt{2,iBurst}, [], IntNoise{1,iBurst}, IntNoise{2,iBurst}, [], ...
    iBurst, 0, TimeBin, cMinLL, 1:Q, Stufen, llpath, Memo(ill).mu1, tempmu1var, ...
    [], [], bUseBetaFunc);
  title(sprintf('%d  %.4f',ill,LL));
  drawnow;
  
end
