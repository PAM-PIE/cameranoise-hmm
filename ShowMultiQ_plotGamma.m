%ShowMultiQ_plotGamma
sSample={'DNA3','DNA12','DNA18','DNA19','Sim1','Sim2'};
sState={'Dynamic','Steady'};
clf; 

for iState=1:2
  if iSample<=4
    filepath=['D:\Schluesche\Maindata\',sSample{iSample},'\',sState{iState},'FRET\'];
  else
    filepath=['D:\Schluesche\Maindata\',sSample{iSample},'\'];
  end
  filename=['EkorrV',sAuswVer,'.mat'];
  load([filepath,filename]);

  subplot(2,1,iState);
  ShowPlot2D(log2(BurstGamma),500*BurstGammaW,[-2 2],[0 1],[300,300],[1 1]*8);
  hold on;
  gammamean=mean(BurstGamma);
  plot(log2(mean(gammamean))*[1 1],ylim,'w-.','LineWidth',2);
  sDummy=sSample{iSample};
  if iSample<=4, sDummy=[sDummy,' ',sState{iState}]; end
  title(sprintf('%s (V%s)\n<\\gamma>=%.2f',sDummy,sAuswVer,gammamean));
  set(gca,'XTick',-2:1:2);
  set(gca,'XTickLabels',2.^[-2:1:2]);
  xlabel('\gamma');
  ylabel('max(corrcoef)');
  if iSample>4, break; end
end

set(gcf,'InvertHardcopy','off');
set(gcf,'PaperPositionMode','auto');
filepath_='D:\Schluesche\Auswertung\';
filename_=sprintf('Ausw%s_%s %s GammaCorr',...
  sAuswVer,sBurstSel,sSample{iSample});
saveas(gcf,[filepath_,filename_],'png');
