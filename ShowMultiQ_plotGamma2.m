% Show Ekorr verteilung
sSample={'DNA3','DNA12','DNA18','DNA19','Sim1','Sim2'};
sState={'Dynamic','Steady'};

iState=1;
OrigExp=[];
MaxAnzBurst=Inf;

for iSample=1:6

  filepath=['D:\Schluesche\MainData\',sSample{iSample},'\',sState{iState},'FRET\'];
  if strcmp(sSample{iSample},'Sim1')
    filepath='D:\Schluesche\Maindata\Sim1\';
    filename='Sim_*.txt';
  end
  if strcmp(sSample{iSample},'Sim2')
    filepath='D:\Schluesche\Maindata\Sim2\';
    filename='Sim_*.txt';
  end

  load([filepath,'EkorrV',sAuswVer,'.mat']);
  HMMLoadData;

  subplot(6,1,iSample);
  xhist=interp1([0 1],log2([0.25 4]),0:1/50:1);
  yhist=hist(log2(Ekorr(BurstID)),xhist);
  yhist=yhist/sum(yhist);
  stairs(2.^xhist,yhist);
  set(gca,'XScale','log');
  set(gca,'XTick',2.^(-2:1:2));
  %set(gca,'XTickLabels',2.^[-2:1:2]);
  xlim([0.25 4]);
  title(sprintf('%s %s (%s V%s)',sSample{iSample},sState{iState},sBurstSel,sAuswVer));

  OrigExp(:,1)=2.^xhist;
  OrigExp(:,2)=yhist;

  filepath_='D:\Schluesche\Auswertung\';
  filename_=sprintf('Ausw%s_%s %s_%s GammaCorr',...
    sAuswVer,sBurstSel,sSample{iSample},sState{iState});
  %   set(gcf,'InvertHardcopy','off');
  %   set(gcf,'PaperPositionMode','auto');
  %   saveas(gcf,[filepath_,filename_],'png');
  fid=fopen([filepath_,filename_,'.txt'],'w');
  fprintf(fid,'%s\t%f',sSample{iSample},mean(Ekorr(BurstID)));
  fprintf(fid,'\n');
  fclose(fid);
  dlmwrite([filepath_,filename_,'.txt'],OrigExp,'-append','delimiter','\t');

end
