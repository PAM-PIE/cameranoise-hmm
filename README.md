# CameraNoise-HMM

Code for the Hidden Markov Model (HMM) analysis of single-molecule FRET time traces with correction for the noise characteristics of the detector.

See Zarrabi et al. (Add reference here)