%HMMMultiAusw_CalcChiRed2
sSample={'DNA3','DNA12','DNA18','DNA19'};
sState={'Dynamic','Steady'};

bsave=true;

Anz=7;
filepath1=['D:\Schluesche\Maindata\',sSample{iSample},'\',sState{1},'FRET\'];
filepath2=['D:\Schluesche\Maindata\',sSample{iSample},'\',sState{2},'FRET\'];
filename=sprintf('Ausw%s_%s_m%02d.mat',sAuswVer,sBurstSel,Anz);
[MultiQ,ChiSquare1,BIC1]=CalcChiSquare2([filepath1,filename]);
[MultiQ,ChiSquare2,BIC2]=CalcChiSquare2([filepath2,filename]);
OrigHeader={'MultiQ','ChiSquare1','ChiSquare2','BIC1','BIC2'};
OrigExport=[MultiQ',ChiSquare1',ChiSquare2',BIC1',BIC2'];

subplot(2,1,1); cla; box on;
hold off
%plot(MultiQ,1+0*MultiQ,'color',[1 1 1]*.7);
hold on
plot(MultiQ,ChiSquare1,'.-','DisplayName',sState{1});
plot(MultiQ,ChiSquare2,'.-','color',[0 .5 0],'DisplayName',sState{2});
% plot(MultiQ,BIC1,':');
% plot(MultiQ,BIC2,':','color',[0 .5 0]);
xlabel('number of states');
ylabel('\langle\chi�\rangle');
%ylabel('\Sigma\chi�');
title(sprintf('%s (%s V%s)',sSample{iSample},sBurstSel,sAuswVer));
%ylim([0 2]);
xlim([MultiQ(1) MultiQ(end)]);

subplot(2,1,2); cla; box on;
hold off
%plot(MultiQ,1+0*MultiQ,'color',[1 1 1]*.7);
hold on
plot(MultiQ,BIC1,'.-','DisplayName',sState{1});
plot(MultiQ,BIC2,'.-','color',[0 .5 0],'DisplayName',sState{2});
[wex,iex]=min(BIC1);
plot(MultiQ(iex),BIC1(iex),'or');
[wex,iex]=min(BIC2);
plot(MultiQ(iex),BIC2(iex),'or');
xlabel('number of states');
ylabel('BIC');
title(sprintf('%s (%s V%s)',sSample{iSample},sBurstSel,sAuswVer));
xlim([MultiQ(1) MultiQ(end)]);

legend(sState,'Location',[.5 .49 .4 .05]);

if bsave
  set(gcf,'InvertHardcopy','off');
  set(gcf,'PaperPositionMode','auto');
  saveas(gcf,['D:\Schluesche\Auswertung\',...
    sprintf('Ausw%s_%s %s ChiRed2',...
    sAuswVer,sBurstSel,sSample{iSample})],'png');

  fid=fopen(['D:\Schluesche\Auswertung\',...
    sprintf('Ausw%s_%s %s ChiRed2',...
    sAuswVer,sBurstSel,sSample{iSample}),'.txt'],'w');
  for i=1:length(OrigHeader), fprintf(fid,'%s\t',[OrigHeader{i}]); end
  fprintf(fid,'\r\n');
  fprintf(fid,'%d\t%f\t%f\t%f\t%f\r\n',OrigExport');
  fclose(fid);
end
