
AnzBursts=length(AlldataInt);
c=1;

Imean=zeros(c,AnzBursts);
Ivar=zeros(c,AnzBursts);
for iBurst=1:AnzBursts
  I=(AlldataInt{1,iBurst}+AlldataInt{2,iBurst}); 
  %I=smooth(I,100);
  I=I(1:div(length(I),c)*c);
  Imean(:,iBurst)=mean(reshape(I,c,[])');
  Ivar(:,iBurst)=var(reshape(I,c,[])');
end
Imean=Imean(:);
Ivar=Ivar(:);
iSelBurst=find(Ivar(:)<10^2.8);
%iSelBurst=find(Ivar(:)<inf);
if 0
  %loglog(Imean(:),Ivar(:),'o')
  ShowContourPlot(log10(Imean(iSelBurst)),log10(Ivar(iSelBurst)),1:0.1:3,0:0.1:5,[],[],false);
  %axis([1e0 1e5 1e0 1e5]);
  daspect([1 1 1]);
  xlabel('mean');
  ylabel('var');
  title(sprintf('#%d',length(iSelBurst)));
end

iBurst=0;
for i=1:length(iSelBurst)
  iBurst=iBurst+1;
  IntNoise_{1,iBurst}=IntNoise{1,iSelBurst(i)};
  IntNoise_{2,iBurst}=IntNoise{2,iSelBurst(i)};
  AlldataInt_{1,iBurst}=AlldataInt{1,iSelBurst(i)};
  AlldataInt_{2,iBurst}=AlldataInt{2,iSelBurst(i)};
end
IntNoise=IntNoise_;
AlldataInt=AlldataInt_;
clear IntNoise_ AlldataInt_ i AnzBursts

