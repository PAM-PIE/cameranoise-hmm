function [datatxt]=ShowBurstWithStufen(...
  dataDonor, dataAkzeptor, dataALEX, dataPF, ...
  Inoise1, Inoise2, BurstState, ...
  IDBurst, dataStartMS, TimeBin, cMinLL, Qremain, Stufen, llpath, mu1, mu1var, gamma, R0, ...
  bBeta, SimStufen, SimParam);

cShowLL = false;
bShowVarTrace = true;
bCorrIntensities = false;
bIncStufenLen = true;  % only for binwise analysis
bPublication = true;
cConfidenceInterval = .95; %entspricht 1.96*sigma 
% norminv(cConfidenceInterval/2*[-1 1]+[.5 .5],0,1)
% 2 Sigma w�ren cConfidenceInterval=.9545

% auto determine cAuswType
cAuswType = 1;  %1: PF, 2: EFRET, 3: Dist
if exist('gamma')==1, if ~isempty(gamma), cAuswType = 2; end; end
if exist('R0')==1, if ~isempty(R0), cAuswType = 3; end; end
if bShowVarTrace|cShowLL, AnzPlots=4; else AnzPlots=3; end

if bIncStufenLen
  for i=1:length(Stufen)
    Stufen(i).End=Stufen(i).End+1;
  end
end
%dataDonor = abs(dataInt).*(1-dataPF);
%dataAkzeptor = abs(dataInt).*dataPF;
%data=dataPF;
%dataw=dataInt; %normpdf(dataInt,50,20);
%dataw=dataw./sum(dataw)*length(dataw);
dataDonorKorr=max(0,dataDonor-Inoise1);
dataAkzeptorKorr=max(0,dataAkzeptor-Inoise2);
%if ~isempty(gamma')
% dataAkzeptorKorr=max(0,1/gamma*(dataAkzeptor-Inoise2));
%else
%  dataAkzeptorKorr=max(0,dataAkzeptor-Inoise2);
%end
if isempty(dataPF)
  dataPF = dataAkzeptor./(dataDonor+dataAkzeptor+(dataDonor+dataAkzeptor==0));
  dataPFkorr = dataAkzeptorKorr./(dataDonorKorr+dataAkzeptorKorr+(dataDonorKorr+dataAkzeptorKorr==0));
else
  dataPFkorr = dataPF;
end
switch cAuswType
  case 1
    dataAusw=dataPF;
    slabel='Prox.Fact';
    %mu1=mu1;
  case 2
    dataAusw=CalcEfretFromPF(dataPFkorr,gamma);
    slabel='E_F_R_E_T';
    mu1=CalcEfretFromPF(mu1,gamma);
    if exist('SimStufen')==1, Simmu1=CalcEfretFromPF(SimParam.mu1,gamma); end
  case 3
    dataAusw=CalcDistFromPF(dataPFkorr,R0,gamma);
    slabel='distance / nm';
    mu1=CalcDistFromPF(mu1,R0,gamma);
    if exist('SimStufen')==1, Simmu1=CalcDistFromPF(SimParam.mu1,R0,gamma); end
end

if nargout==0
  hold off; clf;
  sLineStyle={};
  for i=1:length(Stufen),
    if (Stufen(i).LL>=cMinLL)&(sum(Stufen(i).PFclass==Qremain)),
      sLineStyle{i}='-';
    else
      sLineStyle{i}=':';
    end;
  end

  ax(1)=subplot(AnzPlots,1,1); cla; box;
  %plot(dataPF,'b');
  %plotwithcol([1:length(dataPF)]',dataPF',dataInt',0.7,[0 0 1]);

  if (cAuswType==1)&(~bCorrIntensities)
    path=ones(length(dataAusw),1);
    mu1_=[NaN, mu1];
    mu1var_=[NaN, mu1var];
    for i=1:numel(Stufen)
      istart=min(length(path),fix(Stufen(i).Start));
      iend=min(length(path),fix(Stufen(i).End));
      path(istart:iend)=Stufen(i).PFclass+1;
    end
    I1=max(0,(dataDonor+dataAkzeptor-Inoise1-Inoise2).*(1-mu1_(path))+Inoise1);
    I2=max(0,(dataDonor+dataAkzeptor-Inoise1-Inoise2).*(mu1_(path))+Inoise2);
    %xmax=1./(1+(I1-1)./(I2-1));
    if bBeta
      xmax=(I2-1)./max(1,I1+I2-2);
      %    plot(xmax,'color',[1 0.8 0.7]);
      if isfinite(cConfidenceInterval)
        x1=betainv((1-cConfidenceInterval)/2,I2,I1);
        x2=betainv(1-(1-cConfidenceInterval)/2,I2,I1);

        if 1
          h = area([x1;x2-x1]'); % Set BaseValue via argument
          set(h(1),'FaceColor','none','EdgeColor','none');
          set(h(2),'FaceColor',[1 .95 .9],'EdgeColor','none');
          %set(h,'LineStyle','-','EdgeColor',[1 .8 .7]); % Set all to same value
          hold on;
        end
        if 1
          plot(x1,'color',[1 .8 .7]);
          hold on;
          plot(x2,'color',[1 .8 .7]);
        end
      end
    else
      %Gau�
      xmax=mu1_(path);
      if isfinite(cConfidenceInterval)
        x1=norminv((1-cConfidenceInterval)/2,mu1_(path),sqrt(mu1var_(path)));
        x2=norminv(1-(1-cConfidenceInterval)/2,mu1_(path),sqrt(mu1var_(path)));

        if 1
          h = area([x1;x2-x1]'); % Set BaseValue via argument
          set(h(1),'FaceColor','none','EdgeColor','none');
          set(h(2),'FaceColor',[1 .95 .9],'EdgeColor','none');
          %set(h,'LineStyle','-','EdgeColor',[1 .8 .7]); % Set all to same value
          hold on;
        end
        if 1
          plot(x1,'color',[1 .8 .7]);
          hold on;
          plot(x2,'color',[1 .8 .7]);
        end
      end
    end
  end

  set(gca,'LineStyleOrder',{'-'});
  if bPublication, set(gca,'FontSize',20); end
  h=plot(dataAusw);
  if bPublication, set(h,'LineWidth',2); end
  hold on;

  if numel(Stufen)>0
    set(gca,'LineStyleOrder',sLineStyle);
    h=plot([[Stufen(:).Start];[Stufen(:).End]],...
      [mu1([Stufen(:).PFclass]);mu1([Stufen(:).PFclass])],...
      'color',[1 0.5 0],'LineWidth',2);
    if bPublication, set(h,'LineWidth',3); end
  end
  title([sprintf('Burst %d',[IDBurst]),': ',sprintf('%g ms (%d * [%g ms])',[dataStartMS,length(dataDonor),TimeBin])]);
  xlim([1 length(dataDonor)]);
  if cAuswType<3, ylim([0 1]); else ylim([3 7]); end
  ylabel(slabel);
  ylimval=ylim;
  for i=1:length(Stufen)
    h=text((Stufen(i).Start+Stufen(i).End)/2, 0.9*ylimval(2), int2str(Stufen(i).PFclass),...
      'HorizontalAlignment','center','Color',[1 0.5 0]);
    if bPublication, set(h,'FontSize',18); end
  end
  % SimStufen
  if exist('SimStufen')==1
    for i=1:length(SimStufen.PFclass)
      text((SimStufen.Start(i)+SimStufen.End(i))/2, 0.1, int2str(SimStufen.PFclass(i)),...
        'HorizontalAlignment','center','Color',[0 0 0]);
    end
    stairs([SimStufen.Start;SimStufen.End(end)],...
      [Simmu1(SimStufen.PFclass),Simmu1(SimStufen.PFclass(end))],...
      'color',[0 0 0],'LineStyle',':');
    %plot([SimStufen.Start';SimStufen.End'],...
    %    [Simmu1(SimStufen.PFclass);Simmu1(SimStufen.PFclass)],...
    %    'color',[1 0 0]);
  end

  ax(3)=subplot(AnzPlots,1,3); cla; box;
  if bPublication, set(gca,'FontSize',20); end
  set(gca,'LineStyleOrder',{'-'});
  if cShowLL
    plot(llpath);
    hold on;
    if numel(Stufen)>0
      set(gca,'LineStyleOrder',sLineStyle,'ColorOrder',[1 0.5 0]);
      plot([[Stufen(:).Start];[Stufen(:).End]],...
        [Stufen(:).LL;Stufen(:).LL],...
        'LineWidth',2);
    end
    if exist('SimStufen')==1
      verify=VerifyStufen({Stufen}, [length(dataDonor)], {SimStufen});
      title(sprintf('Vergleich: %.2f%%',100*verify));
    end
  else
    h=plot(dataDonor+dataAkzeptor);
    if bPublication, set(h,'LineWidth',2); end
    ymax=max(dataDonor+dataAkzeptor);
    hold on;
    if ~isempty(dataALEX)
      h=plot(dataALEX,'color',[.5 .2 0]);
      if bPublication, set(h,'LineWidth',2); end
      ymax=max(ymax,max(dataALEX));
    end
    if numel(Stufen)>0
      %dataStufen = max(dataInt) * 0.95 * Stufen2Traj(Stufen);
      %plot([1:length(dataStufen)]+1,dataStufen,'color',[0.5 0.5 0]);
      dataStufenx=reshape([[Stufen.Start]',[Stufen.Start]',[Stufen.End]']',length(Stufen)*3,1);
      dataStufeny=repmat([0;ymax*0.95;0],length(Stufen),1);
      stairs(dataStufenx,dataStufeny,'color',[0.5 0.5 0]);
    end
  end
  xlim([1 length(dataDonor)]);
  if cShowLL
    ylim([1e-2 1e2]);
    set(gca,'YScale','log');
    ylabel('LL');
  else
    ylim([0 ymax]);
    set(gca,'YScale','lin');
    ylabel('total intensity');
  end

  %Stufen = Path2Stufen(path, Qremain, 1, dataDonor, dataAkzeptor, NoiseDonor, NoiseAkzeptor, llpath, cMinLL);  %Nur Stufen 1..Q-1 ausw�hlen, min. Stufenl�nge
  ax(2)=subplot(AnzPlots,1,2); cla; box;
  hold on;
  ymax=max(dataDonor+dataAkzeptor);
  if (~isempty(dataALEX))&(cShowLL)
    ymax=max(ymax,max(dataALEX));
  end
  bar((BurstState==0)*ymax*0.95,'EdgeColor','none','FaceColor',[.93 .93 .93],'BarWidth',1);
  bar((BurstState==-1)*ymax*0.95,'EdgeColor','none','FaceColor',[.9 1 .9],'BarWidth',1);
  bar((BurstState==-2)*ymax*0.95,'EdgeColor','none','FaceColor',[1 .9 .9],'BarWidth',1);
  if bPublication, set(gca,'FontSize',20); end
  h=plot(dataDonor,'color',[0 0.5 0]);
  if bPublication, set(h,'LineWidth',2); end
  title(sprintf('%d FRET level',[length(Stufen)]));
  h=plot(dataAkzeptor,'color',[1 0 0]);
  if bPublication, set(h,'LineWidth',2); end
  if (~isempty(dataALEX))&(cShowLL)
    h=plot(dataALEX,'color',[.5 .2 0]);
    if bPublication, set(h,'LineWidth',2); end
  end
  if numel(Stufen)>0
    %dataStufen = max(dataInt) * 0.95 * Stufen2Traj(Stufen);
    %plot([1:length(dataStufen)]+1,dataStufen,'color',[0.5 0.5 0]);
    dataStufenx=reshape([[Stufen.Start]',[Stufen.Start]',[Stufen.End]']',length(Stufen)*3,1);
    dataStufeny=repmat([0;ymax*0.95;0],length(Stufen),1);
    stairs(dataStufenx,dataStufeny,'color',[0.5 0.5 0]);
  end
  if ~bCorrIntensities
    if length(Inoise1)==1
      plot([1 length(dataDonor)],[1 1]*Inoise1,'-','color',[0 .5 0]);
      plot([1 length(dataDonor)],[1 1]*Inoise2,'-','color',[1 0 0]);
    else
      plot(Inoise1,'-','color',[.5 .7 .5]);
      plot(Inoise2,'-','color',[1 .7 .7]);
    end
  end
  xlim([1 length(dataDonor)]);
  ylim([0 max(1,ymax)]);
  ylabel('counts / ms');

  ax(4)=subplot(AnzPlots,1,4); cla; box;
  hold off
  ShowVarTrace(dataDonor, dataAkzeptor, 10, [0 0 1]);
  hold on
  ShowVarTrace(dataDonor, dataAkzeptor, 50, [1 0 0]);
  semilogy([1 length(dataDonor)],[1 1],'k');
  xlim([1 length(dataDonor)]);
  ylim([1/3 3]);
  ylabel('var-trace');

  %Achsen verlinken
  linkaxes(ax,'x');
  %disp(sprintf('Burst %d von %d',[iBurst,AnzBursts]));
else
  %Stufen = Path2Stufen(path, Qremain, 1, dataDonor, dataAkzeptor, NoiseDonor, NoiseAkzeptor, llpath, cMinLL);  %Nur Stufen 1..Q-1 ausw�hlen, min. Stufenl�nge
  dataStufen = zeros(1,length(dataDonor));
  for i=1:length(Stufen), dataStufen(Stufen(i).Start:Stufen(i).End)=mu1(Stufen(i).PFclass); end;
  datatxt=[[1:length(dataDonor)]'-1,dataDonor(:),dataAkzeptor(:),dataAusw(:),dataStufen(:)];
end

  function ShowVarTrace(dataDonor, dataAkzeptor, d, col);
    x=1:length(dataDonor)-d;
    k=zeros(1,length(x));
    for i=x
      I1=dataDonor(i:i+d);
      I2=dataAkzeptor(i:i+d);
      k(i)=var(I1+I2)/(var(I1)+var(I2));
    end
    semilogy(x+d/2,k,'color',col); hold on
  end

end
