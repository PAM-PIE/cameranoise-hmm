function [MultiQ,LzeroArr,HArr]=CalcHessLogL(filename);

load([filename,'.mat'],'MultiQData','dataPF','dataPFsigma','MultiQ','Qn');
% dataPF={dataPF{1:10}};
% dataPFsigma={dataPFsigma{1:10}};
for iMulti=1:length(MultiQData)
  mu1=MultiQData{iMulti}.mu1;
  Sigma1=MultiQData{iMulti}.Sigma1;
  transmat1=MultiQData{iMulti}.transmat1;
  %prior1=MultiQData{iMulti}.prior1;
  %obsvect=MultiQData{iMulti}.obsvect;

  Qo=length(mu1); Q=Qo*Qn;

  obsvect = zeros(Qo,Qn);
  for o=1:Qo, obsvect(o,:) = o; end
  obsvect = reshape(obsvect,1,Q);

  prior1 = zeros(Q,1);
  for o=1:Qo
    prior1((o-1)*Qn+1)=1/Qo;
  end

  [Lzero LLerr]=m2hmm_logprob_kn(dataPF,dataPFsigma,prior1,transmat1,obsvect,mu1,Sigma1);
  p=length(mu1(:))+length(Sigma1(:));
  pdist=CalcPDist(mu1,Sigma1);
  for i=1:p
    for j=1:p
      if i==j
        fprintf('.');
        [mu1_,Sigma1_]=VaryParam(mu1,Sigma1,i,pdist(i));
        [Lplus(i) LLerr]=m2hmm_logprob_kn(dataPF,dataPFsigma,prior1,transmat1,obsvect,mu1_,Sigma1_);
        fprintf('.');
        [mu1_,Sigma1_]=VaryParam(mu1,Sigma1,-i,pdist(i));
        [Lminus(i) LLerr]=m2hmm_logprob_kn(dataPF,dataPFsigma,prior1,transmat1,obsvect,mu1_,Sigma1_);
      elseif j<i
        Lij(i,j)=Lij(j,i);
      else
        fprintf('.');
        mu1_=mu1;
        Sigma1_=Sigma1;
        [mu1_,Sigma1_]=VaryParam(mu1_,Sigma1_,i,pdist(i));
        [mu1_,Sigma1_]=VaryParam(mu1_,Sigma1_,j,pdist(j));
        [Lij(i,j) LLerr]=m2hmm_logprob_kn(dataPF,dataPFsigma,prior1,transmat1,obsvect,mu1_,Sigma1_);
      end
    end
  end

  H=NaN*zeros(p);
  for i=1:p
    for j=1:p
      if i==j
        H(i,j)=1/(pdist(i)*pdist(j))*(Lplus(i)+Lminus(i)-2*Lzero);
      else
        H(i,j)=1/(pdist(i)*pdist(j))*(Lij(i,j)+Lzero-Lplus(i)-Lplus(j));
      end
    end
  end

  LzeroArr(iMulti)=Lzero;
  HArr{iMulti}=H;
  
  save([filename,sprintf('_HessTemp%d.mat',iMulti)]);
  fprintf('\n');
end

end

%-------------------

function [mu1_,Sigma1_]=VaryParam(mu1,Sigma1,i,di);
c=sign(i)*di;
i=abs(i);
mu1_=mu1;
Sigma1_=Sigma1;
if i<=length(mu1(:))
  %mu1_(i)=mu1(i)*(1+c);
  mu1_(i)=min(1,max(0,mu1(i)+c));
else
  i2=i-length(mu1);
  %Sigma1_(i2)=Sigma1(i2)*(1+c);
  Sigma1_(i2)=max(0,Sigma1(i2)+c);
end
end

function pdist=CalcPDist(mu1,Sigma1);
pdist=[];
for i=1:(length(mu1(:))+length(Sigma1(:)))
  if i<=length(mu1)
    pdist(i)=0.001;
  else
    pdist(i)=0.0001;
  end
end
end
