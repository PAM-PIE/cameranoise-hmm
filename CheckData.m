function [iError]=CheckData(data)

iError=[];
[AnzKanaele AnzBursts]=size(data);
for iBurst=1:AnzBursts
  itemp=[];
  for iKanal=1:AnzKanaele
    itemp=[itemp,find(isnan(data{iKanal,iBurst}))];
    itemp=[itemp,find(~isfinite(data{iKanal,iBurst}))];
    itemp=[itemp,find(data{iKanal,iBurst}<=0)];
  end
  if ~isempty(itemp)
    iError=[iError; iBurst+0*itemp', itemp'];
  end
end
