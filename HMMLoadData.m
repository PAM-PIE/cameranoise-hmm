%HMMLoadData

if cAuswVer>=18
    load([filepath,'EkorrV',sAuswVer,'.mat']);
end
%Ekorr=normrnd(.654,.123,size(Ekorr));
%Ekorr=.654*ones(size(Ekorr));
%Ekorr=.7927*ones(size(Ekorr));

if isfinite(MaxAnzBurst)
  tempfilename=sprintf('%sdata_Burst%d',filepath,MaxAnzBurst);
else
  tempfilename=sprintf('%sdata_gesamt',filepath);
end

if exist([tempfilename,'.mat'],'file') & 1
  load(tempfilename);
  fprintf(1,'%d traces loaded.\n',length(data));
  for i=1:length(data)
    data(i).filename = strrep(filename,'*','_');
  end
else
  files=dir([filepath,filename]);
  data=[];
  fprintf('Load files...\n');
  if length(files)==0, error('search path is empty!'); end
  for ifile=1:length(files)
    filename1=files(ifile).name;
    fprintf(1,'Load file "%s"...',[filepath,filename1]);
    if exist([filepath,filename1],'file')
      [data1, header] = LoadTracesProV51([filepath,filename1]);
      dataerr = LoadTracesProErr(ChangeFileExt([filepath,filename1],'.err'));
      data1 = AddErrInfo(data1,dataerr);
      for i=1:length(data1)
        data1(i).filename = filename1;
      end
    else
      error('file not found!');
    end
    fprintf(1,'%d traces.\n',length(data1));
    data=[data,data1];
    if length(data)>=MaxAnzBurst, break; end
  end
  save(tempfilename,'data');
  
end
if (MaxAnzBurst<inf) && (length(data)>MaxAnzBurst)
  data=data(1:MaxAnzBurst);
end

clear dataInt;
clear dataPF;
clear dataPFsigma;
AlldataInt = {};
AlldataPF = {};
AlldataPFsigma = {};
IntNoise = {};
FileList = [];
FileListIdx = [];
if ~exist('Ekorr','var')
  Ekorr=ones(1,length(data));
  EkorrW=ones(1,length(data));
end
Ekorr=[data(:).GammaFactor];
%tBleachInt=[];
%tBleachFRET=[];
for i=1:length(data)
  %tBleachInt(i) = FindTrajEnd(data(i).Donor+data(i).Acc,40,10)-1;
  %tBleachFRET(i) = FindTrajEnd(data(i).FRET,-0.5,1)-1;
  %data(i).StopAnalysis=800;
  %data(i).StopAnalysis = min(tBleachFRET,tBleachInt);
  
  j=data(i).StartAnalysis;
  while (data(i).Donor(j)+data(i).Acc(j)<40)&(j<data(i).StopAnalysis), j=j+1; end
  data(i).StartAnalysis=j;
  j=data(i).StopAnalysis;
  while (data(i).Donor(j)+data(i).Acc(j)<40)&(j>data(i).StartAnalysis), j=j-1; end
  data(i).StopAnalysis=j;
  %data(i).StopAnalysis=800;
  
  if data(i).StopAnalysis-data(i).StartAnalysis<2, data(i).StopAnalysis=data(i).StartAnalysis; end
  
  %rebin part1
  cbin=max(1,fix(TimeBinNew/TimeBin));
  len=data(i).StopAnalysis-data(i).StartAnalysis+1;
  memlen(i)=len;
  len=floor(len/TimeBinNew)*TimeBinNew;
  if len<2*TimeBinNew, 
    data(i).StopAnalysis=data(i).StartAnalysis; 
    EkorrW(i)=0;
  else
    data(i).StopAnalysis=data(i).StartAnalysis+len-1;
  end
  %rebin part1

  CT=data(1).CrosstalkFactor; %Crosstalk
  
  AlldataPF{i}=max(0,min(1,(data(i).FRET(data(i).StartAnalysis:data(i).StopAnalysis))'));
  %AlldataPF{i}=max(0,min(1,AlldataPF{i}));
  IntNoise{1,i}=data(i).DonorErr(data(i).StartAnalysis:data(i).StopAnalysis)';
  IntNoise{2,i}=data(i).AccErr(data(i).StartAnalysis:data(i).StopAnalysis)';
  AlldataInt{1,i}=max(0,data(i).Donor(data(i).StartAnalysis:data(i).StopAnalysis)');
  AlldataInt{2,i}=max(0,data(i).Acc(data(i).StartAnalysis:data(i).StopAnalysis)');
  
  %Crosstalk-Korrektur ab V23
  %AlldataInt{2,i}=max(0,AlldataInt{2,i}-CT*AlldataInt{1,i});
  AlldataInt{2,i}=AlldataInt{2,i}-CT*AlldataInt{1,i};
  
  %rebin part2
  if EkorrW(i)>0
    IntNoise{1,i}=sum(reshape(IntNoise{1,i},cbin,[]),1);
    IntNoise{2,i}=sum(reshape(IntNoise{2,i},cbin,[]),1);
    AlldataInt{1,i}=sum(reshape(AlldataInt{1,i},cbin,[]),1);
    AlldataInt{2,i}=sum(reshape(AlldataInt{2,i},cbin,[]),1);
  end
  %rebin part2
    
  k=2*(1+(IntNoise{1,i}+IntNoise{2,i})./max(1,(AlldataInt{1,i}+AlldataInt{2,i})));
  AlldataPFsigma{i}=max(1,(AlldataInt{1,i}+AlldataInt{2,i})./k);
  
  %AlldataPF{i}=max(0,min(1,AlldataPF{i}./(Ekorr(i).*(1-AlldataPF{i})+AlldataPF{i})));
  AlldataPF{i}=max(0,min(1,AlldataInt{2,i}./(AlldataInt{1,i}*Ekorr(i)+AlldataInt{2,i}+...
    (AlldataInt{1,i}*Ekorr(i)+AlldataInt{2,i}==0))));
  
  j=strfind(data(i).filename,'_'); j(find(j==1))=[];
  if ~isempty(j)
    sfilenameBase=data(i).filename(1:j(1)-1);
  end
  if isempty(strmatch(sfilenameBase,FileList))
    FileList=strvcat(FileList,sfilenameBase);
  end
  FileListIdx(i)=strmatch(sfilenameBase,FileList);
end
fprintf('\n');

TimeBin=TimeBinNew;
%HMMSelectBursts2;
%HMMSelectBursts3;
HMMSelectBursts4;
