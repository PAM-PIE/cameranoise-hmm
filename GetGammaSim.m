if 1
  clear variables;
  gamma0=.6;
  T=400;
  tseq=[.2 .1 .1 .1 .25 .05 .2];
  pseq=[.8 .6 .8 .4 .8 .8 .8];
  sseq=[100 100 100 100 100 50 100];
  P0=[]; S=[];
  for i=1:length(tseq)
    P0=[P0,pseq(i)*ones(1,tseq(i)*T)];
    S=[S,sseq(i)*ones(1,tseq(i)*T)];
  end
  I1=poissrnd((1-P0).*S/gamma0);
  I2=poissrnd(P0.*S);
  Isum=I1+I2;
end

%---------

P=I2./(I1+I2);
xgamma=2.^(-2:0.01:2);
y=zeros(size(xgamma)); p=y; ylo=y; yup=y;
for i=1:length(xgamma)
  E=I2./(I1*xgamma(i)+I2);
  I=(I1*xgamma(i)+I2);
  [ytemp,ptemp,ytemplo,ytempup]=corrcoef(E,I);
  y(i)=ytemp(2,1);
  p(i)=ptemp(2,1);
  ylo(i)=ytemplo(2,1);
  yup(i)=ytempup(2,1);
end
i=find(y<=0,1,'first');
gamma1=xgamma(i);

E=I2./(I1*gamma1+I2);
I=(I1*gamma1+I2);

%---------

clf;

subplot(4,1,1); hold on; box on;
plot(Isum);
plot(I,'color',[1 .5 0]);
plot(S,'r:');
ylim([0 max(S)*1.5]);

subplot(4,1,2); hold on; box on;
plot(P);
plot(E,'color',[1 .5 0]);
plot(P0,'r:');
ylim([0 1]);

subplot(4,1,3);
plot([I1',I2']);
ylim([0 max(S)]);

subplot(4,1,4); hold on; box on
set(gca,'XScale','log');
h = area(xgamma,[ylo(:) yup(:)-ylo(:)]); % Set BaseValue via argument
set(h(1),'FaceColor','none');
set(h(2),'FaceColor',[.7 .7 1]);
set(h,'EdgeColor','none');
plot(xgamma,y);
plot(xgamma,p);
axis tight;
set(gca,'XTick',2.^[log2(min(xgamma)):log2(max(xgamma))]);
ylim([-1 1]);
plot(gamma1*[1 1],ylim,'color',[1 .5 0]);
plot(gamma0*[1 1],ylim,'r:');
grid on
