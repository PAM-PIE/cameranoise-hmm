%ShowMemo
clf;
ax(1)=subplot(2,1,1);
plotLL(trajLL); box

cAusw=5;
cshift=-2;

ax(2)=subplot(2,1,2);
switch cAusw
  case 1
    x=[0:1:length(Memo)];
    y=[mu0;reshape([Memo.mu1],4,[])'];
    plot(x,y,'.-');
    ylim([0 1]);
    ylabel('�');
  case 2
    x=[0:1:length(Memo)];
    y=zeros(length(Memo),length(mu1));
    y(1,:)=dwelltime0;
    for i=1:length(Memo)
      dtrans=diag(Memo(i).transmat1)';
      y(i+1,:)=-TimeBin./log(dtrans);
    end
    plot(x,y,'.-');
    ylabel('\tau/ms');
  case 3
    x=[0:1:length(Memo)];
    i=0;
    y=diag(circshift(transmat0,[0 cshift]))';
    for i=1:length(Memo)
      y(i+1,:)=diag(circshift(Memo(i).transmat1,[0 cshift]))';
    end
    plot(x,y,'.-');
    ylabel(sprintf('T %d',cshift));
    legend(int2str(circshift(eye(4),[0 cshift])));
  case 4
    x=[1:1:length(Memo)];
    y=[reshape([Memo.lltrace],length(Memo(1).lltrace),[])'];
    plot(x,y,'.-');
    ylabel('lltrace');
  case 5
    x=[1:1:length(Memo)];
    x_=repmat(diff(trajLL'),1,size([Memo(1).lltrace],2));
    y=[reshape([Memo.lltrace],length(Memo(1).lltrace),[])'];
    y=diff(y); y=y./abs(y); y=sum(y,1);
    %y=sum(diff(y)./x_,1);
    plot(y,'.');
    %plot(x(1:end-1),diff(y)./x_,'-');
    ylabel('diff lltrace');
    %axis tight;
    cla; hold on
    [w,iy]=sort(y,'descend');
    for i=1:AnzBursts
      yMW=(1*[AllStufen{iy(i)}.MW]);
      plot(i*ones(size(yMW)),yMW,'.');
    end
    plot([ones(size(mu1)); AnzBursts*ones(size(mu1))],[mu1; mu1],'r');
    axis tight;
  case 6
    x=[1:1:length(Memo)];
    y=[reshape([Memo.lltrace],length(Memo(1).lltrace),[])'];
    plot(var(y),'.-');
    ylabel('bad traces');
    axis tight;
  case 7
    x=[1:1:length(Memo)];
    y=[reshape([Memo.lltrace],length(Memo(1).lltrace),[])'];
    i0=div(length(trajLL),2);
    y1=mean(y(2:i0,:),1);
    y2=mean(y(i0+1:end,:),1);
    plot(y2-y1,'.');
    ylabel('bad traces');
    axis tight;
    TraceProp=[];
    TraceProp.dLL=log(y2./y1);
    for iBurst=1:AnzBursts
      TraceProp.StepsCount(iBurst)=length(AllStufen{iBurst}); 
      TraceProp.Len(iBurst)=length(dataInt{1,iBurst});
      TraceProp.I1min(iBurst)=min(dataInt{1,iBurst});
      TraceProp.I2min(iBurst)=min(dataInt{2,iBurst});
      TraceProp.I1mean(iBurst)=mean(dataInt{1,iBurst});
      TraceProp.I2mean(iBurst)=mean(dataInt{2,iBurst});
    end
    i=find((y2>y1));
    %i=1:length(TraceProp.I1);
    plot(TraceProp.dLL(i),TraceProp.I1min(i),'o'); 
    xlim([-1 1]*.1);
    
    xhist=1:1:1000;
    yhist=hist([dataInt{1,:}],xhist);
    [w,i1]=max(smooth(yhist));
    x1=xhist(i1);
    %x1=mean([dataInt{1,:}]);
    yhist=hist([dataInt{2,:}],xhist);
    [w,i2]=max(smooth(yhist));
    x2=xhist(i2);
    %x2=mean([dataInt{2,:}]);
    dist=sqrt((TraceProp.I1mean-x1).^2+(TraceProp.I2mean-x2).^2);
    [w,i]=sort(dist); i=i(1:50); w=w(1:50);
    
    i2=1:length(Memo(1).lltrace);
    i2(i)=[];
    x=ones(size(TraceProp.Len'));
    x(i2)=0;
    trajLL_=(y*x);
    %trajLL_=sum(y,2);
    plotLL(trajLL_);
    
    title(int2str(length(i)));
    
    [w,i]=sort(y2-y1,'descend'); j=0;
end

if cAusw<5
  linkaxes(ax,'x');
end
