%SimShowBurst

subplot(3,1,1); cla; hold on;
title('Simulated Trace');
plot(dataInt{1,iBurst},'color',[0 .5 0]);
plot(dataInt{2,iBurst},'color',[1 0 0]);
ylim([0 150]);
ylabel(sprintf('Donor and\nAcceptor Intensity'));

subplot(3,1,2); cla; hold on;
plot(dataInt{1,iBurst}+dataInt{2,iBurst});
ylim([0 150]);
ylabel('Sum Intensity');

subplot(3,1,3); cla; hold on;
plot(dataPF{iBurst});
plot(muX(SimStates(:,iBurst)),'-','color',[1 .5 0],'LineWidth',2);
title(sprintf('Sim Burst %d',iBurst));
ylim([0 1]);
ylabel('FRET efficiency');

xlabel('time [5 ms]');
drawnow;

set(gcf,'InvertHardcopy','off');
set(gcf,'PaperPositionMode','auto');
saveas(gcf,'SimData','png');
saveas(gcf,'SimData','fig');
