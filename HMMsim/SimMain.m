%HMMsim

clear variables;

DataLen         = 800;
AnzBursts       = 20;
TimeBin         = 5;
Qo              = 4;
muX             = (1:Qo)*1/(Qo+1)+0.05;
dwelltimeX      = 100*ones(1,Qo);
muXvar          = 0.002*ones(1,Qo);

learn_mu        = ones(1,Qo);
learn_muvar     = ones(1,Qo);
learn_dwelltime = ones(1,Qo);

FileListIdx = ones(1,AnzBursts);

Mcount = 1;          %Number of mixtures
cov_type = 'diag';  %'full', 'diag' or 'spherical'

Qn=1;
Q=Qn*Qo;
obsvect = zeros(Qo,Qn);
for o=1:Qo, obsvect(o,:) = o; end
obsvect = reshape(obsvect,1,Q);
Ocount = size(muX,1);

SigmaX=zeros(Ocount,Ocount,Qo,Mcount);
for q=1:Qo, for i=1:Ocount, SigmaX(i,i,q)=muXvar(:,q); end; end

transmatX = zeros(Q);
if Qo==1
  transmatX=1;
else
  for oi=1:Qo
    probii = exp(-TimeBin/dwelltimeX(oi));
    probij = (1-probii)/(Qo-1);
    transmatX(oi,oi)=probii;
    if Qn>1
      for oj=2:Qo
        col = Qo + (mod(oi-1+oj-1,Qo)+1)*(Qn-1);
        transmatX(oi,col)=probij;
      end
      for n=2:Qn-1
        row = Qo+(oi-1)*(Qn-1)+n-1;
        col = row + 1;
        transmatX(row,col) = 1;
      end
      row = Qo+(oi-1)*(Qn-1)+Qn-1;
      col = oi;
      transmatX(row,col) = 1;
    else
      for oj=2:Qo
        col = mod(oi-1+oj-1,Qo)+1;
        transmatX(oi,col)=probij;
      end
    end
  end
end

transmatX = mk_stochastic(transmatX);
priorX = zeros(Q,1);
for o=1:Qo
  priorX((o-1)*Qn+1)=1/Qo;
end
mixmatX = ones(Q,Mcount);

[SimData, SimStates] = mhmm_sample(DataLen, AnzBursts, priorX, transmatX, muX, SigmaX, mixmatX);
for iBurst=1:AnzBursts
  pfX = SimData(:,:,iBurst);
  I0 = rand*100+10;
  dataSum{iBurst} = I0 * ones(size(pfX));
  
  I1 = max(0, (1-pfX) .* dataSum{iBurst});
  I2 = max(0, pfX .* dataSum{iBurst});
  k = 2;
  dataInt{1,iBurst} = max(0,normrnd(I1,sqrt(k*I1)));
  dataInt{2,iBurst} = max(0,normrnd(I2,sqrt(k*I2)));
  dataPF{iBurst} = dataInt{2,iBurst} ./ (max(1,dataInt{1,iBurst}+dataInt{2,iBurst}));
  dataPF{iBurst} = max(0,min(1,dataPF{iBurst}));
  dataPFsigma{iBurst} = max(1,(dataInt{1,iBurst}+dataInt{2,iBurst})./k);
end

iBurst=AnzBursts;
SimShowBurst;

save('SimData.mat');
