How to use this code

1. start SimMain.m to generate simulated traces and save them as a matlab data file
2. start SimLearn.m to perform a hidden markov analysis. Set the var "bHMM_kn" to "true" when using the extended HMM, setting it to "false" uses the standard HMM aproach
