%HMMShowBurst

if 0
 iBurstSel = iBurstSel + 1;
 iBurst = BurstSel(iBurstSel);
end
%iBurst = iBurst + 1;
%DNA18dyn Burst No 99
%DNA12dyn Burst No 20

if bLearnBurstwise
  mu1=MultiQData{1}(iBurst).mu1;
  mu1var=MultiQData{1}(iBurst).mu1var;
  Sigma1=MultiQData{1}(iBurst).Sigma1;
  transmat1=MultiQData{1}(iBurst).transmat1;
  dwelltime1=MultiQData{1}(iBurst).dwelltime1;
end


if ~exist('bSimData','var'), bSimData=false; end
if bSimData
  ShowBurstWithStufenPF(...
    dataPF{iBurst}, dataPFsigma{iBurst}, dataInt{1,iBurst}, dataInt{2,iBurst}, ...
    iBurst, 0, TimeBin, cMinLL, 1:Q, AllStufen{iBurst}, dataLL{iBurst}, mu1, Sigma1,...
    SimStufen{iBurst}, SimParam);
else
  data=ShowBurstWithStufenPF(...
    dataPF{iBurst}, dataPFsigma{iBurst}, dataInt{1,iBurst}, dataInt{2,iBurst}, ...
    iBurst, 0, TimeBin, cMinLL, 1:Q, AllStufen{iBurst}, dataLL{iBurst}, mu1, Sigma1);
  ShowBurstWithStufenPF(...
    dataPF{iBurst}, dataPFsigma{iBurst}, dataInt{1,iBurst}, dataInt{2,iBurst}, ...
    iBurst, 0, TimeBin, cMinLL, 1:Q, AllStufen{iBurst}, dataLL{iBurst}, mu1, Sigma1);
end

I1=dataInt{1,iBurst}; I2=dataInt{2,iBurst}; var(I2./(I1+I2));

if exist('BurstGamma','var')
  subplot(4,1,4); 
  title(sprintf('\\gamma=%.2f, w=%.1fe-3',BurstGamma(BurstID(iBurst)),1e3*BurstGammaW(BurstID(iBurst))));
end

dI1=diff([AllStufen{iBurst}.Iavg1]);
dI2=diff([AllStufen{iBurst}.Iavg2]);
x=sum((dI1+dI2).^2)/length(dI1);
fprintf('Burst %3d: %8.1f\n',iBurst,x);
