%ShowBIC
sSample={'DNA3','DNA12','DNA18','DNA19','Donor_Only','Acceptor_Only','Sim1'};
sState={'Dynamic','Steady',''};
filepathStamm=['D:\Schluesche\Maindata\',sSample{iSample},'\'];
if iSample<=4
  filepathStamm=[filepathStamm,'\',sState{iState},'FRET\'];
end

LL=[];
BIC=[];
States=[];
q=1:7;
NumParam=q.*(q-1)+2*q;  %Transmat+mu+mu1var

load([filepathStamm,'Ausw16_Sim1_m07.mat'],'dataPF','MultiQData');
dataNumBins=length([dataPF{:}]);

subplot(2,1,1); cla;
for q=1:length(MultiQData)
  LL(q)=(MultiQData{q}.trajLL(end)); 
  States(q)=length(MultiQData{q}.mu1);
  BIC(q)=2*LL(q)-NumParam(States(q))'.*log(dataNumBins);
end
plot(States,LL,'.-');
title(sprintf('%s (%s V%s)',sSample{iSample},sBurstSel,sAuswVer));
ylabel('logL');

subplot(2,1,2); cla;
plot(States,BIC,'.-');
ylabel('BIC');

[w,i]=max(BIC);
hold on
plot(States(i),w,'or');

set(gcf,'InvertHardcopy','off');
set(gcf,'PaperPositionMode','auto');
saveas(gcf,['D:\Schluesche\Auswertung\',...
  sprintf('Ausw%s_%s %s BIC',...
  sAuswVer,sBurstSel,sSample{iSample})],'png');
