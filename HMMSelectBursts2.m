
if ~exist([filepath,'BurstFilter.txt'],'file'), return; end

AnzBursts=length(AlldataInt);
temp=dlmread([filepath,'BurstFilter.txt'],'\t');

iBurst_=0;
iDel=[];
for iBurst=1:AnzBursts
  i=find(temp(:,1)==iBurst);
  bBurstOk=true;
  if ~isempty(i)
    istart=max(1,temp(i,2));
    iend=min(length(AlldataInt{1,iBurst}),temp(i,3));
    if (iend-istart>0)&(iend>0)&(istart>0)&(istart<length(AlldataInt{1,iBurst}))
      AlldataInt{1,iBurst}=AlldataInt{1,iBurst}(istart:iend);
      AlldataInt{2,iBurst}=AlldataInt{2,iBurst}(istart:iend);
      IntNoise{1,iBurst}=IntNoise{1,iBurst}(istart:iend);
      IntNoise{2,iBurst}=IntNoise{2,iBurst}(istart:iend);
    else
      bBurstOk=false;
    end
  end
  if bBurstOk
    iBurst_=iBurst_+1;
    IntNoise_{1,iBurst_}=IntNoise{1,iBurst};
    IntNoise_{2,iBurst_}=IntNoise{2,iBurst};
    AlldataInt_{1,iBurst_}=AlldataInt{1,iBurst};
    AlldataInt_{2,iBurst_}=AlldataInt{2,iBurst};
    AlldataPF_{iBurst_}=AlldataPF{iBurst};
    AlldataPFsigma_{iBurst_}=AlldataPFsigma{iBurst};
  else
    iDel=[iDel iBurst];
  end
end
IntNoise=IntNoise_;
AlldataInt=AlldataInt_;
AlldataPF=AlldataPF_;
AlldataPFsigma=AlldataPFsigma_;
data(iDel)=[];
clear IntNoise_ AlldataInt_ i AnzBursts temp istart iend iDel
