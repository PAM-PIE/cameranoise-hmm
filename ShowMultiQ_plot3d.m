%ShowMultiQ_plot3d
%Transition rates
sSample={'DNA3','DNA12','DNA18','DNA19'};
sState={'Dynamic','Steady'};

iSample1=3;
iSample2=4;

AnzArr=7:-1:4;
cShowArr={'mu','seq','RMS'};
NSeq=3; %Sequence Depth
bShowInitSeq=false;
cLabelCount=6;
cShow=1;
%1: Mu
%2: Seq-Histogramme
%3: RMS-Verteilung

for iAnz=1:length(AnzArr)
  Anz=AnzArr(iAnz);
  TimeBin=5;
  xhistMu=0:0.02:1;
  dxhistMu=xhistMu(2)-xhistMu(1);
  K={};
  W={};
  Mu1={};
  yhistMu={};
  yhistSeq={};
  index={};
  Test1=[];
  iperms=perms(1:Anz);
  subplot(length(AnzArr),1,iAnz);
  cla; hold on;

  %     if ~exist('MetaQData')
  %       filepath_=['D:\Schluesche\Maindata\'];
  %       load([filepath_,'Ausw',sAuswVer,'_',sBurstSel,'_MetaData.mat']);
  %       load([filepath_,'Ausw',sAuswVer,'_',sBurstSel,'_MetaStufen.mat']);
  %     end
  %filepath_=['D:\Schluesche\Maindata\',sSample{iSample},'\',sState{iState},'FRET\'];
  %load([filepath_,'Ausw',sAuswVer,'_',sBurstSel,'_m0',int2str(Anz),'.mat'],'StufenArr');
  %StufenArr=MetaStufenArr{iSample,iState,Anz};

  MultiQData=MetaQData{iSample1,iState};
  Q=length(diag(MultiQData{Anz}.transmat1));

  T1=MultiQData{Anz}.transmat1;
  R1=1./(TimeBin*1e-3).*logm(T1);
  Mu1{1}=MultiQData{Anz}.mu1;
  AllStufen=MultiQData{Anz}.AllStufen;
  Jumps1=zeros(Q);
  for iBurst=1:length(AllStufen)
    len=length(AllStufen{iBurst});
    if len>1
      Jumps1=Jumps1+accumarray([[AllStufen{iBurst}(1:len-1).PFclass]',[AllStufen{iBurst}(2:len).PFclass]'],ones(1,len-1),[Q Q]);
    end
  end
  PFhist1=zeros(size(xhistMu));
  for iBurst=1:length(AllStufen)
    PFhist1=PFhist1+hist([AllStufen{iBurst}(:).MW],xhistMu);
  end

  MultiQData=MetaQData{iSample2,iState};
  T2=MultiQData{Anz}.transmat1;
  R2=1./(TimeBin*1e-3).*logm(T2);
  Mu1{2}=MultiQData{Anz}.mu1;
  AllStufen=MultiQData{Anz}.AllStufen;
  Jumps2=zeros(Q);
  for iBurst=1:length(AllStufen)
    len=length(AllStufen{iBurst});
    if len>1
      Jumps2=Jumps2+accumarray([[AllStufen{iBurst}(1:len-1).PFclass]',[AllStufen{iBurst}(2:len).PFclass]'],ones(1,len-1),[Q Q]);
    end
  end
  PFhist2=zeros(size(xhistMu));
  for iBurst=1:length(AllStufen)
    PFhist2=PFhist2+hist([AllStufen{iBurst}(:).MW],xhistMu);
  end

  iperms=perms(1:Q);
  chiSqr=[];
  chiPerm=[];
  for iLoop2=1:size(iperms,1)
    iperm=iperms(iLoop2,:);
    rdummy=T2(iperm,:); 
    rdummy=rdummy(:,iperm);
    jdummy=Jumps2(iperm,:);
    jdummy=jdummy(:,iperm);
    wdummy=(Jumps1(:)/sum(Jumps1(:))+jdummy(:)/sum(jdummy(:)))/2;
    chiSqr(iLoop2)=sqrt(sum(wdummy.*((T1(:)-rdummy(:)).^2)));
    chiPerm(iLoop2,:)=iperm;
  end
  [wsort,isort]=sort(chiSqr);

  switch cShow
    case 1
      yPosOfs=0.5;
      yhist1=PFhist1;
      yhist2=PFhist2;
      yhist1=yhist1./max(yhist1);
      yhist2=yhist2./max(yhist2);
      stairs(xhistMu-dxhistMu/2,Anz-yPosOfs+.35*yhist1);  %hist erzeugt zentrierte bins
      stairs(xhistMu-dxhistMu/2,Anz-yPosOfs+1-.35*yhist2);
      for i=1:Q
        x1=Mu1{1}(i);
        x2=Mu1{2}(chiPerm(isort(1),i));
        [w1,i1]=min(abs(xhistMu-x1)); %i1=find(xhistMu<=x1,1,'last');
        [w2,i2]=min(abs(xhistMu-x2)); %i2=find(xhistMu+dxhistMu/2<=x2,1,'last');
        plot([x1 x1],Anz-yPosOfs+[.35*yhist1(i1) .4],':','color',[1 1 1]*.3);
        plot([x2 x2],Anz-yPosOfs+1-[.35*yhist2(i2) .4],':','color',[1 1 1]*.3);
        plot(x1,Anz-yPosOfs+.35*yhist1(i1),'r.');
        plot(x2,Anz-yPosOfs+1-.35*yhist2(i2),'r.');
        plot([x1 x2],Anz-yPosOfs+[.4 1-.4],'r');
      end
      box on
      axis tight;
      set(gca,'YTick',Anz);
      for iLoop2=1:3
        fprintf('%f',wsort(iLoop2));
        fprintf('\t%d',chiPerm(isort(iLoop2),:));
        fprintf('\n');
      end
    case 2
      idx2=index{2};
      for ni=1:NSeq
        idx2(:,ni)=ip(idx2(:,ni)+1)'-1;
      end
      idx=idx2*(Q.^((NSeq-1):-1:0))';
      yhist2=hist(idx,xhistSeq);
      %yhist2=yhistSeq{2};
      yhist1=yhistSeq{1};
      yhist1=yhist1./max(yhist1);
      yhist2=yhist2./max(yhist2);
      bar(xhistSeq,Anz+.4*yhist1,'EdgeColor','b','FaceColor',[.9 .9 1],...
        'BarWidth',1,'BaseValue',Anz);
      bar(xhistSeq,Anz+1-.4*yhist2,'EdgeColor','b','FaceColor',[.9 .9 1],...
        'BarWidth',1,'BaseValue',Anz+1);
      %stairs(xhistSeq-(xhistSeq(2)-xhistSeq(1))/2,Anz+.4*yhist1);
      %stairs(xhistSeq-(xhistSeq(2)-xhistSeq(1))/2,Anz+1-.4*yhist2);
      [w,i2]=sort(yhist1,'descend');
      for i=1:min(length(i2),cLabelCount) %length(xhistSeq)
        if yhist1(i2(i))>mean(yhist1)
          sdummy=FuncSeqPlus1(dec2base(xhistSeq(i2(i)),Q),NSeq);
          ytext=Anz+.4*yhist1(i2(i))+.05;
          if yhist1(i2(i))>.5, ytext=ytext-.1; end
          text(xhistSeq(i2(i)),ytext,sdummy,...
            'rotation',45,'color','b',...
            'HorizontalAlignment','center','VerticalAlignment','middle');
        end
      end
      [w,i2]=sort(yhist2,'descend');
      for i=1:min(length(i2),cLabelCount) %length(xhistSeq)
        if yhist2(i2(i))>mean(yhist2)
          sdummy=FuncSeqPlus1(dec2base(xhistSeq(i2(i)),Q),NSeq);
          for i3=1:length(sdummy)
            sdummy(i3)=num2str(ip(str2num(sdummy(i3))));
          end
          ytext=Anz+1-(.4*yhist2(i2(i))+.05);
          if yhist2(i2(i))>.5, ytext=ytext+.1; end
          text(xhistSeq(i2(i)),ytext,sdummy,...
            'rotation',-45,'color','b',...
            'HorizontalAlignment','center','VerticalAlignment','middle');
        end
      end
      box on
      axis tight;
      set(gca,'YTick',Anz);
    case 3
      plot(w,'.-');
      set(gca,'YTickMode','auto');
      axis tight;
    case 4
      I=[StufenArr(:).SumIntens]./([StufenArr(:).Length]+1);
      hist(I,0:20:500);
      axis tight;
  end
end

subplot(length(AnzArr),1,1);
title(sprintf('%s-%s (%s V%s)',sSample{iSample1},sSample{iSample2},sBurstSel,sAuswVer));

if 0
  set(gcf,'InvertHardcopy','off');
  set(gcf,'PaperPositionMode','auto');
  saveas(gcf,['D:\Schluesche\Auswertung\',...
    sprintf('Ausw%s_%s %s_%s_%s Compare_%s',...
    sAuswVer,sBurstSel,sSample{iSample1},sSample{iSample2},sState{iState},cShowArr{cShow})],'png');
end
