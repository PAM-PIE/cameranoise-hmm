%Sim BIC
if 1
  clear variables;

  Narr=100:20:1000;
  y=NaN*ones(size(Narr));
  for Ni=1:length(Narr)
    N=Narr(Ni);
    mu0=0.2;
    mu0var=0.01;
    data=normrnd(mu0,sqrt(mu0var),[1 N]);
    %plot(data);

    mu1=mean(data);
    mu1var=var(data);

    muarr=(interp1([1 100],1+[-1 1]*.5,1:1:100))*mu1;
    dmuarr=muarr(2)-muarr(1);
    %vararr=(interp1([1 100],1+[-1 1]*.5,1:1:100))*mu1var;
    vararr=(interp1([1 3],1+[-1 1]*.01,1:3))*mu1var;
    dvararr=vararr(2)-vararr(1);
    logL=zeros(size(vararr));

%     for i=1:length(muarr)
%       logL(i)=sum(log(mixgauss_prob(data,muarr(i),mu1var)));
%     end
%     imax=round(interp1(muarr,1:length(muarr),mu1));
%     d2_logL=(diff(diff(logL)))/(dmuarr^2);
%     y(Ni)=d2_logL(imax);
    
    for i=1:length(vararr)
      logL(i)=sum(log(mixgauss_prob(data,mu1,vararr(i))));
    end
    imax=max(1,min(length(vararr)-2,round(interp1(vararr,1:length(vararr),mu1var))));
    d2_logL=(diff(diff(logL)))/(dvararr^2);
    y(Ni)=d2_logL(imax);
    
    fprintf('.');
  end
  %
  % subplot(2,1,1);
  % plot(muarr,logL(:,round(length(vararr)/2)));
  % subplot(2,1,2);
  % plot(muarr(1:end-2),diff(diff(logL(:,round(length(vararr)/2)))));
  fprintf('\n');
end

%y2=-y.*(mu1var./Narr);
y2=-y.*(2*mu1var^2*(Narr-1)./(Narr.^2));
m=mean(y2./Narr);
c=mean(y2);

clf; hold on
plot(Narr,y2,'.-');
plot(Narr,c*ones(size(Narr)),'r');
hold off
grid;
title(sprintf('m = %.2f, c = %.2f',m,c));
