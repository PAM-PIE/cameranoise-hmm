%HMMMultiAusw_CalcChiRed
sSample={'DNA3','DNA12','DNA18','DNA19'};
sState={'Dynamic','Steady'};

Anz=7;
bShowLL=false;
filepath1=['D:\Schluesche\Maindata\',sSample{iSample},'\',sState{1},'FRET\'];
filepath2=['D:\Schluesche\Maindata\',sSample{iSample},'\',sState{2},'FRET\'];
filename=sprintf('Ausw%s_%s_m%02d.mat',sAuswVer,sBurstSel,Anz);
[MultiQ,ChiSquare1,ChiSquare1red]=CalcChiSquare([filepath1,filename]);
[MultiQ,ChiSquare2,ChiSquare2red]=CalcChiSquare([filepath2,filename]);
OrigHeader={'MultiQ','ChiSquare1','ChiSquare2','ChiSquare1red','ChiSquare2red'};
OrigExport=[MultiQ',ChiSquare1',ChiSquare2',ChiSquare1red',ChiSquare2red'];

subplot(2,1,1);
if bShowLL, subplot(Anz,Anz,[Anz*2+1 Anz*Anz]); end
hold off
plot(MultiQ,ChiSquare1,'.-','DisplayName',sState{1});
hold on
plot(MultiQ,ChiSquare2,'.-','color',[0 .5 0],'DisplayName',sState{2});
legend show
xlabel('number of states');
ylabel('\langle\chi�\rangle');
%ylabel('\Sigma\chi�');
title(sprintf('%s (%s V%s)',sSample{iSample},sBurstSel,sAuswVer));
%ylim([0 2]);
xlim([MultiQ(1) MultiQ(end)]);
  
subplot(2,1,2);
if bShowLL, subplot(Anz,Anz,[Anz*2+1 Anz*Anz]); end
hold off
plot(MultiQ,1+0*MultiQ,'color',[1 1 1]*.7);
hold on
plot(MultiQ,ChiSquare1red,'.-','DisplayName',sState{1});
plot(MultiQ,ChiSquare2red,'.-','color',[0 .5 0],'DisplayName',sState{2});
xlabel('number of states');
ylabel('\langle\chi_r_e_d�\rangle');
%ylabel('\Sigma\chi_r_e_d�');
title(sprintf('%s (%s V%s)',sSample{iSample},sBurstSel,sAuswVer));
ylim(1+.1*[-1 1]);
xlim([MultiQ(1) MultiQ(end)]);

if bShowLL

  load([filepath1,filename],'MultiQData','MultiQ');
  for q=1:Anz
    subplot(Anz,Anz,q);
    plotLL(MultiQData{q}.trajLL);
    if q==1, ylabel(sState{1}); end
  end

  load([filepath2,filename],'MultiQData','MultiQ');
  for q=1:Anz
    subplot(Anz,Anz,q+Anz);
    plotLL(MultiQData{q}.trajLL);
    if q==1, ylabel(sState{2}); end
  end

end

set(gcf,'InvertHardcopy','off');
set(gcf,'PaperPositionMode','auto');
saveas(gcf,['D:\Schluesche\Auswertung\',...
  sprintf('Ausw%s_%s %s ChiRed',...
  sAuswVer,sBurstSel,sSample{iSample})],'png');

fid=fopen(['D:\Schluesche\Auswertung\',...
  sprintf('Ausw%s_%s %s ChiRed',...
  sAuswVer,sBurstSel,sSample{iSample}),'.txt'],'w');
for i=1:length(OrigHeader), fprintf(fid,'%s\t',[OrigHeader{i}]); end
fprintf(fid,'\r\n');
fprintf(fid,'%d\t%f\t%f\t%f\t%f\r\n',OrigExport');
fclose(fid);
